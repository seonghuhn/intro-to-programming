def enter_number_lbyl():
    while True:
        num = input('Enter number: ')
        if num.isdigit():
            num = int(num)
            break
        print('A number!!!!!!')
    print(num)


def enter_number_eafp():
    while True:
        try:
            num = int(input('Enter number: '))
            break
        except ValueError:
            print('A number!!!!!!')
    print(num)


def div_lbyl():
    a = int(input('Enter number: '))
    b = int(input('Enter 2nd number: '))
    if b == 0:
        print('Division by zero stinks')
    else:
        print(a / b)


def div_eafp():
    a = int(input('Enter number: '))
    b = int(input('Enter 2nd number: '))
    try:
        print(a / b)
    except ZeroDivisionError:
        print('Division by zero stinks')


ceos = [
    ['Jeff', 'Bezos', 71],
    ['Bill', 'Gates', 95],
    ['Mark', 'Zuckerberg'],
]

ceos_and_actors = [
    ['Jeff', 'Bezos', 71],
    ['Bill', 'Gates', 95],
    ['Mark', 'Zuckerberg'],
]


def exams_lbyl(students):
    exams = []
    for student in students:
        exam = {}
        exam['name'] = student[0:2]
        if len(student) == 3:
            exam['exam'] = student[2]
        else:
            exam['exam'] = 0
        exams.append(exam)
    return exams


def exams_eafp_index_error(students):
    exams = []
    for student in students:
        exam = {'name': student[0:2]}
        try:
            exam['exam'] = student[2]
        except IndexError:
            exam['exam'] = 0
        exams.append(exam)
    return exams


def exams_eafp_value_error(students):
    exams = []
    for student in students:
        exam = {}
        try:
            exam['exam'] = int(student[-1])
            exam['name'] = student[0:-1]
        except ValueError:
            exam['exam'] = 0
            exam['name'] = student
        exams.append(exam)
    return exams


def get_ratios(l1, l2):
    """
    Calculates the ratios of each respective element in l1 and l2.
    Assumes l1 and l2 are lists of equal length of numbers.
    :param l1: first list of numbers
    :param l2: second list of numbers
    :return: a list containing l1[i]/l2[i]
    """
    ratios = []
    for index in range(len(l1)):
        try:
            ratios.append(l1[index] / l2[index])
        except ZeroDivisionError:
            # in case of division by zero append Not a Number for the ratio
            ratios.append(float('NaN'))
        except:
            # unexpected error
            raise ValueError('get_ratios called with bad arg')
    return ratios


heroes = [
    ['Peter', 'Parker', 95, 93, 97],
    ['Clark', 'Kent', 75, 75, 75],
    ['Diana', 'Prince', 93],
    ['King', "T'Challa", 100, 100, 100, 100],
]


def grade_reports_naive(students):
    reports = []
    for student in students:
        report = {}
        report['name'] = student[0] + ' ' + student[1]
        report['grades'] = student[2:]
        report['avg'] = sum(report['grades']) / len(report['grades'])
        reports.append(report)
    return reports


def grade_reports(students, avg_func):
    reports = []
    for student in students:
        report = {}
        report['name'] = student[0] + ' ' + student[1]
        report['grades'] = student[2:]
        report['avg'] = avg_func(report['grades'])
        reports.append(report)
    return reports


def avg_naive(grades):
    try:
        return sum(grades) / len(grades)
    except ZeroDivisionError:
        print('no grades data')


def avg(grades):
    try:
        return sum(grades) / len(grades)
    except ZeroDivisionError:
        return 0


def fancy_divide_4(list_of_numbers, index):
    try:
        try:
            raise Exception("0")
        finally:
            denom = list_of_numbers[index]
            for i in range(len(list_of_numbers)):
                list_of_numbers[i] /= denom
    except Exception as ex:
        print(ex)


def fancy_divide_5(list_of_numbers, index):
    try:
        try:
            denom = list_of_numbers[index]
            for i in range(len(list_of_numbers)):
                list_of_numbers[i] /= denom
        finally:
            raise Exception("0")
    except Exception as ex:
        print(ex)


if __name__ == "__main__":
    # enter_number_lbyl()
    # enter_number_eafp()

    # div_lbyl()
    # div_eafp()

    # print(exams_lbyl(ceos))
    # print(exams_eafp_index_error(ceos))
    ceos.append(['Musk', 59])
    ceos.append(['Tim', 'Donald', 'Cook', 99])
    # print(exams_eafp_index_error(ceos))
    # print(exams_eafp_value_error(ceos))

    # print(get_ratios([16, 8, 4], [1, 2, 3]))
    # print(get_ratios([16, 8, 4], [1, 2]))
    # print(get_ratios([16, 8, 4], [1, 2, 0]))
    # print(get_ratios([16, 8, 4], [1, 2, 'foo']))

    # print(grade_reports_naive(heroes))
    heroes.append(['Bruce', 'Wayne'])
    # print(grade_reports_naive(heroes))
    print(grade_reports(heroes, avg_naive))
    print(grade_reports(heroes, avg))

    fancy_divide_4([0, 2, 4], 0)
    fancy_divide_5([0, 2, 4], 0)
