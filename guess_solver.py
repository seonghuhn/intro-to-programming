value = int(input("Value: "))

lo = 1
hi = 100
guess = None

while guess != value:
    guess = (hi + lo) // 2
    print(lo, hi, guess)

    if guess < value:
        lo = guess
    elif guess > value:
        hi = guess
