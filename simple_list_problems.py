def reverse(l):
    ret = []
    for e in l:
        ret.insert(0, e)
    return ret


def reverse_efficient(l):
    i = 0
    j = len(l) - 1
    while i < j:
        tmp = l[j]
        l[j] = l[i]
        l[i] = tmp
        i += 1
        j -= 1
    return l


if __name__ == "__main__":
    assert reverse([]) == []
    assert reverse([1]) == [1]
    assert reverse([1, 4, 3]) == [3, 4, 1]
    assert reverse(['s', 't', 'r', 'e', 's', 's', 'e', 'd']) == ['d', 'e', 's', 's', 'e', 'r', 't', 's']

    assert reverse_efficient([]) == []
    assert reverse_efficient([1]) == [1]
    assert reverse_efficient([1, 4, 3]) == [3, 4, 1]
    assert reverse_efficient(['s', 't', 'r', 'e', 's', 's', 'e', 'd']) == ['d', 'e', 's', 's', 'e', 'r', 't', 's']
