def filter_nums_with_3(end):
    nums = []
    for num in range(end):
        if '3' in str(num):
            nums.append(num)
    return nums


def lc_nums_with_3(end):
    nums = [num for num in range(end) if '3' in str(num)]
    return nums


def filter_remove_vowels(string):
    no_vowels = ''
    for letter in string:
        if letter not in 'aeiou':
            no_vowels = no_vowels + letter
    return no_vowels


def lc_remove_vowels(string):
    no_vowels = [letter for letter in string if letter not in 'aeiou']
    str_no_vowels = ''
    return str_no_vowels.join(no_vowels)


def filter_find_words_4(string):
    word_list = string.split()
    words = []
    for word in word_list:
        if len(word) < 4:
            words.append(word)
    return words

def lc_find_words_4(string):
    word_list = string.split()
    words = [word for word in word_list if len(word) < 4]
    return words


def sc_lengths(string):
    words = string.split()
    lengths = {len(word) for word in words}
    return lengths


def map_grading(grades):
    def grade_convert(grade):
        if grade >= 90:
            return 'a'
        elif grade >= 80:
            return 'b'
        elif grade >= 70:
            return 'c'
        elif grade >= 65:
            return 'd'
        return 'f'
    grades = []
    map(lambda  x: grade_convert(x), grades)
    return grades


def lc_grading(grades):
    # TODO
    pass


def dc_grading(grades):
    # TODO
    pass


class MySet(object):
    def __init__(self):
        self._vals = []
        self._len = 0
        for _ in range(7):
            self._vals.append([])

    def insert(self, e):
        idx = hash(e) % len(self._vals)
        if e not in self._vals[idx]:
            self._vals[idx].append(e)
            self._len += 1

    def member(self, e):
        idx = hash(e) % len(self._vals)
        return e in self._vals[idx]

    def remove(self, e):
        idx = hash(e) % len(self._vals)
        vals = self._vals[idx]
        for ii in range(len(vals) - 1, -1, -1):
            if vals[ii] == e:
                del vals[ii]
                self._len -= 1
                return
        raise ValueError

    def intersect(self, other):
        intersection = MySet()
        # TODO: change to use one list comprehension to flatten the nested lists
        for vals in self._vals:
            for val in vals:
                if other.member(val):
                    intersection.insert(val)
        return intersection

    def __len__(self):
        return self._len

    def __str__(self):
        # TODO: change to use one list comprehension to flatten the nested lists
        vals = []
        for vv in self._vals:
            vals.extend(vv)
        vals.sort()
        return '{' + ','.join([str(e) for e in vals]) + '}'


if __name__ == '__main__':
    # test numbers with 3 in it
    assert filter_nums_with_3(1) == []
    assert filter_nums_with_3(10) == [3]
    assert filter_nums_with_3(20) == [3, 13]
    assert filter_nums_with_3(100) == [3, 13, 23, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 43, 53, 63, 73, 83, 93]
    print('\033[0;36mFilter Numbers with 3 works\t\t\t\t\t\t\t\t\033[0;33m1 pt')
    assert lc_nums_with_3(1) == []
    assert lc_nums_with_3(10) == [3]
    assert lc_nums_with_3(20) == [3, 13]
    assert lc_nums_with_3(100) == [3, 13, 23, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 43, 53, 63, 73, 83, 93]
    print('\033[0;36mList Comprehension Numbers with 3 works\t\t\t\t\t\033[0;33m1 pt')

    # test remove vowels
    assert filter_remove_vowels('') == ''
    assert filter_remove_vowels('aeiou') == ''
    assert filter_remove_vowels('bdf') == 'bdf'
    assert filter_remove_vowels('Remove all of the vowels in a string') == 'Rmv ll f th vwls n  strng'
    print('\033[0;31mFilter Remove Vowels works\t\t\t\t\t\t\t\t\033[0;33m1 pt')
    assert lc_remove_vowels('') == ''
    assert lc_remove_vowels('aeiou') == ''
    assert lc_remove_vowels('bdf') == 'bdf'
    assert lc_remove_vowels('Remove all of the vowels in a string') == 'Rmv ll f th vwls n  strng'
    print('\033[0;31mList Comprehension Remove Vowels works\t\t\t\t\t\033[0;33m1 pt')

    # test find words less than 4
    assert filter_find_words_4('') == []
    assert filter_find_words_4('Find words') == []
    assert filter_find_words_4('Find all of the words in a string that are less than 4 letters') == ['all', 'of', 'the',
                                                                                                     'in', 'a', 'are',
                                                                                                     '4']
    print('\033[0;34mFilter Find Words Length Less Than 4 works\t\t\t\t\033[0;33m1 pt')
    assert lc_find_words_4('') == []
    assert lc_find_words_4('Find words') == []
    assert lc_find_words_4('Find all of the words in a string that are less than 4 letters') == ['all', 'of', 'the',
                                                                                                 'in', 'a', 'are', '4']
    print('\033[0;34mList Comprehension Find Words Length Less Than 4 works\t\033[0;33m1 pt')

    # test lengths
    assert sc_lengths('') == set()
    assert sc_lengths(' ') == set()
    assert sc_lengths('   ') == set()
    assert sc_lengths('Count the lengths of the words in a string') == {5, 3, 7, 2, 1, 6}
    print('\033[0;35mSet Comprehension Length of Words works\t\t\t\t\t\033[0;33m1 pt')

    # test grading
    assert map_grading([]) == []
    assert map_grading([99, 88, 77, 66, 55, 0]) == ['A', 'B', 'C', 'D', 'F', 'F']
    assert map_grading(range(100, 49, -5)) == ['A', 'A', 'A', 'B', 'B', 'C', 'C', 'D', 'D', 'F', 'F']
    print('\033[0;32mMap Grading works\t\t\t\t\t\t\t\t\t\t\033[0;33m1 pt')
    assert lc_grading([]) == []
    assert lc_grading([99, 88, 77, 66, 55, 0]) == ['A', 'B', 'C', 'D', 'F', 'F']
    assert lc_grading(range(100, 49, -5)) == ['A', 'A', 'A', 'B', 'B', 'C', 'C', 'D', 'D', 'F', 'F']
    print('\033[0;32mList Comprehension Grading works\t\t\t\t\t\t\033[0;33m1 pt')
    assert dc_grading([]) == {}
    assert dc_grading([99, 88, 77, 66, 55, 0]) == {99: 'A', 88: 'B', 77: 'C', 66: 'D', 55: 'F', 0: 'F'}
    assert dc_grading(range(100, 49, -5)) == {100: 'A', 95: 'A', 90: 'A', 85: 'B', 80: 'B', 75: 'C', 70: 'C', 65: 'D',
                                              60: 'D', 55: 'F', 50: 'F'}
    print('\033[0;32mDictionary Comprehension Grading works\t\t\t\t\t\033[0;33m1 pt')

    # test intersection of two empty sets
    set_1 = MySet()
    assert str(set_1) == '{}'
    set_2 = MySet()
    assert str(set_2) == '{}'
    set_3 = set_1.intersect(set_2)
    assert len(set_3) == 0
    assert str(set_3) == '{}'

    # test intersection of two non-empty sets
    set_1.insert(-12)
    set_1.insert(2)
    set_1.insert(8)
    set_1.insert(3.14)
    set_2.insert(-20)
    set_2.insert(-16)
    set_2.insert(-11)
    assert len(set_2) == 3
    set_3 = set_1.intersect(set_2)
    assert len(set_3) == 0
    set_2.insert(3.14)
    set_3 = set_1.intersect(set_2)
    assert len(set_3) == 1
    print('\033[0;37mSet Intersect works\t\t\t\t\t\t\t\t\t\t\033[0;33m1 bonus pt')

    # test __str__
    assert str(set_1) == '{-12,2,3.14,8}'
    assert str(set_2) == '{-20,-16,-11,3.14}'
    assert str(set_3) == '{3.14}'
    print('\033[0;37mSet Str works\t\t\t\t\t\t\t\t\t\t\t\033[0;33m1 bonus pt')
