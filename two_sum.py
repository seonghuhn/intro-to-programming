"""
Design and implement a TwoSum class. It should support the following operations: add and find.

add - Add the number to an internal data structure.
find - Find if there exists any pair of numbers whose sum is equal to the value.

Example 1:
two_sum = TwoSum()
two_sum.add(1)
two_sum.add(3)
two_sum.add(5)
two_sum.find(4) -> True
two_sum.find(7) -> False

Example 2:
two_sum = TwoSum()
two_sum.add(3)
two_sum.add(1)
two_sum.add(2)
two_sum.find(3) is True
two_sum.find(6) is False
two_sum.add(3)
two_sum.find(6) is True

https://leetcode.com/problems/two-sum-iii-data-structure-design/
"""


class TwoSum(object):
    def __init__(self):
        self._nums = {}

    def add(self, num):
        if num in self._nums:
            self._nums[num] += 1
        else:
            self._nums[num] = 1

    def find(self, sum_):
        for num, freq in self._nums.items():
            other_num = sum_ - num
            if other_num == num and freq > 1 or other_num != num and other_num in self._nums:
                return True
        return False


if __name__ == "__main__":
    two_sum = TwoSum()
    two_sum.add(1)
    two_sum.add(3)
    two_sum.add(5)
    assert two_sum.find(4) is True
    assert two_sum.find(6) is True
    assert two_sum.find(8) is True
    assert two_sum.find(7) is False

    two_sum = TwoSum()
    two_sum.add(3)
    two_sum.add(1)
    two_sum.add(2)
    assert two_sum.find(3) is True
    assert two_sum.find(4) is True
    assert two_sum.find(5) is True
    assert two_sum.find(6) is False
    two_sum.add(3)
    assert two_sum.find(6) is True
