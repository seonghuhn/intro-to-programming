def encrypt(string, shift):
    cipher = ''
    for char in string:
        if char == ' ':
            cipher = cipher + char
        elif char.isupper():
            cipher = cipher + chr((ord(char) + shift - 65) % 26 + 65)
        else:
            cipher = cipher + chr((ord(char) + shift - 97) % 26 + 97)


    return cipher

def main():
    print("Welcome to the totally not illegal decoder")
    text = input("Input a string.")
    s = int(input("How far should it shift?"))
    print("original string: ", text)
    print("encrypted string: ", encrypt(text, s))
    print("Take as much time as you need")

if __name__ == '__main__':
    main()