
print('Welcome to Caesar Encoder!')
encode_decode = input('Would you like to encode a message or decode?: ')

decryption = ''
encryption = ''
alphabet = 'abcdefghijklmnopqrstuvwxyz'

if encode_decode == 'encode'.lower():
    message = input('Input message to encrypt: ')
    shift = int(input('How many letters would you like to shift the alphabet? '))
    for letter in message:
        if letter.lower():
            n = ord(letter)
            n_index = n - ord('a')
            new = (n_index + shift) % 26
            new_message = new + ord('a')
            new_letter = chr(new_message)
            if letter == ' ':
                new_letter = ' '
            if letter not in alphabet:
                new_letter = letter
            encryption = encryption + new_letter
        else:
            encryption += letter
    print(encryption)

elif encode_decode == 'decode'.lower():
    encrypted_message = input('Input message to decrypt: ').lower()
    shift = int(input('How many letters has the alphabet been shifted? '))
    for letter in encrypted_message:
        if letter.lower():
            n = ord(letter)
            n_index = n - ord('a')
            new = (n_index - shift) % 26
            new_message = new + ord('a')
            new_letter = chr(new_message)
            if letter == ' ':
                new_letter = ' '
            if letter not in alphabet:
                new_letter = letter
            decryption = decryption + new_letter
        else:
            decryption += letter
    print(decryption)

print('Roma Victa!')
