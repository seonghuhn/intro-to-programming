

def decoder(user_message, user_shift):
    decryption = ''

    for letter in user_message:
        if letter.lower():
            t = ord(letter)
            t_ = t - ord('a')
            n = (t_-user_shift) % 26
            umessage = n + ord('a')
            nletter = chr(umessage)
            decryption = decryption + nletter

    return decryption

def encoder(user_message, user_shift):
    encryption = ''

    for letter in user_message:
        if letter.lower():
            t = ord(letter)
            t_ = t - ord('a')
            n = (t_+ user_shift) % 26
            umessage = n + ord('a')
            nletter = chr(umessage)
            encryption += nletter
        else:
            encryption += letter


    return encryption


if __name__ == "__main__":
    alphastring = ''
    string = ''
    print('Welcome to the Caesar Cipher Encoder/Decoder')
    user_input = input('Would you like to use the Cipher to decode or encode a statement: ')
    user_input = user_input.lower()
    user_message = input(f'What message would you like to {user_input} ')
    user_shift = int(input('How many keys would you like to shift: '))



    length = len(user_message)
    for a in range(length):
        length -= 1
        if user_message[length].isalnum() == True:
            string += user_message[length]
            alphastring += user_message[a]
    user_message = string


    if user_input == 'decode':
        print(decoder(user_message, user_shift))
    elif user_input == 'encode':
        print(encoder(user_message, user_shift))
    else:
        print('Invalid')
