import string


def cipher(message, shift, decode=False):
    code = ""
    if decode:
        shift = -shift
    for x in range(len(message)):
        if not message[x].isalpha():
            code += message[x]
        for a in range(26):
            if message[x].islower():
                if message[x] == string.ascii_lowercase[a]:
                    while ((a + shift) / 26) >= 1:
                        a -= 26
                    while ((a + shift) / 26) <= -1:
                        a += 26
                    code += string.ascii_lowercase[a + shift]
                    break
            if message[x].isupper():
                if message[x] == string.ascii_uppercase[a]:
                    while ((a + shift) / 26) >= 1:
                        a -= 26
                    while ((a + shift) / 26) <= -1:
                        a += 26
                    code += string.ascii_uppercase[a + shift]
                    break
    return code


if __name__ == "__main__":
    print("Welcome to the Caesar Cipher Encoder/Decoder!")
    b = input("Do you wish to encode a message?(Y/N) ")
    if b.lower() == "y":
        b = False
    else:
        b = True
    c = input("What is your shift key? ")
    d = input("What is the message you wish to encode/decode? ")
    print(cipher(d, int(c), b))
    print("Godspeed!")
