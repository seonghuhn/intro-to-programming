import string
low = string.ascii_lowercase
cap = string.ascii_uppercase


def encode(message, key):
    out = ''
    for l in message:
        index = low.find(l)
        if index != -1:
            out += low[(index + key) % 26]
        else:
            index = cap.find(l)
            if index != -1:
                out += cap[(index + key) % 26]
            else:
                out += l
    return out


if __name__ == "__main__":
    print('\nWelcome to Lucas\'s Caesar decoder/encoder!\n')
    enc = input('Would you like to encode a message or decode a message? ')
    mul = 1
    if enc == 'decode':
        mul = -1
    elif enc != 'encode':
        print('That was not an option, goodbye.')
        exit(0)
    sk = int(input('What would you like the shift key to be? '))
    mes = input('What message would you like to ' + enc + '? ')

    print(f'Your {enc}d message is:\n')

    print(encode(mes, sk * mul))

    print('\nThank you for trusting me with your secrets. I record them all..... ')
