import string
low = string.ascii_lowercase
up = string.ascii_uppercase
print("Hello there!")
decode_encode = input("Do you wish to decode or encode? (enter 'decode' or 'encode'): ")
num_of_shift = int(input("How many keys do you want to shift? "))
code = input("enter a code: ")
if decode_encode == "encode":
    for c in code:
        num_of_c = ord(c)
        if c in up:
            num_of_c = num_of_c + 32
        letter = int(num_of_c) + num_of_shift - 26
        encode = int(num_of_c) + num_of_shift
        if letter < 97:
            letter = letter + 26
        if encode < 97:
            encode = encode + 26
        if num_of_c + num_of_shift > 122:
            print(chr(letter), end="", flush=True)
        else:
            print(chr(num_of_c + num_of_shift),end='')
if decode_encode == "decode":
    for c in code:
        num_of_c = ord(c)
        if c in up:
            num_of_c = num_of_c + 32
        num_of_c = ord(c)
        letter = int(num_of_c) - num_of_shift - 26
        decode = num_of_c - num_of_shift
        if letter < 97:
            letter = letter + 26
        if decode < 97:
            decode = decode + 26
        if num_of_c + num_of_shift > 122:
            print(chr(letter), end="", flush=True)
        else:
            print(chr(decode), end='')

print("",end='\n')
print("Goodbye!")
