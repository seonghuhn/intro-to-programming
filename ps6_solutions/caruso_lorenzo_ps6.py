print('Welcome to the encoder/decoder!')
code = input('Would you like to encode or decode today?     ')
code = code.lower()
shift_key = input('What would you like the shift key to be?     ')
shift_key = int(shift_key)
message = input(f'What message would you like to {code}?     ')
message = message.lower()

alphabet = 'abcdefghijklmnopqrstuvwxyz'
new_message = ''
for letter in message:
    index = alphabet.find(letter)
    if code == 'encode':
        new_index = index + int(shift_key)
        if new_index >= 26:
            new_index = new_index - 26
    elif code == 'decode':
        new_index = index - int(shift_key)
        if new_index >= 26:
            new_index = new_index - 26
    if letter == ' ':
        new_letter = ' '
    elif letter not in alphabet:
        new_letter = letter
    else:
        new_letter = alphabet[new_index]
    new_message = new_message + new_letter
print(new_message)
print('Strength and honor! May this message serve you well.')
#   The bonus message is in the shift key of 3. If you plug it into my 'encode' one then it will work.
#   I am so happy that this actually works!!!
