def create_set(a_list):
    """
    Creates a set from the given list.
    """


def contains_duplicate(nums):
    """
    Given an array of integers, find if the array contains any duplicates.

    Your function should return true if any value appears at least twice in the array, and it should return false if
    every element is distinct.
    """


def single_number(nums):
    """
    Given a non-empty array of integers nums, every element appears twice except for one. Find that single one.
    """
