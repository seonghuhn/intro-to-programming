from utils import get_current_temp, get_current_air_quality


def conv_fahrenheit_celsius(temp_f):
    """
    Converts temperature in Fahrenheit to Celsius.
    :param temp_f: the temperature in Fahrenheit
    :return: the temperature in Celsius rounded to two decimal places
    """
    # TODO: Replace with code to convert the temperature from Fahrenheit to Celsius.
    return 0.0


def air_quality_level(aqi):
    """
    Determines the AQI level based on the AQI.
    :param aqi: the air quality index
    :return: the AQI level based on https://www.airnow.gov/aqi/aqi-basics/
    """
    # TODO: Replace with code to determine the AQI level based on the AQI.
    return "Good"


def main():
    # TODO: Get Californian city from user's input.
    city = "Santa Clara"

    # TODO: Remove dry_run=True when your program is complete and ready to test.
    temp_f = get_current_temp(city, dry_run=True)
    temp_c = conv_fahrenheit_celsius(temp_f)
    aqi = get_current_air_quality(city, dry_run=True)
    level = air_quality_level(aqi)

    # TODO: Output temperature in Fahrenheit and Celsius.

    # TODO: Output AQI and AQI level.


if __name__ == "__main__":
    main()
