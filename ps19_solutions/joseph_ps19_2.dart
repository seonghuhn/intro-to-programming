import 'package:flutter/material.dart';
import 'dart:math';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  MyApp({Key key}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final _random = Random();
  var _verses = ['Through honor and dishonor, through slander and praise. We are treated as impostors, and yet are true; 2 Corinthians 6:8',
    'Beware of false prophets, who come to you in sheep\'s clothing but inwardly are ravenous wolves. Matthew 7:15',
    'The Lord your God will raise up for you a prophet like me from among you, from your brothers—it is to him you shall listen— Deuteronomy 18:15',
    'He came to his own, and his own people did not receive him. John 1:11',
    'Let not your hearts be troubled. Believe in God; believe also in me. John 14:1'];

  void _randomVerse() {
    setState(() {
      // This call to setState tells the Flutter framework that something has
      // changed in this State, which causes it to rerun the build method below
      // so that the display can reflect the updated values. If we changed
      // _counter without calling setState(), then the build method would not be
      // called again, and so nothing would appear to happen.
    });
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text('Hello'),
        ),
        body: Column(
          children: [
            GridView.count(
              shrinkWrap: true,
              crossAxisCount: 3,
              children: List.generate(3, (index) {
                return Center(
                  child: Container(
                    margin: EdgeInsets.all(5.0),
                    padding: EdgeInsets.all(10.0),
                    decoration: BoxDecoration(
                      border: Border.all(color: Colors.blueAccent)
                    ),
                    child: Text(
                      '${_verses[_random.nextInt(_verses.length)]}',
                      textAlign: TextAlign.center,
                    ),
                  ),
                );
              }),
            ),
            FloatingActionButton(
              onPressed: () {
                _randomVerse();
              },
              child: Icon(Icons.ac_unit_sharp),
            ),
          ],
        ),
      ),
    );
  }
}
