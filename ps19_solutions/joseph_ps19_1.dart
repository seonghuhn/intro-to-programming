import 'package:flutter/material.dart';
import 'dart:math';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  MyApp({Key key}) : super(key: key);

  
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final _random = Random();
  var _verses = ['Through honor and dishonor, through slander and praise. We are treated as impostors, and yet are true; 2 Corinthians 6:8',
    'Beware of false prophets, who come to you in sheep\'s clothing but inwardly are ravenous wolves. Matthew 7:15',
    'The Lord your God will raise up for you a prophet like me from among you, from your brothers—it is to him you shall listen— Deuteronomy 18:15',
    'He came to his own, and his own people did not receive him. John 1:11',
    'Let not your hearts be troubled. Believe in God; believe also in me. John 14:1'];
  var _verseIndex = 0;

  void _randomVerse() {
    setState(() {
      _verseIndex = _random.nextInt(_verses.length);
    });
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called
    return MaterialApp(
        home: Scaffold(
          appBar: AppBar(
            centerTitle: true,
            title: Text('Random Bible Verse'),
          ),
          body: Center(
            child: Column(
              children: [
                FloatingActionButton(
                  onPressed: () {
                _randomVerse();
                },
                child: Icon(Icons.alarm_on)
                  ),
                  Text('${_verses[_verseIndex]}\n', textAlign: TextAlign.center),
            ],
          ),
        ),
      ),
    );
  }
}
