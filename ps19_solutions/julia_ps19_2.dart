import 'dart:math';

import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

const List<String> _bibleVerses = [
  "I consider that the sufferings of this present time are as\n"
      "nothing compared with the glory to be revealed for us.\n"
      "Romans 8:18",
  "Rejoice in hope, endure in affliction, persevere in prayer\n"
      "Romans 12:12",
  "I command you: be firm and steadfast!\n"
      "Do not fear nor be dismayed, for the Lord,\n"
      "your God, is with you wherever you go.\n"
      "Joshua 1:9",
  "In his mind a man plans his course,\n"
      "but the Lord directs his steps.\n"
      "Proverbs 16:9",
  "Rejoice always. Pray without ceasing.\n"
      "In all circumstances give thanks,\n"
      "for this is the will of God for you in Christ Jesus.\n"
      "1 Thessalonians 5:16-18 "
];

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: _MyHomePage(),
    );
  }
}

class _MyHomePage extends StatefulWidget {
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State {
  String _bibleverse = "verse";
  var r = Random();

  void _changeText() {
    setState(_getNewText);
  }

  void _getNewText() {
    _bibleverse = _bibleVerses[r.nextInt(_bibleVerses.length)];
  }

  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
          child: Material(
        color: Colors.pink[100],
        child: Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 20.0,
          ),
          child: buildColumn(context),
        ),
        // child: FloatingActionButton(
        //   onPressed: _changeText, //_changeBox,
        //   child: Icon(Icons.thumb_up),
        //   backgroundColor: Colors.pink,
      )),

      //child: Center(
    );
  }

  Widget buildTitleText() {
    return Text(
      "Bible Verses",
      textScaleFactor: 2.0,
      textAlign: TextAlign.center,
    );
  }

  Widget buildRoundedBox(
    String label, {
    double height = 250.0,
  }) {
    return Container(
      height: height,
      width: 100.0,
      alignment: Alignment(0.0, 0.0),
      decoration: BoxDecoration(
        color: Colors.pink[50],
        border: Border.all(color: Colors.white),
        borderRadius: BorderRadius.all(
          Radius.circular(10.0),
        ),
      ),
      child: Text(
        label,
        textAlign: TextAlign.center,
      ),
    );
  }

  Widget buildColumn(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        buildTitleText(),
        SizedBox(height: 20.0),
        _buildRowOfThree(),
      ],
    );
  }

  Widget _buildRowOfThree() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        buildRoundedBox(
            _bibleverse = _bibleVerses[r.nextInt(_bibleVerses.length)]),
        buildRoundedBox(
            _bibleverse = _bibleVerses[r.nextInt(_bibleVerses.length)]),
        buildRoundedBox(
            _bibleverse = _bibleVerses[r.nextInt(_bibleVerses.length)]),
      ],
    );
  }
}
