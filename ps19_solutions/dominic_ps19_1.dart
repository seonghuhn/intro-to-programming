import 'dart:math';

import 'package:flutter/material.dart';

void main() => runApp(ButtonApp());

const List<String> _bibleVerses = [
  "I consider that the sufferings of this present time are as\n"
      "nothing compared with the glory to be revealed for us.\n"
      "Romans 8:18",
  "Rejoice in hope, endure in affliction, persevere in prayer\n"
      "Romans 12:12",
  "I command you: be firm and steadfast!\n"
      "Do not fear nor be dismayed, for the Lord,\n"
      "your God, is with you wherever you go.\n"
      "Joshua 1:9",
  "In his mind a man plans his course,\n"
      "but the Lord directs his steps.\n"
      "Proverbs 16:9",
  "Rejoice always. Pray without ceasing.\n"
      "In all circumstances give thanks,\n"
      "for this is the will of God for you in Christ Jesus.\n"
      "1 Thessalonians 5:16-18 "
];

class ButtonApp extends StatelessWidget {
  Widget build(BuildContext context) {
    return MaterialApp(
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State {
  String _bibleverse = "Press the button!";
  var r = Random();

  void _changeText() {
    setState(_getNewText);
  }

  void _getNewText() {
    _bibleverse = _bibleVerses[r.nextInt(_bibleVerses.length)];
  }

  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
          child: Text(
            _bibleverse,
          ),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: _changeText,
          child: Icon(Icons.circle),
          backgroundColor: Colors.black,
        ));
  }
}
