import 'dart:math';

import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

const List<String> BibleVerses = [
  "Everyone comes naked from their mother’s womb\n,"
      "and as everyone comes, so they depart.\n"
      "They take nothing from their toil\n"
      "that they can carry in their hands.\n"
      "Ecclesiastes 5:15",
  "Here I am! I stand at the door and knock. If anyone hears my voice and opens the door, I will come in and eat with that person, and they with me.\n"
      "Revelation 3:20",
  "While he was still speaking, a bright cloud covered them, and a voice from the cloud said,\n"
      "This is my Son, whom I love; with him I am well pleased. Listen to him!\n"
      "Matthew 17:5",
  "Whoever serves me must follow me; and where I am,\n"
      "my servant also will be. My Father will honor the one who serves me.\n"
      "John 12:26",
  "For as in Adam all die, so in Christ all will be made alive.\n"
      "1 Corinthians 15:22"
];

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: _MyHomePage(),
    );
  }
}

class _MyHomePage extends StatefulWidget {
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State {
  String _bibleverse = "verse";
  var random = Random();

  void _changeText() {
    setState(_getNewText);
  }

  void _getNewText() {
    _bibleverse = BibleVerses[random.nextInt(BibleVerses.length)];
  }

  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
          child: Material(
        color: Colors.white,
        child: Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 20.0,
          ),
          child: buildColumn(context),
        ),
        // child: FloatingActionButton(
        //   onPressed: _changeText, //_changeBox,
        //   child: Icon(Icons.thumb_up),
        //   backgroundColor: Colors.pink,
      )),

      //child: Center(
    );
  }

  Widget buildTitleText() {
    return Text(
      "Bible Verses",
      textScaleFactor: 2.0,
      textAlign: TextAlign.center,
    );
  }

  Widget buildRoundedBox(
    String label, {
    double height = 200.0,
  }) {
    return Container(
      height: height,
      width: 200.0,
      alignment: Alignment(0.0, 0.0),
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border.all(color: Colors.red),
        borderRadius: BorderRadius.all(
          Radius.circular(30.0),
        ),
      ),
      child: Text(
        label,
        textAlign: TextAlign.center,
      ),
    );
  }

  Widget buildColumn(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        buildTitleText(),
        SizedBox(height: 20.0),
        _buildRowOfThree(),
      ],
    );
  }

  Widget _buildRowOfThree() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        buildRoundedBox(
            _bibleverse = BibleVerses[random.nextInt(BibleVerses.length)]),
        buildRoundedBox(
            _bibleverse = BibleVerses[random.nextInt(BibleVerses.length)]),
        buildRoundedBox(
            _bibleverse = BibleVerses[random.nextInt(BibleVerses.length)]),
      ],
    );
  }
}
