import 'dart:math';

import 'package:flutter/material.dart';

void main() => runApp(ButtonApp());

class ButtonApp extends StatelessWidget {
  Widget build(BuildContext context) {
    return MaterialApp(
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State {
  Widget build(BuildContext context) {
    Random _random = Random();

    return Scaffold(
        appBar: AppBar(
          title: const Text('Mini Bible'),
        ),
        body: Center(
          child: Text(
            _bibleVerses[_random.nextInt(5)],
          ),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: _changeText,
          backgroundColor: Colors.pink,
        ));
  }

  String _verses = _bibleVerses[1];

  void _changeText() {
    setState(_getNewText);
  }

  void _getNewText() {
    Random r = Random();

    int i = r.nextInt(5);

    String _verse = _bibleVerses[i];

    _verses = _verse;
  }
}

List<String> _bibleVerses = [
  "In the beginning God created the heavens and the earth.\n"
      "Genesis 1:1, NIV",
  " Love does no harm to a neighbor. \n"
      "Therefore love is the fulfillment of the law.\n"
      "Romans 13:10, NIV",
  "Gracious words are a honeycomb,\n"
      "sweet to the soul and healing to the bones.\n"
      "Proverbs 16:24, NIV",
  "The earth is the Lord’s, and everything in it,\n"
      "the world, and all who live in it;\n"
      "Psalm 24:1, NIV",
  "Do nothing from selfish ambition or conceit, but in humility count others more significant than yourselves. \n"
      "Philippians 2:3, NIV"
];

