import 'dart:math';

import 'package:flutter/material.dart';

void main() {
  runApp(
    MaterialApp(home: _MyHomePage()), // use MaterialApp
  );
}

class _MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.grey[400],
      child: Padding(
        padding: const EdgeInsets.symmetric(
          horizontal: 20.0,
        ),
        child: buildColumn(context),
      ),
    );
  }
}

Widget button(BuildContext context) {
  return Scaffold(
    floatingActionButton: FloatingActionButton.extended(
      onPressed: () {},
      label: const Text('Approve'),
      icon: const Icon(Icons.thumb_up),
      backgroundColor: Colors.pink,
    ),
  );
}

Widget buildTitleText() {
  return Text(
    "My Pet Shop",
    textScaleFactor: 3.0,
    textAlign: TextAlign.center,
  );
}

Widget buildRoundedBox(
  String label, {
  double height = 200.0,
}) {
  return Container(
    height: height,
    width: 200.0,
    alignment: Alignment(0.0, 0.0),
    decoration: BoxDecoration(
      color: Colors.white,
      border: Border.all(color: Colors.black),
      borderRadius: BorderRadius.all(
        Radius.circular(10.0),
      ),
    ),
    child: Text(
      label,
      textAlign: TextAlign.center,
    ),
  );
}

Widget buildColumn(BuildContext context) {
  return Column(
    mainAxisAlignment: MainAxisAlignment.center,
    crossAxisAlignment: CrossAxisAlignment.stretch,
    children: <Widget>[
      buildTitleText(),
      SizedBox(height: 20.0),
      _buildRowOfThree(),
    ],
  );
}

Widget _buildRowOfThree() {
  Random r = Random();

  int i = r.nextInt(5);

  int e = r.nextInt(5);

  int w = r.nextInt(5);

  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    children: <Widget>[
      buildRoundedBox(_bibleVerses[i]),
      buildRoundedBox(_bibleVerses[e]),
      buildRoundedBox(_bibleVerses[w]),
    ],
  );
}

List<String> _bibleVerses = [
  "In the beginning God created the heavens and the earth.\n"
      "Genesis 1:1, NIV",
  " Love does no harm to a neighbor. \n"
      "Therefore love is the fulfillment of the law.\n"
      "Romans 13:10, NIV",
  "Gracious words are a honeycomb,\n"
      "sweet to the soul and healing to the bones.\n"
      "Proverbs 16:24, NIV",
  "The earth is the Lord’s, and everything in it,\n"
      "the world, and all who live in it;\n"
      "Psalm 24:1, NIV",
  "Do nothing from selfish ambition or conceit, but in humility count others more significant than yourselves. \n"
      "Philippians 2:3, NIV"
];

