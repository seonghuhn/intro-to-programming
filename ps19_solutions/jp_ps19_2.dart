import 'package:flutter/material.dart';
import 'dart:math';

void main() {
  runApp(MyClass());
}

class MyClass extends StatefulWidget {
  @override
  MyApp createState() => MyApp();
}

class MyApp extends State<MyClass> {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
          backgroundColor: Colors.grey[400],
          appBar: AppBar(
            title: Text('Bible Verses')
          ),
          body: Center(
            child: Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 60.0,
              ),
                child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
                  buildColumn(context),
                  Text('\n'),
                  FloatingActionButton(onPressed: () {
                    setState(() {
                      var random = new Random();
                      verse[random.nextInt(3)] = randVerse();
                    });
                  })
                ])
            )

          ),
        )
    );
  }
  Widget buildRoundedBox(
      String label, {
        double height = 176.0,
        double width = 179.0,
}) {
    return Container (
      height: height,
      width: width,
      alignment: Alignment(0.0, 0.0),
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border.all(color: Colors.black),
        borderRadius: BorderRadius.all(
          Radius.circular(10.0),
        ),
      ),
      child: Text(
        label,
        textAlign: TextAlign.center,
      ),
    );
  }

  Widget buildColumn(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        SizedBox(height: 20.0),
        _buildRowOfThree()
        ],
    );
  }

  Widget _buildRowOfThree() {
    return Row(
      mainAxisAlignment:
        MainAxisAlignment.spaceBetween,
      children: <Widget>[
        buildRoundedBox(verse[0]),
        buildRoundedBox(verse[1]),
        buildRoundedBox(verse[2]),
      ],
    );
  }
}

randVerse() {
  var random = new Random();
  return bibleVerses[random.nextInt(4)];
}

List verse = [randVerse(), randVerse(), randVerse()];

const List<String> bibleVerses = [
  "1 Corinthians 16:14\n"
    "Let all that you do be done in love.",
  "1 John 4:19\n"
    "We love because he first loved us.",
  "Proverbs 10:12\n"
    "Hatred stirs up strife, but love covers all offenses.",
  "Proverbs 17:17\n"
    "A friend loves at all times, and a brother is born for adversity.",
];
