import 'dart:math';

import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePage createState() => _MyHomePage();
}

class _MyHomePage extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: buildTitleText(),
      ),
      body: buildBoxes(),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          setState(() {
            MyHomePage;
          });
          ;
        },
        child: const Icon(Icons.book),
        backgroundColor: Colors.purple,
      ),
    );
  }
}

Widget buildTitleText() {
  return Center(
      child: Text(
    "Three Random Bible Verses",
    textScaleFactor: 1.0,
    textAlign: TextAlign.center,
  ));
}

Widget buildBoxes() {
  var list = [
    'For God so loved the world, that he gave his only Son, that whoever believes in him should not perish but have eternal life. John 3:16',
    'Trust in the LORD with all your heart, and do not lean on your own understanding. In all your ways acknowledge him, and he will make straight your paths. Proverbs 3:15-16',
    'Even though I walk through the valley of the shadow of death, I will fear no evil, for you are with me; your rod and your staff, they comfort me. Psalm 23:4',
    'I can do all things through him who strengthens me. Phillipians 4:13',
    'This is the day that the LORD has made; let us rejoice and be glad in it. Psalms 118:24'
  ];
  final _random = new Random();
  var element = list[_random.nextInt(list.length)];
  return Center(
      child: Container(
    height: 200.0,
    width: 200.0,
    alignment: Alignment(0.0, 0.0),
    decoration: BoxDecoration(
      color: Colors.white,
      border: Border.all(color: Colors.black),
      borderRadius: BorderRadius.all(
        Radius.circular(10.0),
      ),
    ),
    child: Text(
      element,
      textAlign: TextAlign.center,
    ),
  ));
}
