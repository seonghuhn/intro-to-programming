import 'dart:math';

import 'package:flutter/material.dart';

void main() {
  runApp(_MyHomePage());
}

// void main() {
//   runApp(ButtonApp());
// }

class ButtonApp extends StatelessWidget {
  Widget build(BuildContext context) {
    return MaterialApp(
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State {
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            title: Center(
          child: const Text('Bible Verses'),
        )),
        body: Center(
          child: Text(
            _pressedOrNot,
          ),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: _changeText,
          child: Icon(Icons.comment),
          backgroundColor: Colors.blue,
        ));
  }

  String _pressedOrNot = "Press the button!";
  var r = Random();
  void _changeText() {
    setState(_getNewText);
  }

  void _getNewText() {
    _pressedOrNot = _bibleVerses[r.nextInt(_bibleVerses.length)];
  }
}

const List<String> _bibleVerses = [
  "With my own voice I will call out to the Lord\n"
      "and he will answer me from His holy mountain\n"
      "Father, full of grace and truth.\n"
      "Psalms 3:5 NABRE",
  "Behold my servant whom I have chosen,\n"
      "my beloved in whom I delight;\n"
      "I shall place my spirit upon him,\n"
      "and he will proclaim justice to the Gentiles.\n"
      "Matthew 12:18 NABRE",
  "Though darkness covers the earth\n"
      "and thick clouds, the peoples,\n"
      "Upon you the Lord will dawn,\n"
      "and over you his glory will be seen.\n"
      "Isaiah 60:2 NABRE",
  "A mild answer turns back wrath,\n"
      "but a harsh word stirs up anger.\n"
      "Proverbs 15:1 NABRE",
  "The one who has the bride is the bridegroom;\n"
      "the best man, who stands and listens to him,\n"
      "rejoices greatly at the bridegroom's voice.\n"
      "So this joy of mine had been made complete.\n"
      "John 4:29 NABRE",
];

class _MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
      appBar: AppBar(
        title: const Text("Floating Action Verses"),
      ),
      body: Material(
        color: Colors.grey[400],
        child: Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 2.0,
          ),
          child: buildColumn(context),
        ),
      ),
    ));
  }
}

Widget buildTitleText() {
  return Text(
    "Bible Verses",
    textScaleFactor: 3.0,
    textAlign: TextAlign.center,
  );
}

Widget buildRoundedBox(
  String label, {
  double height = 250.0,
}) {
  return Container(
    height: height,
    width: 125.0,
    alignment: Alignment(0.0, 0.0),
    decoration: BoxDecoration(
      color: Colors.white,
      border: Border.all(color: Colors.blue),
      borderRadius: BorderRadius.all(
        Radius.circular(10.0),
      ),
    ),
    child: Text(
      label,
      textAlign: TextAlign.center,
    ),
  );
}

Widget buildColumn(BuildContext context) {
  return Column(
    mainAxisAlignment: MainAxisAlignment.center,
    crossAxisAlignment: CrossAxisAlignment.stretch,
    children: <Widget>[
      buildTitleText(),
      SizedBox(height: 20.0),
      _buildRowOfThree(),
    ],
  );
}

Widget _buildRowOfThree() {
  var r = Random();
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    children: <Widget>[
      buildRoundedBox(_bibleVerses[r.nextInt(_bibleVerses.length)]),
      buildRoundedBox(_bibleVerses[r.nextInt(_bibleVerses.length)]),
      buildRoundedBox(_bibleVerses[r.nextInt(_bibleVerses.length)]),
    ],
  );
}
