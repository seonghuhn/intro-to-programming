class Foo1(object):
    def __init__(self, x):
        self.x = x


foo = Foo1(5)
print(foo.x)
foo.x = 7
print(foo.x)


class Foo2(object):
    def __init__(self, x):
        self._x = x

    def get_x(self):
        return self._x

    def set_x(self, x):
        self._x = x


foo = Foo2(5)
print(foo.get_x())
foo.set_x(7)
print(foo.get_x())


class Foo2a(object):
    def __init__(self, x):
        self._x = x

    def get_x(self):
        if self._x > 100:
            return 100
        return self._x

    def set_x(self, x):
        if isinstance(x, int):
            self._x = x
        else:
            raise ValueError("Only ints")


foo = Foo2a(5)
print(foo.get_x())
foo.set_x(7)
print(foo.get_x())
foo.set_x(101)
print(foo.get_x())
try:
    foo.set_x("x")
    print(foo.get_x())
except Exception as ex:
    print(ex)


class Foo3(object):
    def __init__(self, x):
        self._x = x

    @property
    def x(self):
        return self._x

    @x.setter
    def x(self, x):
        self._x = x


foo = Foo3(5)
print(foo.x)
foo.x = 7
print(foo.x)


class Spell(object):
    def __init__(self, incantation, name):
        self.name = name
        self.incantation = incantation

    def __str__(self):
        return self.name + ' ' + self.incantation + '\n' + self.description

    @property
    def description(self):
        return 'No description'

    def execute(self):
        print(self.incantation)


class Accio(Spell):
    def __init__(self):
        Spell.__init__(self, 'Accio', 'Summoning Charm')


class Confundo(Spell):
    def __init__(self):
        Spell.__init__(self, 'Confundo', 'Confundus Charm')

    @property
    def description(self):
        return 'Causes the victim to become confused and befuddled.'


def study_spell(spell):
    print(spell)


spell = Accio()
spell.execute()
study_spell(spell)
study_spell(Confundo())
