"""
https://realpython.com/beginners-guide-python-turtle/
"""

import turtle

s = turtle.getscreen()

turtle.title("My Turtle Play Program")

t = turtle.Turtle()

t.begin_fill()
t.fd(100)
t.rt(90)
t.fd(100)
t.rt(90)
t.fd(100)
t.rt(90)
t.fd(100)
t.end_fill()

turtle.bgcolor("blue")
t.color("green", "red")
t.pensize(5)
t.circle(60)
t.dot(30)

s.exitonclick()
