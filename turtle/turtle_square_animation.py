"""
https://wecode24.com/stories/Abraham/animation-with-turtle-graphics/
"""
import turtle


def square_animation(smooth):
    screen = turtle.Screen()  # create a new screen
    screen.setup(500, 500)  # 500 x 500 window
    if smooth:
        screen.tracer(0)  # tell screen to not show automatically

    don = turtle.Turtle()  # create a new [ninja] turtle
    don.speed(0)  # make it move faster
    don.width(3)
    if smooth:
        don.hideturtle()  # hide donatello, we only want to see the drawing

    def draw_square():  # a function that draws one square
        for side in range(4):
            don.forward(100)
            don.left(90)

    don.penup()  # go off-screen on the left
    don.goto(-350, 0)
    don.pendown()

    while True:  # now do this repeatedly, to animate :
        don.clear()  # - clear all the turtle's previous drawings
        draw_square()  # - draw a square
        if smooth:
            screen.update()  # only now show the screen, as one of the frames
            don.forward(0.2)  # - move forward a very little bit
        else:
            don.forward(10)  # - move forward a bit


if __name__ == "__main__":
    square_animation(smooth=False)
    # square_animation(smooth=True)
