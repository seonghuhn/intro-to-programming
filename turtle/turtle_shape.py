"""
https://realpython.com/beginners-guide-python-turtle/
"""

import turtle

t = turtle.Turtle()

num_sides = input("Number of sides: ")
while num_sides:
    num_sides = int(num_sides)

    color = input("Fill color: ")
    if color:
        t.fillcolor(color)

    t.clear()
    angle = 360 / num_sides

    if color:
        t.begin_fill()

    for n in range(num_sides):
        t.fd(100)
        t.rt(angle)

    if color:
        t.end_fill()

    num_sides = input("Number of sides: ")

print('Goodbye!')
