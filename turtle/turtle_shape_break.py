"""
https://realpython.com/beginners-guide-python-turtle/

Same as turtle_shape.py except do input within loop and break out if num_sides is empty.
"""

import turtle

t = turtle.Turtle()

while True:
    num_sides = input("Number of sides: ")
    if not num_sides:
        break
    num_sides = int(num_sides)

    color = input("Fill color: ")
    if color:
        t.fillcolor(color)

    t.clear()
    angle = 360 / num_sides

    if color:
        t.begin_fill()

    for n in range(num_sides):
        t.fd(100)
        t.rt(angle)

    if color:
        t.end_fill()

print('Goodbye!')
