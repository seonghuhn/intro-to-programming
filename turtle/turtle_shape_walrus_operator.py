"""
https://realpython.com/beginners-guide-python-turtle/

Same as turtle_shape.py except use walrus operator so you can do input in condition of while.
"""

import turtle

t = turtle.Turtle()

while num_sides := input("Number of sides: "):
    num_sides = int(num_sides)

    color = input("Fill color: ")
    if color:
        t.fillcolor(color)

    t.clear()
    angle = 360 / num_sides

    if color:
        t.begin_fill()

    for n in range(num_sides):
        t.fd(100)
        t.rt(angle)

    if color:
        t.end_fill()

print('Goodbye!')
