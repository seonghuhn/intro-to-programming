"""
https://realpython.com/beginners-guide-python-turtle/#while-loops

Draws circles, each circle being bigger than the previous.
https://files.realpython.com/media/TURTLE_EDIT_WHILE_LOOP_GIF.c0b9eae029cc.gif
"""

import turtle


def circles():
    radius = 10
    while radius <= 40:
        turtle.circle(radius)
        radius += 10


if __name__ == "__main__":
    s = turtle.Screen()

    turtle.speed(0)

    circles()

    s.exitonclick()
