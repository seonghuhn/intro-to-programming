"""
https://www.tutorialspoint.com/turtle-programming-in-python

Draws circles, each circle being bigger than the previous and their line of center slightly rotated from the previous.
https://www.tutorialspoint.com/assets/questions/media/13542/another_pattern.jpg
"""

import turtle


def circles():
    for i in range(1, 30):
        turtle.circle(5 * i)
        turtle.circle(-5 * i)
        turtle.left(i)


if __name__ == "__main__":
    s = turtle.Screen()

    turtle.speed(0)

    circles()

    s.exitonclick()
