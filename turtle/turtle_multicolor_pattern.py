"""
https://www.tutorialspoint.com/turtle-programming-in-python

Draws multicolor pattern.
https://www.tutorialspoint.com/assets/questions/media/13542/drawing_pattern.jpg
"""
import turtle

if __name__ == "__main__":
    colors = ["red", "purple", "blue", "green", "orange", "yellow"]

    s = turtle.Screen()
    turtle.bgcolor("black")
    turtle.speed(0)

    for i in range(360):
        turtle.pencolor(colors[i % 6])
        turtle.width(i // 100 + 1)
        turtle.forward(i)
        turtle.left(59)

    s.exitonclick()
