"""
https://www.tutorialspoint.com/turtle-programming-in-python

Draws concentric squares.
https://www.tutorialspoint.com/assets/questions/media/13542/square_box.jpg
"""

import turtle


def square(size):
    for i in range(4):
        turtle.fd(size)
        turtle.left(90)
        size = size - 5


# Draw concentric squares.
def squares():
    for size in range(146, 6, -20):
        square(size)


if __name__ == "__main__":
    s = turtle.Screen()
    s.title("Turtle")

    turtle.bgcolor("light blue")
    turtle.color("black")
    # print(turtle.speed())
    turtle.speed(5)

    squares()

    s.exitonclick()
