import turtle

screen = turtle.Screen()
screen.setup(500, 500)
screen.tracer(0)
# https://giphy.com/stickers/deviantart-super-mario-12oufCB0MyZ1Go/media
screen.register_shape("mario.gif")  # register the image with the screen as a shape

mario = turtle.Turtle()
mario.speed(0)
mario.shape("mario.gif")  # now set the turtle's shape to it

mario.penup()
mario.goto(-100, 0)

while True:
    screen.update()
    mario.forward(1)
