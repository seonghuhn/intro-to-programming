num = int(input("Number: "))

guess = 0
while guess ** 3 < num:
    guess += 1

if guess ** 3 == num:
    print(f"{guess} is the perfect cube root of {num}")
else:
    print(f"No perfect cube root found for {num}")
