import string


def shift_uc(message, key):
    """
    Shifts the message by the key.
    :param message: the message to shift, assumes it is uppercase only
    :param key: the shift key. A key of 1, for example, means that A becomes B, B becomes C, etc.
    :return: the shifted message
    """
    shifted_message = ''
    for c in message:
        idx = string.ascii_uppercase.find(c)
        shifted_message += string.ascii_uppercase[(idx + key) % 26]
    return shifted_message


def shift_alpha(message, key):
    """
    Shifts the message by the key.
    :param message: the message to shift, assumes it is alphabets only
    :param key: the shift key. A key of 1, for example, means that A becomes B, b becomes c, etc.
    :return: the shifted message
    """
    shifted_message = ''
    for c in message:
        if (idx := string.ascii_lowercase.find(c)) != -1:
            shifted_message += string.ascii_lowercase[(idx + key) % 26]
        elif (idx := string.ascii_uppercase.find(c)) != -1:
            shifted_message += string.ascii_uppercase[(idx + key) % 26]
    return shifted_message


def shift(message, key):
    """
    Shifts the message by the key.
    :param message: the message to shift
    :param key: the shift key. A key of 1, for example, means that A becomes B, b becomes c, etc.
    :return: the shifted message
    """
    shifted_message = ''
    for c in message:
        if (idx := string.ascii_lowercase.find(c)) != -1:
            shifted_message += string.ascii_lowercase[(idx + key) % 26]
        elif (idx := string.ascii_uppercase.find(c)) != -1:
            shifted_message += string.ascii_uppercase[(idx + key) % 26]
        else:
            shifted_message += c
    return shifted_message


def shift_unicode(message, key):
    """
    Shifts the message by the key.
    :param message: the message to shift
    :param key: the shift key. A key of 1, for example, means that A becomes B, b becomes c, etc.
    :return: the shifted message
    """
    shifted_message = ''
    for c in message:
        if c.isalpha():
            shifted_c = chr(ord(c) + key)
            if shifted_c.isalpha():
                shifted_message += shifted_c
            else:
                if key > 0:
                    shifted_message += chr(ord(shifted_c) - 26)
                else:
                    shifted_message += chr(ord(shifted_c) + 26)
        else:
            shifted_message += c
    return shifted_message


if __name__ == "__main__":
    print(
        shift("Fglzafy ak ugnwjwv mh lzsl oadd fgl tw jwnwsdwv, gj zavvwf lzsl oadd fgl tw cfgof. Dmcw 12:2 WKN", -18))
