import datetime


class Person(object):
    def __init__(self, name):
        self._name = name
        self._last_name = name.split(' ')[-1]
        self._birthday = None

    @property
    def last_name(self):
        return self._last_name

    # not a traditional setter because does not take a birthday as argument
    def set_birthday(self, month, day, year):
        self._birthday = datetime.date(year, month, day)

    @property
    def age(self):
        return (datetime.date.today() - self._birthday).days

    def __lt__(self, other):
        if self.last_name == other.last_name:
            return self._name < other._name
        return self.last_name < other.last_name

    def __str__(self):
        return self._name


joe = Person("Joe Biden")
joe.set_birthday(11, 20, 1942)
print(joe, joe.age)
barack = Person("Barack Obama")
barack.set_birthday(8, 4, 1961)
print(joe < barack)
kamala = Person("Kamala Harris")
kamala.set_birthday(10, 20, 1964)
george = Person("George W Bush")
george.set_birthday(7,6,1946)
george_sr = Person("George H W Bush")

print()
presidents = [joe, barack, kamala, george, george_sr]
for president in presidents:
    print(president)
print()

presidents.sort()
for president in presidents:
    print(president)
