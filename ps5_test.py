import easy_string_interview_problems
from ps5_solutions import kehl_dominic_ps5, sepka_jp_ps5, shen_joseph_ps5, kehl_julia_ps5, caruso_lorenzo_ps5, \
    aubin_lucas_ps5, deboard_sean_ps5


def test_reverse_string():
    assert easy_string_interview_problems.reverse_string("") == ""
    assert easy_string_interview_problems.reverse_string("brag") == "garb"
    assert easy_string_interview_problems.reverse_string("hello") == "olleh"
    assert easy_string_interview_problems.reverse_string("Hannah") == "hannaH"

    assert aubin_lucas_ps5.reverse_string("") == ""
    assert aubin_lucas_ps5.reverse_string("brag") == "garb"
    assert aubin_lucas_ps5.reverse_string("hello") == "olleh"
    assert aubin_lucas_ps5.reverse_string("Hannah") == "hannaH"

    assert caruso_lorenzo_ps5.reverse_string("") == ""
    assert caruso_lorenzo_ps5.reverse_string("brag") == "garb"
    assert caruso_lorenzo_ps5.reverse_string("hello") == "olleh"
    assert caruso_lorenzo_ps5.reverse_string("Hannah") == "hannaH"

    assert deboard_sean_ps5.reverse_string("") == ""
    assert deboard_sean_ps5.reverse_string("brag") == "garb"
    assert deboard_sean_ps5.reverse_string("hello") == "olleh"
    assert deboard_sean_ps5.reverse_string("Hannah") == "hannaH"

    assert kehl_dominic_ps5.reverse_string("") == ""
    assert kehl_dominic_ps5.reverse_string("brag") == "garb"
    assert kehl_dominic_ps5.reverse_string("hello") == "olleh"
    assert kehl_dominic_ps5.reverse_string("Hannah") == "hannaH"

    assert kehl_julia_ps5.reverse_string("") == ""
    assert kehl_julia_ps5.reverse_string("brag") == "garb"
    assert kehl_julia_ps5.reverse_string("hello") == "olleh"
    assert kehl_julia_ps5.reverse_string("Hannah") == "hannaH"

    assert sepka_jp_ps5.reverse_string("") == ""
    assert sepka_jp_ps5.reverse_string("brag") == "garb"
    assert sepka_jp_ps5.reverse_string("hello") == "olleh"
    assert sepka_jp_ps5.reverse_string("Hannah") == "hannaH"

    assert shen_joseph_ps5.reverse_string("") == ""
    assert shen_joseph_ps5.reverse_string("brag") == "garb"
    assert shen_joseph_ps5.reverse_string("hello") == "olleh"
    assert shen_joseph_ps5.reverse_string("Hannah") == "hannaH"


def test_first_unique_char():
    assert easy_string_interview_problems.first_unique_char("a") == 0
    assert easy_string_interview_problems.first_unique_char("Ecclesiastes") == 3
    assert easy_string_interview_problems.first_unique_char("lovelife") == 1
    assert easy_string_interview_problems.first_unique_char("leetcode") == 0
    assert easy_string_interview_problems.first_unique_char("dermatoglyphics") == 0
    assert easy_string_interview_problems.first_unique_char("uncopyrightable") == 0
    # https://en.wikipedia.org/wiki/Heterogram_(literature)
    assert easy_string_interview_problems.first_unique_char("Heizölrückstoßabdämpfung") == 0
    assert easy_string_interview_problems.first_unique_char("senselessness") == 5
    assert easy_string_interview_problems.first_unique_char("aaAab") == 4
    assert easy_string_interview_problems.first_unique_char("antitrinitarian") == -1
    assert easy_string_interview_problems.first_unique_char("nonsensuousness") == -1
    assert easy_string_interview_problems.first_unique_char("Abba") == -1
    assert easy_string_interview_problems.first_unique_char("") == -1

    # assert aubin_lucas_ps5.first_unique_char("a") == 0
    # assert aubin_lucas_ps5.first_unique_char("Ecclesiastes") == 3
    # assert aubin_lucas_ps5.first_unique_char("lovelife") == 1
    # assert aubin_lucas_ps5.first_unique_char("leetcode") == 0
    # assert aubin_lucas_ps5.first_unique_char("dermatoglyphics") == 0
    # assert aubin_lucas_ps5.first_unique_char("uncopyrightable") == 0
    # assert aubin_lucas_ps5.first_unique_char("Heizölrückstoßabdämpfung") == 0
    # assert aubin_lucas_ps5.first_unique_char("senselessness") == 5
    # assert aubin_lucas_ps5.first_unique_char("aaAab") == 4
    assert aubin_lucas_ps5.first_unique_char("antitrinitarian") == -1
    assert aubin_lucas_ps5.first_unique_char("nonsensuousness") == -1
    assert aubin_lucas_ps5.first_unique_char("Abba") == -1
    assert aubin_lucas_ps5.first_unique_char("") == -1

    assert caruso_lorenzo_ps5.first_unique_char("a") == 0
    assert caruso_lorenzo_ps5.first_unique_char("Ecclesiastes") == 3
    assert caruso_lorenzo_ps5.first_unique_char("lovelife") == 1
    assert caruso_lorenzo_ps5.first_unique_char("leetcode") == 0
    assert caruso_lorenzo_ps5.first_unique_char("dermatoglyphics") == 0
    assert caruso_lorenzo_ps5.first_unique_char("uncopyrightable") == 0
    assert caruso_lorenzo_ps5.first_unique_char("Heizölrückstoßabdämpfung") == 0
    assert caruso_lorenzo_ps5.first_unique_char("senselessness") == 5
    assert caruso_lorenzo_ps5.first_unique_char("aaAab") == 4
    assert caruso_lorenzo_ps5.first_unique_char("antitrinitarian") == -1
    assert caruso_lorenzo_ps5.first_unique_char("nonsensuousness") == -1
    assert caruso_lorenzo_ps5.first_unique_char("Abba") == -1
    assert caruso_lorenzo_ps5.first_unique_char("") == -1

    assert deboard_sean_ps5.first_unique_char("a") == 0
    assert deboard_sean_ps5.first_unique_char("Ecclesiastes") == 3
    assert deboard_sean_ps5.first_unique_char("lovelife") == 1
    assert deboard_sean_ps5.first_unique_char("leetcode") == 0
    assert deboard_sean_ps5.first_unique_char("dermatoglyphics") == 0
    assert deboard_sean_ps5.first_unique_char("uncopyrightable") == 0
    assert deboard_sean_ps5.first_unique_char("Heizölrückstoßabdämpfung") == 0
    assert deboard_sean_ps5.first_unique_char("senselessness") == 5
    assert deboard_sean_ps5.first_unique_char("aaAab") == 4
    assert deboard_sean_ps5.first_unique_char("antitrinitarian") == -1
    assert deboard_sean_ps5.first_unique_char("nonsensuousness") == -1
    assert deboard_sean_ps5.first_unique_char("Abba") == -1
    # assert deboard_sean_ps5.first_unique_char("") == -1

    assert kehl_dominic_ps5.first_unique_char("a") == 0
    assert kehl_dominic_ps5.first_unique_char("Ecclesiastes") == 3
    assert kehl_dominic_ps5.first_unique_char("lovelife") == 1
    assert kehl_dominic_ps5.first_unique_char("leetcode") == 0
    assert kehl_dominic_ps5.first_unique_char("dermatoglyphics") == 0
    assert kehl_dominic_ps5.first_unique_char("uncopyrightable") == 0
    assert kehl_dominic_ps5.first_unique_char("Heizölrückstoßabdämpfung") == 0
    assert kehl_dominic_ps5.first_unique_char("senselessness") == 5
    assert kehl_dominic_ps5.first_unique_char("aaAab") == 4
    assert kehl_dominic_ps5.first_unique_char("antitrinitarian") == -1
    assert kehl_dominic_ps5.first_unique_char("nonsensuousness") == -1
    assert kehl_dominic_ps5.first_unique_char("Abba") == -1
    # assert kehl_dominic_ps5.first_unique_char("") == -1

    assert kehl_julia_ps5.first_unique_char("a") == 0
    assert kehl_julia_ps5.first_unique_char("Ecclesiastes") == 3
    assert kehl_julia_ps5.first_unique_char("lovelife") == 1
    assert kehl_julia_ps5.first_unique_char("leetcode") == 0
    assert kehl_julia_ps5.first_unique_char("dermatoglyphics") == 0
    assert kehl_julia_ps5.first_unique_char("uncopyrightable") == 0
    assert kehl_julia_ps5.first_unique_char("Heizölrückstoßabdämpfung") == 0
    assert kehl_julia_ps5.first_unique_char("senselessness") == 5
    assert kehl_julia_ps5.first_unique_char("aaAab") == 4
    assert kehl_julia_ps5.first_unique_char("antitrinitarian") == -1
    assert kehl_julia_ps5.first_unique_char("nonsensuousness") == -1
    assert kehl_julia_ps5.first_unique_char("Abba") == -1
    # assert kehl_julia_ps5.first_unique_char("") == -1

    assert sepka_jp_ps5.first_unique_char("a") == 0
    assert sepka_jp_ps5.first_unique_char("Ecclesiastes") == 3
    assert sepka_jp_ps5.first_unique_char("lovelife") == 1
    assert sepka_jp_ps5.first_unique_char("leetcode") == 0
    assert sepka_jp_ps5.first_unique_char("dermatoglyphics") == 0
    assert sepka_jp_ps5.first_unique_char("uncopyrightable") == 0
    assert sepka_jp_ps5.first_unique_char("Heizölrückstoßabdämpfung") == 0
    assert sepka_jp_ps5.first_unique_char("senselessness") == 5
    assert sepka_jp_ps5.first_unique_char("aaAab") == 4
    assert sepka_jp_ps5.first_unique_char("antitrinitarian") == -1
    assert sepka_jp_ps5.first_unique_char("nonsensuousness") == -1
    assert sepka_jp_ps5.first_unique_char("Abba") == -1
    assert sepka_jp_ps5.first_unique_char("") == -1

    assert shen_joseph_ps5.first_unique_char("a") == 0
    assert shen_joseph_ps5.first_unique_char("Ecclesiastes") == 3
    assert shen_joseph_ps5.first_unique_char("lovelife") == 1
    assert shen_joseph_ps5.first_unique_char("leetcode") == 0
    assert shen_joseph_ps5.first_unique_char("dermatoglyphics") == 0
    assert shen_joseph_ps5.first_unique_char("uncopyrightable") == 0
    assert shen_joseph_ps5.first_unique_char("Heizölrückstoßabdämpfung") == 0
    assert shen_joseph_ps5.first_unique_char("senselessness") == 5
    assert shen_joseph_ps5.first_unique_char("aaAab") == 4
    assert shen_joseph_ps5.first_unique_char("antitrinitarian") == -1
    assert shen_joseph_ps5.first_unique_char("nonsensuousness") == -1
    assert shen_joseph_ps5.first_unique_char("Abba") == -1
    assert shen_joseph_ps5.first_unique_char("") == -1


def test_valid_palindrome():
    assert easy_string_interview_problems.valid_palindrome("banana") is False
    assert easy_string_interview_problems.valid_palindrome("")
    assert easy_string_interview_problems.valid_palindrome("saippuakivikauppias")
    assert easy_string_interview_problems.valid_palindrome("Bob")
    assert easy_string_interview_problems.valid_palindrome("Anna")
    assert easy_string_interview_problems.valid_palindrome("An.na")
    assert easy_string_interview_problems.valid_palindrome("101")
    assert easy_string_interview_problems.valid_palindrome("F1f")
    assert easy_string_interview_problems.valid_palindrome("F1 f")
    assert easy_string_interview_problems.valid_palindrome("A man, a plan, a canal: Panama")

    assert aubin_lucas_ps5.valid_palindrome("banana") is False
    assert aubin_lucas_ps5.valid_palindrome("")
    assert aubin_lucas_ps5.valid_palindrome("saippuakivikauppias")
    assert aubin_lucas_ps5.valid_palindrome("Bob")
    assert aubin_lucas_ps5.valid_palindrome("Anna")
    assert aubin_lucas_ps5.valid_palindrome("An.na")
    assert aubin_lucas_ps5.valid_palindrome("101")
    assert aubin_lucas_ps5.valid_palindrome("F1f")
    assert aubin_lucas_ps5.valid_palindrome("F1 f")
    assert aubin_lucas_ps5.valid_palindrome("A man, a plan, a canal: Panama")

    assert caruso_lorenzo_ps5.valid_palindrome("banana") is False
    assert caruso_lorenzo_ps5.valid_palindrome("")
    assert caruso_lorenzo_ps5.valid_palindrome("saippuakivikauppias")
    assert caruso_lorenzo_ps5.valid_palindrome("Bob")
    assert caruso_lorenzo_ps5.valid_palindrome("Anna")
    assert caruso_lorenzo_ps5.valid_palindrome("An.na")
    assert caruso_lorenzo_ps5.valid_palindrome("101")
    assert caruso_lorenzo_ps5.valid_palindrome("F1f")
    # assert caruso_lorenzo_ps5.valid_palindrome("F1 f")
    assert caruso_lorenzo_ps5.valid_palindrome("A man, a plan, a canal: Panama")

    assert deboard_sean_ps5.valid_palindrome("banana") is False
    assert deboard_sean_ps5.valid_palindrome("")
    assert deboard_sean_ps5.valid_palindrome("saippuakivikauppias")
    assert deboard_sean_ps5.valid_palindrome("Bob")
    assert deboard_sean_ps5.valid_palindrome("Anna")
    assert deboard_sean_ps5.valid_palindrome("An.na")
    assert deboard_sean_ps5.valid_palindrome("101")
    assert deboard_sean_ps5.valid_palindrome("F1f")
    # assert deboard_sean_ps5.valid_palindrome("F1 f")
    # assert deboard_sean_ps5.valid_palindrome("A man, a plan, a canal: Panama")

    assert kehl_dominic_ps5.valid_palindrome("banana") is False
    assert kehl_dominic_ps5.valid_palindrome("")
    assert kehl_dominic_ps5.valid_palindrome("saippuakivikauppias")
    assert kehl_dominic_ps5.valid_palindrome("Bob")
    assert kehl_dominic_ps5.valid_palindrome("Anna")
    # assert kehl_dominic_ps5.valid_palindrome("An.na")
    assert kehl_dominic_ps5.valid_palindrome("101")
    assert kehl_dominic_ps5.valid_palindrome("F1f")
    assert kehl_dominic_ps5.valid_palindrome("F1 f")
    assert kehl_dominic_ps5.valid_palindrome("A man, a plan, a canal: Panama")

    assert kehl_julia_ps5.valid_palindrome("banana") is False
    assert kehl_julia_ps5.valid_palindrome("")
    assert kehl_julia_ps5.valid_palindrome("saippuakivikauppias")
    assert kehl_julia_ps5.valid_palindrome("Bob")
    assert kehl_julia_ps5.valid_palindrome("Anna")
    # assert kehl_julia_ps5.valid_palindrome("An.na")
    assert kehl_julia_ps5.valid_palindrome("101")
    assert kehl_julia_ps5.valid_palindrome("F1f")
    # assert kehl_julia_ps5.valid_palindrome("F1 f")
    # assert kehl_julia_ps5.valid_palindrome("A man, a plan, a canal: Panama")

    assert sepka_jp_ps5.valid_palindrome("banana") is False
    assert sepka_jp_ps5.valid_palindrome("")
    assert sepka_jp_ps5.valid_palindrome("saippuakivikauppias")
    assert sepka_jp_ps5.valid_palindrome("Bob")
    assert sepka_jp_ps5.valid_palindrome("Anna")
    assert sepka_jp_ps5.valid_palindrome("An.na")
    assert sepka_jp_ps5.valid_palindrome("101")
    assert sepka_jp_ps5.valid_palindrome("F1f")
    # assert sepka_jp_ps5.valid_palindrome("F1 f")
    # assert sepka_jp_ps5.valid_palindrome("A man, a plan, a canal: Panama")

    assert shen_joseph_ps5.valid_palindrome("banana") is False
    assert shen_joseph_ps5.valid_palindrome("")
    assert shen_joseph_ps5.valid_palindrome("saippuakivikauppias")
    assert shen_joseph_ps5.valid_palindrome("Bob")
    assert shen_joseph_ps5.valid_palindrome("Anna")
    assert shen_joseph_ps5.valid_palindrome("An.na")
    assert shen_joseph_ps5.valid_palindrome("101")
    assert shen_joseph_ps5.valid_palindrome("F1f")
    assert shen_joseph_ps5.valid_palindrome("F1 f")
    assert shen_joseph_ps5.valid_palindrome("A man, a plan, a canal: Panama")
