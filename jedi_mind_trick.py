def jedi_mind_trick(stormtroopers_request):
    """
    Determines the Jedi mind trick response to the Stormtroopers request.
    :param stormtroopers_request: the Stormtroopers request, e.g. "Droids"
    :return: the Jedi mink trick response, e.g. "These aren't the droids you're looking for."
    """
    if "jedi" in stormtroopers_request.lower():
        answer = "You will remove these restraints and leave the cell with the door open."
    elif "droids" in stormtroopers_request.lower():
        answer = "These aren't the droids you're looking for."
    elif "obi-wan" in stormtroopers_request.lower():
        answer = "It's okay that we're here."
    elif "rebels" in stormtroopers_request.lower():
        answer = "You want to go home and rethink your life."
    else:
        answer = "I find your lack of faith disturbing."
    return answer


def robot():
    print("Good afternoon Stormtroopers.")
    stormtroopers_request = input("Whom are you looking for? ")
    print(jedi_mind_trick(stormtroopers_request))
    print("Have a pleasant day.")


# Start the robot program
if __name__ == "__main__":
    robot()
