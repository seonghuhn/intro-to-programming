def jedi_mind_trick(stormtroopers_request):
    """
    Determines the Jedi mind trick response to the Stormtroopers request.
    :param stormtroopers_request: the Stormtroopers request, e.g. "Droids"
    :return: the Jedi mink trick response, e.g. "These aren't the droids you're looking for."
    """
    # TODO: Determine correct response to storm_troopers_request
    return "Down with the Empire!"


def robot():
    # TODO: Greet Stormtroopers and ask whom they are looking for.
    stormtroopers_request = "Live Oak Academy"

    print(jedi_mind_trick(stormtroopers_request))

    # TODO: Wish Stormtroopers a pleasant day.


# Start the robot program
if __name__ == "__main__":
    robot()
