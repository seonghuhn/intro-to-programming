def reverse_string(s):
    """
    Reverses the string.
    :param s: the input string
    :return: the reversed string
    """
    # NOTE: Do not use the Python reversed function in this function.
    rs = s[::-1]
    return rs


def first_unique_char(s):
    """
    Finds the index of the first unique character in the string, ignoring case.
    :param s: the input string
    :return: the index of the first unique character if one is found; otherwise returns -1
    """
    s = s.lower()
    unique = 0
    for x in range(len(s)):
        if s[0] not in s[1:]:
            unique = 0
            break
        elif 0 < x < len(s):
            if (s[x] in s[:x]) or (s[x] in s[x + 1:]):
                continue
            else:
                unique = x
                break
        else:
            unique = -1
    return unique


def valid_palindrome(s):
    """
    Determine if the string is a valid palindrome, a word, phrase, or sequence that reads the same backward as forward,
    e.g., "madam" or "nurses run".
    :param s: the input string
    :return: True if the input string is a valid palindrome. False otherwise.
    """
    rev_s = reverse_string(s.lower().replace(" ", ""))
    if rev_s == s.lower():
        return True
    else:
        return False


if __name__ == "__main__":
    # returns "olleh"
    print(reverse_string("hello"))
    # returns "hannaH"
    print(reverse_string("Hannah"))

    # returns 3
    print(first_unique_char("Ecclesiastes"))
    # returns 1
    print(first_unique_char("lovelife"))

    # returns False
    print(valid_palindrome("banana"))
    # returns True
    print(valid_palindrome("saippuakivikauppias"))
    # returns True
    print(valid_palindrome("Bob"))
    # returns True
    print(valid_palindrome("A man, a plan, a canal: Panama"))
