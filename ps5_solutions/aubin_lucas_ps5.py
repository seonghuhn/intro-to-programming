def reverse_string(s):
    """
    Reverses the string.
    :param s: the input string
    :return: the reversed string
    """
    # NOTE: Do not use the Python reversed function in this function.

    out = ''
    for a in range(len(s) - 1, -1, -1):
        out += s[a]

    return out

def first_unique_char(s):
    """
    Finds the index of the first unique character in the string, ignoring case.
    :param s: the input string
    :return: the index of the first unique character if one is found; otherwise returns -1
    """
    l = s.lower()

    for a in range(len(l)):
        sub1 = l[a + 1:]
        sub2 = l[:a]
        if l[a] not in sub1 and l[a] not in sub2:
            return l[a]
    return -1


def valid_palindrome(s):
    """
    Determine if the string is a valid palindrome, a word, phrase, or sequence that reads the same backward as forward,
    e.g., "madam" or "nurses run".
    :param s: the input string
    :return: True if the input string is a valid palindrome. False otherwise.
    """
    l = ''

    for a in s.lower():
        if a.isalnum():
            l += a

    return l == reverse_string(l)


if __name__ == "__main__":
    # returns "olleh"
    print(reverse_string("hello"))
    # returns "hannaH"
    print(reverse_string("Hannah"))

    # returns 3
    print(first_unique_char("Ecclesiastes"))
    # returns 1
    print(first_unique_char("lovelife"))

    # returns False
    print(valid_palindrome("banana"))
    # returns True
    print(valid_palindrome("saippuakivikauppias"))
    # returns True
    print(valid_palindrome("Bob"))
    # returns True
    print(valid_palindrome("A man, a plan, a canal: Panama"))
