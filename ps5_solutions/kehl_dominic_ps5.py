
def reverse_string(s):
    """
    Reverses the string.
    :param s: the input string
    :return: the reversed string
    """
    stringinput = s
    reverse_string = ''
    length = len(stringinput)

    for a in range(len(s)):
        length = length - 1
        reverse_string += stringinput[length]
    return reverse_string


def first_unique_char(s):
    """
    Finds the index of the first unique character in the string, ignoring case.
    :param s: the input string
    :return: the index of the first unique character if one is found; otherwise returns -1
    """
    counter = 0
    letter = s[0]
    unique_letter = ''
    len_s = len(s)

    for b in range(0, len_s):
        counter = 0
        letter = s[b].lower()
        for j in range(0, len_s):
            other_letter = s[j].lower()
            if other_letter == letter:
                counter += 1
        if counter == 1:
            unique_letter = s[b]
            return b
    if not unique_letter:
        return -1


def valid_palindrome(s):
    """
    Determine if the string is a valid palindrome, a word, phrase, or sequence that reads the same backward as forward,
    e.g., "madam" or "nurses run".
    :param s: the input string
    :return: True if the input string is a valid palindrome. False otherwise.
    """
    stringinput = s.lower()
    alphastring = ''
    reverse_string = ''
    length = len(stringinput)


    for a in range(len(s)):
        length -= 1
        if stringinput[length].isalnum() == True:
            reverse_string += stringinput[length]
            alphastring += stringinput[a]
    non_reversed_string = reversed(reverse_string)
    if list(non_reversed_string) == list(reverse_string):
        return True
    else:
        return False



if __name__ == "__main__":
    # returns "olleh"
    print(reverse_string("hello"))
    # returns "hannaH"
    print(reverse_string("Hannah"))

    # returns 3
    print(first_unique_char("Ecclesiastes"))
    # returns 1
    print(first_unique_char("lovelife"))

    # returns False
    print(valid_palindrome("banana"))
    # returns True
    print(valid_palindrome("saippuakivikauppias"))
    # returns True
    print(valid_palindrome("Bob"))
    # returns True
    print(valid_palindrome('A man, a plan, a canal: Panama'))