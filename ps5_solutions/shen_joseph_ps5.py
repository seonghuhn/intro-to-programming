def reverse_string(s):
    """
    Reverses the string.
    :param s: the input string
    :return: the reversed string
    """
    # NOTE: Do not use the Python reversed function in this function.
    return s[::-1]


def first_unique_char(s):
    """
    Finds the index of the first unique character in the string, ignoring case.
    :param s: the input string
    :return: the index of the first unique character if one is found; otherwise returns -1
    """
    s = s.lower()
    counts = {}
    for char in s:
        counts[char] = counts[char] + 1 if char in counts else 1

    for char in s:
        if counts[char] == 1:
            return s.find(char)
    return -1


def valid_palindrome(s):
    """
    Determine if the string is a valid palindrome, a word, phrase, or sequence that reads the same backward as forward,
    e.g., "madam" or "nurses run".
    :param s: the input string
    :return: True if the input string is a valid palindrome. False otherwise.
    """
    s = s.lower()
    punctuation = '''!()-[]{};:'", <>./?@#$%^&*_~'''
    for char in s:
        if char in punctuation:
            s = s.replace(char, "")
    return s[:] == s[::-1]

if __name__ == "__main__":
    # returns "olleh"
    print(reverse_string("hello"))
    # returns "hannaH"
    print(reverse_string("Hannah"))

    # returns 3
    print(first_unique_char("Ecclesiastes"))
    # returns 1
    print(first_unique_char("lovelife"))

    # returns False
    print(valid_palindrome("banana"))
    # returns True
    print(valid_palindrome("saippuakivikauppias"))
    # returns True
    print(valid_palindrome("Bob"))
    # returns True
    print(valid_palindrome("A man, a plan, a canal: Panama"))
