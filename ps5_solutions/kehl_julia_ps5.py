def reverse_string(s):
    """
    Reverses the string.
    :param s: the input string
    :return: the reversed string
    """
    # NOTE: Do not use the Python reversed function in this function.

    reverse_string = ''
    input_string = s
    length = len(input_string)

    for i in range(len(s)):
        length = length - 1
        reverse_string += input_string[length]
    return reverse_string


def first_unique_char(s):
    """
    Finds the index of the first unique character in the string, ignoring case.
    :param s: the input string
    :return: the index of the first unique character if one is found; otherwise returns -1
    """

    letter = s[0]
    unique_letter = ''
    match_counter = 0

    for i in range(0, len(s)):
        match_counter = 0
        letter = s[i].lower()
        for j in range(0, len(s)):
            inside_letter = s[j].lower()
            if inside_letter == letter:
                match_counter += 1
        if match_counter == 1:
            unique_letter = s[i]
            return i
    if not unique_letter:
        return -1

def valid_palindrome(s):
    """
    Determine if the string is a valid palindrome, a word, phrase, or sequence that reads the same backward as forward,
    e.g., "madam" or "nurses run".
    :param s: the input string
    :return: True if the input string is a valid palindrome. False otherwise.
    """
    reverse_palindrome = ''
    valid_palindrome = s.lower()

    for i in range(len(s) - 1, -1, -1):
        if valid_palindrome[i].isalnum():
            reverse_palindrome += valid_palindrome[i]
    if valid_palindrome.lower() == reverse_palindrome.lower():
        return True
    else:
        return False




if __name__ == "__main__":
    # # returns "olleh"
    print(reverse_string("hello"))
    # # returns "hannaH"
    print(reverse_string("Hannah"))
    #
    # # returns 3
    print(first_unique_char("Ecclesiastes"))
    # # returns 1
    print(first_unique_char("lovelife"))
    #
    # # returns False
    print(valid_palindrome("banana"))
    # # returns True
    print(valid_palindrome("saippuakivikauppias"))
     # returns True
    print(valid_palindrome("Bob"))
    # returns True
    print(valid_palindrome("A man, a plan, a canal: Panama"))
