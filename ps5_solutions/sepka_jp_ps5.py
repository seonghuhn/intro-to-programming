def reverse_string(s):
    """
    Reverses the string.
    :param s: the input string
    :return: the reversed string
    """
    s_reverse = ""
    a = len(s)
    while a != 0:
        s_reverse += s[a - 1:a]
        a -= 1
    return s_reverse


def first_unique_char(s):
    """
    Finds the index of the first unique character in the string, ignoring case.
    :param s: the input string
    :return: the index of the first unique character if one is found; otherwise returns -1
    """
    index = -1
    a = 0
    s = s.lower()
    while a != len(s):
        end_str = s[a + 1:]
        start_str = s[:a]
        stitched_str = end_str + start_str
        if s[a:a + 1] not in stitched_str:
            index = a
            break
        a += 1

    return index


def valid_palindrome(s):
    """
    Determine if the string is a valid palindrome, a word, phrase, or sequence that reads the same backward as forward,
    e.g., "madam" or "nurses run".
    :param s: the input string
    :return: True if the input string is a valid palindrome. False otherwise.
    """
    palindrome = True
    for x in range(len(s)):
        if s.lower()[x:x + 1] != reverse_string(s.lower())[x:x + 1]:
            palindrome = False
            break

    return palindrome


if __name__ == "__main__":
    # returns "olleh"
    print(reverse_string("hello"))
    # returns "hannaH"
    print(reverse_string("Hannah"))

    # returns 3
    print(first_unique_char("Ecclesiastes"))
    # returns 1
    print(first_unique_char("lovelife"))

    # returns False
    print(valid_palindrome("banana"))
    # returns True
    print(valid_palindrome("saippuakivikauppias"))
    # returns True
    print(valid_palindrome("Bob"))
    # returns True
    print(valid_palindrome("A man, a plan, a canal: Panama"))
