"""
Design a parking system for a parking lot. The parking lot has three kinds of parking spaces: big, medium, and small,
with a fixed number of slots for each size.

Implement the ParkingSystem class:

ParkingSystem(big, medium, small) Initializes object of the ParkingSystem class. The number of slots for each parking
space are given as part of the constructor.

bool add_car(car_type) Checks whether there is a parking space of car_type for the car that wants to get into the
parking lot. car_type can be of three kinds: big, medium, or small, which are represented by 1, 2, and 3 respectively.
A car can only park in a parking space of its car_type. If there is no space available, return False, else park the car
in that size space and return True.


Example 1:
parking_system = ParkingSystem(1, 1, 0)
parking_system.add_car(1) # return True because there is 1 available slot for a big car
parking_system.add_car(2) # return True because there is 1 available slot for a medium car
parking_system.add_car(3) # return False because there is no available slot for a small car
parking_system.add_car(1) # return False because there is no available slot for a big car. It is already occupied.
"""


class ParkingSystem(object):
    def __init__(self, big: int, medium: int, small: int):
        self._parking_slots = [None, big, medium, small]

    def add_car(self, car_type: int) -> bool:
        if self._parking_slots[car_type] > 0:
            self._parking_slots[car_type] -= 1
            return True
        return False


if __name__ == "__main__":
    parking_system = ParkingSystem(1, 1, 0)
    assert parking_system.add_car(1) is True
    assert parking_system.add_car(2) is True
    assert parking_system.add_car(3) is False
    assert parking_system.add_car(1) is False
