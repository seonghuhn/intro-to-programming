import datetime
import json

PSALM_119_VERSES = (0, 32, 64, 96, 128, 152, 176)


def load_bible():
    # https://github.com/bkuhl/bible-verse-counts-per-chapter/blob/master/bible.json
    with open('bible.json') as f:
        bible = json.load(f)

    total_verses = 0
    for book in bible:
        for chapter in book['chapters']:
            total_verses += int(chapter['verses'])

    max_verses_per_day = total_verses / 365

    return bible, total_verses, max_verses_per_day


def find_best_carry_delta(bible, total_verses, max_verses_per_day):
    best_min_verses = 0
    best_max_verses = total_verses
    best_delta = 0
    # for delta in range(int(max_verses_per_day / 2)):
    for delta in range(41, 42):
        verses = 0
        carry = 0
        min_verses = total_verses
        max_verses = 0

        for book in bible:
            for chapter in book['chapters']:
                if book['abbr'] == 'Ps' and chapter['chapter'] == '119':
                    for _ in range(2):
                        verses += 88  # first half
                        carry = verses + carry - max_verses_per_day
                        if verses < min_verses:
                            min_verses = verses
                        if verses > max_verses:
                            max_verses = verses
                        verses = 0
                else:
                    verses += int(chapter['verses'])
                    if verses + carry > max_verses_per_day - delta:
                        carry = verses + carry - max_verses_per_day
                        if verses < min_verses:
                            min_verses = verses
                        if verses > max_verses:
                            max_verses = verses
                        verses = 0

        # print(f"delta={delta}, min={min_verses}, max={max_verses}")
        if min_verses >= best_min_verses and max_verses <= best_max_verses:
            best_min_verses = min_verses
            best_max_verses = max_verses
            best_delta = delta

    print(f"best_delta={best_delta}, best_min_verses={best_min_verses}, best_max_verses={best_max_verses}")
    return best_delta


def reading_plan_carry_delta(bible, total_verses, max_verses_per_day, delta):
    now = datetime.datetime.now()
    date = datetime.date(now.year, 1, 1)

    verses = 0
    carry = 0
    start = False
    start_book = 'Gen'
    start_chapter = '1'
    min_verses = total_verses
    max_verses = 0
    for book in bible:
        for chapter in book['chapters']:
            if start:
                start_book = book['abbr']
                start_chapter = chapter['chapter']
                start = False
            if book['abbr'] == 'Ps' and chapter['chapter'] == '119':
                start_verse = 1
                end_verse = 88
                for _ in range(2):
                    verses += 88  # first half
                    carry = verses + carry - max_verses_per_day
                    print(
                        f"{date.strftime('%a %b %d')} {start_book}:{start_chapter}:{start_verse}-{book['abbr']}:{chapter['chapter']}:{end_verse} {verses}")
                    if verses < min_verses:
                        min_verses = verses
                    if verses > max_verses:
                        max_verses = verses
                    date += datetime.timedelta(1)
                    start_verse = end_verse + 1
                    end_verse = start_verse + 87
                    verses = 0
                start = True
            else:
                verses += int(chapter['verses'])
                if verses + carry > max_verses_per_day - delta:
                    print(
                        f"{date.strftime('%a %b %d')} {start_book}:{start_chapter}-{book['abbr']}:{chapter['chapter']} {verses}")
                    carry = verses + carry - max_verses_per_day
                    if verses < min_verses:
                        min_verses = verses
                        print(f"min={min_verses}")
                    if verses > max_verses:
                        max_verses = verses
                        print(f"max={max_verses}")
                    verses = 0
                    start = True
                    date += datetime.timedelta(1)

    print(f"min={min_verses}, max={max_verses}")


def reading_plan_tomorrow(bible, total_verses, max_verses_per_day):
    now = datetime.datetime.now()
    date = datetime.date(now.year + 1, 1, 1)

    verses = 0
    carry = 0
    start = False
    start_book = 'Gen'
    start_chapter = '1'
    min_verses = total_verses
    max_verses = 0
    for i in range(len(bible)):
        book = bible[i]
        if i < len(bible) - 1:
            next_book = bible[i + 1]
        else:
            next_book = book
        for j in range(len(book['chapters'])):
            chapter = book['chapters'][j]
            if j < len(book['chapters']) - 1:
                next_chapter = book['chapters'][j + 1]
            else:
                next_chapter = next_book['chapters'][0]
            if start:
                start_book = book['abbr']
                start_chapter = chapter['chapter']
                start = False
            if book['abbr'] == 'Ps' and chapter['chapter'] == '119':
                start_verse = 1
                end_verse = 88
                for _ in range(2):
                    verses += 88  # first half
                    carry = verses + carry - max_verses_per_day
                    print(
                        f"{date.strftime('%a %b %d')}\t{start_book}:{start_chapter}:{start_verse}-{book['abbr']}:{chapter['chapter']}:{end_verse} {verses}")
                    if verses < min_verses:
                        min_verses = verses
                    if verses > max_verses:
                        max_verses = verses
                    date += datetime.timedelta(1)
                    start_verse = end_verse + 1
                    end_verse = start_verse + 87
                    verses = 0
                start = True
            elif book['abbr'] == 'Ps' and (chapter['chapter'] == '107' or chapter['chapter'] == '114'):
                # exception to end a day's reading on Psalm 107
                verses += int(chapter['verses'])
                carry = verses + carry - max_verses_per_day
                print(
                    f"{date.strftime('%a %b %d')}\t{start_book}:{start_chapter}-{book['abbr']}:{chapter['chapter']} {verses}")
                if verses < min_verses:
                    min_verses = verses
                if verses > max_verses:
                    max_verses = verses
                verses = 0
                start = True
                date += datetime.timedelta(1)
            else:
                verses += int(chapter['verses'])
                verses_tomorrow = verses + int(next_chapter['verses'])
                # print("verses", verses, "verses_tomorrow", verses_tomorrow, "delta",
                #       round(max_verses_per_day - verses, 0), "tomorrow", round(abs(
                #         verses_tomorrow - max_verses_per_day), 0))
                if verses + carry > max_verses_per_day and verses > max_verses_per_day - 20 or (
                        max_verses_per_day - verses < abs(
                    verses_tomorrow - max_verses_per_day) and abs(
                    verses_tomorrow - max_verses_per_day) > abs(carry)):
                    print(
                        f"{date.strftime('%a %b %d')}\t{start_book}:{start_chapter}-{book['abbr']}:{chapter['chapter']} {verses}")
                    carry = verses + carry - max_verses_per_day
                    # print("carry", round(carry, 0))
                    if verses < min_verses:
                        min_verses = verses
                        # print(f"min={min_verses}")
                    if verses > max_verses:
                        max_verses = verses
                        # print(f"max={max_verses}")
                    verses = 0
                    start = True
                    date += datetime.timedelta(1)

    print(f"min={min_verses}, max={max_verses}")


def reading_plan_tomorrow_psalm(bible, total_verses, max_verses_per_day):
    now = datetime.datetime.now()
    date = datetime.date(now.year, 1, 1)

    verses_today = 0
    carry = 0
    start = False
    start_book = 'Gen'
    start_chapter = '1'
    psalm_chapter = 0
    psalm_119_index = 0
    psalms = bible[18]
    min_verses = total_verses
    max_verses = 0
    for i in range(len(bible)):
        book = bible[i]

        # don't read Psalms by themselves
        if book['abbr'] == 'Ps':
            continue

        if i < len(bible) - 1:
            next_book = bible[i + 1]
        else:
            next_book = book

        for j in range(len(book['chapters'])):
            chapter = book['chapters'][j]
            if j < len(book['chapters']) - 1:
                next_chapter = book['chapters'][j + 1]
            else:
                next_chapter = next_book['chapters'][0]
            if start:
                start_book = book['abbr']
                start_chapter = chapter['chapter']
                start = False
            # if book['abbr'] == 'Ps' and chapter['chapter'] == '119':
            #     start_verse = 1
            #     end_verse = 88
            #     for _ in range(2):
            #         verses += 88  # first half
            #         carry = verses + carry - max_verses_per_day
            #         print(
            #             f"{date.strftime('%a %b %d')}\t{start_book}:{start_chapter}:{start_verse}-{book['abbr']}:{chapter['chapter']}:{end_verse} {verses}")
            #         if verses < min_verses:
            #             min_verses = verses
            #         if verses > max_verses:
            #             max_verses = verses
            #         date += datetime.timedelta(1)
            #         start_verse = end_verse + 1
            #         end_verse = start_verse + 87
            #         verses = 0
            #     start = True
            # elif book['abbr'] == 'Ps' and (chapter['chapter'] == '107' or chapter['chapter'] == '114'):
            #     # exception to end a day's reading on Psalm 107
            #     verses += int(chapter['verses'])
            #     carry = verses + carry - max_verses_per_day
            #     print(
            #         f"{date.strftime('%a %b %d')}\t{start_book}:{start_chapter}-{book['abbr']}:{chapter['chapter']} {verses}")
            #     if verses < min_verses:
            #         min_verses = verses
            #     if verses > max_verses:
            #         max_verses = verses
            #     verses = 0
            #     start = True
            #     date += datetime.timedelta(1)
            # else:
            include_psalm = False
            verses_today += int(chapter['verses'])
            verses_tomorrow = verses_today + int(next_chapter['verses'])
            try:
                psalm = psalms['chapters'][psalm_chapter]
                if psalm_chapter == 118:
                    psalm_verses = PSALM_119_VERSES[psalm_119_index + 1] - PSALM_119_VERSES[psalm_119_index]
                else:
                    psalm_verses = int(psalm['verses'])
            except IndexError:
                psalm_verses = 0
            verses_left_to_read = max_verses_per_day - verses_today
            carry_tomorrow = max(0, verses_tomorrow - max_verses_per_day)
            # print("verses", verses_today, "verses_tomorrow", verses_tomorrow, "verses_left_to_read",
            #       round(verses_left_to_read, 0), "carry_tomorrow", round(carry_tomorrow, 0),
            #       "psalm:" + psalm['chapter'], psalm_verses)
            if (psalm_chapter == 77 or 104 <= psalm_chapter <= 106 or psalm_chapter == 118) and carry >= -20:
                # special cases for psalms that are large so include it with one chapter of another book
                start = True
                include_psalm = True
            elif verses_today + carry > max_verses_per_day:
                # if verses + carry > max_verses_per_day and (verses_left_to_read < abs(
                #         verses_tomorrow - max_verses_per_day) and abs(verses_tomorrow - max_verses_per_day) > abs(carry)):
                start = True
                if psalm_verses > 0 and verses_today + psalm_verses < max_verses_per_day:
                    include_psalm = True
            elif max_verses_per_day < verses_today + psalm_verses + carry and verses_today + psalm_verses < verses_tomorrow:
            #elif verses_today < max_verses_per_day < verses_today + psalm_verses + carry and verses_today + psalm_verses < verses_tomorrow:
                start = True
                include_psalm = True
            elif verses_left_to_read < carry_tomorrow:
                start = True
                if psalm_verses > 0 and  verses_today + psalm_verses < max_verses_per_day:
                    include_psalm = True
            # elif 0 < verses_left_to_read - psalm_verses < carry_tomorrow and carry >= -20:
            #     start = True
            #     print("include psalm cond #4")
            #     include_psalm = True

            if start:
                if start_book == book['abbr']:
                    daily_reading = f"{date.strftime('%b %d')}\t{start_book} {start_chapter}-{chapter['chapter']}"
                else:
                    daily_reading = f"{date.strftime('%b %d')}\t{start_book} {start_chapter}-{book['abbr']}:{chapter['chapter']}"
                if include_psalm:
                    verses_today += psalm_verses
                    if psalm_chapter == 118:
                        # psalm 119
                        daily_reading += f", Ps 119:{PSALM_119_VERSES[psalm_119_index] + 1}-"
                        psalm_119_index += 1
                        daily_reading += f"{PSALM_119_VERSES[psalm_119_index]}"
                        if psalm_119_index == len(PSALM_119_VERSES) - 1:
                            # end of psalm 119
                            psalm_chapter += 1
                    else:
                        psalm_chapter += 1
                        daily_reading += f", Ps {psalm_chapter}"
                daily_reading += f" {verses_today}"
                print(daily_reading)

                carry = verses_today + carry - max_verses_per_day
                # print("carry", round(carry, 0))
                # if carry > 10:
                #     print("++++++++++++++ READING TOO MUCH")
                # if carry < -10:
                #     print("-------------- READING TOO LITTLE")
                if verses_today < min_verses:
                    min_verses = verses_today
                    # print(f"min={min_verses}")
                    # if min_verses < 70:
                    #     print("******************* TOO SMALL")
                if verses_today > max_verses:
                    max_verses = verses_today
                    # print(f"max={max_verses}")
                    # if max_verses > 110:
                    #     print("################### TOO BIG")
                verses_today = 0
                date += datetime.timedelta(1)

    # print(f"min={min_verses}, max={max_verses}")


def find_best_daily_min_verses(bible, total_verses, max_verses_per_day):
    best_daily_min_verses = int(max_verses_per_day)
    best_range = 2 * best_daily_min_verses
    for daily_min_verses in range(int(max_verses_per_day), 0, -1):
        verses = 0
        carry = 0
        min_verses = total_verses
        max_verses = 0
        for book in bible:
            for chapter in book['chapters']:
                if book['abbr'] == 'Ps' and chapter['chapter'] == '119':
                    for _ in range(2):
                        verses += 88  # first half
                        carry = verses + carry - max_verses_per_day
                        if verses < min_verses:
                            min_verses = verses
                        if verses > max_verses:
                            max_verses = verses
                        verses = 0
                else:
                    verses += int(chapter['verses'])
                    if verses + carry > max_verses_per_day or verses > daily_min_verses:
                        carry = verses + carry - max_verses_per_day
                        if verses < min_verses:
                            min_verses = verses
                        if verses > max_verses:
                            max_verses = verses
                        verses = 0

        print(f"daily_min_verses={daily_min_verses}, min={min_verses}, max={max_verses}")
        if max_verses - min_verses < best_range:
            best_range = max_verses - min_verses
            best_daily_min_verses = daily_min_verses

    print(f"best_daily_min_verses={best_daily_min_verses}, best_range={best_range}")
    return best_daily_min_verses


def reading_plan_daily_min_verses(bible, total_verses, max_verses_per_day, daily_min_verses):
    now = datetime.datetime.now()
    date = datetime.date(now.year + 1, 1, 1)

    verses = 0
    carry = 0
    start = False
    start_book = 'Gen'
    start_chapter = '1'
    min_verses = total_verses
    max_verses = 0
    for book in bible:
        for chapter in book['chapters']:
            if start:
                start_book = book['abbr']
                start_chapter = chapter['chapter']
                start = False
            if book['abbr'] == 'Ps' and chapter['chapter'] == '119':
                start_verse = 1
                end_verse = 88
                for _ in range(2):
                    verses += 88  # first half
                    carry = verses + carry - max_verses_per_day
                    print(
                        f"{date.strftime('%a %b %d')} {start_book}:{start_chapter}:{start_verse}-{book['abbr']}:{chapter['chapter']}:{end_verse} {verses}")
                    if verses < min_verses:
                        min_verses = verses
                    if verses > max_verses:
                        max_verses = verses
                    date += datetime.timedelta(1)
                    start_verse = end_verse + 1
                    end_verse = start_verse + 87
                    verses = 0
                start = True
            else:
                verses += int(chapter['verses'])
                if verses + carry > max_verses_per_day or verses > daily_min_verses:
                    print(
                        f"{date.strftime('%a %b %d')} {start_book}:{start_chapter}-{book['abbr']}:{chapter['chapter']} {verses}")
                    carry = verses + carry - max_verses_per_day
                    # print("carry", round(carry, 0))
                    if verses < min_verses:
                        min_verses = verses
                        # print(f"min={min_verses}")
                    if verses > max_verses:
                        max_verses = verses
                        # print(f"max={max_verses}")
                    verses = 0
                    start = True
                    date += datetime.timedelta(1)

    print(f"daily_min_verses={daily_min_verses}, min={min_verses}, max={max_verses}")


def reading_plan():
    bible, total_verses, max_verses_per_day = load_bible()
    # print(max_verses_per_day)

    # best_delta=41, best_min_verses=26, best_max_verses=153
    # best_delta = find_best_carry_delta(bible, total_verses, max_verses_per_day)
    # reading_plan_carry_delta(bible, total_verses, max_verses_per_day, best_delta)

    # min=50, max=121
    # reading_plan_tomorrow(bible, total_verses, max_verses_per_day)

    # min=55, max=125
    reading_plan_tomorrow_psalm(bible, total_verses, max_verses_per_day)

    # daily_min_verses=57, min=58, max=121
    # best_daily_min_verses = find_best_daily_min_verses(bible, total_verses, max_verses_per_day)
    # reading_plan_daily_min_verses(bible, total_verses, max_verses_per_day, best_daily_min_verses)


if __name__ == "__main__":
    reading_plan()
