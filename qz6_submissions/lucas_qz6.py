class Matrix(object):

    def __init__(self, rows, cols):
        self._matrix = [[0 for num in range(cols)] for num in range(rows)]

    def __getitem__(self, row):
        return self._matrix[row]

    def find(self, num):
        if len([row for row in self._matrix if num in row]) > 0:
            return True
        return False

    def map(self, f):
        self._matrix = [[f(num) for num in row] for row in self._matrix]

    def transpose(self):
        new_matrix = [[] for x in self._matrix]
        for x in self._matrix:
            idx = 0
            while idx < len(x):
                new_matrix[idx].append(x[idx])
                idx += 1
        self._matrix = new_matrix

    def add(self, other):
        new_matrix = [[x + y for x, y in zip(rowx, rowy)] for rowx, rowy in zip(self._matrix, other._matrix)]
        return new_matrix

    def row_by_column(self, row, col):
        sum = 0
        for x, y in zip(row, col):
            sum += (x * y)
        return sum

    def multiply(self, other):
        new_matrix = [[self.row_by_column(row, [other_row[col_idx] for other_row in other._matrix]) for col_idx in
                       range(len(row) - 1)] for row in self._matrix]
        return new_matrix


def init():
    """
    Creates a matrix with 40 at (0, 0), 7 at (3, 2), 0 everywhere else.
    """
    matrix = Matrix(4, 3)
    for row in range(4):
        for col in range(3):
            assert matrix[row][col] == 0
    matrix[0][0] = 40
    matrix[3][2] = 7
    return matrix


def test():
    # initialize
    matrix = init()
    assert matrix[0][0] == 40
    assert matrix[0][1] == 0
    assert matrix[0][2] == 0
    assert matrix[1][0] == 0
    assert matrix[1][1] == 0
    assert matrix[1][2] == 0
    assert matrix[2][0] == 0
    assert matrix[2][1] == 0
    assert matrix[2][2] == 0
    assert matrix[3][0] == 0
    assert matrix[3][1] == 0
    assert matrix[3][2] == 7

    # test find
    assert matrix.find(0)
    assert matrix.find(-1) is False
    assert matrix.find(40)
    assert matrix.find(7)
    assert matrix.find(6) is False

    # test map
    matrix.map(lambda x: x + 1)
    assert matrix[0][0] == 41
    assert matrix[0][1] == 1
    assert matrix[0][2] == 1
    assert matrix[1][0] == 1
    assert matrix[1][1] == 1
    assert matrix[1][2] == 1
    assert matrix[2][0] == 1
    assert matrix[2][1] == 1
    assert matrix[2][2] == 1
    assert matrix[3][0] == 1
    assert matrix[3][1] == 1
    assert matrix[3][2] == 8

    # test transpose
    matrix = init()
    matrix[1][1] = 2
    matrix[1][2] = 3
    matrix[2][1] = 4
    matrix.transpose()
    assert matrix[0][0] == 40
    assert matrix[0][1] == 0
    assert matrix[0][2] == 0
    assert matrix[0][3] == 0
    assert matrix[1][0] == 0
    assert matrix[1][1] == 2
    assert matrix[1][2] == 4
    assert matrix[1][3] == 0
    assert matrix[2][0] == 0
    assert matrix[2][1] == 3
    assert matrix[2][2] == 0
    assert matrix[2][3] == 7

    # test add
    matrix_1 = Matrix(2, 2)
    matrix_1[0][0] = 3
    matrix_1[0][1] = 8
    matrix_1[1][0] = 4
    matrix_1[1][1] = 6
    matrix_2 = Matrix(2, 2)
    matrix_2[0][0] = 4
    matrix_2[0][1] = 0
    matrix_2[1][0] = 1
    matrix_2[1][1] = -9
    matrix_3 = matrix_1.add(matrix_2)
    assert matrix_3[0][0] == 7
    assert matrix_3[0][1] == 8
    assert matrix_3[1][0] == 5
    assert matrix_3[1][1] == -3

    # test multiply
    matrix_1 = Matrix(2, 3)
    num = 1
    for yy in range(2):
        for xx in range(3):
            matrix_1[yy][xx] = num
            num += 1
    matrix_2 = Matrix(3, 2)
    for yy in range(3):
        for xx in range(2):
            matrix_2[yy][xx] = num
            num += 1
    matrix_3 = matrix_1.multiply(matrix_2)
    assert matrix_3[0][0] == 58
    assert matrix_3[0][1] == 64
    assert matrix_3[1][0] == 139
    assert matrix_3[1][1] == 154


if __name__ == '__main__':
    test()
