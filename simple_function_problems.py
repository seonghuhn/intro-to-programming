def maximum(n1, n2=0, n3=0):
    """
    Returns the maximum of the three numbers.
    :param n1: the first number
    :param n2: the second number
    :param n3: the third number
    :return: the maximum
    """
    if n1 >= n2 >= n3:
        return n1
    if n2 >= n3:
        return n2
    return n3


def factorial(n):
    """
    Returns the factorial for the number.
    n! = n * (n-1) * (n-2) * (n-3) ... 3 * 2 * 1.
    Note the factorial of 0 is 1.
    :param n: the integer number greater than or equal to zero
    :return: the factorial of the number
    """
    fact = 1
    for i in range(2, n + 1):
        fact *= i

    return fact


def fibonacci(n):
    """
    Returns the nth Fibonacci number.
    The Fibonacci sequence is a series of numbers where a number is the addition of the last two numbers, starting with
    0 and 1. Written as a rule: Fn = Fn-1 + Fn-2.
    :param n: the index of the Fibonacci number requested, starting at 0
    :return: the nth Fibonacci number
    """
    if n == 1:
        return 0
    if n == 2:
        return 1

    # n = 3
    fib_0 = 0
    fib_1 = 1
    fib = fib_0 + fib_1

    # n = 4...n
    for i in range(4, n + 1):
        fib_0 = fib_1
        fib_1 = fib
        fib = fib_0 + fib_1

    return fib
