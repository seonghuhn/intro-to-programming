from termcolor import colored
from utils import get_current_temp, get_current_air_quality


def conv_fahrenheit_celsius(temp):
    """
    Converts temperature in Fahrenheit to Celsius.
    :param temp: the temperature in Fahrenheit
    :return: the temperature in Celsius rounded to two decimal places
    """
    return round((temp - 32) * 5 / 9, 2)


def air_quality_level(aqi):
    """
    Determines the AQI level based on the AQI.
    :param aqi: the air quality index
    :return: the AQI level based on https://www.airnow.gov/aqi/aqi-basics/
    """
    if aqi > 300:
        level = "Hazardous"
    elif aqi > 200:
        level = "Very Unhealthy"
    elif aqi > 150:
        level = "Unhealthy"
    elif aqi > 100:
        level = "Unhealthy for Sensitive Groups"
    elif aqi > 50:
        level = "Moderate"
    else:
        level = "Good"
    return level


def air_quality_level_color(aqi):
    """
    Determines the AQI level color based on the AQI.
    :param aqi: the air quality index
    :return: the AQI level color based on https://www.airnow.gov/aqi/aqi-basics/
    """
    if aqi > 300:
        color = "cyan"
    elif aqi > 200:
        color = "magenta"
    elif aqi > 100:
        color = "red"
    elif aqi > 50:
        color = "yellow"
    else:
        color = "green"
    return color


def main():
    city = input("City [Santa Clara]? ")
    if not city:
        city = "Santa Clara"
    state = input("State [California]? ")
    temp_f = get_current_temp(city)
    temp_c = conv_fahrenheit_celsius(temp_f)
    if not state:
        aqi = get_current_air_quality(city)
    else:
        aqi = get_current_air_quality(city, state)
    level = air_quality_level(aqi)
    print(f"Current temperature in Fahrenheit {temp_f}")
    print(f"Current temperature in Celsius {temp_c}")
    level_colored = colored(level, air_quality_level_color(aqi))
    print(f"Air Quality Index {aqi} {level_colored}")


if __name__ == "__main__":
    main()
