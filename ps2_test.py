import jedi_mind_trick
from ps2_solutions import ps2_john_paul_sepka, ps2_joseph_shen, ps2_julia_kehl, ps2_lorenzo_caruso, ps2_sean_deboard


def test_jedi_mind_trick():
    assert jedi_mind_trick.jedi_mind_trick(
        "Jedi") == "You will remove these restraints and leave the cell with the door open."
    assert jedi_mind_trick.jedi_mind_trick("Droids") == "These aren't the droids you're looking for."
    assert jedi_mind_trick.jedi_mind_trick("Obi-Wan") == "It's okay that we're here."
    assert jedi_mind_trick.jedi_mind_trick("Rebels") == "You want to go home and rethink your life."
    assert jedi_mind_trick.jedi_mind_trick("Rey") == "I find your lack of faith disturbing."

    assert jedi_mind_trick.jedi_mind_trick(
        "Jedi scum") == "You will remove these restraints and leave the cell with the door open."
    assert jedi_mind_trick.jedi_mind_trick("Droids like R2D2") == "These aren't the droids you're looking for."
    assert jedi_mind_trick.jedi_mind_trick("Old Obi-Wan") == "It's okay that we're here."
    assert jedi_mind_trick.jedi_mind_trick("Rebels scum") == "You want to go home and rethink your life."
    assert jedi_mind_trick.jedi_mind_trick("Rey Skywalker") == "I find your lack of faith disturbing."

    assert jedi_mind_trick.jedi_mind_trick(
        "jedi") == "You will remove these restraints and leave the cell with the door open."
    assert jedi_mind_trick.jedi_mind_trick("droids") == "These aren't the droids you're looking for."
    assert jedi_mind_trick.jedi_mind_trick("obi-wan") == "It's okay that we're here."
    assert jedi_mind_trick.jedi_mind_trick("rebels") == "You want to go home and rethink your life."
    assert jedi_mind_trick.jedi_mind_trick("rey") == "I find your lack of faith disturbing."

    assert jedi_mind_trick.jedi_mind_trick(
        "jedi masters") == "You will remove these restraints and leave the cell with the door open."
    assert jedi_mind_trick.jedi_mind_trick("droids like C3PO") == "These aren't the droids you're looking for."
    assert jedi_mind_trick.jedi_mind_trick("obi-wan kenobi ") == "It's okay that we're here."
    assert jedi_mind_trick.jedi_mind_trick("rebels so we can join them") == "You want to go home and rethink your life."
    assert jedi_mind_trick.jedi_mind_trick("rey the scavenger") == "I find your lack of faith disturbing."

    # john_paul_sepka
    # assert ps2_john_paul_sepka.jedi_mind_trick(
    #     "Jedi") == "You will remove these restraints and leave the cell with the door open."
    # assert ps2_john_paul_sepka.jedi_mind_trick("Droids") == "These aren't the droids you're looking for."
    # assert ps2_john_paul_sepka.jedi_mind_trick("Obi-Wan") == "It's okay that we're here."
    # assert ps2_john_paul_sepka.jedi_mind_trick("Rebels") == "You want to go home and rethink your life."
    # assert ps2_john_paul_sepka.jedi_mind_trick("Rey") == "I find your lack of faith disturbing."

    # assert ps2_john_paul_sepka.jedi_mind_trick(
    #     "Jedi scum") == "You will remove these restraints and leave the cell with the door open."
    # assert ps2_john_paul_sepka.jedi_mind_trick("Droids like R2D2") == "These aren't the droids you're looking for."
    # assert ps2_john_paul_sepka.jedi_mind_trick("Old Obi-Wan") == "It's okay that we're here."
    # assert ps2_john_paul_sepka.jedi_mind_trick("Rebels scum") == "You want to go home and rethink your life."
    # assert ps2_john_paul_sepka.jedi_mind_trick("Rey Skywalker") == "I find your lack of faith disturbing."

    assert ps2_john_paul_sepka.jedi_mind_trick(
        "jedi") == "You will remove these restraints and leave the cell with the door open."
    assert ps2_john_paul_sepka.jedi_mind_trick("droids") == "These aren't the droids you're looking for."
    assert ps2_john_paul_sepka.jedi_mind_trick("obi-wan") == "It's okay that we're here."
    assert ps2_john_paul_sepka.jedi_mind_trick("rebels") == "You want to go home and rethink your life."
    assert ps2_john_paul_sepka.jedi_mind_trick("rey") == "I find your lack of faith disturbing."

    assert ps2_john_paul_sepka.jedi_mind_trick(
        "jedi masters") == "You will remove these restraints and leave the cell with the door open."
    assert ps2_john_paul_sepka.jedi_mind_trick("droids like C3PO") == "These aren't the droids you're looking for."
    assert ps2_john_paul_sepka.jedi_mind_trick("obi-wan kenobi ") == "It's okay that we're here."
    assert ps2_john_paul_sepka.jedi_mind_trick(
        "rebels so we can join them") == "You want to go home and rethink your life."
    assert ps2_john_paul_sepka.jedi_mind_trick("rey the scavenger") == "I find your lack of faith disturbing."

    # joseph_shen
    assert ps2_joseph_shen.jedi_mind_trick(
        "Jedi") == "You will remove these restraints and leave the cell with the door open."
    assert ps2_joseph_shen.jedi_mind_trick("Droids") == "These aren't the droids you're looking for."
    assert ps2_joseph_shen.jedi_mind_trick("Obi-Wan") == "It's okay that we're here."
    assert ps2_joseph_shen.jedi_mind_trick("Rebels") == "You want to go home and rethink your life."
    assert ps2_joseph_shen.jedi_mind_trick("Rey") == "I find your lack of faith disturbing."

    assert ps2_joseph_shen.jedi_mind_trick(
        "Jedi scum") == "You will remove these restraints and leave the cell with the door open."
    assert ps2_joseph_shen.jedi_mind_trick("Droids like R2D2") == "These aren't the droids you're looking for."
    assert ps2_joseph_shen.jedi_mind_trick("Old Obi-Wan") == "It's okay that we're here."
    assert ps2_joseph_shen.jedi_mind_trick("Rebels scum") == "You want to go home and rethink your life."
    assert ps2_joseph_shen.jedi_mind_trick("Rey Skywalker") == "I find your lack of faith disturbing."

    assert ps2_joseph_shen.jedi_mind_trick(
        "jedi") == "You will remove these restraints and leave the cell with the door open."
    assert ps2_joseph_shen.jedi_mind_trick("droids") == "These aren't the droids you're looking for."
    assert ps2_joseph_shen.jedi_mind_trick("obi-wan") == "It's okay that we're here."
    assert ps2_joseph_shen.jedi_mind_trick("rebels") == "You want to go home and rethink your life."
    assert ps2_joseph_shen.jedi_mind_trick("rey") == "I find your lack of faith disturbing."

    assert ps2_joseph_shen.jedi_mind_trick(
        "jedi masters") == "You will remove these restraints and leave the cell with the door open."
    assert ps2_joseph_shen.jedi_mind_trick("droids like C3PO") == "These aren't the droids you're looking for."
    assert ps2_joseph_shen.jedi_mind_trick("obi-wan kenobi ") == "It's okay that we're here."
    assert ps2_joseph_shen.jedi_mind_trick(
        "rebels so we can join them") == "You want to go home and rethink your life."
    assert ps2_joseph_shen.jedi_mind_trick("rey the scavenger") == "I find your lack of faith disturbing."

    # julia_kehl
    assert ps2_julia_kehl.jedi_mind_trick(
        "Jedi") == "You will remove these restraints and leave the cell with the door open."
    assert ps2_julia_kehl.jedi_mind_trick("Droids") == "These aren't the droids you're looking for."
    assert ps2_julia_kehl.jedi_mind_trick("Obi-Wan") == "It's okay that we're here."
    assert ps2_julia_kehl.jedi_mind_trick("Rebels") == "You want to go home and rethink your life."
    assert ps2_julia_kehl.jedi_mind_trick("Rey") == "I find your lack of faith disturbing."

    assert ps2_julia_kehl.jedi_mind_trick(
        "Jedi scum") == "You will remove these restraints and leave the cell with the door open."
    assert ps2_julia_kehl.jedi_mind_trick("Droids like R2D2") == "These aren't the droids you're looking for."
    assert ps2_julia_kehl.jedi_mind_trick("Old Obi-Wan") == "It's okay that we're here."
    assert ps2_julia_kehl.jedi_mind_trick("Rebels scum") == "You want to go home and rethink your life."
    assert ps2_julia_kehl.jedi_mind_trick("Rey Skywalker") == "I find your lack of faith disturbing."

    assert ps2_julia_kehl.jedi_mind_trick(
        "jedi") == "You will remove these restraints and leave the cell with the door open."
    assert ps2_julia_kehl.jedi_mind_trick("droids") == "These aren't the droids you're looking for."
    assert ps2_julia_kehl.jedi_mind_trick("obi-wan") == "It's okay that we're here."
    assert ps2_julia_kehl.jedi_mind_trick("rebels") == "You want to go home and rethink your life."
    assert ps2_julia_kehl.jedi_mind_trick("rey") == "I find your lack of faith disturbing."

    assert ps2_julia_kehl.jedi_mind_trick(
        "jedi masters") == "You will remove these restraints and leave the cell with the door open."
    assert ps2_julia_kehl.jedi_mind_trick("droids like C3PO") == "These aren't the droids you're looking for."
    assert ps2_julia_kehl.jedi_mind_trick("obi-wan kenobi ") == "It's okay that we're here."
    assert ps2_julia_kehl.jedi_mind_trick(
        "rebels so we can join them") == "You want to go home and rethink your life."
    assert ps2_julia_kehl.jedi_mind_trick("rey the scavenger") == "I find your lack of faith disturbing."

    # lorenzo_caruso
    assert ps2_lorenzo_caruso.jedi_mind_trick(
        "Jedi") == "You will remove these restraints and leave the cell with the door open."
    assert ps2_lorenzo_caruso.jedi_mind_trick("Droids") == "These aren't the droids you're looking for."
    assert ps2_lorenzo_caruso.jedi_mind_trick("Obi-Wan") == "It's okay that we're here."
    assert ps2_lorenzo_caruso.jedi_mind_trick("Rebels") == "You want to go home and rethink your life."
    assert ps2_lorenzo_caruso.jedi_mind_trick("Rey") == "I find your lack of faith disturbing."

    assert ps2_lorenzo_caruso.jedi_mind_trick(
        "Jedi scum") == "You will remove these restraints and leave the cell with the door open."
    assert ps2_lorenzo_caruso.jedi_mind_trick("Droids like R2D2") == "These aren't the droids you're looking for."
    assert ps2_lorenzo_caruso.jedi_mind_trick("Old Obi-Wan") == "It's okay that we're here."
    assert ps2_lorenzo_caruso.jedi_mind_trick("Rebels scum") == "You want to go home and rethink your life."
    assert ps2_lorenzo_caruso.jedi_mind_trick("Rey Skywalker") == "I find your lack of faith disturbing."

    assert ps2_lorenzo_caruso.jedi_mind_trick(
        "jedi") == "You will remove these restraints and leave the cell with the door open."
    assert ps2_lorenzo_caruso.jedi_mind_trick("droids") == "These aren't the droids you're looking for."
    assert ps2_lorenzo_caruso.jedi_mind_trick("obi-wan") == "It's okay that we're here."
    assert ps2_lorenzo_caruso.jedi_mind_trick("rebels") == "You want to go home and rethink your life."
    assert ps2_lorenzo_caruso.jedi_mind_trick("rey") == "I find your lack of faith disturbing."

    assert ps2_lorenzo_caruso.jedi_mind_trick(
        "jedi masters") == "You will remove these restraints and leave the cell with the door open."
    assert ps2_lorenzo_caruso.jedi_mind_trick("droids like C3PO") == "These aren't the droids you're looking for."
    assert ps2_lorenzo_caruso.jedi_mind_trick("obi-wan kenobi ") == "It's okay that we're here."
    assert ps2_lorenzo_caruso.jedi_mind_trick(
        "rebels so we can join them") == "You want to go home and rethink your life."
    assert ps2_lorenzo_caruso.jedi_mind_trick("rey the scavenger") == "I find your lack of faith disturbing."

    # sean_deboard
    assert ps2_sean_deboard.jedi_mind_trick(
        "Jedi") == "You will remove these restraints and leave the cell with the door open."
    assert ps2_sean_deboard.jedi_mind_trick("Droids") == "These aren't the droids you're looking for."
    assert ps2_sean_deboard.jedi_mind_trick("Obi-Wan") == "It's okay that we're here."
    assert ps2_sean_deboard.jedi_mind_trick("Rebels") == "You want to go home and rethink your life."
    assert ps2_sean_deboard.jedi_mind_trick("Rey") == "I find your lack of faith disturbing."

    assert ps2_sean_deboard.jedi_mind_trick(
        "Jedi scum") == "You will remove these restraints and leave the cell with the door open."
    assert ps2_sean_deboard.jedi_mind_trick("Droids like R2D2") == "These aren't the droids you're looking for."
    assert ps2_sean_deboard.jedi_mind_trick("Old Obi-Wan") == "It's okay that we're here."
    assert ps2_sean_deboard.jedi_mind_trick("Rebels scum") == "You want to go home and rethink your life."
    assert ps2_sean_deboard.jedi_mind_trick("Rey Skywalker") == "I find your lack of faith disturbing."

    assert ps2_sean_deboard.jedi_mind_trick(
        "jedi") == "You will remove these restraints and leave the cell with the door open."
    assert ps2_sean_deboard.jedi_mind_trick("droids") == "These aren't the droids you're looking for."
    assert ps2_sean_deboard.jedi_mind_trick("obi-wan") == "It's okay that we're here."
    assert ps2_sean_deboard.jedi_mind_trick("rebels") == "You want to go home and rethink your life."
    assert ps2_sean_deboard.jedi_mind_trick("rey") == "I find your lack of faith disturbing."

    assert ps2_sean_deboard.jedi_mind_trick(
        "jedi masters") == "You will remove these restraints and leave the cell with the door open."
    assert ps2_sean_deboard.jedi_mind_trick("droids like C3PO") == "These aren't the droids you're looking for."
    assert ps2_sean_deboard.jedi_mind_trick("obi-wan kenobi ") == "It's okay that we're here."
    assert ps2_sean_deboard.jedi_mind_trick(
        "rebels so we can join them") == "You want to go home and rethink your life."
    assert ps2_sean_deboard.jedi_mind_trick("rey the scavenger") == "I find your lack of faith disturbing."
