def create_set(a_list):
    """
    Creates a set from the given list.
    """
    end_set = set()
    for x in a_list:
        end_set.add(x)
    return a_list


def contains_duplicate(nums):
    """
    Given an array of integers, find if the array contains any duplicates.

    Your function should return true if any value appears at least twice in the array, and it should return false if
    every element is distinct.
    """
    set_nums = set(nums)
    if len(set_nums) == len(nums):
        return False
    return True


def single_number(nums):
    """
    Given a non-empty array of integers nums, every element appears twice except for one. Find that single one.
    """


if __name__ == '__main__':
    assert create_set([]) == set()
    assert create_set([1, 1, 1]) == {1}
    assert create_set([1, 2, 3, 4, 5]) == {1, 2, 3, 4, 5}
    print("\033[0;34mCreate Set works\t\t\t\t\t\033[0;33m3 pts")

    assert contains_duplicate([1, 2, 3, 1]) is True
    assert contains_duplicate([1, 2, 3, 4]) is False
    assert contains_duplicate([1, 1, 1, 3, 3, 4, 3, 2, 4, 2]) is True
    assert contains_duplicate([1, 2, 7, 3, 4, 43, 17, 256, -1]) is False
    print("\033[0;35mContains Duplicate works\t\t\t\033[0;33m5 pts")
