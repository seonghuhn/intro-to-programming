a_list = (1, 1, 2, 3)


def create_set(a_list):
    the_set = set()
    for c in a_list:
        if c not in the_set:
            the_set.add(c)
    return (the_set)


# print(create_set(a_list))

nums = (1, 1, 2, 3)


def contains_duplicate(nums):
    return len(nums) != len(set(nums))


# print(contains_duplicate(nums))

nums = (1, 1, 2, 2, 3, 3, 4, 5, 7, 7, 5)


def single_number(nums):
    seen_numbers = set()
    pop_numbers = []
    for x in nums:
        if x not in seen_numbers:
            seen_numbers.add(x)
        else:
            pop_numbers.append(x)
    return set(nums) - set(pop_numbers)  # WOW
    # for v in nums:
    #   if v not in pop_numbers:
    #      return v


# print(single_number(nums))

if __name__ == '__main__':
    assert create_set([]) == set()
    assert create_set([1, 1, 1]) == {1}
    assert create_set([1, 2, 3, 4, 5]) == {1, 2, 3, 4, 5}
    print("\033[0;34mCreate Set works\t\t\t\t\t\033[0;33m3 pts")

    assert contains_duplicate([1, 2, 3, 1]) is True
    assert contains_duplicate([1, 2, 3, 4]) is False
    assert contains_duplicate([1, 1, 1, 3, 3, 4, 3, 2, 4, 2]) is True
    assert contains_duplicate([1, 2, 7, 3, 4, 43, 17, 256, -1]) is False
    print("\033[0;35mContains Duplicate works\t\t\t\033[0;33m5 pts")

    assert (single_number([2, 2, 1])) == 1
    assert (single_number([4, 1, 2, 1, 2])) == 4
    assert (single_number([1])) == 1
    print("\033[0;36mSingle Number works\t\t\t\t\t\033[0;33m5 pts")
