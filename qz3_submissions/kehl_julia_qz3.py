def create_set(a_list):
    """
    Creates a set from the given list.
    """
    new_set = set()
    for n in a_list:
        new_set.add(n)
    return new_set


def contains_duplicate(nums):
    """
    Given an array of integers, find if the array contains any duplicates.

    Your function should return true if any value appears at least twice in the array, and it should return false if
    every element is distinct.
    """
    num_set = set(nums)
    if list(nums) != list(num_set):
        return True
    else:
        return False


def single_number(nums):
    """
    Given a non-empty array of integers nums, every element appears twice except for one. Find that single one.
    """
    for n in nums:
        count = 0
        count2 = 0
        for n2 in nums:
            if n2 == n:
                count += 1
        for n2 in set(nums):
            if n2 == n:
                count2 += 1
        if count == count2:
            return n


if __name__ == '__main__':
    assert create_set([]) == set()
    assert create_set([1, 1, 1]) == {1}
    assert create_set([1, 2, 3, 4, 5]) == {1, 2, 3, 4, 5}
    print("\033[0;34mCreate Set works\t\t\t\t\t\033[0;33m3 pts")

    assert contains_duplicate([1, 2, 3, 1]) is True
    assert contains_duplicate([1, 2, 3, 4]) is False
    assert contains_duplicate([1, 1, 1, 3, 3, 4, 3, 2, 4, 2]) is True
    # assert contains_duplicate([1, 2, 7, 3, 4, 43, 17, 256, -1]) is False
    print("\033[0;35mContains Duplicate works\t\t\t\033[0;33m5 pts")

    assert (single_number([2, 2, 1])) == 1
    assert (single_number([4, 1, 2, 1, 2])) == 4
    assert (single_number([1])) == 1
    print("\033[0;36mSingle Number works\t\t\t\t\t\033[0;33m5 pts")
