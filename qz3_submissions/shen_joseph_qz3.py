from typing import Union


def create_set(a_list: Union[list, tuple]) -> set:
    """
    Creates a set from the given list.
    """
    a_set = set()
    for element in a_list:
        a_set.add(element)
    return a_set


def contains_duplicate(nums: Union[list, tuple]) -> bool:
    """
    Given an array of integers, find if the array contains any duplicates.

    Your function should return true if any value appears at least twice in the array, and it should return false if
    every element is distinct.
    """
    return len(nums) > len(set(nums))


def single_number(nums: Union[list, tuple]) -> int:
    """
    Given a non-empty array of integers nums, every element appears twice except for one. Find that single one.
    """
    for num in nums:
        if nums.count(num) == 1:
            return num


def main():
    assert create_set([]) == set()
    assert create_set([1, 1, 1]) == {1}
    assert create_set([1, 2, 3, 4, 5]) == {1, 2, 3, 4, 5}
    assert create_set(("apple", "cat", "dog")) == {"apple", "cat", "dog"}
    assert create_set([1, 1, 2, 3]) == {1, 2, 3}
    print("\033[0;34mCreate Set works\t\t\t\t\t\033[0;33m3 pts")

    assert contains_duplicate([1, 2, 3, 1]) is True
    assert contains_duplicate([1, 2, 3, 4]) is False
    assert contains_duplicate([1, 1, 1, 3, 3, 4, 3, 2, 4, 2]) is True
    assert contains_duplicate([1, 2, 7, 3, 4, 43, 17, 256, -1]) is False
    assert contains_duplicate((1, 2, 5)) is False
    assert contains_duplicate([1, 1, 2, 3]) is True
    print("\033[0;35mContains Duplicate works\t\t\t\033[0;33m5 pts")

    assert (single_number([2, 2, 1])) == 1
    assert (single_number([4, 1, 2, 1, 2])) == 4
    assert (single_number([1])) == 1
    print("\033[0;36mSingle Number works\t\t\t\t\t\033[0;33m5 pts")


if __name__ == "__main__":
    main()
