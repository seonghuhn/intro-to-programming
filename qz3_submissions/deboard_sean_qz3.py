def create_set(a_list):
    set_of_list = set()
    for x in a_list:
        set_of_list.add(x)
    return set_of_list


def contains_duplicate(nums):
    if len(create_set(nums)) < len(nums):
        return True
    return False


def single_number(nums):
    len_of_num = 0
    new_set = set()
    for c in nums:
        new_set.add(c)
        len_of_num += 1
        if len(new_set) != len_of_num:
            new_set.remove(c)
            len_of_num -= 2
    new_list = list(new_set)
    return new_list[0]


if __name__ == '__main__':
    assert create_set([]) == set()
    assert create_set([1, 1, 1]) == {1}
    assert create_set([1, 2, 3, 4, 5]) == {1, 2, 3, 4, 5}
    print("\033[0;34mCreate Set works\t\t\t\t\t\033[0;33m3 pts")

    assert contains_duplicate([1, 2, 3, 1]) is True
    assert contains_duplicate([1, 2, 3, 4]) is False
    assert contains_duplicate([1, 1, 1, 3, 3, 4, 3, 2, 4, 2]) is True
    assert contains_duplicate([1, 2, 7, 3, 4, 43, 17, 256, -1]) is False
    print("\033[0;35mContains Duplicate works\t\t\t\033[0;33m5 pts")

    assert (single_number([2, 2, 1])) == 1
    assert (single_number([4, 1, 2, 1, 2])) == 4
    assert (single_number([1])) == 1
    print("\033[0;36mSingle Number works\t\t\t\t\t\033[0;33m5 pts")
