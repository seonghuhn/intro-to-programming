def test():
    f = set()
    set1 = f.add({'Tumorrow'})
    return set1


def create_set(a_list):
    """
    Creates a set from the given list.
    """

    return set(a_list)


def contains_duplicate(nums):
    """
    Given an array of integers, find if the array contains any duplicates.

    Your function should return true if any value appears at least twice in the array, and it should return false if
    every element is distinct.
    """
    seen = set()
    seen_add = seen.add
    seen_twice = set(x for x in nums if x in seen or seen_add(x))  # =)
    if len(seen_twice) > 0:
        return True
    else:
        return False


def single_number(nums):
    """
    Given a non-empty array of integers nums, every element appears twice except for one. Find that single one.
    """
    seen = set()
    seen_add = seen.add
    seen_twice = set(x for x in nums if x in seen or seen_add(x))  # =)
    for i in seen_twice:
        seen.remove(i)
    return seen


if __name__ == '__main__':
    # print(test)
    # print(contains_duplicate((1, 2, 2, 3)))
    # assert create_set(("apple", "cat", "dog")) == {"apple", "cat", "dog"}
    # assert create_set([1, 1, 2, 3]) == {1, 2, 3}
    # print('Create set works')
    # print(single_number((1, 2, 2, 3, 3)))

    assert create_set([]) == set()
    assert create_set([1, 1, 1]) == {1}
    assert create_set([1, 2, 3, 4, 5]) == {1, 2, 3, 4, 5}
    print("\033[0;34mCreate Set works\t\t\t\t\t\033[0;33m3 pts")

    assert contains_duplicate([1, 2, 3, 1]) is True
    assert contains_duplicate([1, 2, 3, 4]) is False
    assert contains_duplicate([1, 1, 1, 3, 3, 4, 3, 2, 4, 2]) is True
    assert contains_duplicate([1, 2, 7, 3, 4, 43, 17, 256, -1]) is False
    print("\033[0;35mContains Duplicate works\t\t\t\033[0;33m5 pts")

    assert (single_number([2, 2, 1])) == 1
    assert (single_number([4, 1, 2, 1, 2])) == 4
    assert (single_number([1])) == 1
    print("\033[0;36mSingle Number works\t\t\t\t\t\033[0;33m5 pts")
