import requests


def get_current_temp(city, country="US", dry_run=False):
    """
    Gets the current temperature for the given city using the Open Weather Map API.
    See https://rapidapi.com/community/api/open-weather-map/endpoints.
    :param city: the name of the city, e.g. "Santa Clara"
    :param country: the two character abbreviation for the country, e.g. "US". This is an optional argument and
    defaults to "US".
    :param dry_run: if True then return a test temperature instead of making an HTTP request
    :return: the temperature in imperial units, e.g. 75.2
    """
    if dry_run:
        # https://www.washingtonpost.com/weather/2020/08/16/death-valley-heat-record/
        return 130.0

    open_weather_map_api_url = "https://community-open-weather-map.p.rapidapi.com/weather"

    headers = {
        "x-rapidapi-host": "community-open-weather-map.p.rapidapi.com",
        "x-rapidapi-key": "7a485ba20amsh2112acb3b5874f1p1309efjsn4c6b5554d3aa"
    }

    querystring = {"units": "imperial", "q": f"{city}, {country}"}

    response = requests.request("GET", open_weather_map_api_url, headers=headers, params=querystring)
    # print(response.text)

    if response.json().get("cod") != 200:
        raise ValueError(f"{city}, {country} is invalid")

    return response.json()["main"]["temp"]


def get_current_air_quality(city, state="California", country="USA", dry_run=False):
    """
    Gets the current temperature for the given city using the Air Visual API.
    See https://api-docs.iqair.com/?version=latest.
    :param city: the name of the city, e.g. "Santa Clara"
    :param state: the name of the state, e.g. "California". This is an optional argument and defaults to "California".
    :param country: the name of the country, e.g. "USA". This is an optional argument and defaults to "USA".
    :param dry_run: if True then return a test air quality instead of making an HTTP request
    :return: the temperature in imperial units, e.g. 75.2
    """
    if dry_run:
        # https://www.iqair.com/us/blog/air-quality/surprising-places-with-worst-air-pollution
        return 386.0

    air_visual_api_url = "http://api.airvisual.com/v2/city"

    querystring = {"city": city, "state": state, "country": country, "key": "f02b30ab-8682-47d6-92d2-2ffe4390b375"}

    response = requests.request("GET", air_visual_api_url, params=querystring)
    # print(response.text)

    if response.json()["status"] == "fail":
        raise ValueError(f"{city}, {state}, {country} is invalid")

    return response.json()["data"]["current"]["pollution"]["aqius"]


if __name__ == "__main__":
    print(get_current_temp("Santa Clara"))
    print(get_current_air_quality("Santa Clara"))
