def count_vowels(s):
    vowels = 0
    for c in s.lower():
        if c == "a" or c == "e" or c == "i" or c == "o" or c == "u":
            vowels += 1
    return vowels


def is_prime(n):
    if n == 0 or n == 1:
        return False
    for i in range(2, n):
        if n % i == 0:
            return False
    return True


if __name__ == "__main__":
    print("Question #1")
    num = 7
    while num > 3:
        num -= 1
        print(num)

    print("\nQuestion #2")
    count = 1
    for letter in "Live Oak Academy":
        print("#", count, "is", letter)
        count += 1
        if count == 3:
            break
    print(count)

    print("\nQuestion #3")
    for i in range(0, 5, 2):
        for j in range(2, 0, -1):
            if j == 1:
                break
            print(i, j)

    print("\nQuestion #4")
    value = 62
    lo = 1
    hi = 100
    guess = None
    while guess != value:
        guess = (hi + lo) // 2
        print(lo, hi, guess)
        if guess < value:
            lo = guess
        elif guess > value:
            hi = guess

    print("\nQuestion #5")
    print(count_vowels("Live Oak Academy"))

    print("\nQuestion #6")
    print(is_prime(37))
    print(is_prime(95))
