import datetime


class Person(object):
    def __init__(self, name):
        self._name = name
        self._last_name = name.split(' ')[-1]
        self._birthday = None

    @property
    def name(self):
        return self._name

    @property
    def last_name(self):
        return self._last_name

    def __lt__(self, other):
        if self.last_name == other.last_name:
            return self.name < other.name
        return self.last_name < other.last_name

    def __str__(self):
        return self._name


class Student(Person):
    def __init__(self, name, major):
        super().__init__(name)
        majors = ['Engineering', 'Education', 'English', 'Economics']
        if major not in majors:
            raise ValueError
        self._major = major

    @property
    def major(self):
        return self._major

    def __str__(self):
        return self.name + ', major:' + self.major


class IntegerSet(object):
    """An IntegerSet is a set of integers
    The value is represented by a list of ints, self._vals.
    Each int in the set occurs in self._vals exactly once."""

    def __init__(self):
        """Create an empty set of integers"""
        self._vals = []

    def insert(self, e):
        """Assumes e is an integer and inserts e into self"""
        if e not in self._vals:
            self._vals.append(e)

    def member(self, e):
        """Assumes e is an integer
           Returns True if e is in self, and False otherwise"""
        return e in self._vals

    def remove(self, e):
        """Assumes e is an integer and removes e from self
           Raises ValueError if e is not in self"""
        if e in self._vals:
            self._vals.remove(e)
        else:
            raise ValueError(str(e) + ' not found')

    def __str__(self):
        """Returns a string representation of self"""
        self._vals.sort()
        return '{' + ','.join([str(e) for e in self._vals]) + '}'

    def intersect(self, other):
        i = IntegerSet()
        for x in self._vals:
            if other.member(x):
                i.insert(x)
        return i

    def __len__(self):
        return len(self._vals)


if __name__ == "__main__":
    mark = Student("Mark Zuckerberg", "Engineering")
    assert mark.last_name == "Zuckerberg"
    assert mark.major == "Engineering"
    assert str(mark) == "Mark Zuckerberg, major:Engineering"

    amy = Student("Amy Tan", "English")
    assert amy.last_name == "Tan"
    assert amy.major == "English"
    assert str(amy) == "Amy Tan, major:English"

    iman = Student("Iman", "Economics")
    assert iman.name == "Iman"
    assert iman.last_name == "Iman"
    assert iman.major == "Economics"
    assert str(iman) == "Iman, major:Economics"

    try:
        iman = Student("Sonia Sotomayor", "History")
        assert False
    except ValueError:
        print("\033[0;34mStudent works\t\t\t\t\t\033[0;33m2 pts")

    # create set 1
    set_1 = IntegerSet()
    assert len(set_1) == 0
    assert str(set_1) == "{}"

    # create set 2
    set_2 = IntegerSet()
    assert len(set_2) == 0
    assert str(set_2) == "{}"

    # test intersection of two empty sets
    set_3 = set_1.intersect(set_2)
    assert len(set_3) == 0

    # test insert, remove, member
    set_1.insert(-17)
    assert len(set_1) == 1
    assert set_1.member(-17) is True
    set_1.insert(-12)
    set_1.insert(-6)
    set_1.insert(2)
    set_1.insert(7)
    assert len(set_1) == 5
    set_1.insert(7)
    assert len(set_1) == 5
    assert str(set_1) == "{-17,-12,-6,2,7}"
    print("\033[0;32mSet Insert works\t\t\t\t\033[0;33m2 pts")
    assert set_1.member(-12) is True
    assert set_1.member(-6) is True
    assert set_1.member(2) is True
    assert set_1.member(7) is True
    set_1.remove(-17)
    assert set_1.member(-17) is False
    assert len(set_1) == 4
    assert str(set_1) == "{-12,-6,2,7}"
    print("\033[0;36mSet Member works\t\t\t\t\033[0;33m2 pts")
    try:
        set_1.remove(-17)
        assert False
    except ValueError as ex:
        assert str(ex) == "-17 not found"
    print("\033[0;35mSet Remove works\t\t\t\t\033[0;33m2 pts")

    # test intersection
    set_3 = set_1.intersect(set_2)
    assert len(set_3) == 0
    set_2.insert(-20)
    set_2.insert(-16)
    set_2.insert(-11)
    assert len(set_2) == 3
    assert str(set_2) == "{-20,-16,-11}"
    set_3 = set_1.intersect(set_2)
    assert len(set_3) == 0
    set_2.insert(2)
    set_3 = set_1.intersect(set_2)
    assert len(set_3) == 1
    assert str(set_3) == "{2}"

    print("\033[0;35mSet Intersect works\t\t\t\t\033[0;33m2 pts")
