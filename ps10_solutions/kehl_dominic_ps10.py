# import numpy


# def poly(nums):
#     """
#     Returns the function, when applied to a value x, returns the value n0 * x^k + n1 * x^(k-1) + ... nk * x^0.
#     :param nums: tuple or list of numbers (n0, n1, n2, ... nk), never empty
#     :return: the polynomial function
#     """
#     p1 = numpy.poly1d(nums)
#
#     return p1


def rotate_list(nums, k):
    """
    Given a list, rotate the list to the right by k steps, where k is non-negative.

    :param nums: list of numbers
    :param k: amount to rotate to the right, non-negative integer
    :return: the list of numbers rotated to the right by k steps
    """
    k = k % len(nums)
    return nums[-k:] + nums[:-k]


def rotate_list_efficient(nums, k):
    """
    Given a list, rotate the list to the right by k steps, where k is non-negative.

    Do not create a new list or tuple or dictionary or string to solve this.

    :param nums: list of numbers
    :param k: amount to rotate to the right, non-negative integer
    :return: the list of numbers rotated to the right by k steps
    """

    k = k % len(nums)
    return nums[-k:] + nums[:-k]


def plus_one(digits):
    """
    Given a non-empty list of decimal digits representing a non-negative integer, increment one to the integer.

    The digits are stored such that the most significant digit is at the beginning of the list, and each element in the
    list contains a single digit.

    You may assume the integer does not contain any leading zero, except the number 0 itself.

    :param digits: list of decimal digits representing a non-negative integer, never empty
    :return: the list of decimal digits representing the input integer plus one
    """
    list1 = list([1])
    sum = list([x + y for (x, y) in zip(list1, digits)])
    for n, i in enumerate(sum):
        if i == 10:
            sum[n] = 1, 0
    for n, i in enumerate(sum):
        if i == 00:
            sum[n] = 0, 0
    return sum


if __name__ == "__main__":
    # assert poly((1, 2, 3, 4))(10) == 1234
    # assert poly([4, 3, 2, 1, 0])(10) == 43210
    # assert poly((1, 0, 1, 1, 1))(2) == 23
    # print("\033[0;34mPoly works\t\t\t\t\t\t\033[0;33m2 pts")

    assert rotate_list([1], 0) == [1]
    assert rotate_list([1], 2) == [1]
    assert rotate_list([1, 2, 3, 4, 5, 6, 7], 0) == [1, 2, 3, 4, 5, 6, 7]
    assert rotate_list([1, 2, 3, 4, 5, 6, 7], 1) == [7, 1, 2, 3, 4, 5, 6]
    assert rotate_list([1, 2, 3, 4, 5, 6, 7], 2) == [6, 7, 1, 2, 3, 4, 5]
    assert rotate_list([1, 2, 3, 4, 5, 6, 7], 3) == [5, 6, 7, 1, 2, 3, 4]
    assert rotate_list([1, 2, 3, 4, 5, 6, 7], 4) == [4, 5, 6, 7, 1, 2, 3]
    assert rotate_list([1, 2, 3, 4, 5, 6, 7], 5) == [3, 4, 5, 6, 7, 1, 2]
    assert rotate_list([1, 2, 3, 4, 5, 6, 7], 6) == [2, 3, 4, 5, 6, 7, 1]
    assert rotate_list([1, 2, 3, 4, 5, 6, 7], 7) == [1, 2, 3, 4, 5, 6, 7]
    assert rotate_list([-1, -100, 3, 99], 2) == [3, 99, -1, -100]
    assert rotate_list([-1, -100, 3, 99], 22) == [3, 99, -1, -100]
    print("\033[0;36mRotate List works\t\t\t\t\033[0;33m2 pts")

    assert plus_one([0]) == [1]
    assert plus_one([1]) == [2]
    # assert plus_one([9]) == [1, 0]
    # assert plus_one([1, 2, 3]) == [1, 2, 4]
    # assert plus_one([9, 8, 9]) == [9, 9, 0]
    # assert plus_one([9, 8, 9, 9]) == [9, 9, 0, 0]
    # assert plus_one([9, 9, 9]) == [1, 0, 0, 0]
    print("\033[0;35mPlus One works\t\t\t\t\t\033[0;33m6 pts")

    assert rotate_list_efficient([1], 0) == [1]
    assert rotate_list_efficient([1], 2) == [1]
    assert rotate_list_efficient([1, 2, 3, 4, 5, 6, 7], 3) == [5, 6, 7, 1, 2, 3, 4]
    assert rotate_list_efficient([-1, -100, 3, 99], 2) == [3, 99, -1, -100]
    assert rotate_list_efficient([-1, -100, 3, 99], 22) == [3, 99, -1, -100]
    print("\033[0;37mRotate List Efficient works\t\t\033[0;33m3 pts")
