def poly(nums):
    def something(x):
        lenght = len(nums)
        total = 0
        for c in nums:
            lenght -= 1
            total += c * x ** lenght
        return (total)

    return something


def rotate_list(nums, k):
    new_list = [0] * len(nums)
    the_plase = k
    for c in nums:
        new_list[the_plase % len(nums)] = c
        the_plase += 1
    return new_list


def rotate_list_efficient(nums, k):
    for f in range(k):
        previous = nums[0]
        curent = nums[0]
        counter = 0
        for c in nums:
            next_index = (1 + counter) % len(nums)
            curent = nums[next_index]
            nums[next_index] = previous
            previous = curent
            counter += 1
    return nums


def plus_one(digits):
    counter = 0
    for c in reversed(digits):
        counter += 1
        if digits[0] == 9 and c == digits[0] and counter == len(digits):
            digits.append(0)
            digits[0] = 1
        c += 1
        if c == 10:
            digits[0 - counter] = 0
        else:
            digits[0 - counter] += 1
            break
    return digits


if __name__ == "__main__":
    assert poly((1, 2, 3, 4))(10) == 1234
    assert poly([4, 3, 2, 1, 0])(10) == 43210
    assert poly((1, 0, 1, 1, 1))(2) == 23
    print("\033[0;34mPoly works\t\t\t\t\t\t\033[0;33m2 pts")

    assert rotate_list([1], 0) == [1]
    assert rotate_list([1], 2) == [1]
    assert rotate_list([1, 2, 3, 4, 5, 6, 7], 3) == [5, 6, 7, 1, 2, 3, 4]
    assert rotate_list([-1, -100, 3, 99], 2) == [3, 99, -1, -100]
    assert rotate_list([-1, -100, 3, 99], 22) == [3, 99, -1, -100]
    print("\033[0;36mRotate List works\t\t\t\t\033[0;33m2 pts")

    assert plus_one([0]) == [1]
    assert plus_one([1]) == [2]
    assert plus_one([9]) == [1, 0]
    assert plus_one([1, 2, 3]) == [1, 2, 4]
    assert plus_one([9, 8, 9]) == [9, 9, 0]
    assert plus_one([9, 8, 9, 9]) == [9, 9, 0, 0]
    assert plus_one([9, 9, 9]) == [1, 0, 0, 0]
    print("\033[0;35mPlus One works\t\t\t\t\t\033[0;33m6 pts")

    assert rotate_list_efficient([1], 0) == [1]
    assert rotate_list_efficient([1], 2) == [1]
    assert rotate_list_efficient([1, 2, 3, 4, 5, 6, 7], 3) == [5, 6, 7, 1, 2, 3, 4]
    assert rotate_list_efficient([-1, -100, 3, 99], 2) == [3, 99, -1, -100]
    assert rotate_list_efficient([-1, -100, 3, 99], 22) == [3, 99, -1, -100]
    print("\033[0;37mRotate List Efficient works\t\t\033[0;33m3 pts")
