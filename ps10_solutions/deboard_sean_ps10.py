def poly(nums, x):
    ex = len(nums) - 1
    newnum = 0
    for el in nums:
        y = el * x ** ex
        newnum += y
        ex -= 1
    return newnum


def rotate_list(nums, k):
    while k != 0:
        lastnum = nums[-1]
        nums.insert(0, lastnum)
        nums.pop(-1)
        k -= 1
    return nums


def rotate_list_efficient(nums, k):
    """
    Given a list, rotate the list to the right by k steps, where k is non-negative.

    Do not create a new list or tuple or dictionary or string to solve this.

    :param nums: list of numbers
    :param k: amount to rotate to the right, non-negative integer
    :return: the list of numbers rotated to the right by k steps
    """
    return rotate_list(nums, k)


def plus_one(digits):
    digits[-1] += 1
    carrynum = 2
    zeronum = len(digits)
    for x in digits:
        if digits[0] == 10:
            digits = []
            digits.append(1)
            while zeronum > 0:
                digits.append(0)
                zeronum -= 1
        if 10 in digits:
            digits[-carrynum] += 1
            digits.remove(10)
            digits.append(0)
        carrynum += 1
    return digits


if __name__ == "__main__":
    assert poly((1, 2, 3, 4), 10) == 1234
    assert poly([4, 3, 2, 1, 0], 10) == 43210
    assert poly((1, 0, 1, 1, 1), 2) == 23
    print("\033[0;34mPoly works\t\t\t\t\t\t\033[0;33m2 pts")

    assert rotate_list([1], 0) == [1]
    assert rotate_list([1], 2) == [1]
    assert rotate_list([1, 2, 3, 4, 5, 6, 7], 0) == [1, 2, 3, 4, 5, 6, 7]
    assert rotate_list([1, 2, 3, 4, 5, 6, 7], 1) == [7, 1, 2, 3, 4, 5, 6]
    assert rotate_list([1, 2, 3, 4, 5, 6, 7], 2) == [6, 7, 1, 2, 3, 4, 5]
    assert rotate_list([1, 2, 3, 4, 5, 6, 7], 3) == [5, 6, 7, 1, 2, 3, 4]
    assert rotate_list([1, 2, 3, 4, 5, 6, 7], 4) == [4, 5, 6, 7, 1, 2, 3]
    assert rotate_list([1, 2, 3, 4, 5, 6, 7], 5) == [3, 4, 5, 6, 7, 1, 2]
    assert rotate_list([1, 2, 3, 4, 5, 6, 7], 6) == [2, 3, 4, 5, 6, 7, 1]
    assert rotate_list([1, 2, 3, 4, 5, 6, 7], 7) == [1, 2, 3, 4, 5, 6, 7]
    assert rotate_list([-1, -100, 3, 99], 2) == [3, 99, -1, -100]
    assert rotate_list([-1, -100, 3, 99], 22) == [3, 99, -1, -100]
    print("\033[0;36mRotate List works\t\t\t\t\033[0;33m2 pts")

    assert plus_one([0]) == [1]
    assert plus_one([1]) == [2]
    assert plus_one([9]) == [1, 0]
    assert plus_one([1, 2, 3]) == [1, 2, 4]
    assert plus_one([9, 8, 9]) == [9, 9, 0]
    assert plus_one([9, 8, 9, 9]) == [9, 9, 0, 0]
    assert plus_one([9, 9, 9]) == [1, 0, 0, 0]
    print("\033[0;35mPlus One works\t\t\t\t\t\033[0;33m6 pts")

    assert rotate_list_efficient([1], 0) == [1]
    assert rotate_list_efficient([1], 2) == [1]
    assert rotate_list_efficient([1, 2, 3, 4, 5, 6, 7], 3) == [5, 6, 7, 1, 2, 3, 4]
    assert rotate_list_efficient([-1, -100, 3, 99], 2) == [3, 99, -1, -100]
    assert rotate_list_efficient([-1, -100, 3, 99], 22) == [3, 99, -1, -100]
    print("\033[0;37mRotate List Efficient works\t\t\033[0;33m3 pts")
