
def poly(nums):
    def nomial(k):
        total = 0
        for num in nums:
            total = k * total + num
        return total
    return nomial


def rotate_list(nums, k):
    while k > len(nums):
        k -= len(nums)
    return nums[-k:] + nums[:-k]


def rotate_list_efficient(nums, k):
    while k > len(nums):
        k -= len(nums)
    return nums[-k:] + nums[:-k]


def plus_one(digits):
    n = len(digits) - 1
    while digits[n] == 9:
        digits[n] = 0
        n -= 1
    if n < 0:
        digits = [1] + digits
    else:
        digits[n] += 1
    return digits


if __name__ == "__main__":
    assert poly((1, 2, 3, 4))(10) == 1234
    assert poly([4, 3, 2, 1, 0])(10) == 43210
    assert poly((1, 0, 1, 1, 1))(2) == 23
    print("\033[0;34mPoly works\t\t\t\t\t\t\033[0;33m2 pts")

    assert rotate_list([1], 0) == [1]
    assert rotate_list([1], 2) == [1]
    assert rotate_list([1, 2, 3, 4, 5, 6, 7], 0) == [1, 2, 3, 4, 5, 6, 7]
    assert rotate_list([1, 2, 3, 4, 5, 6, 7], 1) == [7, 1, 2, 3, 4, 5, 6]
    assert rotate_list([1, 2, 3, 4, 5, 6, 7], 2) == [6, 7, 1, 2, 3, 4, 5]
    assert rotate_list([1, 2, 3, 4, 5, 6, 7], 3) == [5, 6, 7, 1, 2, 3, 4]
    assert rotate_list([1, 2, 3, 4, 5, 6, 7], 4) == [4, 5, 6, 7, 1, 2, 3]
    assert rotate_list([1, 2, 3, 4, 5, 6, 7], 5) == [3, 4, 5, 6, 7, 1, 2]
    assert rotate_list([1, 2, 3, 4, 5, 6, 7], 6) == [2, 3, 4, 5, 6, 7, 1]
    assert rotate_list([1, 2, 3, 4, 5, 6, 7], 7) == [1, 2, 3, 4, 5, 6, 7]
    assert rotate_list([-1, -100, 3, 99], 2) == [3, 99, -1, -100]
    assert rotate_list([-1, -100, 3, 99], 22) == [3, 99, -1, -100]
    print("\033[0;36mRotate List works\t\t\t\t\033[0;33m2 pts")

    assert plus_one([0]) == [1]
    assert plus_one([1]) == [2]
    assert plus_one([9]) == [1, 0]
    assert plus_one([1, 2, 3]) == [1, 2, 4]
    assert plus_one([9, 8, 9]) == [9, 9, 0]
    assert plus_one([9, 8, 9, 9]) == [9, 9, 0, 0]
    assert plus_one([9, 9, 9]) == [1, 0, 0, 0]
    print("\033[0;35mPlus One works\t\t\t\t\t\033[0;33m6 pts")

    assert rotate_list_efficient([1], 0) == [1]
    assert rotate_list_efficient([1], 2) == [1]
    assert rotate_list_efficient([1, 2, 3, 4, 5, 6, 7], 3) == [5, 6, 7, 1, 2, 3, 4]
    assert rotate_list_efficient([-1, -100, 3, 99], 2) == [3, 99, -1, -100]
    assert rotate_list_efficient([-1, -100, 3, 99], 22) == [3, 99, -1, -100]
    print("\033[0;37mRotate List Efficient works\t\t\033[0;33m3 pts")
