num = int(input('Please enter a number: '))

print()
if num % 2 == 0:
    if num % 3 == 0:
        print('Divisible by 2 and 3')
    else:
        print('Divisible by 2 and not by 3')
elif num % 3 == 0:
    print('Divisible by 3 and not by 2')
else:
    print('Not divisible by either 2 or 3')

print()
print('Thank you!')
