num = int(input("Number: "))

for guess in range(0, num + 1):
    cube = guess ** 3
    if cube >= num:
        if cube == num:
            print(f"{guess} is the perfect cube root of {num}")
        else:
            print(f"No perfect cube root found for {num}")
        break
