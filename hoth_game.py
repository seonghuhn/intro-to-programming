# Imperial Starship starting distance in kilometers
IMPERIAL_STARSHIP_STARTING_DISTANCE = 8500

# Maximum range of ion cannon in kilometers
MAX_ION_CANNON_RANGE = 1000

# Time to charge up ion cannon in seconds
TIME_TO_CHARGE_ION_CANNON = 180


def is_commander(identifier):
    return identifier == 'CMDR'


def calc_time_to_be_in_range(velocity):
    """

    :param velocity:
    :return:
    """
    return round((IMPERIAL_STARSHIP_STARTING_DISTANCE - MAX_ION_CANNON_RANGE) / velocity, 2)


def should_leave_hoth(velocity):
    """

    :param velocity:
    :return:
    """
    time_to_reach_hoth = IMPERIAL_STARSHIP_STARTING_DISTANCE / velocity
    return time_to_reach_hoth < TIME_TO_CHARGE_ION_CANNON


def main():
    identifier = input("Identifier: ")
    if not is_commander(identifier):
        print("Unauthorized!")
        return

    for ship_num in range(5):
        velocity = float(input("Velocity of Imperial Starship (km/s): "))
        if should_leave_hoth(velocity):
            print("Get out of the base!")
            break

        time_until_fire = calc_time_to_be_in_range(velocity)
        print(f"Time until Imperial Starship in range {time_until_fire}s")


if __name__ == "__main__":
    main()
