class Coordinate(object):
    def __init__(self, x, y):
        self.x = x
        self.y = y
        pass


class Circle(object):
    def __init__(self, r):
        self.r = r
        pass

    def inside(self, coordinate):
        try:
            return self.r > (coordinate.x ** 2 + coordinate.y ** 2) ** (1 / 2)
        except ZeroDivisionError:
            return self.r > 0
    '''
    In order to modify Circle() to work with a circle that isn't centered on the origin,I would add in the ability 
    to input an extra coordinate pair for the location of the center of the circle. Then I would insert the x and y 
    values into where the distance formula is used in my code.
    '''


class Line(object):
    def __init__(self, coordinate_1, coordinate_2):
        self.coordinate_1 = coordinate_1
        self.coordinate_2 = coordinate_2
        pass

    def x_intercept(self):
        c1 = self.coordinate_1
        c2 = self.coordinate_2

        if c1.x == 0:
            return c2.x
        return c1.x - (c2.x - c1.x) / (c2.y - c1.y) * c1.y
        # b = x - y/m
        # 'b' in this case represents the x-intercept

    def y_intercept(self):
        c1 = self.coordinate_1
        c2 = self.coordinate_2

        if c1.y == 0:
            return c2.y
        return c1.y - (c2.y - c1.y) / (c2.x - c1.x) * c1.x
        # b = y - mx


if __name__ == "__main__":
    assert Circle(5).inside(Coordinate(0, 0)) is True
    assert Circle(5).inside(Coordinate(-2, -2)) is True
    assert Circle(5).inside(Coordinate(-2.9, 4)) is True
    assert Circle(5).inside(Coordinate(3, -3.95)) is True
    assert Circle(5).inside(Coordinate(3, 4)) is False
    assert Circle(5).inside(Coordinate(4, 3)) is False
    assert Circle(5).inside(Coordinate(5, 0)) is False
    assert Circle(5).inside(Coordinate(0, -5)) is False
    assert Circle(5).inside(Coordinate(3.1, 4.1)) is False
    print("\033[0;34mInside Circle works\t\t\t\t\t\t\033[0;33m2 pts")

    line = Line(Coordinate(5, 0), Coordinate(0, 10))
    assert line.x_intercept() == 5
    assert line.y_intercept() == 10
    line = Line(Coordinate(0, 0), Coordinate(-4324, 10432))
    assert line.x_intercept() == -4324
    assert line.y_intercept() == 10432
    line = Line(Coordinate(1.5, 3.75), Coordinate(6.5, 23.75))
    assert line.x_intercept() == 0.5625
    assert line.y_intercept() == -2.25
    print("\033[0;34mLine Intercepts works\t\t\t\t\t\033[0;33m3 pts")
