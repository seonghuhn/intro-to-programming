import math
class Coordinate(object):
    def __init__(self, x, y):
        self.x=x
        self.y=y

class Circle(object):
    def __init__(self, r):
        self.r=r
        self.center=Coordinate(0,0)  # NOW THE CENTER IS WHATEVER YOU WANT IT TO BE
    def inside(self, coordinate):
        if math.sqrt((coordinate.x-self.center.x)**2+(coordinate.y-self.center.y)**2) < self.r:
            return True
        else:
            return False


class Line(object):
    def __init__(self, coordinate_1, coordinate_2):
        self.coordinate_1=coordinate_1
        self.coordinate_2=coordinate_2
    def x_intercept(self):
        slope = (self.coordinate_2.y-self.coordinate_1.y)/(self.coordinate_2.x-self.coordinate_1.x)
        b = -slope*self.coordinate_1.x+self.coordinate_1.y
        x = b/-slope
        return x

    def y_intercept(self):
        slope = (self.coordinate_2.y-self.coordinate_1.y)/(self.coordinate_2.x-self.coordinate_1.x)
        b = -slope*self.coordinate_1.x+self.coordinate_1.y
        return b


if __name__ == "__main__":
    assert Circle(5).inside(Coordinate(0, 0)) is True
    assert Circle(5).inside(Coordinate(-2, -2)) is True
    assert Circle(5).inside(Coordinate(-2.9, 4)) is True
    assert Circle(5).inside(Coordinate(3, -3.95)) is True
    assert Circle(5).inside(Coordinate(3, 4)) is False
    assert Circle(5).inside(Coordinate(4, 3)) is False
    assert Circle(5).inside(Coordinate(5, 0)) is False
    assert Circle(5).inside(Coordinate(0, -5)) is False
    assert Circle(5).inside(Coordinate(3.1, 4.1)) is False
    print("\033[0;34mInside Circle works\t\t\t\t\t\t\033[0;33m2 pts")

    line = Line(Coordinate(5, 0), Coordinate(0, 10))
    assert line.x_intercept() == 5
    assert line.y_intercept() == 10
    line = Line(Coordinate(0, 0), Coordinate(-4324, 10432))
    assert line.x_intercept() == 0
    assert line.y_intercept() == 0
    line = Line(Coordinate(1.5, 3.75), Coordinate(6.5, 23.75))
    assert line.x_intercept() == 0.5625
    assert line.y_intercept() == -2.25
    print("\033[0;34mLine Intercepts works\t\t\t\t\t\033[0;33m3 pts")


