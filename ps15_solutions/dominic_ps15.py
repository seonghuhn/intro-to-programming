class Coordinate(object):
    def __init__(self, x, y):
        self.x = x
        self.y = y

class Circle(object):
    def __init__(self, r):
        self.r = r
    def inside(self, coordinate):
        return (coordinate.x ** 2) + (coordinate.y ** 2) < self.r ** 2
class Line(object):
    def __init__(self, coordinate_1, coordinate_2):
        self.x1 = coordinate_1.x
        self.x2 = coordinate_2.x
        self.y1 = coordinate_1.y
        self.y2 = coordinate_2.y
    def x_intercept(self):
        i = (self.y2 - self.y1) / (self.x2 - self.x1)
        m = self.y1 - (i * self.x1)
        return - m / i

    def y_intercept(self):
        i = (self.y2 - self.y1) / (self.x2 - self.x1)
        return  self.y1 - (i * self.x1)

if __name__ == "__main__":

    assert Circle(5).inside(Coordinate(0, 0)) is True
    assert Circle(5).inside(Coordinate(-2, -2)) is True
    assert Circle(5).inside(Coordinate(-2.9, 4)) is True
    assert Circle(5).inside(Coordinate(3, -3.95)) is True
    assert Circle(5).inside(Coordinate(3, 4)) is False
    assert Circle(5).inside(Coordinate(4, 3)) is False
    assert Circle(5).inside(Coordinate(5, 0)) is False
    assert Circle(5).inside(Coordinate(0, -5)) is False
    assert Circle(5).inside(Coordinate(3.1, 4.1)) is False
    print("\033[0;34mInside Circle works\t\t\t\t\t\t\033[0;33m2 pts")

    line = Line(Coordinate(5, 0), Coordinate(0, 10))
    assert line.x_intercept() == 5
    assert line.y_intercept() == 10

    line = Line(Coordinate(0, 0), Coordinate(-4324, 10432))
    assert line.x_intercept() == 0
    assert line.y_intercept() == 0

    line = Line(Coordinate(1.5, 3.75), Coordinate(6.5, 23.75))
    assert line.x_intercept() == 0.5625
    assert line.y_intercept() == -2.25
    print("\033[0;34mLine Intercepts works\t\t\t\t\t\033[0;33m3 pts")
