import math
class Coordinate(object):
    def __init__(self, x, y):
        self.x = x
        self.y = y

class Circle(object):
    def __init__(self, r):
        self.r = r

    def inside(self, coordinate):
        return False if math.dist([0,0], [coordinate.x, coordinate.y]) >= self.r else True

class Line(object):
    def __init__(self, coordinate_1, coordinate_2):
        self.c1 = coordinate_1
        self.c2 = coordinate_2

    def x_intercept(self):
        if self.c2.x == self.c1.x:
            return self.c1.x
        else:
            m = (self.c2.y - self.c1.y) / (self.c2.x - self.c1.x)
            b = self.c1.y - (m * self.c1.x)
            return b * -1 / m

    def y_intercept(self):
        if self.c2.x == self.c1.x:
            return 'there is no y intercept'
        else:
            m = (self.c2.y - self.c1.y) / (self.c2.x - self.c1.x)
            return self.c1.y - (m * self.c1.x)



if __name__ == "__main__":
    assert Circle(5).inside(Coordinate(0, 0)) is True
    assert Circle(5).inside(Coordinate(-2, -2)) is True
    assert Circle(5).inside(Coordinate(-2.9, 4)) is True
    assert Circle(5).inside(Coordinate(3, -3.95)) is True
    assert Circle(5).inside(Coordinate(3, 4)) is False
    assert Circle(5).inside(Coordinate(4, 3)) is False
    assert Circle(5).inside(Coordinate(5, 0)) is False
    assert Circle(5).inside(Coordinate(0, -5)) is False
    assert Circle(5).inside(Coordinate(3.1, 4.1)) is False
    print("\033[0;34mInside Circle works\t\t\t\t\t\t\033[0;33m2 pts")

    line = Line(Coordinate(5, 0), Coordinate(0, 10))
    assert line.x_intercept() == 5
    assert line.y_intercept() == 10
    line = Line(Coordinate(0, 0), Coordinate(-4324, 10432))
    assert line.x_intercept() == 0
    assert line.y_intercept() == 0
    line = Line(Coordinate(1.5, 3.75), Coordinate(6.5, 23.75))
    assert line.x_intercept() == 0.5625
    assert line.y_intercept() == -2.25
    print("\033[0;34mLine Intercepts works\t\t\t\t\t\033[0;33m3 pts")