import random


class Animal(object):
    def __init__(self, name, age):
        self._name = name
        self._age = age

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, name):
        self._name = name

    @property
    def age(self):
        return self._age

    @age.setter
    def age(self, age):
        self._age = age

    def __lt__(self, other):
        return self.name < other.name

    def __str__(self):
        return "Animal: " + self._name + ", " + str(self._age) + " years old"


jaws = Animal("Jaws", 50)
print(jaws)


class Cat(Animal):
    def speak(self):
        print("meow")

    def __str__(self):
        return "Cat: " + self._name + ", " + str(self._age) + " years old"


garfield = Cat("Garfield", 10)
print(garfield)
garfield.speak()


class Dog(Animal):
    def speak(self):
        print("ruff")

    def __str__(self):
        return "Dog: " + self._name + ", " + str(self._age) + " years old"


lassie = Dog("Lassie", 5)
print(lassie)
lassie.speak()


class Person(Animal):
    def __init__(self, name, age, message):
        super().__init__(name, age)
        self._message = message
        self._friends = []

    @property
    def friends(self):
        return self._friends

    def add_friend(self, friend):
        self._friends.append(friend)

    def speak(self):
        print(self._message)

    def __str__(self):
        return self._name + ", " + str(self._age) + " years old, friends: " + ', '.join(
            friend.name for friend in self._friends)


joe = Person("Joe", 78, "Here's the deal")
joe.speak()
barack = Person("Barack", 59, "Make no mistake")
barack.speak()
kamala = Person("Kamala", 56, "Let's be clear about this")
kamala.speak()
joe.add_friend(barack)
joe.add_friend(kamala)
print(joe)


class Student(Person):
    def speak(self):
        r = random.random()
        if r < 0.25:
            print("I have homework")
        elif r < 0.5:
            print("I need sleep")
        elif r < 0.75:
            print("I am watching Netflix")
        else:
            super().speak()


grace = Student("Grace", 15, "I should eat")
grace.speak()


class Rabbit(Animal):
    tag = 1

    def __init__(self, name, age, parent_1=None, parent_2=None):
        super().__init__(name, age)
        self._parent_1 = parent_1
        self._parent_2 = parent_2
        self._id = Rabbit.tag
        Rabbit.tag += 1

    @property
    def id(self):
        # pad zeroes at front to make a three digit ID
        return str(self._id).zfill(3)

    @property
    def parent_1(self):
        return self._parent_1

    @property
    def parent_2(self):
        return self._parent_2

    def __add__(self, other):
        return Rabbit(0, "", self, other)

    def __lt__(self, other):
        return self.id < other.id


peter = Rabbit("Peter", 2)
print(peter.name, peter.id)
hopsy = Rabbit("Hopsy", 3)
print(hopsy.name, hopsy.id)
cotton = Rabbit("Cotton", 1, hopsy, peter)
print(cotton.name, cotton.id, cotton.parent_1, cotton.parent_2)
mopsy = hopsy + peter
mopsy.name = "Mopsy"
print(mopsy.name, mopsy.id, mopsy.parent_1, mopsy.parent_2)
print(peter < hopsy)
print(mopsy < cotton)
# AttributeError: 'Person' object has no attribute 'id'
# print(mopsy < joe)
print(joe < mopsy)
