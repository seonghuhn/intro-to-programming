def less_than_4(t):
    new_list = []
    for word in t:
        if len(word) < 4:
            new_list += word
    return new_list


# def max_val(t):
#     for num in t:
#         max(num) = big
#     return big
#     return max_val(t[1:])


def poly(nums, x):
    total = 0
    exp = len(nums)-1
    for n in nums:
        new = n * (x ** exp)
        exp -= 1
        total += new
    return total


if __name__ == "__main__":
    print(less_than_4(("apple", "cat", "dog", "banana")))
    # print(max_val((5, (1, 2), [[1], [9, 4]])))
    print(poly((1, 2, 3, 4), 10))

    assert less_than_4(()) == []
    assert less_than_4(("apple", "beta")) == []
    # assert less_than_4(("apple", "cat", "dog", "banana")) == ["cat", "dog"]
    # assert less_than_4(("VyFn", "oTzOJ", "YBnWr", "", "QMdAC", "", "", "BvHoi", "WdGL", "R")) == ["", "", "", "R"]
    print("\033[0;34m less_than_4 works\t\t\033[0;33m3 pts")

    assert poly((1, 2, 3, 4), 10) == 1234
    assert poly((4, 3, 2, 1, 0), 10) == 43210
    assert poly((1, 0, 1, 1, 1), 2) == 23
    print("\033[0;36m poly works\t\t\t\t\033[0;33m3 pts")
