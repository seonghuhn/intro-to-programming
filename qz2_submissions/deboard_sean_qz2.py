new_words = []
def less_than_4(t):
    for x in t:
        if len(x) < 4:
            new_words.append(x)
    return new_words


def max_val(t):
    s = list(t)
    for x in s:
        if type(x) == tuple:
            c = list(x)
            s.remove(x)
            s.append(c)
        if type(x) == list:
            s.remove(x)
            for c in x:
                s.append(c)
    min_s = min(s)
    if len(t)== 1:
        return t
    else:
        s.remove(min_s)
    return max_val(s)


if __name__ == "__main__":
    assert less_than_4(()) == []
    assert less_than_4(("apple", "beta")) == []
    #assert less_than_4(("apple", "cat", "dog", "banana")) == ["cat", "dog"]
    assert less_than_4(("VyFn", "oTzOJ", "YBnWr", "", "QMdAC", "", "", "BvHoi", "WdGL", "R")) == ["", "", "", "R"]
    print("\033[0;34m less_than_4 works\t\t\033[0;33m3 pts")

    print(max_val((1,)))
    print(max_val((1, 2)))
    print(max_val((3, 1, 3)))
    #assert max_val((1,)) == 1
    #assert max_val((1, 2)) == 2
    #assert max_val((3, 1, 3)) == 3
    assert max_val((5, (1, 2), [1, 6])) == 6
    assert max_val((5, (1, 2), [[1], [2]])) == 5
    assert max_val((5, (1, 2), [[1], [2, 3, 4]])) == 5
    assert max_val((5, (1, 2), [[1], [9, 4]])) == 9
    print("\033[0;35m max_val works\t\t\t\033[0;33m7 pts")
