def less_than_4(t):
    t = list(t)
    for el in list(t):
        if len(el) > 4:
            t.remove(el)
    return t


def max_val(t):
    new_list = []
    for x in t:
        if type(x) == int:
            new_list.append(x)
        else:
            x = int
            new_list.append(x)
    return max_val


def poly(nums, x):
    for n in nums:
        return (nums[0]**x) + poly(nums[1:]**x)


if __name__ == '__main__':
    print(less_than_4(("apple", "cat", "dog", "banana")))
    print(less_than_4(("apple", "banana")))
    print(less_than_4(()))
    print(less_than_4(("apple",)))
    print(less_than_4(("cat",)))

    print(max_val(((5, (1, 2), [[1], [2]]))))
    print(max_val((5, (1, 2), [[1], [9, 4]])))

    # print(poly((1, 2, 3, 4), 10))

    assert less_than_4(()) == []
    assert less_than_4(("apple", "beta")) == []
    assert less_than_4(("apple", "cat", "dog", "banana")) == ["cat", "dog"]
    assert less_than_4(("VyFn", "oTzOJ", "YBnWr", "", "QMdAC", "", "", "BvHoi", "WdGL", "R")) == ["", "", "", "R"]
    print("\033[0;34m less_than_4 works\t\t\033[0;33m3 pts")

    # assert max_val((1,)) == 1
    # assert max_val((1, 2)) == 2
    # assert max_val((3, 1, 3)) == 3
    # assert max_val((5, (1, 2), [1, 6])) == 6
    # assert max_val((5, (1, 2), [[1], [2]])) == 5
    # assert max_val((5, (1, 2), [[1], [2, 3, 4]])) == 5
    # assert max_val((5, (1, 2), [[1], [9, 4]])) == 9
    # print("\033[0;35m max_val works\t\t\t\033[0;33m7 pts")

    assert poly((1, 2, 3, 4), 10) == 1234
    assert poly((4, 3, 2, 1, 0), 10) == 43210
    assert poly((1, 0, 1, 1, 1), 2) == 23
    print("\033[0;36m poly works\t\t\t\t\033[0;33m3 pts")
