'''less_than_4(("apple", "cat", "dog", "banana")) returns ["cat", "dog"]
less_than_4(("apple", "banana")) returns []'''

def less_than_4(t):
    """ t, tuple
        Each element of t is a string
        t could be empty
        Returns the list of strings in t that have fewer than 4 characters
    """
    c = 0
    listof = []
    lent = len(t)
    for l in t:
        # should be less than 4
        if len(t[c]) <= 4:
            # should be += [t[c]]
            listof += t[c]
            c += 1
        else:
            c += 1
    return listof



def max_val(t):
    result = 0
    result = _max_val(t, result)
    return result
def _max_val(t, highest):
    """ t, tuple
            Each element of t is either an int, a tuple, or a list
            No tuple or list is empty
            All ints are positive
            Returns the maximum int in t or (recursively) in an element of t"""
    for c in t:
        if type(c) != int:
            highest = _max_val(c, highest)
        else:
            if c > highest:
                highest = c
    return highest


def poly(nums, x):
    """ nums, a tuple of numbers (n0, n1, n2, ... nk)
        nums is never empty
        x, value to apply to polynomial
        Returns the value of the polynomial applied to the value x, i.e.
        n0 * x^k + n1 * x^(k-1) + ... nk * x^0
    """
    y = 0
    f = len(nums)
    i = 0
    g = 0
    while f != 0:
        f -= 1
        l = nums[y]
        i = l*x**f
        g += i
        y += 1
        if f == 0:
            return g







if __name__ == '__main__':
    print(less_than_4(("apple", "cat", "dog", "banana")))
    print(less_than_4(("apple", "banana")))
    print(max_val((5, (1, 2), [[1], [2]])))
    print(max_val((5, (1, 2), [[1], [100, 4]])))
    print(poly((1, 2, 3, 4), 10))
    print(poly((1, 0, 1, 1, 1), 2))

    assert less_than_4(()) == []
    assert less_than_4(("apple", "beta")) == []
    assert less_than_4(("apple", "cat", "dog", "banana")) == ["cat", "dog"]
    assert less_than_4(("VyFn", "oTzOJ", "YBnWr", "", "QMdAC", "", "", "BvHoi", "WdGL", "R")) == ["", "", "", "R"]
    print("\033[0;34m less_than_4 works\t\t\033[0;33m3 pts")

    assert max_val((1,)) == 1
    assert max_val((1, 2)) == 2
    assert max_val((3, 1, 3)) == 3
    assert max_val((5, (1, 2), [1, 6])) == 6
    assert max_val((5, (1, 2), [[1], [2]])) == 5
    assert max_val((5, (1, 2), [[1], [2, 3, 4]])) == 5
    assert max_val((5, (1, 2), [[1], [9, 4]])) == 9
    print("\033[0;35m max_val works\t\t\t\033[0;33m7 pts")

    assert poly((1, 2, 3, 4), 10) == 1234
    assert poly((4, 3, 2, 1, 0), 10) == 43210
    assert poly((1, 0, 1, 1, 1), 2) == 23
    print("\033[0;36m poly works\t\t\t\t\033[0;33m3 pts")
