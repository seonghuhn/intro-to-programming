def less_than_4(t):
    """ t, tuple
        Each element of t is a string
        t could be empty
        Returns the list of strings in t that have fewer than 4 characters
    """
    less_than_4 = []
    for i in t:
        if len(i) < 4:
            less_than_4 += [i]
    return less_than_4


def poly(nums, x):
    """ nums, a tuple of numbers (n0, n1, n2, ... nk)
        nums is never empty
        x, value to apply to polynomial
        Returns the value of the polynomial applied to the value x, i.e.
        n0 * x^k + n1 * x^(k-1) + ... nk * x^0
    """
    k = len(nums) - 1
    total = 0
    while k >= 0:
        total += nums[(len(nums) - k - 1)] * (x ** k)
        k -= 1
    return total


if __name__ == "__main__":
    assert less_than_4(()) == []
    assert less_than_4(("apple", "beta")) == []
    assert less_than_4(("apple", "cat", "dog", "banana")) == ["cat", "dog"]
    assert less_than_4(("VyFn", "oTzOJ", "YBnWr", "", "QMdAC", "", "", "BvHoi", "WdGL", "R")) == ["", "", "", "R"]
    print("\033[0;34m less_than_4 works\t\t\033[0;33m3 pts")

    assert poly((1, 2, 3, 4), 10) == 1234
    assert poly((4, 3, 2, 1, 0), 10) == 43210
    assert poly((1, 0, 1, 1, 1), 2) == 23
    print("\033[0;36m poly works\t\t\t\t\033[0;33m3 pts")
