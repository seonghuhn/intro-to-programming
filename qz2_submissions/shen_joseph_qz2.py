def less_than_4(t):
    d = []
    for x in t:
        if len(x) < 4:
            d.append(x)
    return d


def max_val(t):
    tuple_list = t
    string_list = str(tuple_list)
    string_list = string_list.replace('[', '').replace(']', '').replace(' ', '').replace('(', '').replace(')', '')
    clean_list = list(map(int, string_list.split(',')))
    greatest_number = max(clean_list)
    return greatest_number


if __name__ == "__main__":
    assert less_than_4(()) == []
    assert less_than_4(("apple", "beta")) == []
    assert less_than_4(("apple", "cat", "dog", "banana")) == ["cat", "dog"]
    assert less_than_4(("VyFn", "oTzOJ", "YBnWr", "", "QMdAC", "", "", "BvHoi", "WdGL", "R")) == ["", "", "", "R"]
    print("\033[0;34m less_than_4 works\t\t\033[0;33m3 pts")

    # assert max_val((1,)) == 1
    assert max_val((1, 2)) == 2
    assert max_val((3, 1, 3)) == 3
    assert max_val((5, (1, 2), [1, 6])) == 6
    assert max_val((5, (1, 2), [[1], [2]])) == 5
    assert max_val((5, (1, 2), [[1], [2, 3, 4]])) == 5
    assert max_val((5, (1, 2), [[1], [9, 4]])) == 9
    print("\033[0;35m max_val works\t\t\t\033[0;33m7 pts")
