def less_than_4(t):
    y = []
    for x in t:
        if len(x) < 4:
            y.append(x)
    return y


def poly(nums, x):
    bruh = 0
    for y in range(len(nums)):
        bruh += nums[y] * x ** (len(nums) - y - 1)
    return bruh


if __name__ == "__main__":
    assert less_than_4(()) == []
    assert less_than_4(("apple", "beta")) == []
    assert less_than_4(("apple", "cat", "dog", "banana")) == ["cat", "dog"]
    assert less_than_4(("VyFn", "oTzOJ", "YBnWr", "", "QMdAC", "", "", "BvHoi", "WdGL", "R")) == ["", "", "", "R"]
    print("\033[0;34m less_than_4 works\t\t\033[0;33m3 pts")

    assert poly((1, 2, 3, 4), 10) == 1234
    assert poly((4, 3, 2, 1, 0), 10) == 43210
    assert poly((1, 0, 1, 1, 1), 2) == 23
    print("\033[0;36m poly works\t\t\t\t\033[0;33m3 pts")
