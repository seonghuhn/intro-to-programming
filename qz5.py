class MyDict(object):
    def __init__(self):
        """
        Initialization of dictionary.
        """
        # TODO

    def assign(self, k, v):
        """
        Assigns a value to a key. If the key is already in the dictionary then it updates its value. If it is not then
        it adds the key value pair to the dictionary.
        :param k: the key
        :param v: the value
        """
        # TODO

    def getval(self, k):
        """
        Gets the value stored for the key. If the key is not in the dictionary then a KeyError is raised.
        :param k: the key
        :return: the value for the key if found; otherwise KeyError raised
        """
        # TODO

    def delete(self, k):
        """
        Deletes the key, value pair. If the key is not in the dictionary then a KeyError is raised.
        :param k: the key
        """
        # TODO

    def __len__(self):
        """
        Gets the number of key, value pairs, i.e. the length of the dictionary.
        :return: the number of key, value pairs
        """
        # TODO
