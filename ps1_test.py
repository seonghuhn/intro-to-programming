import current_temp_air_quality
from ps1_solutions import ps1_john_paul_sepka, ps1_lucas_aubin, ps1_julia_kehl, ps1_joseph_shen, ps1_sean_deboard


def test_conv_fahrenheit_celsius():
    assert current_temp_air_quality.conv_fahrenheit_celsius(32) == 0.0
    assert current_temp_air_quality.conv_fahrenheit_celsius(212) == 100.0
    assert current_temp_air_quality.conv_fahrenheit_celsius(100) == 37.78
    assert current_temp_air_quality.conv_fahrenheit_celsius(0) == -17.78
    assert current_temp_air_quality.conv_fahrenheit_celsius(-40) == -40.0
    assert current_temp_air_quality.conv_fahrenheit_celsius(-50) == -45.56

    # dominic kehl did not use the conv_fahrenheit_celsius function

    assert ps1_joseph_shen.conv_fahrenheit_celsius(32) == 0.0
    assert ps1_joseph_shen.conv_fahrenheit_celsius(212) == 100.0
    assert ps1_joseph_shen.conv_fahrenheit_celsius(100) == 37.78
    assert ps1_joseph_shen.conv_fahrenheit_celsius(0) == -17.78
    assert ps1_joseph_shen.conv_fahrenheit_celsius(-40) == -40.0
    assert ps1_joseph_shen.conv_fahrenheit_celsius(-50) == -45.56

    assert ps1_john_paul_sepka.conv_fahrenheit_celsius(32) == 0.0
    assert ps1_john_paul_sepka.conv_fahrenheit_celsius(212) == 100.0
    assert ps1_john_paul_sepka.conv_fahrenheit_celsius(100) == 37.78
    assert ps1_john_paul_sepka.conv_fahrenheit_celsius(0) == -17.78
    assert ps1_john_paul_sepka.conv_fahrenheit_celsius(-40) == -40.0
    assert ps1_john_paul_sepka.conv_fahrenheit_celsius(-50) == -45.56

    # julia kehl did not round off to two decimal places in her function
    assert round(ps1_julia_kehl.conv_fahrenheit_celsius(32), 2) == 0.0
    assert round(ps1_julia_kehl.conv_fahrenheit_celsius(212), 2) == 100.0
    assert round(ps1_julia_kehl.conv_fahrenheit_celsius(100), 2) == 37.78
    assert round(ps1_julia_kehl.conv_fahrenheit_celsius(0), 2) == -17.78
    assert round(ps1_julia_kehl.conv_fahrenheit_celsius(-40), 2) == -40.0
    assert round(ps1_julia_kehl.conv_fahrenheit_celsius(-50), 2) == -45.56

    assert ps1_lucas_aubin.conv_fahrenheit_celsius(32) == 0.0
    assert ps1_lucas_aubin.conv_fahrenheit_celsius(212) == 100.0
    assert ps1_lucas_aubin.conv_fahrenheit_celsius(100) == 37.78
    assert ps1_lucas_aubin.conv_fahrenheit_celsius(0) == -17.78
    assert ps1_lucas_aubin.conv_fahrenheit_celsius(-40) == -40.0
    assert ps1_lucas_aubin.conv_fahrenheit_celsius(-50) == -45.56

    assert ps1_sean_deboard.conv_fahrenheit_celsius(32) == 0.0
    assert ps1_sean_deboard.conv_fahrenheit_celsius(212) == 100.0
    # assert ps1_solutions.ps1_sean_deboard.conv_fahrenheit_celsius(100) == 37.78
    # assert ps1_solutions.ps1_sean_deboard.conv_fahrenheit_celsius(0) == -17.78
    assert ps1_sean_deboard.conv_fahrenheit_celsius(-40) == -40.0
    # assert ps1_solutions.ps1_sean_deboard.conv_fahrenheit_celsius(-50) == -45.56


def test_air_quality_level():
    assert current_temp_air_quality.air_quality_level(0) == "Good"
    assert current_temp_air_quality.air_quality_level(50) == "Good"
    assert current_temp_air_quality.air_quality_level(51) == "Moderate"
    assert current_temp_air_quality.air_quality_level(100) == "Moderate"
    assert current_temp_air_quality.air_quality_level(101) == "Unhealthy for Sensitive Groups"
    assert current_temp_air_quality.air_quality_level(150) == "Unhealthy for Sensitive Groups"
    assert current_temp_air_quality.air_quality_level(151) == "Unhealthy"
    assert current_temp_air_quality.air_quality_level(200) == "Unhealthy"
    assert current_temp_air_quality.air_quality_level(201) == "Very Unhealthy"
    assert current_temp_air_quality.air_quality_level(300) == "Very Unhealthy"
    assert current_temp_air_quality.air_quality_level(301) == "Hazardous"

    # dominic changed the air_quality_level function

    # john paul used colored for the first two levels so it doesn't work
    # assert ps1_john_paul_sepka.air_quality_level(0) == "Good"
    # assert ps1_john_paul_sepka.air_quality_level(50) == "Good"
    # assert ps1_john_paul_sepka.air_quality_level(51) == "Moderate"
    # assert ps1_john_paul_sepka.air_quality_level(100) == "Moderate"
    assert ps1_john_paul_sepka.air_quality_level(101) == "Unhealthy for Sensitive Groups"
    assert ps1_john_paul_sepka.air_quality_level(150) == "Unhealthy for Sensitive Groups"
    assert ps1_john_paul_sepka.air_quality_level(151) == "Unhealthy"
    assert ps1_john_paul_sepka.air_quality_level(200) == "Unhealthy"
    assert ps1_john_paul_sepka.air_quality_level(201) == "Very Unhealthy"
    # assert ps1_john_paul_sepka.air_quality_level(300) == "Very Unhealthy"
    assert ps1_john_paul_sepka.air_quality_level(301) == "Hazardous"

    assert ps1_joseph_shen.air_quality_level(0) == "Good"
    assert ps1_joseph_shen.air_quality_level(50) == "Good"
    assert ps1_joseph_shen.air_quality_level(51) == "Moderate"
    assert ps1_joseph_shen.air_quality_level(100) == "Moderate"
    assert ps1_joseph_shen.air_quality_level(101) == "Unhealthy for Sensitive Groups"
    assert ps1_joseph_shen.air_quality_level(150) == "Unhealthy for Sensitive Groups"
    assert ps1_joseph_shen.air_quality_level(151) == "Unhealthy"
    assert ps1_joseph_shen.air_quality_level(200) == "Unhealthy"
    assert ps1_joseph_shen.air_quality_level(201) == "Very Unhealthy"
    assert ps1_joseph_shen.air_quality_level(300) == "Very Unhealthy"
    assert ps1_joseph_shen.air_quality_level(301) == "Hazardous"

    assert ps1_julia_kehl.air_quality_level(0) == "Good"
    assert ps1_julia_kehl.air_quality_level(50) == "Good"
    assert ps1_julia_kehl.air_quality_level(51) == "Moderate"
    assert ps1_julia_kehl.air_quality_level(100) == "Moderate"
    # Julia returned "Unhealthy for sensitive groups"
    assert ps1_julia_kehl.air_quality_level(101) == "Unhealthy for sensitive groups"
    assert ps1_julia_kehl.air_quality_level(150) == "Unhealthy for sensitive groups"
    assert ps1_julia_kehl.air_quality_level(151) == "Unhealthy"
    assert ps1_julia_kehl.air_quality_level(200) == "Unhealthy"
    assert ps1_julia_kehl.air_quality_level(201) == "Very Unhealthy"
    # assert ps1_solutions.ps1_julia_kehl.air_quality_level(300) == "Very Unhealthy"
    # Missed 301
    assert ps1_julia_kehl.air_quality_level(302) == "Hazardous"

    # Lucas appended the color to the beginning
    assert ps1_lucas_aubin.air_quality_level(0)[10:] == "Good"
    assert ps1_lucas_aubin.air_quality_level(50)[10:] == "Good"
    assert ps1_lucas_aubin.air_quality_level(51)[10:] == "Moderate"
    assert ps1_lucas_aubin.air_quality_level(100)[10:] == "Moderate"
    assert ps1_lucas_aubin.air_quality_level(101)[10:] == "Unhealthy for Sensitive Groups"
    assert ps1_lucas_aubin.air_quality_level(150)[10:] == "Unhealthy for Sensitive Groups"
    assert ps1_lucas_aubin.air_quality_level(151)[10:] == "Unhealthy"
    assert ps1_lucas_aubin.air_quality_level(200)[10:] == "Unhealthy"
    assert ps1_lucas_aubin.air_quality_level(201)[10:] == "Very Unhealthy"
    assert ps1_lucas_aubin.air_quality_level(300)[10:] == "Very Unhealthy"
    assert ps1_lucas_aubin.air_quality_level(301)[10:] == "Hazardous"

    # Sean returned a tuple with the first value starting with "Air Quality is "
    assert ps1_sean_deboard.air_quality_level(0)[0][15:] == "Good"
    # assert ps1_solutions.ps1_sean_deboard.air_quality_level(50)[0][15:] == "Good"
    assert ps1_sean_deboard.air_quality_level(51)[0][15:] == "Moderate"
    # assert ps1_solutions.ps1_sean_deboard.air_quality_level(100)[0][15:] == "Moderate"
    assert ps1_sean_deboard.air_quality_level(101)[0][15:] == "Unhealthy for Sensitive Groups"
    # assert ps1_solutions.ps1_sean_deboard.air_quality_level(150)[0][15:] == "Unhealthy for Sensitive Groups"
    assert ps1_sean_deboard.air_quality_level(151)[0][15:] == "Unhealthy"
    # assert ps1_solutions.ps1_sean_deboard.air_quality_level(200)[0][15:] == "Unhealthy"
    assert ps1_sean_deboard.air_quality_level(201)[0][15:] == "Very Unhealthy"
    # assert ps1_solutions.ps1_sean_deboard.air_quality_level(300)[0][15:] == "Very Unhealthy"
    assert ps1_sean_deboard.air_quality_level(301)[0][15:] == "Hazardous"
