def reverse_string(s):
    """
    Reverses the string.
    :param s: the input string
    :return: the reversed string
    """
    # NOTE: Do not use the Python reversed function in this function.
    ret = ""
    for i in range(len(s) - 1, -1, -1):
        ret += s[i]
    return ret


def first_unique_char(s):
    """
    Finds the index of the first unique character in the string, ignoring case.
    :param s: the input string
    :return: the index of the first unique character if one is found; otherwise returns -1
    """
    if len(s) == 0:
        return -1

    s = s.lower()
    if len(s) == 1:
        return 0

    for i in range(len(s)):
        if s[i] not in s[0:i] and s[i] not in s[i+1:]:
            return i
    return -1


def valid_palindrome(s):
    """
    Determine if the string is a valid palindrome, a word, phrase, or sequence that reads the same backward as forward,
    e.g., "madam" or "nurses run".
    :param s: the input string
    :return: True if the input string is a valid palindrome. False otherwise.
    """
    i = 0
    j = len(s) - 1
    s = s.lower()
    while i < j:
        if not s[i].isalnum():
            i += 1
            continue
        if not s[j].isalnum():
            j -= 1
            continue
        if s[i] != s[j]:
            return False
        i += 1
        j -= 1
    return True


if __name__ == "__main__":
    # returns "olleh"
    print(reverse_string("hello"))
    # returns "hannaH"
    print(reverse_string("Hannah"))

    # returns 3
    print(first_unique_char("Ecclesiastes"))
    # returns 1
    print(first_unique_char("lovelife"))

    # returns False
    print(valid_palindrome("banana"))
    # returns True
    print(valid_palindrome("saippuakivikauppias"))
    # returns True
    print(valid_palindrome("Bob"))
    # returns True
    print(valid_palindrome("A man, a plan, a canal: Panama"))
