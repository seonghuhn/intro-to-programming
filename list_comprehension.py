"""
Solutions to examples from:
https://realpython.com/list-comprehension-python/
https://www.reddit.com/r/learnpython/comments/4d2yl7/i_need_list_comprehension_exercises_to_drill/
"""

import random


def using_for_loop():
    _sqs = []
    for _num in range(10):
        _sqs.append(_num * _num)
    print(_sqs)


def using_map():
    _nums = []
    for _num in range(10):
        _nums.append(_num)
    _sqs = map(lambda x: x * x, _nums)
    print(list(_sqs))


def using_list_comprehension():
    sqs = [num * num for num in range(10)]
    print(sqs)


def evens_filter():
    _nums = []
    for _num in range(10):
        _nums.append(_num)
    _evens = filter(lambda x: x % 2 == 0, _nums)
    print(list(_evens))


def get_weather_data():
    return random.randrange(90, 110)


def flatten(matrix):
    _flat = []
    for row in matrix:
        for num in row:
            _flat.append(num)
    return _flat


def count_spaces(string):
    return len([c for c in string if c == ' '])


def is_vowel(c):
    return c.lower() in ('a', 'e', 'i', 'o', 'u')


def remove_vowels(string):
    return "".join([c for c in string if is_vowel(c) is False])


def find_words_4(string):
    words = string.split()
    return [word for word in words if len(word) < 4]


def count_words_len(string):
    words = string.split()
    ret = {}
    for word in words:
        if len(word) in ret:
            ret[len(word)] += 1
        else:
            ret[len(word)] = 1
    return ret


if __name__ == "__main__":
    using_for_loop()
    using_map()
    using_list_comprehension()

    evens_filter()
    evens = [num for num in range(10) if num % 2 == 0]
    print(evens)

    hot_temps = [temp for _ in range(20) if (temp := get_weather_data()) >= 100]
    print(hot_temps)

    matrix_012 = [
        [0, 0, 0],
        [1, 1, 1],
        [2, 2, 2],
    ]
    flat = [num for row in matrix_012 for num in row]
    print(flat)
    print(flatten(matrix_012))

    # Find all of the numbers from 1-100 that are divisible by 7
    print([num for num in range(1, 101) if num % 7 == 0])

    # Find all of the numbers from 1-100 that have a 3 in them
    print([num for num in range(1, 101) if str(num).find('3') != -1])

    # Count the number of spaces in a string
    print(count_spaces("Count the number of spaces in a string"))

    # Remove all of the vowels in a string
    print(remove_vowels("Remove all of the vowels in a string"))

    # Find all of the words in a string that are less than 4 letters
    print(find_words_4("Find all of the words in a string that are less than 4 letters"))

    # Use a dictionary comprehension to count the length of each word in a sentence
    print(count_words_len("Use a dictionary comprehension to count the length of each word in a sentence"))

    # Use a nested list comprehension to find all of the numbers from 1-100 that are divisible by any single digit
    # besides 1 (2-9)
    print([num for num in range(1, 101) if num % 2 == 0 or num % 3 == 0 or num % 5 == 0 or num % 7 == 0])
