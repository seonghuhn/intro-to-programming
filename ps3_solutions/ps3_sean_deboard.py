import turtle
t = turtle.Turtle()
while 1:
    number_of_sides = input("number of sides: ")
    if not number_of_sides:
        break
    number_of_sides = int(number_of_sides)
    if number_of_sides == 0:
        break
    angle = 360.0 / number_of_sides
    if number_of_sides == 3:
        t.clear()
        t.fd(100)
        t.left(120)
        t.fd(100)
        t.left(120)
        t.fd(100)
        t.left(120)
    else:
        t.clear()
        for anything in range(0,number_of_sides):
            t.fd(100)
            t.left(angle)
print("Goodbye!")



