# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.


import turtle

t = turtle.Turtle()


def main(t):
    t.begin_fill()
while t:
    t.begin_fill()
    number_of_sides = input("Number of sides desired: ")
    if number_of_sides == '':
        break
    else:
        number_of_sides = int(number_of_sides)
    for _ in range(int(number_of_sides)):
        t.forward(100)
        t.right(360 / number_of_sides)
    s = t.getscreen()
    color = input('What color would you like? ')
    if color == '':
        t.fillcolor()
    else:
        t.fillcolor(color)
    t.end_fill()
    again = input('Would you like to exit: ')
    if not 'yes' in again:
        t.clear()
    if again == 'yes':
        break
    t.end_fill()
print("Goodbye!")





if __name__ == '__main__':
    main(t)
