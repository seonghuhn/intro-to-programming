import turtle

def main():
    t = turtle.Turtle()
    t.pencolor(input("What color would you like?"))
    while sides := input("Would you like sides with that?"):
        sides = int(sides)
        t.clear()
        angle = 360 / sides

        for count in range(sides):
            t.fd(50)
            t.lt(angle)


    s = turtle.getscreen()
    s.exitonclick()
    print("Aight, I'ma head out")


if __name__ == '__main__':
    main()