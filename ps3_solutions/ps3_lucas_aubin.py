import turtle


def draw_polygon(t, num_sides, color):
    """
      Draws an n-sided polygon optionally with a color
      :param t: the turtle object
      :param num_sides: The number of sides that the polygon will have.
      :param color: The color of the polygon
    """

    t.clear()
    circum = 500
    radius = circum / (2 * 3.1415)
    rot_angle = 360.0 / num_sides
    side_length = circum / num_sides

    # Moves turtle to the edge of the shape
    t.penup()
    t.fd(radius)
    t.pendown()
    t.right(90)

    # Draws the polygon
    if color:
        t.fillcolor(color)
        t.begin_fill()
    for side in range (0, num_sides):
        t.fd(side_length)
        t.right(rot_angle)
    t.end_fill()

    # Moves turtle to the center
    t.penup()
    t.goto(0, 0)
    t.pendown()

def get_input():
    t = turtle.Turtle()
    keep_going = True
    while keep_going:
        num_sides = input('How many sides do you want on your shape? ')
        if num_sides:
            color = input('What color do you want your shape to be? ')
            if color:
                print(f'Drawing a {num_sides} sided polygon that is {color}.')
            else:
                print(f'Drawing a {num_sides} sided polygon without a color.')
            draw_polygon(t, int(num_sides), color)
        else:
            print('Have a nice day! ')
            turtle.bye()
            keep_going = False




# Start the drawing program
if __name__ == "__main__":
    get_input()
