import turtle


def polygon(sides):
    if int(sides) >= 3:
        angle = 360 / int(sides)
        return angle


def draw(bruh):
    color = input('What color?')
    turtle.clearscreen()
    t = turtle.Turtle()

    t.fillcolor(color)
    t.begin_fill()

    if int(bruh) < 3:
        print('Invalid number')
        return

    counter = 0
    while counter < int(bruh):
        t.fd(50)
        t.right(polygon(sides=bruh))
        counter += 1
        if counter == bruh:
            break
    t.end_fill()
    return


def main():
    user = 4
    while user:
        user = input('How many sides?')
        if not user:
            print('Goodbye')
            break
        else:
            draw(bruh=user)


main()
