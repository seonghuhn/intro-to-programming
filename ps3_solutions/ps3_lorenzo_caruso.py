import turtle

t = turtle.Turtle()

def main(t):
    t.begin_fill()


while t:
    num = input('How many sides?    ')
    t.clear()
    if num == '':
        print('Goodbye!')
        s = getscreen()
        s.exitonclick
        break
    else:
        num = int(num)
        for _ in range(num):
            t.fd(100)
            t.right(360 / num)
