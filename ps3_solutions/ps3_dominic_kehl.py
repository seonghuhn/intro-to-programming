import turtle

turtle = turtle.Turtle()


def main(turtle):
    sides = input("Enter the number of sides of the polygon: ")
    if not sides:
        print('Goodbye no valid input')
        exit()
    else:
        sides = int(sides)

    while sides <= 3:
        sides = int(input("Enter a number greater than 3: "))
        print(sides)

    color = input('What color would you like to fill your shape: ')
    if not color:
        print('No color input')
    else:
        turtle.fillcolor(color)
        turtle.begin_fill()

    for _ in range(int(sides)):
        turtle.forward(100)
        turtle.right(360 / sides)

    turtle.end_fill()

    answer = input('Would you like to draw another polygon (y)yes or (n)no: ')
    if answer == 'y':
        turtle.clear()
        if __name__ == '__main__':
            main(turtle)
    else:
        print('Click to exit')
        s = turtle.getscreen()
        s.exitonclick()
        print('Goodbye(end)')


if __name__ == '__main__':
    main(turtle)
