"""Hangman game
"""

import random
import string

# -----------------------------------
# Helper code
# You don't need to understand this helper code, but you will have to know how to use the functions (so be sure to read
# the docstrings!)

WORD_LIST_FILENAME = "words.txt"

# https://gist.github.com/chrishorton/8510732aa9a80a03c829b09f12e20d9c
HANG_MAN_PICS = ['''
  +---+
      |
      |
      |
      |
      |
=========''', '''
  +---+
  |   |
      |
      |
      |
      |
=========''', '''
  +---+
  |   |
  O   |
      |
      |
      |
=========''', '''
  +---+
  |   |
  O   |
  |   |
      |
      |
=========''', '''
  +---+
  |   |
  O   |
 /|   |
      |
      |
=========''', '''
  +---+
  |   |
  O   |
 /|\\  |
      |
      |
=========''', '''
  +---+
  |   |
  O   |
 /|\\  |
 /    |
      |
=========''', '''
  +---+
  |   |
  O   |
 /|\\  |
 / \\  |
      |
=========''']


def load_words():
    """
    Returns a list of valid words. Words are strings of lowercase letters.

    Depending on the size of the word list, this function may
    take a while to finish.
    """
    print("Loading word list from file...")
    # in_file: file
    in_file = open(WORD_LIST_FILENAME, 'r')
    # line: string
    line = in_file.readline()
    # word_list: list of strings
    word_list = line.split()
    print("  ", len(word_list), "words loaded.")
    return word_list


def choose_word(word_list):
    """
    Chooses randomly a word from the word list.
    :param word_list: list of words (strings)
    :return: Returns a word from word list at random
    """
    return random.choice(word_list)


# end of helper code
# -----------------------------------

def is_word_guessed(secret_word, letters_guessed):
    secret_list = list(secret_word)
    finished_list = []
    for i in secret_list:
        if i not in finished_list:
            finished_list.append(i)
    for c in letters_guessed:
        if c in finished_list:
            finished_list.remove(c)
        if not finished_list:
            return True
    return False


    """
    Is the word guessed from the letters?
    For example if the secret_word is "apple" and the letters_guessed are ['e', 'i', 'p', 'x'] then this function
    returns False because not all the letters in "apple" have been guessed.

    :param secret_word: string, the word the user is guessing
    :param letters_guessed: list, what letters have been guessed so far
    :return: boolean, True if all the letters of secret_word are in letters_guessed; False otherwise
    """


def get_guessed_word(secret_word, letters_guessed):
    under_scores = []
    for i in secret_word:
        if i in letters_guessed:
            under_scores.append(i)
        else:
            under_scores.append("_")
    score = ''.join(under_scores)
    return score
    """
    Gets a string that is comprised of letters and underscores, based on what letters in letters_guessed are in
    secret_word.
    For example if the secret_word is "apple" and the letters_guessed are ['e', 'i', 'p', 'x'] then this function
    returns "_pp_e" because 'p' and 'e' are guessed correctly and the other letters are not.

    :param secret_word: string, the word the user is guessing
    :param letters_guessed: list, what letters have been guessed so far
    :return: string, composed of letters and underscores that represents what letters in secret_word have been guessed
    so far.
    """


def get_available_letters(letters_guessed):
    alphabet_string = string.ascii_lowercase
    letters_list = []
    letters_list[:0] = alphabet_string
    for i in letters_guessed:
        letters_list.remove(i)
    b = ""
    return b.join(letters_list)
    """
    Gets all the lowercase letters remaining that have not have been guessed.
    For example if the letters_guessed are ['e', 'i', 'k', 'p', 'r', 's'] then this function returns
    "abcdfghjlmnoqtuvwxyz".

    :param letters_guessed: list, what letters have been guessed so far
    :return: string, composed of letters that represents what letters have not yet been guessed.
    """

def hangman(secret_word):
    print("Hello There")
    num_of_letters = len(secret_word)
    print(f"The number of letters is: {num_of_letters}")
    guesses = 8
    letters_guessed = []
    while guesses > 0 or "_" in get_guessed_word(secret_word, letters_guessed):
        print(f"you have {guesses} guesses left")
        print(f"the available letters to chose from are {get_available_letters(letters_guessed)}")
        letter = input("guess a letter: ")
        if letter in letters_guessed:
            print("you have already guessed that letter. Guess again")
        elif letter:
            letters_guessed.append(letter)
            if letter in secret_word:
                print("the letter was in the word!")
            else:
                print("The letter was not in the word. :(")
                guesses -= 1
            print(get_guessed_word(secret_word, letters_guessed))
            if get_guessed_word(secret_word, letters_guessed) == secret_word:
                break
            if guesses == 0:
                break
    if guesses == 0:
        print("You lost the game :(. Better luck next time!")
        print(f"the word was: {secret_word}")
    else:
        print("You won the game!! Great job!")




    """
    Starts up an interactive game of Hangman.

    At the beginning of the game:
    1. Welcome the user to the game.
    2. Tell the user how many letters are in the secret word.

    Each round of the game:
    1. Tell the user how many guesses he/she has left. (You start off with eight guesses).
    2. Tell the user what letters are available to choose from.
    3. Ask the user to guess a letter.
        a. If the user has already guessed this letter ask him/her to guess again.
    4. Tell the user whether the letter is in or is not in the secret word.
    5. Print out what has been guessed correctly and underscores for what has not been.
        a. For example “_pp_e” if the secret word is “apple” and they have guessed correctly so far “p” and “e”.
    6. If the guessed letter is not in the secret word:
        a. Count that as a used guess.
        b. (Bonus) Draw a body part.
        c. If the user has made eight guesses then:
            i. Tell the user he/she has lost.
            ii. Exit the program.
    7. If the guessed letter is in the secret word and the secret word has been completely revealed then:
        a. Congratulate the user.
        b. Exit the program.

    :param secret_word: string, the secret word to guess.
    """



def test_is_word_guessed():
    assert not is_word_guessed("apple", [])
    assert not is_word_guessed("apple", ['i', 'k', 'r', 's'])
    assert not is_word_guessed("apple", ['e', 'i', 'k', 'r', 's'])
    assert not is_word_guessed("apple", ['e', 'i', 'k', 'p', 'r', 's'])
    assert not is_word_guessed("apple", ['e', 'i', 'k', 'p', 'r', 's', 'a'])
    assert is_word_guessed("apple", ['e', 'i', 'k', 'p', 'r', 's', 'a', 'l'])


def test_get_guessed_word():
    assert get_guessed_word("apple", []) == "_____"
    assert get_guessed_word("apple", ['i', 'k', 'r', 's']) == "_____"
    assert get_guessed_word("apple", ['e', 'i', 'k', 'r', 's']) == "____e"
    assert get_guessed_word("apple", ['e', 'i', 'k', 'p', 'r', 's']) == "_pp_e"
    assert get_guessed_word("apple", ['e', 'i', 'k', 'p', 'r', 's', 'a']) == "app_e"
    assert get_guessed_word("apple", ['e', 'i', 'k', 'p', 'r', 's', 'a', 'l']) == "apple"


def test_get_available_letters():
    assert get_available_letters([]) == string.ascii_lowercase
    assert get_available_letters(['e', 'i', 'k', 'p', 'r', 's']) == "abcdfghjlmnoqtuvwxyz"


def test():
    test_is_word_guessed()
    print("\033[0;34mIs the Word Guessed works\t\t\t\t\033[0;33m2 pts")
    test_get_guessed_word()
    print("\033[0;35mGetting the User's Guess works\t\t\t\033[0;33m2 pts")
    test_get_available_letters()
    print("\033[0;36mGetting the Available Letters works\t\t\033[0;33m2 pts")


def test_hangman():
    hangman("apple")


def play_hangman():
    word_list = load_words()
    secret_word = choose_word(word_list).lower()
    hangman(secret_word)


if __name__ == "__main__":
    # TODO: 1. Uncomment to test is_word_guessed, get_guessed_word, get_available_letters.
    #test()

    # TODO: 2. Uncomment to test your hangman program with your own secret word.
    #test_hangman()

    # TODO: 3. Uncomment when you are ready to play hangman program with a computer selected secret word.
    play_hangman()
