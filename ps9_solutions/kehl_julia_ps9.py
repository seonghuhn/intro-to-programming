"""Hangman game
"""

import random
import string

# -----------------------------------
# Helper code
# You don't need to understand this helper code, but you will have to know how to use the functions (so be sure to read
# the docstrings!)

WORD_LIST_FILENAME = "words.txt"

#https://gist.github.com/chrishorton/8510732aa9a80a03c829b09f12e20d9c
HANG_MAN_PICS = ['''
  +---+
      |
      |
      |
      |
      |
=========''', '''
  +---+
  |   |
      |
      |
      |
      |
=========''', '''
  +---+
  |   |
  O   |
      |
      |
      |
=========''', '''
  +---+
  |   |
  O   |
  |   |
      |
      |
=========''', '''
  +---+
  |   |
  O   |
 /|   |
      |
      |
=========''', '''
  +---+
  |   |
  O   |
 /|\  |
      |
      |
=========''', '''
  +---+
  |   |
  O   |
 /|\  |
 /    |
      |
=========''', '''
  +---+
  |   |
  O   |
 /|\  |
 / \  |
      |
=========''']

def load_words():
    print("Loading word list from file...")
    # in_file: file
    in_file = open(WORD_LIST_FILENAME, 'r')
    # line: string
    line = in_file.readline()
    # word_list: list of strings
    word_list = line.split()
    print("  ", len(word_list), "words loaded.")
    return word_list


def choose_word(word_list):
    return random.choice(word_list)


# end of helper code
# -----------------------------------

def is_word_guessed(secret_word, letters_guessed):
    new_word = ''
    for char in secret_word:
        if char in letters_guessed:
            new_word += char
            if new_word == secret_word:
                return True
    return False


def get_guessed_word(secret_word, letters_guessed):
    new_word = ''
    for char in secret_word:
        if char in letters_guessed:
            new_word += char
        else:
            new_word += '_'
    return new_word


def get_available_letters(letters_guessed):
    alphabet = list('abcdefghijklmnopqrstuvwxyz')
    for char in letters_guessed:
        alphabet.remove(char)
    return ''.join(alphabet)

def hangman(secret_word):
    guess = 8
    letters_guessed = ''
    letter_guessed = ''
    print('Welcome to hangman!')
    print('There are', len(secret_word), 'letters in the word')

    
    while guess > 0:
        print('Letters remaining: ', get_available_letters(letters_guessed))
        print('You have', guess, 'guesses available')
        letter_guessed = input('Guess letter: ')

        while letter_guessed in letters_guessed:
            letters_guessed = input('You have already guessed that, please guess again: ')
        if letter_guessed in secret_word:
            print('That letter is in the word!')
            letters_guessed += letter_guessed
        else:
            print('That letter is not in the word')
            guess -= 1
            letters_guessed += letter_guessed
        print(get_guessed_word(secret_word, letters_guessed))
        if get_guessed_word(secret_word, letters_guessed) == secret_word:
            print('You win :)')
            break
    else:
        print('Game over :( the secret word was', secret_word)






def test_is_word_guessed():
    assert not is_word_guessed("apple", [])
    assert not is_word_guessed("apple", ['i', 'k', 'r', 's'])
    assert not is_word_guessed("apple", ['e', 'i', 'k', 'r', 's'])
    assert not is_word_guessed("apple", ['e', 'i', 'k', 'p', 'r', 's'])
    assert not is_word_guessed("apple", ['e', 'i', 'k', 'p', 'r', 's', 'a'])
    assert is_word_guessed("apple", ['e', 'i', 'k', 'p', 'r', 's', 'a', 'l'])


def test_get_guessed_word():
    assert get_guessed_word("apple", []) == "_____"
    assert get_guessed_word("apple", ['i', 'k', 'r', 's']) == "_____"
    assert get_guessed_word("apple", ['e', 'i', 'k', 'r', 's']) == "____e"
    assert get_guessed_word("apple", ['e', 'i', 'k', 'p', 'r', 's']) == "_pp_e"
    assert get_guessed_word("apple", ['e', 'i', 'k', 'p', 'r', 's', 'a']) == "app_e"
    assert get_guessed_word("apple", ['e', 'i', 'k', 'p', 'r', 's', 'a', 'l']) == "apple"


def test_get_available_letters():
    assert get_available_letters([]) == string.ascii_lowercase
    assert get_available_letters(['e', 'i', 'k', 'p', 'r', 's']) == "abcdfghjlmnoqtuvwxyz"

def test():
    test_is_word_guessed()
    print("\033[0;34mIs the Word Guessed works\t\t\t\t\033[0;33m2 pts")
    test_get_guessed_word()
    print("\033[0;35mGetting the User's Guess works\t\t\t\033[0;33m2 pts")
    test_get_available_letters()
    print("\033[0;36mGetting the Available Letters works\t\t\033[0;33m2 pts")


def test_hangman():
    hangman("hannah")


def play_hangman():
    word_list = load_words()
    secret_word = choose_word(word_list).lower()
    hangman(secret_word)


if __name__ == "__main__":
    #test()

    #test_hangman()


    play_hangman()
