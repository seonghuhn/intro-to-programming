"""Hangman game
"""

import random
import string

# -----------------------------------
# Helper code
# You don't need to understand this helper code, but you will have to know how to use the functions (so be sure to read
# the docstrings!)

WORD_LIST_FILENAME = "/Users/sergiikarbuiev/PycharmProjects/pythonProject/words.txt"

# https://gist.github.com/chrishorton/8510732aa9a80a03c829b09f12e20d9c
HANG_MAN_PICS = ['''
  +---+
      |
      |
      |
      |
      |
=========''', '''
  +---+
  |   |
      |
      |
      |
      |
=========''', '''
  +---+
  |   |
  O   |
      |
      |
      |
=========''', '''
  +---+
  |   |
  O   |
  |   |
      |
      |
=========''', '''
  +---+
  |   |
  O   |
 /|   |
      |
      |
=========''', '''
  +---+
  |   |
  O   |
 /|\  |
      |
      |
=========''', '''
  +---+
  |   |
  O   |
 /|\  |
 /    |
      |
=========''', '''
  +---+
  |   |
  O   |
 /|\  |
 / \  |
      |
=========''']


def load_words():
    """
    Returns a list of valid words. Words are strings of lowercase letters.

    Depending on the size of the word list, this function may
    take a while to finish.
    """
    print("Loading word list from file...")
    # in_file: file
    in_file = open(WORD_LIST_FILENAME, 'r')
    # line: string
    line = in_file.readline()
    # word_list: list of strings
    word_list = line.split()
    print("  ", len(word_list), "words loaded.")
    return word_list


def choose_word(word_list):
    """
    Chooses randomly a word from the word list.
    :param word_list: list of words (strings)
    :return: Returns a word from word list at random
    """
    return random.choice(word_list)


# end of helper code
# -----------------------------------

def is_word_guessed(secret_word, letters_guessed):
    for c in secret_word:
        if not c in letters_guessed:
            return False
    print("YOU HAVE WON")
    return True








def get_guessed_word(secret_word, letters_guessed):
    the_line = ""
    for c in secret_word:
        if not c in letters_guessed:
            the_line += "_"
        else:
            the_line += c
    print(the_line)





def get_available_letters(letters_guessed):
   alphabet = "abcdefghijklmnopqrstuvxyz"
   new_alphabet = ""
   for c in alphabet:
       if not c in letters_guessed:
           new_alphabet += c
   print(new_alphabet)



def hangman(secret_word):
    print("Welcome to Hang Man")
    len_of_secret_word = len(secret_word)
    print(f"The length of the secret word is {len_of_secret_word}")
    letters_guessed = []
    wrong_guesses = 0
    while wrong_guesses != 8:
        print(f"You have {8-wrong_guesses} guesses left! ")
        print(f"You can choose from these letters:")
        get_available_letters(letters_guessed)
        user_guess = input("Please enter your guess here: ")
        while user_guess in letters_guessed:
            user_guess = input("Please enter your guess here: ")
        if user_guess in secret_word:
            print("Your guess was correct!")
        else:
            print("OOOOPPSS thats incorrect!!")
            print(HANG_MAN_PICS[wrong_guesses])
            wrong_guesses += 1
        letters_guessed.append(user_guess)
        get_guessed_word(secret_word, letters_guessed)
        if wrong_guesses == 8:
            print("YOU HAVE LOST!")
            print(f"This was the secret word: {secret_word}")
            break
        if is_word_guessed(secret_word, letters_guessed) == True:
            break
        
    """
    Starts up an interactive game of Hangman.

    At the beginning of the game:
  
 
    6. If the guessed letter is not in the secret word:
        a. Count that as a used guess.
        b. (Bonus) Draw a body part.
        c. If the user has made eight guesses then:
            i. Tell the user he/she has lost.
            ii. Exit the program.
    7. If the guessed letter is in the secret word and the secret word has been completely revealed then:
        a. Congratulate the user.
        b. Exit the program.

    :param secret_word: string, the secret word to guess.
    """



def test_is_word_guessed():
    assert not is_word_guessed("apple", [])
    assert not is_word_guessed("apple", ['i', 'k', 'r', 's'])
    assert not is_word_guessed("apple", ['e', 'i', 'k', 'r', 's'])
    assert not is_word_guessed("apple", ['e', 'i', 'k', 'p', 'r', 's'])
    assert not is_word_guessed("apple", ['e', 'i', 'k', 'p', 'r', 's', 'a'])
    assert is_word_guessed("apple", ['e', 'i', 'k', 'p', 'r', 's', 'a', 'l'])


def test_get_guessed_word():
    assert get_guessed_word("apple", []) == "_____"
    assert get_guessed_word("apple", ['i', 'k', 'r', 's']) == "_____"
    assert get_guessed_word("apple", ['e', 'i', 'k', 'r', 's']) == "____e"
    assert get_guessed_word("apple", ['e', 'i', 'k', 'p', 'r', 's']) == "_pp_e"
    assert get_guessed_word("apple", ['e', 'i', 'k', 'p', 'r', 's', 'a']) == "app_e"
    assert get_guessed_word("apple", ['e', 'i', 'k', 'p', 'r', 's', 'a', 'l']) == "apple"


def test_get_available_letters():
    assert get_available_letters([]) == string.ascii_lowercase
    assert get_available_letters(['e', 'i', 'k', 'p', 'r', 's']) == "abcdfghjlmnoqtuvwxyz"


def test():
    test_is_word_guessed()
    print("\033[0;34mIs the Word Guessed works\t\t\t\t\033[0;33m2 pts")
    test_get_guessed_word()
    print("\033[0;35mGetting the User's Guess works\t\t\t\033[0;33m2 pts")
    test_get_available_letters()
    print("\033[0;36mGetting the Available Letters works\t\t\033[0;33m2 pts")


def test_hangman():
    hangman("apple")


def play_hangman():
    word_list = load_words()
    secret_word = choose_word(word_list).lower()
    hangman(secret_word)


if __name__ == "__main__":
    # TODO: 1. Uncomment to test is_word_guessed, get_guessed_word, get_available_letters.
    # test()

    # TODO: 2. Uncomment to test your hangman program with your own secret word.
    test_hangman()

    # TODO: 3. Uncomment when you are ready to play hangman program with a computer selected secret word.
    # play_hangman()