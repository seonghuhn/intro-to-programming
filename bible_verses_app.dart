import 'dart:math';

import 'package:flutter/material.dart';

void main() => runApp(BibleVerseApp());

class BibleVerseApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: BiblePage(),
    );
  }
}

class BiblePage extends StatefulWidget {
  BiblePageState createState() => BiblePageState();
}

const List<String> _bibleVerses = [
  "The Word became flesh and made his dwelling among us. We have\n"
      "seen his glory, the glory of the one and only Son, who came from the\n"
      "Father, full of grace and truth.\n"
      "John 1:14 NIV",
  "I have been crucified with Christ and I no longer live, but Christ lives\n"
      "in me. The live I now live in the body, I live by faith in the Son of\n"
      "God, who loved me and gave himself for me.\n"
      "Galatians 2:20 NIV",
  "Have mercy on me, O God,\n"
      "according to your unfailing love;\n"
      "according to your great compassion\n"
      "blot out my transgressions.\n"
      "Psalm 51:1 NIV84",
  "A gentle answer turns away wrath,\n"
      "but a harsh word stirs up anger.\n"
      "Proverbs 15:1 NIV",
  "For God so loved the world that he gave his one and only son, that whoever "
      "believes in him should not perish but have eternal life.\n"
      "John 3:16 NIV",
];

List<String> _boxVerses = [_bibleVerses[0], _bibleVerses[1], _bibleVerses[2]];

class BiblePageState extends State {
  Random _random = Random();

  void _changeVerse() {
    setState(() {
      int boxIndex = _random.nextInt(3);
      _boxVerses[boxIndex] = _bibleVerses[_random.nextInt(_bibleVerses.length)];
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Bible Verses"),
      ),
      body: Center(
        child: buildColumn(context),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.message),
        onPressed: _changeVerse,
      ),
    );
  }
}

Widget buildColumn(BuildContext context) {
  return Column(
    mainAxisAlignment: MainAxisAlignment.center,
    crossAxisAlignment: CrossAxisAlignment.stretch,
    children: <Widget>[
      _buildRowOfThree(),
    ],
  );
}

Widget _buildRowOfThree() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    children: <Widget>[
      buildRoundedBox(_boxVerses[0]),
      buildRoundedBox(_boxVerses[1]),
      buildRoundedBox(_boxVerses[2]),
    ],
  );
}

Widget buildRoundedBox(
  String label, {
  double height = 300.0,
}) {
  return Container(
    height: height,
    width: 220.0,
    alignment: Alignment(0.0, 0.0),
    decoration: BoxDecoration(
      color: Colors.white,
      border: Border.all(color: Colors.black),
      borderRadius: BorderRadius.all(
        Radius.circular(10.0),
      ),
    ),
    child: Text(
      label,
      textAlign: TextAlign.center,
      textScaleFactor: 1,
    ),
  );
}
