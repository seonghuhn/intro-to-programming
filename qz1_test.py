import qz1
from qz1_submissions import aubin_lucas_qz1, caruso_lorenzo_qz1, deboard_sean_qz1, kehl_dominic_qz1, kehl_julia_qz1, \
    sepka_jp_qz1, shen_joseph_qz1


def test_count_vowels():
    assert qz1.count_vowels("") == 0
    assert qz1.count_vowels("aeiou") == 5
    assert qz1.count_vowels("AEIOU") == 5
    assert qz1.count_vowels("qwrtp") == 0
    assert qz1.count_vowels("QWRTP") == 0
    assert qz1.count_vowels("Live Oak Academy!") == 7

    assert aubin_lucas_qz1.count_vowels("") == 0
    assert aubin_lucas_qz1.count_vowels("aeiou") == 5
    assert aubin_lucas_qz1.count_vowels("AEIOU") == 5
    assert aubin_lucas_qz1.count_vowels("qwrtp") == 0
    assert aubin_lucas_qz1.count_vowels("QWRTP") == 0
    assert aubin_lucas_qz1.count_vowels("Live Oak Academy!") == 7

    assert caruso_lorenzo_qz1.count_vowels("") == 0
    assert caruso_lorenzo_qz1.count_vowels("aeiou") == 5
    assert caruso_lorenzo_qz1.count_vowels("AEIOU") == 5
    assert caruso_lorenzo_qz1.count_vowels("qwrtp") == 0
    assert caruso_lorenzo_qz1.count_vowels("QWRTP") == 0
    assert caruso_lorenzo_qz1.count_vowels("Live Oak Academy!") == 7

    assert deboard_sean_qz1.count_vowels("") == 0
    # assert deboard_sean_qz1.count_vowels("aeiou") == 5
    # assert deboard_sean_qz1.count_vowels("AEIOU") == 5
    # assert deboard_sean_qz1.count_vowels("qwrtp") == 0
    # assert deboard_sean_qz1.count_vowels("QWRTP") == 0
    # assert deboard_sean_qz1.count_vowels("Live Oak Academy!") == 7

    assert kehl_dominic_qz1.count_vowels("") == 0
    assert kehl_dominic_qz1.count_vowels("aeiou") == 5
    # assert kehl_dominic_qz1.count_vowels("AEIOU") == 5
    assert kehl_dominic_qz1.count_vowels("qwrtp") == 0
    assert kehl_dominic_qz1.count_vowels("QWRTP") == 0
    # assert kehl_dominic_qz1.count_vowels("Live Oak Academy!") == 7

    assert kehl_julia_qz1.count_vowels("") == 0
    #assert kehl_julia_qz1.count_vowels("aeiou") == 5
    #assert kehl_julia_qz1.count_vowels("AEIOU") == 5
    assert kehl_julia_qz1.count_vowels("qwrtp") == 0
    assert kehl_julia_qz1.count_vowels("QWRTP") == 0
    #assert kehl_julia_qz1.count_vowels("Live Oak Academy!") == 7

    assert sepka_jp_qz1.count_vowels("") == 0
    assert sepka_jp_qz1.count_vowels("aeiou") == 5
    # assert sepka_jp_qz1.count_vowels("AEIOU") == 5
    assert sepka_jp_qz1.count_vowels("qwrtp") == 0
    assert sepka_jp_qz1.count_vowels("QWRTP") == 0
    # assert sepka_jp_qz1.count_vowels("Live Oak Academy!") == 7

    assert shen_joseph_qz1.count_vowels("") == 0
    assert shen_joseph_qz1.count_vowels("aeiou") == 5
    assert shen_joseph_qz1.count_vowels("AEIOU") == 5
    assert shen_joseph_qz1.count_vowels("qwrtp") == 0
    assert shen_joseph_qz1.count_vowels("QWRTP") == 0
    assert shen_joseph_qz1.count_vowels("Live Oak Academy!") == 7


def test_is_prime():
    assert qz1.is_prime(0) is False
    assert qz1.is_prime(1) is False
    assert qz1.is_prime(2)
    assert qz1.is_prime(3)
    assert qz1.is_prime(4) is False
    assert qz1.is_prime(5)
    assert qz1.is_prime(6) is False
    assert qz1.is_prime(7)
    assert qz1.is_prime(37)
    assert qz1.is_prime(95) is False

    # assert aubin_lucas_qz1.is_prime(0) is False
    # assert aubin_lucas_qz1.is_prime(1) is False
    assert aubin_lucas_qz1.is_prime(2)
    assert aubin_lucas_qz1.is_prime(3)
    assert aubin_lucas_qz1.is_prime(4) is False
    assert aubin_lucas_qz1.is_prime(5)
    assert aubin_lucas_qz1.is_prime(6) is False
    assert aubin_lucas_qz1.is_prime(7)
    assert aubin_lucas_qz1.is_prime(37)
    assert aubin_lucas_qz1.is_prime(95) is False

    # assert deboard_sean_qz1.is_prime(0) is False
    # assert deboard_sean_qz1.is_prime(1) is False
    # assert deboard_sean_qz1.is_prime(2)
    assert deboard_sean_qz1.is_prime(3)
    assert deboard_sean_qz1.is_prime(4) is False
    assert deboard_sean_qz1.is_prime(5)
    assert deboard_sean_qz1.is_prime(6) is False
    assert deboard_sean_qz1.is_prime(7)
    assert deboard_sean_qz1.is_prime(37)
    # assert deboard_sean_qz1.is_prime(95) is False

    # assert kehl_dominic_qz1.is_prime(0) is False
    # assert kehl_dominic_qz1.is_prime(1) is False
    # assert kehl_dominic_qz1.is_prime(2)
    # assert kehl_dominic_qz1.is_prime(3)
    assert kehl_dominic_qz1.is_prime(4) is False
    # assert kehl_dominic_qz1.is_prime(5)
    assert kehl_dominic_qz1.is_prime(6) is False
    # assert kehl_dominic_qz1.is_prime(7)
    # assert kehl_dominic_qz1.is_prime(37)
    assert kehl_dominic_qz1.is_prime(95) is False

    # assert kehl_julia_qz1.is_prime(0) is False
    # assert kehl_julia_qz1.is_prime(1) is False
    # assert kehl_julia_qz1.is_prime(2)
    assert kehl_julia_qz1.is_prime(3)
    assert kehl_julia_qz1.is_prime(4) is False
    assert kehl_julia_qz1.is_prime(5)
    assert kehl_julia_qz1.is_prime(6) is False
    assert kehl_julia_qz1.is_prime(7)
    assert kehl_julia_qz1.is_prime(37)
    # assert kehl_julia_qz1.is_prime(95) is False

    # assert sepka_jp_qz1.is_prime(0) is False
    # assert sepka_jp_qz1.is_prime(1) is False
    assert sepka_jp_qz1.is_prime(2)
    assert sepka_jp_qz1.is_prime(3)
    assert sepka_jp_qz1.is_prime(4) is False
    assert sepka_jp_qz1.is_prime(5)
    assert sepka_jp_qz1.is_prime(6) is False
    assert sepka_jp_qz1.is_prime(7)
    assert sepka_jp_qz1.is_prime(37)
    assert sepka_jp_qz1.is_prime(95) is False

    # assert shen_joseph_qz1.is_prime(0) is False
    # assert shen_joseph_qz1.is_prime(1) is False
    assert shen_joseph_qz1.is_prime(2)
    assert shen_joseph_qz1.is_prime(3)
    # assert shen_joseph_qz1.is_prime(4) is False
    assert shen_joseph_qz1.is_prime(5)
    assert shen_joseph_qz1.is_prime(6) is False
    assert shen_joseph_qz1.is_prime(7)
    assert shen_joseph_qz1.is_prime(37)
    assert shen_joseph_qz1.is_prime(95) is False
