import datetime
import json


def load_bible():
    # https://github.com/bkuhl/bible-verse-counts-per-chapter/blob/master/bible.json
    with open('bible.json') as f:
        bible = json.load(f)

    return bible


def num_books(bible):
    x = 0
    for c in bible:
        x+=1
    return x


def num_chapters(bible):
    num_of_chapters = 0
    for x in bible:
        num_of_chapters += len(x["chapters"])
    return num_of_chapters


def num_verses(bible):
    num_of_verses = 0
    bible_index = 0
    for x in bible:
        chapter_index = 0
        for c in bible[bible_index]['chapters']:
            verse_per_chapter = bible[bible_index]['chapters'][chapter_index]['verses']
            num_verses_per_chapter = int(verse_per_chapter)
            num_of_verses += num_verses_per_chapter
            chapter_index += 1
        bible_index += 1
    return num_of_verses



def books(bible):
    bible_books = []
    bible_index = 0
    for x in bible:
        book = bible[bible_index]["book"]
        bible_books.append(book)
        bible_index += 1
    return bible_books


def sorted_books(bible):
    x = sorted(books(bible))
    return x


def abbr(book, bible):
    bible_index = 0
    for x in bible:
        if bible[bible_index]["book"] == book:
            return bible[bible_index]['abbr']
        bible_index += 1
    """
    Gets the abbreviations of the specified book.
    """
    return None


def reading_plan(bible):
    """
    Generates a 365 day Bible reading plan.
    """
    now = datetime.datetime.now()
    date = datetime.date(now.year, 1, 1)

    for book in bible:
        for chapter in book['chapters']:
            # read one chapter a day
            print(
                f"{date.strftime('%a %b %d')} {book['abbr']}:{chapter['chapter']}-{book['abbr']}:{chapter['chapter']}")

            # go to next day
            date += datetime.timedelta(1)


def test():
    bible = load_bible()

    assert num_books(bible) == 66
    print("\033[0;34mNumber of Books works\t\t\t\t\t\033[0;33m1 pt")

    assert num_chapters(bible) == 1189
    print("\033[0;36mNumber of Chapters works\t\t\t\t\033[0;33m2 pts")

    assert num_verses(bible) == 31102
    print("\033[0;35mNumber of Verses works\t\t\t\t\t\033[0;33m2 pts")

    assert books(bible) == ['Genesis', 'Exodus', 'Leviticus', 'Numbers', 'Deuteronomy', 'Joshua', 'Judges', 'Ruth',
                            '1 Samuel', '2 Samuel', '1 Kings', '2 Kings', '1 Chronicles', '2 Chronicles', 'Ezra',
                            'Nehemiah', 'Esther', 'Job', 'Psalm', 'Proverbs', 'Ecclesiastes', 'Song of Solomon',
                            'Isaiah', 'Jeremiah', 'Lamentations', 'Ezekiel', 'Daniel', 'Hosea', 'Joel', 'Amos',
                            'Obadiah', 'Jonah', 'Micah', 'Nahum', 'Habakkuk', 'Zephaniah', 'Haggai', 'Zechariah',
                            'Malachi', 'Matthew', 'Mark', 'Luke', 'John', 'Acts', 'Romans', '1 Corinthians',
                            '2 Corinthians', 'Galatians', 'Ephesians', 'Philippians', 'Colossians', '1 Thessalonians',
                            '2 Thessalonians', '1 Timothy', '2 Timothy', 'Titus', 'Philemon', 'Hebrews', 'James',
                            '1 Peter', '2 Peter', '1 John', '2 John', '3 John', 'Jude', 'Revelation']
    print("\033[0;31mBooks works\t\t\t\t\t\t\t\t\033[0;33m2 pts")

    assert sorted_books(bible) == ['1 Chronicles', '1 Corinthians', '1 John', '1 Kings', '1 Peter', '1 Samuel',
                                   '1 Thessalonians', '1 Timothy', '2 Chronicles', '2 Corinthians', '2 John', '2 Kings',
                                   '2 Peter', '2 Samuel', '2 Thessalonians', '2 Timothy', '3 John', 'Acts', 'Amos',
                                   'Colossians', 'Daniel', 'Deuteronomy', 'Ecclesiastes', 'Ephesians', 'Esther',
                                   'Exodus', 'Ezekiel', 'Ezra', 'Galatians', 'Genesis', 'Habakkuk', 'Haggai', 'Hebrews',
                                   'Hosea', 'Isaiah', 'James', 'Jeremiah', 'Job', 'Joel', 'John', 'Jonah', 'Joshua',
                                   'Jude', 'Judges', 'Lamentations', 'Leviticus', 'Luke', 'Malachi', 'Mark', 'Matthew',
                                   'Micah', 'Nahum', 'Nehemiah', 'Numbers', 'Obadiah', 'Philemon', 'Philippians',
                                   'Proverbs', 'Psalm', 'Revelation', 'Romans', 'Ruth', 'Song of Solomon', 'Titus',
                                   'Zechariah', 'Zephaniah']
    print("\033[0;32mSorted Books works\t\t\t\t\t\t\033[0;33m1 pts")

    assert abbr('Genesis', bible) == 'Gen'
    assert abbr('Ruth', bible) == 'Ruth'
    assert abbr('2 Samuel', bible) == '2Sam'
    assert abbr('1 Kings', bible) == '1Kgs'
    assert abbr('1 Corinthians', bible) == '1Cor'
    assert abbr('Revelation', bible) == 'Rev'
    print("\033[0;33mAbbreviation works\t\t\t\t\t\t\033[0;33m2 pts")

    print("\033[0m")
    # reading_plan(bible)


if __name__ == "__main__":
    test()
