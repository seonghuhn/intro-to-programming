import datetime
import json


def load_bible():
    # https://github.com/bkuhl/bible-verse-counts-per-chapter/blob/master/bible.json
    with open('bible.json') as f:
        bible = json.load(f)

    return bible


def num_books(bible):
    """
    Counts the number of books in the Bible.
    """
    return len(bible)


def num_chapters(bible):
    """
    Counts the number of chapters in the Bible.
    """
    total = 0
    for x in bible:
        total += len(x['chapters'])
    return total


def num_verses(bible):
    """
    Counts the number of verses in the Bible.
    """
    total = 0
    for x in bible:
        for y in x['chapters']:
            total += int(y['verses'])
    return total


def books(bible):
    """
    Returns the books of the Bible as a list.
    """
    all_books = []
    for x in bible:
        all_books.append(x['book'])
    return all_books


def sorted_books(bible):
    """
    Returns the books of the Bible as a sorted list.
    """
    books_sorted = sorted(books(bible))
    return books_sorted


def abbr(book, bible):
    """
    Gets the abbreviations of the specified book.
    """
    for x in bible:
        if x['book'] == book:
             return x['abbr']
    return True


def reading_plan(bible):
    '''
    Generates a 365 day Bible reading plan.

    My program makes sure that every day you read at least 69 verses
    and also that you end at the end of a chapter every day.
    If a chapter has at least 69 verses than you just read that chapter.
    But if the chapter has less than 69 verses, than you read that chapter
    and however many more chapters you need to read in order to read at
    least 69 verses. On the last day, you read the last remaining chapters.
    (You are essentially reading two days worth in one day, because without
    that part you would end on Jan 1)
    '''
    now = datetime.datetime.now()
    date = datetime.date(now.year, 1, 1)
    book1 = ''
    book2 = ''
    chapter1 = ''
    chapter2 = ''
    amount_of_verses = 0
    keep_chapter1 = 0
    for x in bible:

        for y in x['chapters']:
            if date.strftime('%b %d') == 'Dec 31':
                if keep_chapter1 == 0:
                    chapter1 = y['chapter']
                book2 = 'Revelation'
                chapter2 = '31'
                print(date.strftime('%a %b %d') + ' ' + book1 + ':' + chapter1 + ' - ' + book2 + ':' + chapter2)

                return
            elif keep_chapter1 == 1:
                amount_of_verses += int(y['verses'])
                if amount_of_verses >= 69:
                    book2 = x['book']
                    chapter2 = y['chapter']
                    print(date.strftime('%a %b %d') + ' ' + book1 + ':' + chapter1 + ' - ' + book2 + ':' + chapter2)
                    keep_chapter1 = 0
                    date += datetime.timedelta(1)
                    amount_of_verses = 0
            else:
                book1 = x['book']
                chapter1 = y['chapter']
                if int(y['verses']) >= 69:
                    book2 = x['book']
                    chapter2 = y['chapter']
                    amount_of_verses = 0
                    print(date.strftime('%a %b %d') + ' ' + book1 + ':' + chapter1 + ' - ' + book2 + ':' + chapter2)
                    date += datetime.timedelta(1)
                else:
                    amount_of_verses += int(y['verses'])
                    keep_chapter1 = 1


       # print(
        #    f"{date.strftime('%a %b %d')} {book1['abbr']}:{chapter1['chapter']}-{book2['abbr']}:{chapter2['chapter']}")


def test():
    bible = load_bible()

    assert num_books(bible) == 66
    print("\033[0;34mNumber of Books works\t\t\t\t\t\033[0;33m1 pt")

    assert num_chapters(bible) == 1189
    print("\033[0;36mNumber of Chapters works\t\t\t\t\033[0;33m2 pts")

    assert num_verses(bible) == 31102
    print("\033[0;35mNumber of Verses works\t\t\t\t\t\033[0;33m2 pts")

    assert books(bible) == ['Genesis', 'Exodus', 'Leviticus', 'Numbers', 'Deuteronomy', 'Joshua', 'Judges', 'Ruth',
                            '1 Samuel', '2 Samuel', '1 Kings', '2 Kings', '1 Chronicles', '2 Chronicles', 'Ezra',
                            'Nehemiah', 'Esther', 'Job', 'Psalm', 'Proverbs', 'Ecclesiastes', 'Song of Solomon',
                            'Isaiah', 'Jeremiah', 'Lamentations', 'Ezekiel', 'Daniel', 'Hosea', 'Joel', 'Amos',
                            'Obadiah', 'Jonah', 'Micah', 'Nahum', 'Habakkuk', 'Zephaniah', 'Haggai', 'Zechariah',
                            'Malachi', 'Matthew', 'Mark', 'Luke', 'John', 'Acts', 'Romans', '1 Corinthians',
                            '2 Corinthians', 'Galatians', 'Ephesians', 'Philippians', 'Colossians', '1 Thessalonians',
                            '2 Thessalonians', '1 Timothy', '2 Timothy', 'Titus', 'Philemon', 'Hebrews', 'James',
                            '1 Peter', '2 Peter', '1 John', '2 John', '3 John', 'Jude', 'Revelation']
    print("\033[0;31mBooks works\t\t\t\t\t\t\t\t\033[0;33m2 pts")

    assert sorted_books(bible) == ['1 Chronicles', '1 Corinthians', '1 John', '1 Kings', '1 Peter', '1 Samuel',
                                   '1 Thessalonians', '1 Timothy', '2 Chronicles', '2 Corinthians', '2 John', '2 Kings',
                                   '2 Peter', '2 Samuel', '2 Thessalonians', '2 Timothy', '3 John', 'Acts', 'Amos',
                                   'Colossians', 'Daniel', 'Deuteronomy', 'Ecclesiastes', 'Ephesians', 'Esther',
                                   'Exodus', 'Ezekiel', 'Ezra', 'Galatians', 'Genesis', 'Habakkuk', 'Haggai', 'Hebrews',
                                   'Hosea', 'Isaiah', 'James', 'Jeremiah', 'Job', 'Joel', 'John', 'Jonah', 'Joshua',
                                   'Jude', 'Judges', 'Lamentations', 'Leviticus', 'Luke', 'Malachi', 'Mark', 'Matthew',
                                   'Micah', 'Nahum', 'Nehemiah', 'Numbers', 'Obadiah', 'Philemon', 'Philippians',
                                   'Proverbs', 'Psalm', 'Revelation', 'Romans', 'Ruth', 'Song of Solomon', 'Titus',
                                   'Zechariah', 'Zephaniah']
    print("\033[0;32mSorted Books works\t\t\t\t\t\t\033[0;33m1 pts")

    assert abbr('Genesis', bible) == 'Gen'
    assert abbr('Ruth', bible) == 'Ruth'
    assert abbr('2 Samuel', bible) == '2Sam'
    assert abbr('1 Kings', bible) == '1Kgs'
    assert abbr('1 Corinthians', bible) == '1Cor'
    assert abbr('Revelation', bible) == 'Rev'
    print("\033[0;33mAbbreviation works\t\t\t\t\t\t\033[0;33m2 pts")

    print("\033[0m")
    reading_plan(bible)


if __name__ == "__main__":
    test()
