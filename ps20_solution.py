class MySet(object):
    def __init__(self):
        self._vals = []
        self._len = 0
        for _ in range(37):
            self._vals.append([])

    def insert(self, e):
        idx = hash(e) % len(self._vals)
        if e not in self._vals[idx]:
            self._vals[idx].append(e)
            self._len += 1

    def member(self, e):
        idx = hash(e) % len(self._vals)
        return e in self._vals[idx]

    def remove(self, e):
        idx = hash(e) % len(self._vals)
        vals = self._vals[idx]
        for ii in range(len(vals) - 1, -1, -1):
            if vals[ii] == e:
                del vals[ii]
                self._len -= 1
                return
        raise ValueError

    def intersect(self, other):
        intersection = MySet()
        for vals in self._vals:
            for val in vals:
                if other.member(val):
                    intersection.insert(val)
        return intersection

    def __len__(self):
        return self._len

    def __str__(self):
        vals = []
        for vv in self._vals:
            vals.extend(vv)
        vals.sort()
        return '{' + ','.join([str(e) for e in vals]) + '}'


if __name__ == "__main__":
    # create set 1
    set_1 = MySet()
    assert len(set_1) == 0
    assert str(set_1) == "{}"

    # create set 2
    set_2 = MySet()
    assert len(set_2) == 0
    assert str(set_2) == "{}"

    # test intersection of two empty sets
    set_3 = set_1.intersect(set_2)
    assert len(set_3) == 0

    # test insert, remove, member
    set_1.insert(-17)
    assert len(set_1) == 1
    assert set_1.member(-17) is True
    set_1.insert(-12)
    set_1.insert(-6)
    set_1.insert(20)
    set_1.insert(3.14)
    assert len(set_1) == 5
    set_1.insert(3.14)
    assert len(set_1) == 5
    assert str(set_1) == "{-17,-12,-6,3.14,20}"
    print("\033[0;32mSet Insert works\t\t\t\t\033[0;33m2 pts")
    assert set_1.member(-12) is True
    assert set_1.member(-6) is True
    assert set_1.member(3.14) is True
    assert set_1.member(20) is True
    set_1.remove(-17)
    assert set_1.member(-17) is False
    assert len(set_1) == 4
    assert str(set_1) == "{-12,-6,3.14,20}"
    print("\033[0;36mSet Member works\t\t\t\t\033[0;33m2 pts")
    try:
        set_1.remove(-17)
        assert False
    except ValueError:
        print("\033[0;35mSet Remove works\t\t\t\t\033[0;33m2 pts")

    # test intersection
    set_3 = set_1.intersect(set_2)
    assert isinstance(set_3, MySet)
    assert len(set_3) == 0
    set_2.insert(-20)
    set_2.insert(-16)
    set_2.insert(-11)
    assert len(set_2) == 3
    assert str(set_2) == "{-20,-16,-11}"
    set_3 = set_1.intersect(set_2)
    assert len(set_3) == 0
    set_2.insert(3.14)
    set_3 = set_1.intersect(set_2)
    assert len(set_3) == 1
    assert str(set_3) == "{3.14}"

    print("\033[0;34mSet Intersect works\t\t\t\t\033[0;33m2 pts")

    # test _len__, __str__
    set_4 = MySet()
    set_4.insert(22)
    set_4.insert(2)
    set_4.insert(3)
    assert len(set_4) == 3
    assert str(set_4) == "{2,3,22}"
    set_4.remove(2)
    assert len(set_4) == 2
    assert str(set_4) == "{3,22}"

    print("\033[0;37mLen and Str works\t\t\t\t\033[0;33m2 pts")
