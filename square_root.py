EPSILON = 0.1


def brute_force_guess(num):
    guess = 0
    increment = 0.01
    num_guesses = 1
    while (num - guess ** 2) > EPSILON:
        guess += increment
        num_guesses += 1
    print(f"The square root of {num} is {guess}.")
    print(f"It took {num_guesses} guesses.")


def binary_guess(num):
    lo = 0
    hi = num
    guess = (lo + hi) / 2.0
    num_guesses = 1
    while abs(num - guess ** 2) > EPSILON:
        if guess ** 2 > num:
            hi = guess
        else:
            lo = guess
        num_guesses += 1
        guess = (lo + hi) / 2.0
    print(f"The square root of {num} is {guess}.")
    print(f"It took {num_guesses} guesses.")


def main():
    while num := input("Number: "):
        brute_force_guess(int(num))
        binary_guess(int(num))


if __name__ == "__main__":
    main()
