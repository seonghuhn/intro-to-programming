class Matrix(object):

    def __init__(self, rows, cols):
        self._matrix = _init(rows, cols)

    def __getitem__(self, row):
        return self._matrix[row]

    def find(self, num):
        # for row in self._matrix:
        #     if num in row:
        #         return True
        # return False
        return len([num for row in self._matrix for _num in row if _num == num]) > 0

    def map(self, f):
        # for row in self._matrix:
        #     for xx in range(len(row)):
        #         row[xx] = f(row[xx])
        # for yy in range(len(self._matrix)):
        #     self._matrix[yy] = list(map(f, self._matrix[yy]))
        # self._matrix = [list(map(f, row)) for row in self._matrix]
        self._matrix = [[f(num) for num in row] for row in self._matrix]

    def transpose(self):
        # matrix = _init(len(self._matrix[0]), len(self._matrix))
        # for yy in range(len(self._matrix)):
        #     for xx in range(len(self._matrix[0])):
        #         matrix[xx][yy] = self._matrix[yy][xx]
        # self._matrix = matrix
        self._matrix = [[self._matrix[yy][xx] for yy in range(len(self._matrix))] for xx in range(len(self._matrix[0]))]

    def add(self, other):
        # _sum = Matrix(len(self._matrix), len(other[0]))
        # for yy in range(len(self._matrix)):
        #     for xx in range(len(self._matrix[0])):
        #         _sum[yy][xx] = self._matrix[yy][xx] + other[yy][xx]
        # return _sum
        return [[self._matrix[yy][xx] + other[yy][xx] for xx in range(len(self._matrix[0]))]
                for yy in range(len(self._matrix))]

    def multiply(self, other):
        product = Matrix(len(self._matrix), len(other[0]))
        for yy in range(len(self._matrix)):
            for other_xx in range(len(other[0])):
                _sum = 0
                for xx in range(len(self._matrix[0])):
                    _sum += self._matrix[yy][xx] * other[xx][other_xx]
                product[yy][other_xx] = _sum
        return product


def _init(rows, cols):
    # matrix = []
    # for _ in range(rows):
    #     row = []
    #     for _ in range(cols):
    #         row.append(0)
    # return matrix
    return [[0 for _ in range(cols)] for _ in range(rows)]


def init():
    """
    Creates a matrix with 40 at (0, 0), 7 at (3, 2), 0 everywhere else.
    """
    matrix = Matrix(4, 3)
    for row in range(4):
        for col in range(3):
            assert matrix[row][col] == 0
    matrix[0][0] = 40
    matrix[3][2] = 7
    return matrix


def test():
    # initialize
    matrix = init()
    assert matrix[0][0] == 40
    assert matrix[0][1] == 0
    assert matrix[0][2] == 0
    assert matrix[1][0] == 0
    assert matrix[1][1] == 0
    assert matrix[1][2] == 0
    assert matrix[2][0] == 0
    assert matrix[2][1] == 0
    assert matrix[2][2] == 0
    assert matrix[3][0] == 0
    assert matrix[3][1] == 0
    assert matrix[3][2] == 7

    # test find
    assert matrix.find(0)
    assert matrix.find(-1) is False
    assert matrix.find(40)
    assert matrix.find(7)
    assert matrix.find(6) is False
    matrix[3][2] = 70
    assert matrix.find(70)
    assert matrix.find(7) is False

    # test map
    matrix = init()
    matrix.map(lambda x: x + 1)
    assert matrix[0][0] == 41
    assert matrix[0][1] == 1
    assert matrix[0][2] == 1
    assert matrix[1][0] == 1
    assert matrix[1][1] == 1
    assert matrix[1][2] == 1
    assert matrix[2][0] == 1
    assert matrix[2][1] == 1
    assert matrix[2][2] == 1
    assert matrix[3][0] == 1
    assert matrix[3][1] == 1
    assert matrix[3][2] == 8

    # test transpose
    matrix = init()
    matrix[1][1] = 2
    matrix[1][2] = 3
    matrix[2][1] = 4
    matrix.transpose()
    assert matrix[0][0] == 40
    assert matrix[0][1] == 0
    assert matrix[0][2] == 0
    assert matrix[0][3] == 0
    assert matrix[1][0] == 0
    assert matrix[1][1] == 2
    assert matrix[1][2] == 4
    assert matrix[1][3] == 0
    assert matrix[2][0] == 0
    assert matrix[2][1] == 3
    assert matrix[2][2] == 0
    assert matrix[2][3] == 7

    # test add
    matrix_1 = Matrix(1, 3)
    matrix_1[0][0] = 7
    matrix_1[0][1] = 0
    matrix_1[0][2] = 0
    matrix_2 = Matrix(1, 3)
    matrix_2[0][0] = 1
    matrix_2[0][1] = 4
    matrix_2[0][2] = 2
    matrix_3 = matrix_1.add(matrix_2)
    assert matrix_3[0][0] == 8
    assert matrix_3[0][1] == 4
    assert matrix_3[0][2] == 2

    matrix_1 = Matrix(2, 2)
    matrix_1[0][0] = 3
    matrix_1[0][1] = 8
    matrix_1[1][0] = 4
    matrix_1[1][1] = 6
    matrix_2 = Matrix(2, 2)
    matrix_2[0][0] = 4
    matrix_2[0][1] = 0
    matrix_2[1][0] = 1
    matrix_2[1][1] = -9
    matrix_3 = matrix_1.add(matrix_2)
    assert matrix_3[0][0] == 7
    assert matrix_3[0][1] == 8
    assert matrix_3[1][0] == 5
    assert matrix_3[1][1] == -3

    # test multiply
    matrix_1 = Matrix(2, 3)
    num = 1
    for yy in range(2):
        for xx in range(3):
            matrix_1[yy][xx] = num
            num += 1
    matrix_2 = Matrix(3, 2)
    for yy in range(3):
        for xx in range(2):
            matrix_2[yy][xx] = num
            num += 1
    matrix_3 = matrix_1.multiply(matrix_2)
    assert matrix_3[0][0] == 58
    assert matrix_3[0][1] == 64
    assert matrix_3[1][0] == 139
    assert matrix_3[1][1] == 154


if __name__ == '__main__':
    test()
