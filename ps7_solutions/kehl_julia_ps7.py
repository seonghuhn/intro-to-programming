def maximum(a, b = 0, c = 0):
    if b <= a >= c:
        return a
    if b >= c:
        return b
    if a <= c >= b:
        return c

def factorial(a):
    product = 1
    while a > 0:
        product *= a
        a -= 1
    return product


def fibonacci(n):
    n1 = 0
    n2 = 1
    count = 2
    if n == 1:
        return n1
    if n == 2:
        return n2
    while (n > count):
        nth_term = n1 + n2
        n1 = n2
        n2 = nth_term
        count += 1
    return nth_term


if __name__ == "__main__":
    print(maximum())
    print(factorial())
    print(fibonacci())
