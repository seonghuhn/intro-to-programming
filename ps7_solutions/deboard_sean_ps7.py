def maximum(a=0, b=0, c=0):
    if a != b and b != c and a != c:
        if a > b:
            if a > c:
                return "A is the greatest"
            else:
                return "C is the greatest"
        elif b > a:
            if b > c:
                return "B is the greatest"
            else:
                return "C is the greatest"
        elif c > a or c > b:
            return "C is the greatest"
    else:
        return "The numbers are equal"


def factorial(s):
    if s == 0:
        return 1
    for x in range(1, s):
        s = s * x
    return s


def fibonacci(num):
    first_num = 0
    second_num = 1
    if num == 0:
        return 0
    if num == 1:
        return 1
    else:
        for x in range(2, num):
            fib = first_num + second_num
            first_num = second_num
            second_num = fib
        return second_num
