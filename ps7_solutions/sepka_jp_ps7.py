def maximum(a=-1, b=-1, c=-1):
    if a > b and a > c:
        return a
        pass
    if b > a and b > c:
        return b
    else:
        return c


def factorial(n):
    a = 1
    for x in range(n):
        a = (x+1) * a
    return a


def fibonacci(n):
    a = 0
    b = 1
    for x in range(n-1):
        c = a + b
        a = b
        b = c
    return a


if __name__ == "__main__":
    assert maximum(3, 2, 1) == 3
    assert maximum(3, 1) == 3
    assert factorial(0) == 1
    assert factorial(3) == 6
    assert fibonacci(1) == 0
    assert fibonacci(3) == 1
    assert fibonacci(5) == 3
