def maximum(a, b=-1, c=-1):
    return a if a > b and a > c else b if b > c else c


def factorial(n: int):
    result = 1
    for i in range(1, n+1):
        result *= i
    return result


def fibonacci(n):
    sequence = [0, 1]
    while len(sequence) < n:
        sequence.append(sequence[-1] + sequence[-2])
    return sequence[n-1]


if __name__ == "__main__":
    assert maximum(3, 2, 1) == 3
    assert maximum(3, 1) == 3
    assert factorial(0) == 1
    assert factorial(3) == 6
    assert fibonacci(1) == 0
    assert fibonacci(3) == 1
    assert fibonacci(5) == 3