def maximum(x, y=None, z=None):
    big = x
    if y is not None and y > big:
        big = y
    if z is not None and z > big:
        big = z
    return big

def factorial(n):
    result = 1
    for x in range(2, n + 1):
        result *= x
    return result

def fibonacci(n):
    if n == 1:
        return 0
    a = 0
    b = 1
    c = 1
    n -= 2
    while n:
        c = a + b
        a = b
        b = c
        n -= 1
    return c
