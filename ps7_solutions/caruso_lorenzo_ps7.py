def maximum(a, b=0, c=0):
    if a > b and a > c:
        return 'a'
    if b > c and b > a:
        return 'b'
    if c > a and c > b:
        return 'c'


def factorial(n):
    num = 1
    while n > 0:
        num *= n
        n -= 1
    return num


def fibonacci(n):
    num1 = 0
    num2 = 1
    count = 1
    if n == 1:
        return 0
    if n == 2:
        return 1
    if n == 3:
        return 2
    while n != count:
        num = num1 + num2
        num1 = num2
        num2 = num
        count += 1
    return num


if __name__ == "__main__":
    print(maximum(3, 2, 1))
    print(factorial(6))
    print(fibonacci(100))
