import caesar_cipher
from ps6_solutions import aubin_lucas_ps6, sepka_jp_ps6, shen_joseph_ps6


def test_shift():
    assert caesar_cipher.shift_uc("ABCDEFGHIJKLMNOPQRSTUVWXYZ", 0) == "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    assert caesar_cipher.shift_uc("ABCDEFGHIJKLMNOPQRSTUVWXYZ", 1) == "BCDEFGHIJKLMNOPQRSTUVWXYZA"
    assert caesar_cipher.shift_uc("ABCDEFGHIJKLMNOPQRSTUVWXYZ", 3) == "DEFGHIJKLMNOPQRSTUVWXYZABC"
    assert caesar_cipher.shift_uc("BCDEFGHIJKLMNOPQRSTUVWXYZA", -1) == "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    assert caesar_cipher.shift_uc("ABCDEFGHIJKLMNOPQRSTUVWXYZ", -1) == "ZABCDEFGHIJKLMNOPQRSTUVWXY"
    assert caesar_cipher.shift_uc("ZABCDEFGHIJKLMNOPQRSTUVWXY", -3) == "WXYZABCDEFGHIJKLMNOPQRSTUV"

    assert caesar_cipher.shift_alpha("abcdefghijklmnopqrstuvwxyz", 0) == "abcdefghijklmnopqrstuvwxyz"
    assert caesar_cipher.shift_alpha("abcdefghijklmnopqrstuvwxyz", 1) == "bcdefghijklmnopqrstuvwxyza"
    assert caesar_cipher.shift_alpha("abcdefghijklmnopqrstuvwxyz", 3) == "defghijklmnopqrstuvwxyzabc"
    assert caesar_cipher.shift_alpha("bcdefghijklmnopqrstuvwxyza", -1) == "abcdefghijklmnopqrstuvwxyz"
    assert caesar_cipher.shift_alpha("abcdefghijklmnopqrstuvwxyz", -1) == "zabcdefghijklmnopqrstuvwxy"
    assert caesar_cipher.shift_alpha("zabcdefghijklmnopqrstuvwxy", -3) == "wxyzabcdefghijklmnopqrstuv"

    assert caesar_cipher.shift("Vale in pace. Roma Victa!", 18) == "Nsdw af hsuw. Jges Nauls!"
    assert caesar_cipher.shift("Nsdw af hsuw. Jges Nauls!", -18) == "Vale in pace. Roma Victa!"
    assert caesar_cipher.shift(
        "Nothing is covered up that will not be revealed, or hidden that will not be known. Luke 12:2 ESV",
        -3) == "Klqefkd fp zlsboba rm qexq tfii klq yb obsbxiba, lo efaabk qexq tfii klq yb hkltk. Irhb 12:2 BPS"
    assert caesar_cipher.shift(
        "Klqefkd fp zlsboba rm qexq tfii klq yb obsbxiba, lo efaabk qexq tfii klq yb hkltk. Irhb 12:2 BPS",
        3) == "Nothing is covered up that will not be revealed, or hidden that will not be known. Luke 12:2 ESV"
    assert caesar_cipher.shift_unicode(
        "Nothing is covered up that will not be revealed, or hidden that will not be known. Luke 12:2 ESV",
        -3) == "Klqefkd fp zlsboba rm qexq tfii klq yb obsbxiba, lo efaabk qexq tfii klq yb hkltk. Irhb 12:2 BPS"
    assert caesar_cipher.shift_unicode(
        "Klqefkd fp zlsboba rm qexq tfii klq yb obsbxiba, lo efaabk qexq tfii klq yb hkltk. Irhb 12:2 BPS",
        3) == "Nothing is covered up that will not be revealed, or hidden that will not be known. Luke 12:2 ESV"

    assert aubin_lucas_ps6.encode("ABCDEFGHIJKLMNOPQRSTUVWXYZ", 0) == "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    assert aubin_lucas_ps6.encode("ABCDEFGHIJKLMNOPQRSTUVWXYZ", 1) == "BCDEFGHIJKLMNOPQRSTUVWXYZA"
    assert aubin_lucas_ps6.encode("ABCDEFGHIJKLMNOPQRSTUVWXYZ", 3) == "DEFGHIJKLMNOPQRSTUVWXYZABC"
    assert aubin_lucas_ps6.encode("BCDEFGHIJKLMNOPQRSTUVWXYZA", -1) == "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    assert aubin_lucas_ps6.encode("ABCDEFGHIJKLMNOPQRSTUVWXYZ", -1) == "ZABCDEFGHIJKLMNOPQRSTUVWXY"
    assert aubin_lucas_ps6.encode("ZABCDEFGHIJKLMNOPQRSTUVWXY", -3) == "WXYZABCDEFGHIJKLMNOPQRSTUV"

    assert aubin_lucas_ps6.encode("abcdefghijklmnopqrstuvwxyz", 0) == "abcdefghijklmnopqrstuvwxyz"
    assert aubin_lucas_ps6.encode("abcdefghijklmnopqrstuvwxyz", 1) == "bcdefghijklmnopqrstuvwxyza"
    assert aubin_lucas_ps6.encode("abcdefghijklmnopqrstuvwxyz", 3) == "defghijklmnopqrstuvwxyzabc"
    assert aubin_lucas_ps6.encode("bcdefghijklmnopqrstuvwxyza", -1) == "abcdefghijklmnopqrstuvwxyz"
    assert aubin_lucas_ps6.encode("abcdefghijklmnopqrstuvwxyz", -1) == "zabcdefghijklmnopqrstuvwxy"
    assert aubin_lucas_ps6.encode("zabcdefghijklmnopqrstuvwxy", -3) == "wxyzabcdefghijklmnopqrstuv"

    assert aubin_lucas_ps6.encode("Vale in pace. Roma Victa!", 18) == "Nsdw af hsuw. Jges Nauls!"
    assert aubin_lucas_ps6.encode("Nsdw af hsuw. Jges Nauls!", -18) == "Vale in pace. Roma Victa!"
    assert aubin_lucas_ps6.encode(
        "Nothing is covered up that will not be revealed, or hidden that will not be known. Luke 12:2 ESV",
        -3) == "Klqefkd fp zlsboba rm qexq tfii klq yb obsbxiba, lo efaabk qexq tfii klq yb hkltk. Irhb 12:2 BPS"
    assert aubin_lucas_ps6.encode(
        "Klqefkd fp zlsboba rm qexq tfii klq yb obsbxiba, lo efaabk qexq tfii klq yb hkltk. Irhb 12:2 BPS",
        3) == "Nothing is covered up that will not be revealed, or hidden that will not be known. Luke 12:2 ESV"

    assert sepka_jp_ps6.cipher("ABCDEFGHIJKLMNOPQRSTUVWXYZ", 0) == "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    assert sepka_jp_ps6.cipher("ABCDEFGHIJKLMNOPQRSTUVWXYZ", 1) == "BCDEFGHIJKLMNOPQRSTUVWXYZA"
    assert sepka_jp_ps6.cipher("ABCDEFGHIJKLMNOPQRSTUVWXYZ", 3) == "DEFGHIJKLMNOPQRSTUVWXYZABC"
    assert sepka_jp_ps6.cipher("BCDEFGHIJKLMNOPQRSTUVWXYZA", -1) == "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    assert sepka_jp_ps6.cipher("ABCDEFGHIJKLMNOPQRSTUVWXYZ", -1) == "ZABCDEFGHIJKLMNOPQRSTUVWXY"
    assert sepka_jp_ps6.cipher("ZABCDEFGHIJKLMNOPQRSTUVWXY", -3) == "WXYZABCDEFGHIJKLMNOPQRSTUV"

    assert sepka_jp_ps6.cipher("abcdefghijklmnopqrstuvwxyz", 0) == "abcdefghijklmnopqrstuvwxyz"
    assert sepka_jp_ps6.cipher("abcdefghijklmnopqrstuvwxyz", 1) == "bcdefghijklmnopqrstuvwxyza"
    assert sepka_jp_ps6.cipher("abcdefghijklmnopqrstuvwxyz", 3) == "defghijklmnopqrstuvwxyzabc"
    assert sepka_jp_ps6.cipher("bcdefghijklmnopqrstuvwxyza", -1) == "abcdefghijklmnopqrstuvwxyz"
    assert sepka_jp_ps6.cipher("abcdefghijklmnopqrstuvwxyz", -1) == "zabcdefghijklmnopqrstuvwxy"
    assert sepka_jp_ps6.cipher("zabcdefghijklmnopqrstuvwxy", -3) == "wxyzabcdefghijklmnopqrstuv"

    assert sepka_jp_ps6.cipher("Vale in pace. Roma Victa!", 18) == "Nsdw af hsuw. Jges Nauls!"
    assert sepka_jp_ps6.cipher("Nsdw af hsuw. Jges Nauls!", -18) == "Vale in pace. Roma Victa!"
    assert sepka_jp_ps6.cipher(
        "Nothing is covered up that will not be revealed, or hidden that will not be known. Luke 12:2 ESV",
        -3) == "Klqefkd fp zlsboba rm qexq tfii klq yb obsbxiba, lo efaabk qexq tfii klq yb hkltk. Irhb 12:2 BPS"
    assert sepka_jp_ps6.cipher(
        "Klqefkd fp zlsboba rm qexq tfii klq yb obsbxiba, lo efaabk qexq tfii klq yb hkltk. Irhb 12:2 BPS",
        3) == "Nothing is covered up that will not be revealed, or hidden that will not be known. Luke 12:2 ESV"

    assert shen_joseph_ps6.encrypt("ABCDEFGHIJKLMNOPQRSTUVWXYZ", 0) == "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    assert shen_joseph_ps6.encrypt("ABCDEFGHIJKLMNOPQRSTUVWXYZ", 1) == "BCDEFGHIJKLMNOPQRSTUVWXYZA"
    assert shen_joseph_ps6.encrypt("ABCDEFGHIJKLMNOPQRSTUVWXYZ", 3) == "DEFGHIJKLMNOPQRSTUVWXYZABC"
    assert shen_joseph_ps6.encrypt("BCDEFGHIJKLMNOPQRSTUVWXYZA", -1) == "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    assert shen_joseph_ps6.encrypt("ABCDEFGHIJKLMNOPQRSTUVWXYZ", -1) == "ZABCDEFGHIJKLMNOPQRSTUVWXY"
    assert shen_joseph_ps6.encrypt("ZABCDEFGHIJKLMNOPQRSTUVWXY", -3) == "WXYZABCDEFGHIJKLMNOPQRSTUV"

    assert shen_joseph_ps6.encrypt("abcdefghijklmnopqrstuvwxyz", 0) == "abcdefghijklmnopqrstuvwxyz"
    assert shen_joseph_ps6.encrypt("abcdefghijklmnopqrstuvwxyz", 1) == "bcdefghijklmnopqrstuvwxyza"
    assert shen_joseph_ps6.encrypt("abcdefghijklmnopqrstuvwxyz", 3) == "defghijklmnopqrstuvwxyzabc"
    assert shen_joseph_ps6.encrypt("bcdefghijklmnopqrstuvwxyza", -1) == "abcdefghijklmnopqrstuvwxyz"
    assert shen_joseph_ps6.encrypt("abcdefghijklmnopqrstuvwxyz", -1) == "zabcdefghijklmnopqrstuvwxy"
    assert shen_joseph_ps6.encrypt("zabcdefghijklmnopqrstuvwxy", -3) == "wxyzabcdefghijklmnopqrstuv"


if __name__ == "__main__":
    test_shift()
