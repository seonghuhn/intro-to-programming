def power(base, exp):
    if exp == 0:
        return 1
    else:
        return base * power(base, exp - 1)


def fibonacci(n):
    if n == 1:
        return 0
    if n == 2:
        return 1
    else:
        return fibonacci(n - 1) + fibonacci(n - 2)


def is_in(c, s):
    if s == "":
        return False
    if len(s) == 1:
        return c == s
    elif c == s[len(s)//2]:
        return True
    else:
        if c < s[len(s)//2]:
            return is_in(c, s[0:len(s)//2])
        elif c > s[len(s)//2]:
            return is_in(c, s[len(s)//2:])
    return is_in(c, s)


def is_in_efficient(c, s):
    """
    Is c in s?
    :param c: the character
    :param s: the string
    :return: True if c is in s; False otherwise.
    """
    # TODO: Solve without creating new strings.
    return is_in(c, s)


if __name__ == "__main__":
    assert power(1, 0) == 1
    assert power(1, 1) == 1
    assert power(1, 100) == 1

    assert power(2, 0) == 1
    assert power(2, 1) == 2
    assert power(2, 5) == 32
    assert power(2, 10) == 1024
    assert power(2, 100) == 2 ** 100

    assert power(-3, 0) == 1
    assert power(-3, 1) == -3
    assert power(-3, 5) == (-3) ** 5
    assert power(-3, 10) == (-3) ** 10
    assert power(-3, 100) == (-3) ** 100

    print("\033[0;34mPower works\t\t\t\t\033[0;33m2 pts")

    assert fibonacci(1) == 0
    assert fibonacci(2) == 1
    assert fibonacci(3) == 1
    assert fibonacci(4) == 2
    assert fibonacci(5) == 3
    assert fibonacci(6) == 5
    assert fibonacci(7) == 8
    assert fibonacci(10) == 34
    assert fibonacci(13) == 144

    print("\033[0;35mFibonacci works\t\t\t\033[0;33m2 pts")

    assert not is_in("f", "")
    assert not is_in("f", "g")
    assert not is_in("f", "knotty")
    assert is_in("k", "knotty")
    assert is_in("n", "knotty")
    assert is_in("o", "knotty")
    assert is_in("t", "knotty")
    assert is_in("y", "knotty")
    assert is_in("f", "filly")
    assert is_in("e", "beefy")
    assert is_in("t", "accost")
    assert is_in("s", "choosy")

    print("\033[0;36mIs In works\t\t\t\t\033[0;33m2 pts")

    assert not is_in_efficient("f", "")
    assert not is_in_efficient("f", "g")
    assert not is_in_efficient("f", "knotty")
    assert is_in_efficient("k", "knotty")
    assert is_in_efficient("n", "knotty")
    assert is_in_efficient("o", "knotty")
    assert is_in_efficient("t", "knotty")
    assert is_in_efficient("y", "knotty")
    assert is_in_efficient("f", "filly")
    assert is_in_efficient("e", "beefy")
    assert is_in_efficient("t", "accost")
    assert is_in_efficient("s", "choosy")

    print("\033[1;31mIs In Efficient works\t\033[1;33m2 bonus pts")
