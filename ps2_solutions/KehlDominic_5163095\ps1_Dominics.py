import time
def jedi_mind_trick(storm_troopers_request):
    """
    Determines the Jedi mind trick response to the Storm Troopers request.
    :param storm_troopers_request: the Storm Troopers request, e.g. "Droids"
    :return: the Jedi mink trick response, e.g. "These aren't the droids you're looking for."
    """
    # TODO: Determine correct response to storm_troopers_request

    if 'jedi' in storm_troopers_request.lower():
        return "\033[0;36;20mYou will remove these restraints and leave the cell with the door open. \n" \
                "\033[0;37;20mI will remove these restraints and leave the cell with the door open.  "
    if 'droid' in storm_troopers_request.lower():
        return '\033[0;36;20mThese aren\'t the droids you\'re looking for. \n\033[0;37;20mThese are\'nt the droids your looking for '
    if 'obi-wan' in storm_troopers_request.lower():
        return '\033[0;36;20m It\'s okay that we\'re here. \n\033[0;37;20m It\'s okay that you\'re here'
    if 'rebels' or 'rebel' in storm_troopers_request.lower():
        return '\033[0;36;20mYou want to go home and rethink your life. \n\033[0;37;20m I want to go home and rethink my ' \
               'life. '
    if not 'jedi, droid, obi-wan, rebels' in storm_troopers_request.lower():
        return '\033[0;37;20m I find your lack of faith disturbing.'
    return


def robot():
    # TODO: Greet Storm Troopers and ask whom they are looking for.
    print('\033[0;36;20mHello there')
    time.sleep(1)
    storm_troopers_request = input('\033[0;37;20mIs there someone you are looking for \n')

    print(jedi_mind_trick(storm_troopers_request))

    # TODO: Wish Storm Troopers a pleasant day.
    if storm_troopers_request != 'I find your lack of faith disturbing.':
        time.sleep(1)
        print('\033[0;36;20mThank you have a pleasant day.\n')

# Start the robot program
robot()
