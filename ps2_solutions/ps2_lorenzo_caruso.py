def jedi_mind_trick(storm_troopers_request):
    """
    Determines the Jedi mind trick response to the Storm Troopers request.
    :param storm_troopers_request: the Storm Troopers request, e.g. "Droids"
    :return: the Jedi mink trick response, e.g. "These aren't the droids you're looking for."
    """
    if 'jedi' in (storm_troopers_request).lower():
        return 'You will remove these restraints and leave the cell with the door open.'
    elif 'droids' in (storm_troopers_request).lower():
        return "These aren't the droids you're looking for."
    elif 'obi-wan' in (storm_troopers_request).lower():
        return "It's okay that we're here."
    elif 'rebels' in (storm_troopers_request).lower():
        return 'You want to go home and rethink your life.'
    else:
        return 'I find your lack of faith disturbing.'


def main():
    print('Good Afternoon, my friends.')
    storm_troopers_request = input('Who can I help you locate today?    ')
    print(jedi_mind_trick(storm_troopers_request))
    print('Farewell, enjoy terrorizing the galaxy!')


if __name__ == "__main__":
    main()
