
def jedi_mind_trick(storm_troopers_request):
    """
    Determines the Jedi mind trick response to the Storm Troopers request.
    :param storm_troopers_request: the Storm Troopers request, e.g. "Droids"
    :return: the Jedi mink trick response, e.g. "These aren't the droids you're looking for."
    """
    if "jedi" in storm_troopers_request.lower():
        return "You will remove these restraints and leave the cell with the door open."
    elif "droids" in storm_troopers_request.lower():
        return "These aren't the droids you're looking for."
    elif "obi-wan" in storm_troopers_request.lower():
        return "It's okay that we're here."
    elif "rebels" in storm_troopers_request.lower():
        return "You want to go home and rethink your life."
    else:
        return "I find your lack of faith disturbing."


def robot():
    storm_troopers_request = input("Hello there! Can I help you? ")
    if storm_troopers_request ==' ':
        storm_troopers_request = "Live Oak Academy"

    print(jedi_mind_trick(storm_troopers_request))

    print("We are going to go about our business.")


# Start the robot program
if __name__ == "__main__":
    robot()

