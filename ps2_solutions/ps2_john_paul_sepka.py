import webbrowser


def jedi_mind_trick(storm_troopers_request):
    """
    Determines the Jedi mind trick response to the Storm Troopers request.
    :param storm_troopers_request: the Storm Troopers request, e.g. "Droids"
    :return: the Jedi mink trick response, e.g. "These aren't the droids you're looking for."
    """
    # TODO: Determine correct response to storm_troopers_request
    if "jedi" in storm_troopers_request:
        webbrowser.open("https://www.youtube.com/watch?v=WOVsEKNPyno")
        return "You will remove these restraints and leave the cell with the door open."
    if "droids" in storm_troopers_request:
        webbrowser.open("https://www.youtube.com/watch?v=ihyjXd2C-E8")
        return "These aren't the droids you're looking for."
    if "obi-wan" in storm_troopers_request:
        webbrowser.open("https://www.youtube.com/watch?v=ywnMFkT9j20")
        return "It's okay that we're here."
    if "rebels" in storm_troopers_request:
        webbrowser.open("https://www.youtube.com/watch?v=G-7fcEHyNh8")
        return "You want to go home and rethink your life."
    webbrowser.open("https://www.youtube.com/watch?v=m0XuKORufGk")
    return "I find your lack of faith disturbing."


def robot():
    # TODO: Greet Storm Troopers and ask whom they are looking for.
    storm_troopers_request = str.lower(input("Hello there. What are you looking for? "))
    print(jedi_mind_trick(storm_troopers_request))

    # TODO: Wish Storm Troopers a pleasant day.
    print("Have a nice day.")


# Start the robot program
if __name__ == "__main__":
    robot()
