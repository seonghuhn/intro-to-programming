def jedi_mind_trick(storm_troopers_request):
    """
    Determines the Jedi mind trick response to the Storm Troopers request.
    :param storm_troopers_request: the Storm Troopers request, e.g. "Droids"
    :return: the Jedi mink trick response, e.g. "These aren't the droids you're looking for."
    """

    if 'JEDI' in (storm_troopers_request).upper():
        return 'You will remove these restraints and leave the cell with the door open.'
    if 'DROID' in (storm_troopers_request).upper():
        return 'These aren\'t the droids you\'re looking for.'
    if 'OBI-WAN' in (storm_troopers_request).upper():
        return 'It\'s okay that we\'re here.'
    if 'REBEL' in (storm_troopers_request).upper():
        return 'You want to go home and rethink your life.'
    if not 'JEDI, DROID, OBI-WAN, REBEL' in (storm_troopers_request).upper():
        return 'I find your lack of faith disturbing.'

def robot():
    storm_troopers_request = "Live Oak Academy"

    print('Hello - aren\'t you a little short for a stormtrooper?')
    storm_troopers_request = input('Is there someone you are looking for?\n')
    print(jedi_mind_trick(storm_troopers_request))

    print('\033[1;36;20m')


    print('Good day, may the force be with you.')


# Start the robot program
if __name__ == "__main__":
    robot()
