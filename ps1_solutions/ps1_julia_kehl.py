from utils import get_current_temp, get_current_air_quality


def conv_fahrenheit_celsius(temp):

    """
    Converts temperature in Fahrenheit to Celsius.
    :param temp: the temperature in Fahrenheit
    :return: the temperature in Celsius rounded to two decimal places
    """

    conv_fahrenheit_celsius = ((temp)-32)*(5/9)
    return conv_fahrenheit_celsius

def air_quality_level(aqi):
    """
    Determines the AQI level based on the AQI.
    :param aqi: the air quality index
    :return: the AQI level based on https://www.airnow.gov/aqi/aqi-basics/
    """
    if aqi >= 0 and aqi <= 50:
        level = 'Good'
    elif 51 <= aqi <= 100:
        level = 'Moderate'
    elif 101 <= aqi <= 150:
        level = 'Unhealthy for sensitive groups'
    elif 151 <= aqi <= 200:
        level = 'Unhealthy'
    elif 201 <= aqi <= 300:
        level = 'Very Unhealthy'
    elif aqi > 301:
        level = 'Hazardous'

    if level == 'Hazardous':
        print('\033[1;31;20m')
    if level == 'Very Unhealthy':
        print('\034[1;35;20m')
    if level == 'Unhealthy for sensitive groups':
        print('\033[1;34;20m')
    if level == 'Unhealthy':
        print('\033[1;33;20m')
    if level == 'Moderate':
        print('\033[1;32;20m')
    if level == 'Good':
        print('\033[1;36;20m')

    return level



def main():
    country = 'US'
    city = input('Please enter city in CA: ')
    if city == '':
        city = 'Los Gatos'
    state = 'California'

    print(city + ", " + state + ", " + country)


    temp_f = get_current_temp(city, dry_run=False)
    temp_c = conv_fahrenheit_celsius(temp_f)
    aqi = get_current_air_quality(city, dry_run=False)
    print(str(temp_f) + ' Degrees Fahrenheit')
    print(str(round(temp_c, 2)) + ' Degrees Celsius')
    print('Air Quality Index: ' + str(aqi))
    level = air_quality_level(aqi)
    print(level)


if __name__ == "__main__":
    main()
