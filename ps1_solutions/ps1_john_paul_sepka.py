from utils import get_current_temp, get_current_air_quality


def conv_fahrenheit_celsius(temp_f):
    """
    Converts temperature in Fahrenheit to Celsius.
    :param temp: the temperature in Fahrenheit
    :return: the temperature in Celsius rounded to two decimal places
    """
    temp_c = (temp_f - 32)*5/9
    temp_c = round(temp_c, 2)
    return temp_c

def air_quality_level(aqi):
    """
    Determines the AQI level based on the AQI.
    :param aqi: the air quality index
    :return: the AQI level based on https://www.airnow.gov/aqi/aqi-basics/
    """
    # TODO: Replace with code to determine the AQI level based on the AQI.
    if aqi < 51:
        aqi = colored("Good", green)
    elif aqi < 101:
        aqi = colored("Moderate")
    elif aqi < 151:
        aqi = "Unhealthy for Sensitive Groups"
    elif aqi < 201:
        aqi = "Unhealthy"
    elif aqi < 251:
        aqi = "Very Unhealthy"
    else:
        aqi = ("Hazardous")
    return aqi



def main():
    # TODO: Get Californian city from user's input.
    city = "Santa Clara"
    city = input("What city do you live in?")

    # TODO: Remove dry_run=True when your program is complete and ready to test.
    temp_f = get_current_temp(city)
    temp_c = conv_fahrenheit_celsius(temp_f)
    aqi = get_current_air_quality(city)
    level = air_quality_level(aqi)

    # TODO: Output temperature in Fahrenheit and Celsius.
    print("Current temperature in Farenheit:", temp_f)
    print("Current temperature in Celsius:", temp_c)
    
    # TODO: Output AQI and AQI level.
    print("Air Quality Index:", aqi, level)

if __name__ == "__main__":
    main()
