from utils import get_current_temp, get_current_air_quality


def conv_fahrenheit_celsius(temp):
    """
    Converts temperature in Fahrenheit to Celsius.
    :param temp: the temperature in Fahrenheit
    :return: the temperature in Celsius rounded to two decimal places
    """

    return round((temp-32)*5/9,2)


def air_quality_level(aqi):
    """
    Determines the AQI level based on the AQI.
    :param aqi: the air quality index
    :return: the AQI level based on https://www.airnow.gov/aqi/aqi-basics/
    """
    # Good is green
    if aqi <= 50:
        return "\033[1;32;20mGood"
    # Moderate is yellow
    if aqi <= 100:
        return "\033[1;33;20mModerate"
    # Unhealthy for Sensitive Groups is blue (should be orange)
    if aqi <= 150:
        return "\033[1;34;20mUnhealthy for Sensitive Groups"
    # Unhealthy is red
    if aqi <= 200:
        return "\033[1;31;20mUnhealthy"
    # Very unhealthy is purple
    if aqi <= 300:
        return "\033[1;35;20mVery Unhealthy"
    # Hazardous is white (should be maroon)
    return "\033[1;30;20mHazardous"


def main():

    state = input('Please enter the name of a state that is in the USA: ') or 'Minnesota'
    city = input(f'Please enter the name of a city in {state}: ') or 'Grand Marais'

    temp_f = get_current_temp(city, dry_run=False)
    temp_c = conv_fahrenheit_celsius(temp_f)
    aqi = get_current_air_quality(city, state=state, dry_run=False)
    level = air_quality_level(aqi)

    print(f'Environmental stats for {city}, {state}')
    print(f'Current temperature in Farenheit is {temp_f}°')
    print(f'Current temperature in Celsius is {temp_c}°')
    print(f'The current AQI is {aqi}, this is {level}')



if __name__ == "__main__":
    main()
