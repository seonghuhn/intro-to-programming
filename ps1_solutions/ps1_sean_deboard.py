from utils import get_current_temp, get_current_air_quality
from termcolor import colored, cprint

def conv_fahrenheit_celsius(temp):
    """
    Converts temperature in Fahrenheit to Celsius.
    :param temp: the temperature in Fahrenheit
    :return: the temperature in Celsius rounded to two decimal places
    """
    #temp_f = get_current_temp
    temp_c = (temp - 32) * 5 / 9
    return int(temp_c)

def air_quality_level(aqi):
    """
    Determines the AQI level based on the AQI.
    :param aqi: the air quality index
    :return: the AQI level based on https://www.airnow.gov/aqi/aqi-basics/
    """
    if aqi < 50:
        return("Air Quality is Good", "green")
    elif aqi >= 50 and aqi < 100:
        return("Air Quality is Moderate", "yellow")
    elif aqi >= 100 and aqi < 150:
        return("Air Quality is Unhealthy for Sensitive Groups", "yellow")
    elif aqi >= 150 and aqi <200:
        return("Air Quality is Unhealthy", "magenta")
    elif aqi >= 200 and aqi < 300:
        return("Air Quality is Very Unhealthy", "red")
    elif aqi >= 300:
        return("Air Quality is Hazardous", "red")
def main():
    city = input("What is your city?")
    # city = "Santa Clara"
    state = input("What is your state?")
    temp_f = get_current_temp(city, dry_run=False)
    temp_c = conv_fahrenheit_celsius(temp_f)
    aqi = get_current_air_quality(city, state, dry_run=False)
    level, air_quality_color = air_quality_level(aqi)

    print('the temperature in Fahrenheit is:',temp_f,'the temperature in Celsius is:',temp_c)

    print('The AQI is:', aqi)
    cprint(level, air_quality_color)


if __name__ == "__main__":
    main()
