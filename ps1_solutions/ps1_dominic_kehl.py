from utils import get_current_temp, get_current_air_quality

def conv_fahrenheit_celsius(temp):
    """ 
    Converts temperature in Fahrenheit to Celsius. 
    :param temp: the temperature in Fahrenheit 
    :return: the temperature in Celsius rounded to two decimal places 
    """
    # TODO: Replace with code to convert the temperature from Fahrenheit to Celsius.
    return 0.0


def air_quality_level(aqi, city, state, country):
    """ 
    # Determines the AQI level based on the AQI. 
    # param aqi: the air quality index 
    # return: the AQI level based on https://www.airnow.gov/aqi/aqi-basics/ 
    """
    # TODO: Replace with code to determine the AQI level based on the AQI.

    aqi = get_current_air_quality(city, state, country)
    level = ''
    print('AQI '+ str(aqi))
    if aqi > 0 and aqi < 50:
        print("\033[1;32;20m Air quality is good  \n ")
        level = 'Good'
    elif aqi >= 50 and aqi < 100:
        print('print("\033[1;33;20m Air quality is moderate   \n')
        level = 'Moderate'
    elif aqi >= 100 and aqi < 150:
        print('print("\033[1;35;20m Air quality is unhealthy for sensitive groups \n')
        level = 'Unhealthy for sensitive groups'
    elif aqi >= 150 and aqi < 200:
        print('print("\033[1;31;20m Air quality is unhealthy     \n')
        level = 'Unhealthy'
    elif aqi >= 200 and aqi < 300:
        print('\033[0;35;47m Air quality is very unhealthy   \n')
        level = 'Very Unhealthy'
    elif aqi >= 300 and aqi < 500:
        print('print("\033[1;30;40m Air quality is hazardous    \n')
        level = 'Hazardous'

    return level

def print_results(city='Santa Clara', state='California', country='US', level='', temp_f='', temp_c='', aqi='', pick=''):
    if pick == 'City':
        if city == '':
            print()
        else:
            print("The temperature of " + city + " is " + str(temp_f) + " Fahrenheit and " + str(round(temp_c, 2)) + " Celsius")
            print('The air quality of ' + city + ' is ' + level + ' and the air quality level of ' + city + ' is ' + str(aqi))
    elif pick == 'State':
        if state == '':
            print()
        else:
            print("The temperature of " + state + " is " + str(temp_f) + " Fahrenheit and " + str(round(temp_c, 2)) + " Celsius")
            print('The air quality of ' + state + ' is ' + level + ' and the air quality level of ' + state + ' is ' + str(aqi))
    elif pick == 'Country':
        if country == '':
            print()
        else:
            print("The temperature of " + country + " is " + str(temp_f) + " Fahrenheit and " + str(round(temp_c, 2)) + " Celsius")
            print('The air quality of ' + country + ' is ' + level + ' and the air quality level of ' + country + ' is ' + str(aqi))

        return (pick)


def main():
    # TODO: Get Californian city from user's input.

    #initialize values
    country = 'USA'
    city = 'Santa Clara'
    state = 'California'

    pick = input('Would you like to view the aqi and temperature of a City, State, or Country? ')

    if pick == "City":
        city = input('Please enter a city: ')
    elif pick == "Country":
        country = input('Please enter a Country: ')
    elif pick == "State":
        state = input('Please enter a state: ')
        if state != '':
            input("Please enter a city: ")
    elif pick == "":
        print("Defults are City: " + city + " State: " + state + " Country: " + country )

    temp_f = get_current_temp(city, state, country)

    conv_fahrenheit_celsius = ((temp_f) - 32) * (5 / 9)
    temp_c = conv_fahrenheit_celsius
    aqi = get_current_air_quality(city, state, country)
    level = air_quality_level(aqi, city, state, country)

    if conv_fahrenheit_celsius == int:
        print("")
    else:
        round(conv_fahrenheit_celsius, 2)

    print_results(city, state, country, level, temp_f, temp_c, aqi, pick)
    # TODO: Output temperature in Fahrenheit and Celsius.

    # TODO: Output AQI and AQI level.


if __name__ == "__main__":
    main()
