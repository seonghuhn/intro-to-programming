from utils import get_current_temp, get_current_air_quality


def conv_fahrenheit_celsius(temp):
    """
    Converts temperature in Fahrenheit to Celsius.
    :param temp: the temperature in Fahrenheit
    :return: the temperature in Celsius rounded to two decimal places
    """
    return round((temp-32)*5/9, 2)


def air_quality_level(aqi):
    """
    Determines the AQI level based on the AQI.
    :param aqi: the air quality index
    :return: the AQI level based on https://www.airnow.gov/aqi/aqi-basics/
    """
    if 0 <= aqi <= 50:
        return "Good"
    elif 51 <= aqi <= 100:
        return "Moderate"
    elif 101 <= aqi <= 150:
        return "Unhealthy for Sensitive Groups"
    elif 151 <= aqi <= 200:
        return "Unhealthy"
    elif 201 <= aqi <= 300:
        return "Very Unhealthy"
    elif 251 <= aqi:
        return "Hazardous"


def main():
    city = input()
    if not len(city):
        city = "Santa Clara"

    temp_f = get_current_temp(city)
    temp_c = conv_fahrenheit_celsius(temp_f)
    aqi = get_current_air_quality(city)
    level = air_quality_level(aqi)

    print(f"F temp: {temp_f}, C temp: {temp_c}")
    print(f"aqi: {aqi}, {level}")


if __name__ == "__main__":
    main()
