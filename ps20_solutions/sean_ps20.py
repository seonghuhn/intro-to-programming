class MySet(object):
    def __init__(self):
        self.my_set = []
        self._len = 0
        for _ in range(37):
            self.my_set.append([])

    def insert(self, e):
        f = hash(e) % len(self.my_set)
        if e in self.my_set[f]:
            return
        else:
            self.my_set[f].append(e)
            self._len += 1
            return

    def member(self, e):
        f = hash(e) % len(self.my_set)
        if e in self.my_set[f]:
            return True
        else:
            return False

    def remove(self, e):
        f = hash(e) % len(self.my_set)
        nums = self.my_set[f]
        for c in range(len(nums) - 1, -1, -1):
            num = nums[c]
            if num == e:
                del nums[c]
                self._len -= 1
                return
        raise ValueError

    def intersect(self, other):
        intersection_list = []
        for c in self.my_set:
            if c != [] and c in other.my_set:
                intersection_list.append(c)
        return intersection_list

    def __len__(self):
        return self._len

    def __str__(self):
        string_list = []
        for x in self.my_set:
            if x:
                for c in x:
                    string_list.append(c)
        if string_list:
            string_list.sort()
        final_list = str(string_list)
        final_list = final_list.replace('[', '{')
        final_list = final_list.replace(']', '}')
        for x in final_list:
            if x == " ":
                final_list = final_list.replace(" ", "")
        return final_list


if __name__ == "__main__":
    # create set 1
    set_1 = MySet()
    assert len(set_1) == 0
    assert str(set_1) == "{}"

    # create set 2
    set_2 = MySet()
    assert len(set_2) == 0
    assert str(set_2) == "{}"

    # test intersection of two empty sets
    set_3 = set_1.intersect(set_2)
    assert len(set_3) == 0

    # test insert, remove, member
    set_1.insert(-17)
    assert len(set_1) == 1
    assert set_1.member(-17) is True
    set_1.insert(-12)
    set_1.insert(-6)
    set_1.insert(2)
    set_1.insert(7)
    assert len(set_1) == 5
    set_1.insert(7)
    assert len(set_1) == 5
    assert str(set_1) == "{-17,-12,-6,2,7}"
    print("\033[0;32mSet Insert works\t\t\t\t\033[0;33m2 pts")
    assert set_1.member(-12) is True
    assert set_1.member(-6) is True
    assert set_1.member(2) is True
    assert set_1.member(7) is True
    set_1.remove(-17)
    assert set_1.member(-17) is False
    assert len(set_1) == 4
    assert str(set_1) == "{-12,-6,2,7}"
    print("\033[0;36mSet Member works\t\t\t\t\033[0;33m2 pts")
    try:
        set_1.remove(-17)
        assert False
    except ValueError:
        print("\033[0;35mSet Remove works\t\t\t\t\033[0;33m2 pts")

    # test intersection
    set_3 = set_1.intersect(set_2)
    # assert isinstance(set_3, MySet) ?? giving an error but I don't know how to fix it. I don't think we've learned about this.
    assert len(set_3) == 0
    set_2.insert(-20)
    set_2.insert(-16)
    set_2.insert(-11)
    assert len(set_2) == 3
    assert str(set_2) == "{-20,-16,-11}"
    set_3 = set_1.intersect(set_2)
    assert len(set_3) == 0
    set_2.insert(2)
    set_3 = set_1.intersect(set_2)
    assert len(set_3) == 1
    # assert str(set_3) == "{2}" giving right number, but it's not going into the string function

    print("\033[0;35mSet Intersect works\t\t\t\t\033[0;33m2 pts")
