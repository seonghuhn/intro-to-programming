class MySet(object):
    def __init__(self):
        self.my_set = []
        self.len_set = 0
        self.string_set = ""
        for x in range(37):
            self.my_set.insert(-1, [])

    def insert(self, e):
        f = hash(e) % 37
        if e not in self.my_set[f]:
            self.my_set[f].insert(-1, e)
            if self.len_set > 0:
                self.string_set = self.string_set + "," + str(e)
            else:
                self.string_set = self.string_set + str(e)
            self.len_set += 1
        else:
            raise ValueError

    def member(self, e):
        f = hash(e) % 37
        for e in self.my_set:
            if self.my_set[f] == e:
                return False
            else:
                return True


    def remove(self, e):
        f = hash(e) % 37
        if e in self.my_set[f]:
            try:
                self.my_set[f].remove(e)
            except:
                pass
            self.len_set -= 1
            try:
                self.string_set = self.string_set.replace(str(e)+",","")
            except:
                pass
            return self.string_set
        else:
            raise ValueError

    def intersect(self, other):
        i = MySet()
        for x in self.my_set:
            for y in x:
                if other.member(y):
                    i.insert(y)
        return i

    def __len__(self):
        return self.len_set

    def __str__(self):
        f = self.string_set
        f = '{' + f + '}'
        if '{, ' in str(f):
            f.replace(", ", "")
        else:
            pass
        return str(f)


if __name__ == "__main__":
    # create set 1
    set_1 = MySet()
    assert len(set_1) == 0
    assert str(set_1) == "{}"

    # create set 2
    set_2 = MySet()
    assert len(set_2) == 0
    assert str(set_2) == "{}"

    # test intersection of two empty sets
    set_3 = set_1.intersect(set_2)
    assert len(set_3) == 0

    # test insert, remove, member
    set_1.insert(-17)
    assert len(set_1) == 1
    assert set_1.member(-17) is True
    set_1.insert(-12)
    set_1.insert(-6)
    set_1.insert(2)
    set_1.insert(7)
    assert len(set_1) == 5
    try:
        set_1.insert(7)
    except ValueError:
        pass
    assert len(set_1) == 5
    assert str(set_1) == "{-17,-12,-6,2,7}"
    print("\033[0;32mSet Insert works\t\t\t\t\033[0;33m2 pts")
    assert set_1.member(-12) is True
    assert set_1.member(-6) is True
    assert set_1.member(2) is True
    assert set_1.member(7) is True
    set_1.remove(-17)
    assert set_1.member(-17) is False
    assert len(set_1) == 4
    assert str(set_1) == "{-12,-6,2,7}"
    print("\033[0;36mSet Member works\t\t\t\t\033[0;33m2 pts")
    try:
        set_1.remove(-17)
        assert False
    except ValueError:
        print("\033[0;35mSet Remove works\t\t\t\t\033[0;33m2 pts")

    # test intersection
    set_3 = set_1.intersect(set_2)
    assert isinstance(set_3, MySet)
    assert len(set_3) == 0
    set_2.insert(-20)
    set_2.insert(-16)
    set_2.insert(-11)
    assert len(set_2) == 3
    assert str(set_2) == "{-20,-16,-11}"
    set_3 = set_1.intersect(set_2)
    assert len(set_3) == 0
    set_2.insert(2)
    set_3 = set_1.intersect(set_2)
    assert len(set_3) == 1
    assert str(set_3) == "{2}"

    print("\033[0;35mSet Intersect works\t\t\t\t\033[0;33m2 pts")
