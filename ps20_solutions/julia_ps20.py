
class MySet(object):
    def __init__(self):
        self._set = []
        self._len = 0
        for x in range(37):
            self._set.append([])
        self._string = ""

    def insert(self, e):
        idx = hash(e) % len(self._set)
        if e not in self._set[idx]:
            self._set[idx].append(e)
            if self._len > 0:
                self._string = self._string + "," + str(e)
            else:
                self._string = self._string + str(e)
            self._len += 1
        else:
            raise ValueError

    def member(self, e):
        idx = hash(e) % len(self._set)
        for e in self._set:
            if self._set[idx] != e:
                return True
            else:
                return False


    def remove(self, e):
        idx = hash(e) % len(self._set)
        if e in self._set[idx]:
                self._set[idx].remove(e)
                self._string = self._string.replace(str(e)+",","")
                self._len -= 1
                return self._string
        else:
            raise ValueError

    def intersect(self, other):
        self._other = other
        intersections = MySet()
        for val in self._set:
            for n in val:
                if other.member(n):
                    intersections.insert(n)
        return intersections

    def __len__(self):
        return self._len

    def __str__(self): #just going to note that Dominic helped me with the returning as a string part bc I couldn't figure out how to return the list as a string
        string_set = sorted(self._string)
        string_set = '{' + self._string + '}'
        if '{, ' in str(string_set):
            string_set.replace(", ", "")
        return str(string_set)

if __name__ == "__main__":
    # create set 1
    set_1 = MySet()
    assert len(set_1) == 0
    assert str(set_1) == "{}"

    # create set 2
    set_2 = MySet()
    assert len(set_2) == 0
    assert str(set_2) == "{}"

    # test intersection of two empty sets
    set_3 = set_1.intersect(set_2)
    assert len(set_3) == 0

    # test insert, remove, member
    set_1.insert(-17)
    assert len(set_1) == 1
    assert set_1.member(-17) is True
    set_1.insert(-12)
    set_1.insert(-6)
    set_1.insert(2)
    set_1.insert(7)
    assert len(set_1) == 5
    try:
        set_1.insert(7)
    except ValueError:
        pass
    assert len(set_1) == 5
    assert str(set_1) == "{-17,-12,-6,2,7}"
    print("\033[0;32mSet Insert works\t\t\t\t\033[0;33m2 pts")
    assert set_1.member(-12) is True
    assert set_1.member(-6) is True
    assert set_1.member(2) is True
    assert set_1.member(7) is True
    set_1.remove(-17)
    assert set_1.member(-17) is False
    assert len(set_1) == 4
    assert str(set_1) == "{-12,-6,2,7}"
    print("\033[0;36mSet Member works\t\t\t\t\033[0;33m2 pts")
    try:
        set_1.remove(-17)
        assert False
    except ValueError:
        print("\033[0;35mSet Remove works\t\t\t\t\033[0;33m2 pts")
    # test intersection
    set_3 = set_1.intersect(set_2)
    assert isinstance(set_3, MySet)
    assert len(set_3) == 0
    set_2.insert(-20)
    set_2.insert(-16)
    set_2.insert(-11)
    assert len(set_2) == 3
    assert str(set_2) == "{-20,-16,-11}"
    set_3 = set_1.intersect(set_2)
    assert len(set_3) == 0
    set_2.insert(2)
    set_3 = set_1.intersect(set_2)
    assert len(set_3) == 1
    assert str(set_3) == "{2}"

    print("\033[0;35mSet Intersect works\t\t\t\t\033[0;33m2 pts")
