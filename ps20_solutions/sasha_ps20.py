class MySet(object):
    def __init__(self):
        self.list1 = {}
        self.item_counter = 0
    
    def insert(self, e):
        hash1 = hash(e)
        if self.member(e) == False:
            self.list1[hash1]=e
            self.item_counter += 1
       
    def member(self, e):
        hash1 = hash(e)
        return e in self.list1

    def remove(self, e):
        if self.member(e) == False:
            raise ValueError("this doesnt work try again")
        else:
            hash1 = hash(e)
            self.list1.pop(hash1)
            self.item_counter -= 1
    
    
    def intersect(self, other):
        intersection = MySet()
        for z in self.list1.values():
            if z in other.list1.values():
                intersection.insert(z)
        return intersection

    
    def __len__(self):
        return self.item_counter
    

    def __str__(self):
        string1 = ""
        counter = 0
        for z in self.list1.values():
            string1 += str(z)
            counter += 1
            if counter < self.item_counter:
                string1 += ","
        return "{" + string1 + "}"

if __name__ == "__main__":
    # create set 1
    set_1 = MySet()
    assert len(set_1) == 0
    assert str(set_1) == "{}"

    # create set 2
    set_2 = MySet()
    assert len(set_2) == 0
    assert str(set_2) == "{}"

    # test intersection of two empty sets
    set_3 = set_1.intersect(set_2)
    assert len(set_3) == 0

    # test insert, remove, member
    set_1.insert(-17)
    assert len(set_1) == 1
    assert set_1.member(-17) is True
    set_1.insert(-12)
    set_1.insert(-6)
    set_1.insert(2)
    set_1.insert(7)
    assert len(set_1) == 5
    set_1.insert(7)
    assert len(set_1) == 5
    assert str(set_1) == "{-17,-12,-6,2,7}"
    print("\033[0;32mSet Insert works\t\t\t\t\033[0;33m2 pts")
    assert set_1.member(-12) is True
    assert set_1.member(-6) is True
    assert set_1.member(2) is True
    assert set_1.member(7) is True
    set_1.remove(-17)
    assert set_1.member(-17) is False
    assert len(set_1) == 4
    assert str(set_1) == "{-12,-6,2,7}"
    print("\033[0;36mSet Member works\t\t\t\t\033[0;33m2 pts")
    try:
        set_1.remove(-17)
        assert False
    except ValueError:
        print("\033[0;35mSet Remove works\t\t\t\t\033[0;33m2 pts")

    # test intersection
    set_3 = set_1.intersect(set_2)
    assert isinstance(set_3, MySet)
    assert len(set_3) == 0
    set_2.insert(-20)
    set_2.insert(-16)
    set_2.insert(-11)
    assert len(set_2) == 3
    assert str(set_2) == "{-20,-16,-11}"
    set_3 = set_1.intersect(set_2)
    assert len(set_3) == 0
    set_2.insert(2)
    set_3 = set_1.intersect(set_2)
    assert len(set_3) == 1
    assert str(set_3) == "{2}"

    print("\033[0;35mSet Intersect works\t\t\t\t\033[0;33m2 pts")
