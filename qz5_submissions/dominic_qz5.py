class MyDict(object):
    def __init__(self):

        self.keys = []
        self.values = []

    def assign(self, k, v):

        if k not in self.keys:
            self.keys.append(k)
            self.values.append(v)
        else:
            value = self.keys.index(k)
            self.values[value] = v
        return self.keys and self.values

    def getval(self, k):

        if k in self.keys:
            value = self.keys.index(k)
            return self.values[value]
        else:
            raise KeyError

    def delete(self, k):

        if k in self.keys:
            k = self.keys.index(k)
            self.keys.pop(k)
            self.values.pop(k)
        else:
            raise KeyError
        return self.keys + self.values

    def __len__(self):
        return len(self.keys)


if __name__ == '__main__':
    md = MyDict()
    print(md.assign(1, 2))
    print(f"md getval{md.getval(1)}")
    print(f"md delete{md.delete(1)}")
    print(md.__len__())

    d = {}
    d[1] = 2
    print(f"dict getval{d[1]}")
    del (d[1])
    print(f"dict delete {d}")
    print(len(d))

    md = MyDict()
    assert len(md) == 0
    md.assign(1, 2)
    assert md.getval(1) == 2
    assert len(md) == 1
    md.assign(3, 4)
    assert md.getval(3) == 4
    assert len(md) == 2
    md.assign(5, 6)
    assert md.getval(5) == 6
    assert len(md) == 3
    md.assign(3, 5)
    assert md.getval(3) == 5
    assert len(md) == 3
    print("\033[0;35mTest 1 Passes")

    md = MyDict()
    try:
        md.getval(3)
        assert False
    except KeyError:
        pass
    md.assign(1, 2)
    md.assign(3, 4)
    try:
        md.getval(4)
        assert False
    except KeyError:
        pass
    md.delete(1)
    assert len(md) == 1
    try:
        md.getval(1)
        assert False
    except KeyError:
        pass
    print("\033[0;36mTest 2 Passes")

    md = MyDict()
    md.assign(10, 2)
    assert md.getval(10) == 2
    md.assign(3, 3)
    assert md.getval(3) == 3
    assert md.getval(10) == 2
    md.assign(4, 2)
    assert md.getval(4) == 2
    assert len(md) == 3
    md.assign(3, 6)
    assert md.getval(3) == 6
    assert len(md) == 3
    assert md.getval(10) == 2
    assert md.getval(3) == 6
    assert md.getval(4) == 2
    md.delete(3)
    assert len(md) == 2
    md.delete(10)
    assert len(md) == 1
    assert md.getval(4) == 2
    print("\033[0;34mTest 3 Passes")

    md = MyDict()
    md.assign(2, 3)
    assert md.getval(2) == 3
    assert len(md) == 1
    try:
        md.getval(3)
        assert False
    except KeyError:
        pass
    md.assign(3, 3)
    assert len(md) == 2
    assert md.getval(3) == 3
    assert md.getval(2) == 3
    md.assign(4, 2)
    assert len(md) == 3
    md.delete(3)
    assert len(md) == 2
    assert md.getval(2) == 3
    try:
        md.delete(10)
        assert False
    except KeyError:
        pass
    try:
        md.delete(3)
        assert False
    except KeyError:
        pass
    md.assign(6, 1)
    assert md.getval(6) == 1
    assert len(md) == 3
    print("\033[1;34mTest 4 Passes")
