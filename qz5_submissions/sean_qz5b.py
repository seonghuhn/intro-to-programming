class ParkingSystem(object):
    def __init__(self, big, medium, small):
        self.big = big
        self.medium = medium
        self.small = small

    def add_car(self, car_type):
        if car_type == 1:
            if self.big == 0:
                return False
            else:
                self.big = self.big - 1
                return True
        elif car_type == 2:
            if self.medium == 0:
                return False
            else:
                self.medium = self.medium - 1
                return True
        elif car_type == 3:
            if self.small == 0:
                return False
            else:
                self.small = self.small - 1
                return True


class TwoSum(object):
    def __init__(self):
        self.list_of_num = []

    def add(self, k):
        self.list_of_num.append(k)

    def find(self, x):
        v = 0
        c = 0

        while True:
            if v > len(self.list_of_num) - 1:
                return False
            index = self.list_of_num[c]
            add_index = self.list_of_num[v]
            if c == v:
                v += 1
            else:
                if index + add_index == x:
                    return True
                else:
                    if add_index == self.list_of_num[-1]:
                        c += 1
                        v = 0
                        if index == self.list_of_num[-1]:
                            return False
                    else:
                        v += 1
