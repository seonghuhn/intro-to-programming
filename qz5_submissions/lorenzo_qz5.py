class MyDict(object):
    def __init__(self):
        """
        Initialization of dictionary.
        """
        self.md = list()

    def assign(self, k, v):
        """
        Assigns a value to a key. If the key is already in the dictionary then it updates its value. If it is not then
        it adds the key value pair to the dictionary.
        :param k: the key
        :param v: the value
        """
        pair = (k, v)
        if self.md == list():
            self.md.append(pair)
        else:
            for pair_md in self.md:
                # if k in pair_md:
                if k == pair_md[0]:
                    self.md.remove(pair_md)
                    break
            self.md.append(pair)
        return self.md

    def getval(self, k):
        """
        Gets the value stored for the key. If the key is not in the dictionary then a KeyError is raised.
        :param k: the key
        :return: the value for the key if found; otherwise KeyError raised
        """
        for pair_md in self.md:
            # if k in pair_md:
            if k == pair_md[0]:
                return pair_md[-1]
        # return KeyError
        raise KeyError()

    def delete(self, k):
        """
        Deletes the key, value pair. If the key is not in the dictionary then a KeyError is raised.
        :param k: the key
        """
        for pair_md in self.md:
            # if k in pair_md:
            if k == pair_md[0]:
                self.md.remove(pair_md)
                return self.md
        # return KeyError
        raise KeyError()

    def __len__(self):
        """
        Gets the number of key, value pairs, i.e. the length of the dictionary.
        :return: the number of key, value pairs
        """
        count = 0
        for pair_md in self.md:
            count += 1
        return count


if __name__ == "__main__":
    # Here's my homemade test code. Just for your information, I decided to make my dictionary out of a list of
    # tuples. This worked pretty well and I'm pleased with how it came together.
    md = MyDict()
    assert md.assign('a', 10) == [('a', 10)]
    assert md.assign('b', 3) == [('a', 10), ('b', 3)]
    assert md.assign('b', 5) == [('a', 10), ('b', 5)]
    assert md.assign(50, 5) == [('a', 10), ('b', 5), (50, 5)]

    assert md.getval('a') == 10
    assert md.getval('b') == 5
    # assert md.getval('c') == KeyError
    assert md.getval(50) == 5

    assert md.delete('b') == [('a', 10), (50, 5)]
    # assert md.delete('c') == KeyError

    assert len(md) == 2

    md = MyDict()
    assert len(md) == 0
    md.assign(1, 2)
    assert md.getval(1) == 2
    assert len(md) == 1
    md.assign(3, 4)
    assert md.getval(3) == 4
    assert len(md) == 2
    md.assign(5, 6)
    assert md.getval(5) == 6
    assert len(md) == 3
    md.assign(3, 5)
    assert md.getval(3) == 5
    assert len(md) == 3
    print("\033[0;35mTest 1 Passes")

    md = MyDict()
    try:
        md.getval(3)
        assert False
    except KeyError:
        pass
    md.assign(1, 2)
    md.assign(3, 4)
    try:
        md.getval(4)
        assert False
    except KeyError:
        pass
    md.delete(1)
    assert len(md) == 1
    try:
        md.getval(1)
        assert False
    except KeyError:
        pass
    print("\033[0;36mTest 2 Passes")

    md = MyDict()
    md.assign(10, 2)
    assert md.getval(10) == 2
    md.assign(3, 3)
    assert md.getval(3) == 3
    assert md.getval(10) == 2
    md.assign(4, 2)
    assert md.getval(4) == 2
    assert len(md) == 3
    md.assign(3, 6)
    assert md.getval(3) == 6
    assert len(md) == 3
    assert md.getval(10) == 2
    assert md.getval(3) == 6
    assert md.getval(4) == 2
    md.delete(3)
    assert len(md) == 2
    md.delete(10)
    assert len(md) == 1
    assert md.getval(4) == 2
    print("\033[0;34mTest 3 Passes")

    md = MyDict()
    md.assign(2, 3)
    assert md.getval(2) == 3
    assert len(md) == 1
    try:
        md.getval(3)
        assert False
    except KeyError:
        pass
    md.assign(3, 3)
    assert len(md) == 2
    assert md.getval(3) == 3
    assert md.getval(2) == 3
    md.assign(4, 2)
    assert len(md) == 3
    md.delete(3)
    assert len(md) == 2
    assert md.getval(2) == 3
    try:
        md.delete(10)
        assert False
    except KeyError:
        pass
    try:
        md.delete(3)
        assert False
    except KeyError:
        pass
    md.assign(6, 1)
    assert md.getval(6) == 1
    assert len(md) == 3
    print("\033[1;34mTest 4 Passes")
