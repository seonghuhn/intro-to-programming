class MyDict(object):
    def __init__(self):
        self.dict = "{}"

    def assign(self, k, v):
        if self.dict == '{}':
            new_value = str(k) + ':' + str(v)
        else:
            new_value = ', ' + str(k) + ':' + str(v)
        if str(str(k) + ":") in self.dict:
            split_dict = self.dict.replace('}', '')
            split_dict = split_dict.replace('{', '')
            # split_dict = split_dict.split(',')
            split_dict = split_dict.split(', ')
            counter = -1
            for x in split_dict:
                counter += 1
                if str(str(k) + ":") in x:
                    break
            split_dict.pop(counter)
            # join_split = ''
            join_split = ', '
            join_split = join_split.join(split_dict)
            self.dict = '{' + join_split + '}'
        self.dict = self.dict[:-1] + new_value + self.dict[-1]

    def getval(self, k):
        """
        Gets the value stored for the key. If the key is not in the dictionary then a KeyError is raised.
        :param k: the key
        :return: the value for the key if found; otherwise KeyError raised
        """
        if str(str(k) + ":") in self.dict:
            split_dict = self.dict.replace('}', '')
            split_dict = split_dict.replace('{', '')
            # split_dict = split_dict.split(',')
            split_dict = split_dict.split(', ')
            counter = -1
            for x in split_dict:
                counter += 1
                if str(str(k) + ":") in x:
                    break
            key_and_value = split_dict[counter]
            new_counter = 0
            for x in key_and_value:
                new_counter += 1
                if x == ':':
                    break
            # return key_and_value[new_counter:]
            return int(key_and_value[new_counter:])
        else:
            raise KeyError

    def delete(self, k):
        if str(str(k) + ":") in self.dict:
            split_dict = self.dict.replace('}', '')
            split_dict = split_dict.replace('{', '')
            # split_dict = split_dict.split(',')
            split_dict = split_dict.split(', ')
            counter = -1
            for x in split_dict:
                counter += 1
                if str(str(k) + ":") in x:
                    break
            split_dict.pop(counter)
            # join_split = ''
            join_split = ', '
            join_split = join_split.join(split_dict)
            self.dict = '{' + join_split + '}'
        else:
            raise KeyError

    def __len__(self):
        len = 0
        for x in self.dict:
            if x == ':':
                len += 1
        return len


if __name__ == "__main__":
    md = MyDict()
    assert len(md) == 0
    md.assign(1, 2)
    assert md.getval(1) == 2
    assert len(md) == 1
    md.assign(3, 4)
    assert md.getval(3) == 4
    assert len(md) == 2
    md.assign(5, 6)
    assert md.getval(5) == 6
    assert len(md) == 3
    md.assign(3, 5)
    assert md.getval(3) == 5
    assert len(md) == 3
    print("\033[0;35mTest 1 Passes")

    md = MyDict()
    try:
        md.getval(3)
        assert False
    except KeyError:
        pass
    md.assign(1, 2)
    md.assign(3, 4)
    try:
        md.getval(4)
        assert False
    except KeyError:
        pass
    md.delete(1)
    assert len(md) == 1
    try:
        md.getval(1)
        assert False
    except KeyError:
        pass
    print("\033[0;36mTest 2 Passes")

    md = MyDict()
    md.assign(10, 2)
    assert md.getval(10) == 2
    md.assign(3, 3)
    assert md.getval(3) == 3
    assert md.getval(10) == 2
    md.assign(4, 2)
    assert md.getval(4) == 2
    assert len(md) == 3
    md.assign(3, 6)
    assert md.getval(3) == 6
    assert len(md) == 3
    assert md.getval(10) == 2
    assert md.getval(3) == 6
    assert md.getval(4) == 2
    md.delete(3)
    assert len(md) == 2
    md.delete(10)
    assert len(md) == 1
    assert md.getval(4) == 2
    print("\033[0;34mTest 3 Passes")

    md = MyDict()
    md.assign(2, 3)
    assert md.getval(2) == 3
    assert len(md) == 1
    try:
        md.getval(3)
        assert False
    except KeyError:
        pass
    md.assign(3, 3)
    assert len(md) == 2
    assert md.getval(3) == 3
    assert md.getval(2) == 3
    md.assign(4, 2)
    assert len(md) == 3
    md.delete(3)
    assert len(md) == 2
    assert md.getval(2) == 3
    try:
        md.delete(10)
        assert False
    except KeyError:
        pass
    try:
        md.delete(3)
        assert False
    except KeyError:
        pass
    md.assign(6, 1)
    assert md.getval(6) == 1
    assert len(md) == 3
    print("\033[1;34mTest 4 Passes")
