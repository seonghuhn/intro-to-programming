class MyDict(object):
    def __init__(self):
        """
        Initialization of dictionary.
        """
        self._keys = []
        self._values = []

    def assign(self, k, v):
        """
        Assigns a v to a k. If the k is already in the dictionary then it updates its v. If it is not then
        it adds the k value pair to the dictionary.
        :param k: the key
        :param v: the value
        """
        if k not in self._keys:
            self._keys.append(k)
            self._values.append(v)
        elif k in self._keys:
            val = self._keys.index(k)
            self._values[val] = v
        return self._keys + self._values

    def getval(self, k):
        """
        Gets the value stored for the key. If the key is not in the dictionary then a KeyError is raised.
        :param k: the key
        :return: the value for the key if found; otherwise KeyError raised
        """
        if k in self._keys:
            val = self._keys.index(k)
            return self._values[val]
        else:
            raise KeyError()


    def delete(self, k):
        """
        Deletes the key, value pair. If the key is not in the dictionary then a KeyError is raised.
        :param k: the key
        """
        if k in self._keys:
            val = self._keys.index(k)
            self._keys.pop(val)
            self._values.pop(val)
        elif k not in self._keys:
            raise KeyError()
        return self._keys + self._values
    def __len__(self):
        """
        Gets the number of key, value pairs, i.e. the length of the dictionary.
        :return: the number of key, value pairs
        """
        return len(self._keys)


if __name__ == "__main__":
    md = MyDict()
    print(md.assign(1, 2))
    md.assign(2, 3)
    md.assign(2, 4)
    md.assign(6, 7)
    print(md.getval(1))
    print(md.getval(2))
    print(md.delete(1))
    print(md.__len__())

    md = MyDict()
    assert len(md) == 0
    md.assign(1, 2)
    assert md.getval(1) == 2
    assert len(md) == 1
    md.assign(3, 4)
    assert md.getval(3) == 4
    assert len(md) == 2
    md.assign(5, 6)
    assert md.getval(5) == 6
    assert len(md) == 3
    md.assign(3, 5)
    assert md.getval(3) == 5
    assert len(md) == 3
    print("\033[0;35mTest 1 Passes")

    md = MyDict()
    try:
        md.getval(3)
        assert False
    except KeyError:
        pass
    md.assign(1, 2)
    md.assign(3, 4)
    try:
        md.getval(4)
        assert False
    except KeyError:
        pass
    md.delete(1)
    assert len(md) == 1
    try:
        md.getval(1)
        assert False
    except KeyError:
        pass
    print("\033[0;36mTest 2 Passes")

    md = MyDict()
    md.assign(10, 2)
    assert md.getval(10) == 2
    md.assign(3, 3)
    assert md.getval(3) == 3
    assert md.getval(10) == 2
    md.assign(4, 2)
    assert md.getval(4) == 2
    assert len(md) == 3
    md.assign(3, 6)
    assert md.getval(3) == 6
    assert len(md) == 3
    assert md.getval(10) == 2
    assert md.getval(3) == 6
    assert md.getval(4) == 2
    md.delete(3)
    assert len(md) == 2
    md.delete(10)
    assert len(md) == 1
    assert md.getval(4) == 2
    print("\033[0;34mTest 3 Passes")

    md = MyDict()
    md.assign(2, 3)
    assert md.getval(2) == 3
    assert len(md) == 1
    try:
        md.getval(3)
        assert False
    except KeyError:
        pass
    md.assign(3, 3)
    assert len(md) == 2
    assert md.getval(3) == 3
    assert md.getval(2) == 3
    md.assign(4, 2)
    assert len(md) == 3
    md.delete(3)
    assert len(md) == 2
    assert md.getval(2) == 3
    try:
        md.delete(10)
        assert False
    except KeyError:
        pass
    try:
        md.delete(3)
        assert False
    except KeyError:
        pass
    md.assign(6, 1)
    assert md.getval(6) == 1
    assert len(md) == 3
    print("\033[1;34mTest 4 Passes")
