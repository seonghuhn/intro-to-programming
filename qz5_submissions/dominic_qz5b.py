class ParkingSystem(object):
    def __init__(self, big, medium, small):
        self.big = big
        self.medium = medium
        self.small = small


    def add_car(self, car_type):
        if car_type == 1 and self.big != 0:
            return True
        elif car_type == 1 and self.big == 0:
            return False
        if car_type == 2 and self.medium != 0:
            return True
        elif car_type == 2 and self.medium == 0:
            return False
        if car_type == 3 and self.small != 0:
            return True
        elif car_type == 3 and self.small == 0:
            return False

class TwoSum(object):
    def __init__(self):
        self.list = []

    def add(self, num):
        self.list.append(num)

    def find(self, l):
        n = len(self.list)
        for i in range(0, n):
            for j in range(i + 1, n):
                if (self.list[i] + self.list[j] == l):
                    return True
        return False

two_sum = TwoSum()
print(two_sum.add(3))
print(two_sum.add(2))
print(two_sum.add(1))
print(two_sum.find(4))
print(two_sum.find(6))
print(two_sum.find(7))



