class ParkingSystem:
    def __init__(self, big: int, medium: int, small: int):
        self.types = {
            1: big,
            2: medium,
            3: small,
        }

    def add_car(self, car_type: int):
        if self.types[car_type] > 0:
            self.types[car_type] -= 1
            return True
        else:
            return False


from typing import List


class TwoSum:
    def __init__(self):
        self._set: List[int] = []

    def add(self, number: int):
        self._set.append(number)

    def find(self, two_sum: int):
        for i in range(len(self._set)):
            for j in range(i + 1, len(self._set)):
                if self._set[i] + self._set[j] == two_sum:
                    return True
        return False


def main():
    parking_system = ParkingSystem(1, 1, 0)
    for car_type in [1, 2, 3, 1]:
        print(parking_system.add_car(car_type))

    two_sum = TwoSum()
    two_sum.add(1)
    two_sum.add(3)
    two_sum.add(5)
    for to_find in [4, 7]:
        print(two_sum.find(to_find))

    two_sum = TwoSum()
    two_sum.add(3)
    two_sum.add(1)
    two_sum.add(2)
    for to_find in [3, 6]:
        print(two_sum.find(to_find))
    two_sum.add(3)
    for to_find in [6]:
        print(two_sum.find(to_find))


if __name__ == "__main__":
    main()