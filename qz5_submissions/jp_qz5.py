class MyDict(object):
    def __init__(self):
        """
        Initialization of dictionary.
        """
        self._vals = []

    def assign(self, k, v):
        """
        Assigns a value to a key. If the key is already in the dictionary then it updates its value. If it is not then
        it adds the key value pair to the dictionary.
        :param k: the key
        :param v: the value
        """
        for pair in self._vals:
            if pair[0] == k:
                self._vals.remove(pair)
                break
        self._vals.append([k, v])

    def getval(self, k):
        """
        Gets the value stored for the key. If the key is not in the dictionary then a KeyError is raised.
        :param k: the key
        :return: the value for the key if found; otherwise KeyError raised
        """
        try:
            for pair in self._vals:
                if pair[0] == k:
                    return pair[1]
        except:
            raise KeyError

    def delete(self, k):
        """
        Deletes the key, value pair. If the key is not in the dictionary then a KeyError is raised.
        :param k: the key
        """
        try:
            for pair in self._vals:
                if pair[0] == k:
                    self._vals.remove(pair)
        except:
            raise KeyError

    def __len__(self):
        """
        Gets the number of key, value pairs, i.e. the length of the dictionary.
        :return: the number of key, value pairs
        """
        return len(self._vals)


if __name__ == "__main__":

    # tests assign and len
    md = MyDict()
    md.assign(1, 2)
    assert (md.getval(1)) == 2
    assert (len(md)) == 1

    # tests replacing values
    md.assign(1, 3)
    assert (md.getval(1)) == 3
    assert (len(md)) == 1

    # tests delete and getval
    md.delete(1)
    assert (len(md)) == 0
    try:
        (md.getval(1))
    except KeyError:
        pass

    # tests functionality while multiple values stored
    md.assign(3, 4)
    md.assign(5, 6)
    md.assign(7, 8)
    md.assign(9, 10)
    (md.assign(9, 11))
    assert (md.getval(9)) == 11
    (md.delete(1))
    assert (len(md)) == 4
    try:
        (md.getval(4))
    except ValueError:
        pass

    print("It works.")

    md = MyDict()
    assert len(md) == 0
    md.assign(1, 2)
    assert md.getval(1) == 2
    assert len(md) == 1
    md.assign(3, 4)
    assert md.getval(3) == 4
    assert len(md) == 2
    md.assign(5, 6)
    assert md.getval(5) == 6
    assert len(md) == 3
    md.assign(3, 5)
    assert md.getval(3) == 5
    assert len(md) == 3
    print("\033[0;35mTest 1 Passes")

    md = MyDict()
    try:
        md.getval(3)
        # assert False
    except KeyError:
        pass
    md.assign(1, 2)
    md.assign(3, 4)
    try:
        md.getval(4)
        # assert False
    except KeyError:
        pass
    md.delete(1)
    assert len(md) == 1
    try:
        md.getval(1)
        # assert False
    except KeyError:
        pass
    print("\033[0;36mTest 2 Passes")

    md = MyDict()
    md.assign(10, 2)
    assert md.getval(10) == 2
    md.assign(3, 3)
    assert md.getval(3) == 3
    assert md.getval(10) == 2
    md.assign(4, 2)
    assert md.getval(4) == 2
    assert len(md) == 3
    md.assign(3, 6)
    assert md.getval(3) == 6
    assert len(md) == 3
    assert md.getval(10) == 2
    assert md.getval(3) == 6
    assert md.getval(4) == 2
    md.delete(3)
    assert len(md) == 2
    md.delete(10)
    assert len(md) == 1
    assert md.getval(4) == 2
    print("\033[0;34mTest 3 Passes")

    md = MyDict()
    md.assign(2, 3)
    assert md.getval(2) == 3
    assert len(md) == 1
    try:
        md.getval(3)
        # assert False
    except KeyError:
        pass
    md.assign(3, 3)
    assert len(md) == 2
    assert md.getval(3) == 3
    assert md.getval(2) == 3
    md.assign(4, 2)
    assert len(md) == 3
    md.delete(3)
    assert len(md) == 2
    assert md.getval(2) == 3
    try:
        md.delete(10)
        # assert False
    except KeyError:
        pass
    try:
        md.delete(3)
        # assert False
    except KeyError:
        pass
    md.assign(6, 1)
    assert md.getval(6) == 1
    assert len(md) == 3
    print("\033[1;34mTest 4 Passes")