import 'dart:math';

import 'package:flutter/material.dart';

void main() => runApp(ButtonApp());

class ButtonApp extends StatelessWidget {
  Widget build(BuildContext context) {
    return MaterialApp(
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  _HomePageState createState() => _HomePageState();
}

const List<String> _bibleVerses = [
  "The Word became flesh and made his dwelling among us. We have\n"
      "seen his glory, the glory of the one and only Son, who came from the\n"
      "Father, full of grace and truth.\n"
      "John 1:14 NIV",
  "I have been crucified with Christ and I no longer live, but Christ lives\n"
      "in me. The live I now live in the body, I live by faith in the Son of\n"
      "God, who loved me and gave himself for me.\n"
      "Galatians 2:20 NIV",
  "Have mercy on me, O God,\n"
      "according to your unfailing love;\n"
      "according to your great compassion\n"
      "blot out my transgressions.\n"
      "Psalm 51:1 NIV84",
  "A gentle answer turns away wrath,\n"
      "but a harsh word stirs up anger.\n"
      "Proverbs 15:1 NIV",
];

class _HomePageState extends State {
  String _bibleVerse = "Press the button!";
  Random _random = Random();

  void _changeText() {
    setState(
        () => _bibleVerse = _bibleVerses[_random.nextInt(_bibleVerses.length)]);
  }

  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
          child: Text(
            _bibleVerse,
            textScaleFactor: 1.5,
          ),
        ),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.message),
          onPressed: _changeText,
        ));
  }
}
