"""
Python Set Exercise with Solutions

https://pynative.com/python-set-exercise-with-solutions/
"""


def add_list_to_set(a_list, a_set):
    """
    Adds a list of elements to a given set
    """
    a_set.update(a_list)
    return a_set


def add_list_to_set_2(a_list, a_set):
    """
    Adds a list of elements to a given set (inferior solution).
    """
    for el in a_list:
        a_set.add(el)
    return a_set


def identical(set_1, set_2):
    """
    Returns a set of identical items from two Python sets.
    """
    return set_1.intersection(set_2)


def all_items(set_1, set_2):
    """
    Returns a new set with all items from both sets by removing duplicates.
    """
    return set_1.union(set_2)


def remove_from_first_set_items_in_second_set(set_1, set_2):
    """
    Removes from the first set items that exist in the second set.
    """
    set_1.difference_update(set_2)
    return set_1


def create_set(a_list):
    """
    Creates a set from the given list.
    """
    a_set = set()
    for num in a_list:
        a_set.add(num)
    return a_set


def contains_duplicate(nums):
    """
    Given an array of integers, find if the array contains any duplicates.

    Your function should return true if any value appears at least twice in the array, and it should return false if
    every element is distinct.

    https://leetcode.com/explore/interview/card/top-interview-questions-easy/92/array/578/
    """
    # easier solution
    # nums_set = set(nums)
    # return len(nums_set) != len(nums)

    nums_set = set()
    for num in nums:
        if num in nums_set:
            return True
        nums_set.add(num)
    return False


def single_number(nums):
    """
    Given a non-empty array of integers nums, every element appears twice except for one. Find that single one.

    https://leetcode.com/explore/interview/card/top-interview-questions-easy/92/array/549/
    """
    count_dict = {}
    for num in nums:
        if num in count_dict:
            count_dict[num] += 1
        else:
            count_dict[num] = 1

    for num, count in count_dict.items():
        if count == 1:
            return num
    return None


if __name__ == '__main__':
    assert add_list_to_set(['Blue', 'Green', 'Red'], {'Yellow', 'Orange', 'Black'}) == {'Green', 'Yellow', 'Black',
                                                                                        'Orange', 'Red', 'Blue'}
    assert add_list_to_set_2(['Blue', 'Green', 'Red'], {'Yellow', 'Orange', 'Black'}) == {'Black', 'Blue', 'Green',
                                                                                          'Orange', 'Red', 'Yellow'}

    assert identical({10, 20, 30, 40, 50}, {30, 40, 50, 60, 70}) == {40, 50, 30}

    assert all_items({10, 20, 30, 40, 50}, {30, 40, 50, 60, 70}) == {10, 20, 30, 40, 50, 60, 70}

    assert remove_from_first_set_items_in_second_set({1, 2, 3}, {2, 4, 5}) == {1, 3}

    assert create_set([]) == set()
    assert create_set([1, 1, 1]) == {1}
    assert create_set([1, 2, 3, 4, 5]) == {1, 2, 3, 4, 5}
    print("\033[0;34mCreate Set works\t\t\t\t\t\033[0;33m3 pts")

    assert contains_duplicate([1, 2, 3, 1]) is True
    assert contains_duplicate([1, 2, 3, 4]) is False
    assert contains_duplicate([1, 1, 1, 3, 3, 4, 3, 2, 4, 2]) is True
    assert contains_duplicate([1, 2, 7, 3, 4, 43, 17, 256, -1]) is False
    print("\033[0;35mContains Duplicate works\t\t\t\033[0;33m5 pts")

    assert (single_number([2, 2, 1])) == 1
    assert (single_number([4, 1, 2, 1, 2])) == 4
    assert (single_number([1])) == 1
    print("\033[0;36mSingle Number works\t\t\t\t\t\033[0;33m5 pts")
