from turtle import Turtle, Screen, done
from random import randint
import time

# How often game is updated in milliseconds
FRAME_RATE_MS = 100

# Screen
screen = Screen()
screen.setup(600, 600)
screen.tracer(0)
screen.bgcolor('black')

# Front of the Snake
snake_front = Turtle()
snake_front.shape("square")
snake_front.penup()
snake_front.color('white')
snake_front.goto(0, 0)
snake_direction = ''

# Food
food = Turtle()
food.shape('square')
food.color('red')
food.penup()

# Snake Segments
segments = []

# Score
# From the original pong.py file
score = -10
text = Turtle()
text.penup()
text.hideturtle()
text.goto(0, 250)
text.color('white')


def up():
    global snake_direction

    if snake_direction != 'down':
        snake_direction = 'up'


def down():
    global snake_direction

    if snake_direction != 'up':
        snake_direction = 'down'


def right():
    global snake_direction

    if snake_direction != 'left':
        snake_direction = 'right'


def left():
    global snake_direction

    if snake_direction != 'right':
        snake_direction = 'left'


def snake_movement():
    if snake_direction == 'up':
        yy = snake_front.ycor()
        snake_front.sety(yy + 20)

    if snake_direction == 'down':
        yy = snake_front.ycor()
        snake_front.sety(yy - 20)

    if snake_direction == 'right':
        xx = snake_front.xcor()
        snake_front.setx(xx + 20)

    if snake_direction == 'left':
        xx = snake_front.xcor()
        snake_front.setx(xx - 20)


def add_new_segment():
    global segments, score

    new_segment = Turtle()
    new_segment.shape('square')
    new_segment.penup()
    new_segment.color('white')
    segments.append(new_segment)

    # Updates the score
    score += 10


def segment_movement():
    global segments

    # https://www.edureka.co/blog/python-turtle-module/
    # I looked this part up because I couldn't figure out how to do it
    for index in range(len(segments) - 1, 0, -1):
        x = segments[index - 1].xcor()
        y = segments[index - 1].ycor()
        segments[index].goto(x, y)

    if len(segments) > 0:
        x = snake_front.xcor()
        y = snake_front.ycor()
        segments[0].goto(x, y)


def food_collision():
    # Check if the snake is occupying the same spot as the food
    if snake_front.ycor() == food.ycor() and snake_front.xcor() == food.xcor():
        x = randint(-14, 14)
        y = randint(-14, 14)
        food.goto(x * 20, y * 20)

        # Adds another segment to the snake
        add_new_segment()


def segment_collision():
    global segments, snake_front

    # Takes the X and Y cords of the segments and checks if the snake has collided with it
    for segment in segments[2:]:
        if segment.distance(snake_front) < 20:
            game_over()


def border_collision():
    global snake_front
    if snake_front.xcor() == 300 or snake_front.xcor() == -300 or snake_front.ycor() == 300 or snake_front.ycor() == -300:
        game_over()


def game_over():
    global segments, snake_direction, score

    time.sleep(1)
    snake_front.goto(0, 0)
    snake_direction = ''

    # Reset the Snake Segments
    for segment in segments:
        segment.goto(1000, 1000)
    segments = []

    # Reset the food
    x = randint(-14, 14)
    y = randint(-14, 14)
    food.goto(x * 20, y * 20)
    add_new_segment()

    # Reset the score
    score = -10


# Controls
screen.onkey(up, 'w')
screen.onkey(down, 's')
screen.onkey(right, 'd')
screen.onkey(left, 'a')
screen.listen()


def write_scores():
    text.clear()
    text.write('SCORE:' + str(score), align="center", font=("Arial", 20, "bold"))


def frame():
    food_collision()
    segment_collision()
    border_collision()
    snake_movement()
    segment_movement()
    write_scores()
    screen.update()
    screen.ontimer(frame, FRAME_RATE_MS)


# START GAME
frame()
done()
