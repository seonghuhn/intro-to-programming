import turtle
import random
t = turtle.Turtle()
t.color(1, 1, 1)
t.hideturtle()
s = turtle.Screen()
s.setup(1000, 600)
s.bgcolor(0, 0, 0)
high_score = 0
pen = turtle.Turtle()
pen.speed(0)
pen.shape("square")
pen.color("white")
pen.penup()
pen.hideturtle()
pen.goto(0, 260)
pen.write("Score: 0 High Score: {}".format(high_score), align="center", font=("Courier", 24,  "normal"))
s.tracer(0)
s.listen()
rectangle =turtle.Turtle()
s.register_shape("rectangle", ((-25, -60), (-25, 60), (25, 60), (25, -60)))
rectangle.shape("rectangle")
rectangle.penup()
rectangle.color(1, 1, 1)
rectangle_clone = rectangle.clone()
defense =[]
for i in range(4):
    rectangle_clone.setposition(300-200*i, -150)
    defense.append(rectangle_clone)
    rectangle_clone = rectangle.clone()
rectangle.hideturtle()
rectangle_clone.hideturtle()








new_t = turtle.Turtle()
s.register_shape("ship", ((-3,3.5), (-3,5), (-1,5), (0,6), (1, 5), (3, 5), (3,3.5)))
new_t.shape("ship")
new_t.shapesize(9, 9)
new_t.color(0, 1, 0)
new_t.penup()
new_t.left(90)
new_t.back(280)
def move_left():
    new_t.setx(new_t.xcor()-7)
s.onkey(move_left, "Left")
def move_right():
    new_t.setx(new_t.xcor()+7)
s.onkey(move_right, "Right")

s.register_shape("space_ship", ((0, -2), (-.3, -.5), (-.3, 2), (.3,2), (.3, -.5)))
aliens = turtle.Turtle()
aliens.shape("space_ship")
aliens.color(1, 1, 1)
aliens.shapesize(27, 8)
aliens.left(90)
aliens.penup()
aliens.forward(230)
aliens.right(90)
aliens.forward(390)
aliens.left(90)
alien = []
for q in range(2): 
    aliens.backward(50)
    for t in range(11):
        alien.append(aliens.clone())
        if t == 10:
            break
        aliens.left(90)
        aliens.forward(72)
        aliens.right(90)
    aliens.backward(50)
    for a in range(11):
        alien.append(aliens.clone())
        if a == 10:
            break
        aliens.right(90)
        aliens.forward(72)
        aliens.left(90)
aliens.hideturtle()
for x in alien:
    x.setx((x.xcor())+60)

counter = 0
bullet = turtle.Turtle()
s.register_shape("bullet", ((-1, -2), (-1, 2), (1,2), (1,-2) ))
bullet.shape("bullet")
bullet.hideturtle()
bullet.color(1,1,1)
bullets = []
bullet.penup()
def spawn_bullet():
    global counter
    if counter >= 40:
        new_bullet = bullet.clone()
        new_bullet.showturtle()
        real_height = new_t.ycor()+50
        new_bullet.setposition((new_t.xcor()  ,real_height))
        bullets.append(new_bullet)
        counter = 0
s.onkey(spawn_bullet, "space")
    
alien_bullets = []
def aliens_shooting_RUN(alien_position):
    if random.randint(0,10000) <= 3:
        new_bullet = bullet.clone()
        new_bullet.showturtle()
        real_height = alien_position[1]
        new_bullet.setposition((alien_position[0] ,real_height))
        alien_bullets.append(new_bullet)
        





max = -140
game_over = False
defense_health =[0, 0, 0, 0]
y_speed = 5  
# y speed has to be 5!!
speed = .3
while True:
    counter += 1
    for b in bullets:
        b.sety(b.ycor()+1)
        if b.ycor() > 500:
            b.hideturtle()
            bullets.remove(b)
        for c in alien:
            if b.xcor() <= c.xcor()+6 and b.xcor() >= c.xcor()-6 and b.ycor() <= c.ycor()+6 and b.ycor() >= c.ycor()-6:
                b.hideturtle()
                bullets.remove(b)
                c.hideturtle()
                alien.remove(c)
    for t in alien_bullets:
        t.sety(t.ycor()-1)
        if t.ycor() > 500:
            b.hideturtle()
            bullets.remove(b)
        if t.xcor() <= new_t.xcor()+12 and t.xcor() >= new_t.xcor()-12 and t.ycor() <= new_t.ycor()+12 and t.ycor() >= new_t.ycor()-12:
            t.hideturtle()
            alien_bullets.remove(t)
            new_t.hideturtle()
            s.resetscreen()
            alien_bullets = []
            bullets = []
            defense = []
            game_over = True
            break
        for o in defense:
            if o.xcor() <= t.xcor()+100 and o.xcor() >= t.xcor()-50 and o.ycor() <= t.ycor()+100 and o.ycor() >= t.ycor()-50:
                t.hideturtle()
                defense_health[defense.index(o)] += 1
                alien_bullets.remove(t)
            if defense_health[defense.index(o)] >= 5:
                defense_health.pop(defense.index(o))
                o.hideturtle()
                defense.remove(o)
    for x in alien:
            x.setx((x.xcor())+speed)
            if (x.xcor())+speed> 480:
                for g in alien:
                    g.sety((g.ycor())-y_speed)
                speed = -speed
            if (x.xcor())+speed< -480:
                for g in alien:
                    g.sety((g.ycor())-y_speed)
                speed = -speed
            aliens_shooting_RUN(x.position())
    if x.ycor() <= max:
        s.resetscreen()
        game_over3 = turtle.Turtle()
        game_over3.speed(0)
        game_over3.shape("square")
        game_over3.color("white")
        game_over3.penup()
        game_over3.hideturtle()
        game_over3.goto(0, 0)
        game_over3.write("ALIENS REACHED EARTH, GAME OVER LOOSER!!", align="center", font=("Courier", 24,  "normal"))
    if game_over == True:
        break
    if alien == []:
        s.resetscreen()
        game_over2 = turtle.Turtle()
        game_over2.speed(0)
        game_over2.shape("square")
        game_over2.color("white")
        game_over2.penup()
        game_over2.hideturtle()
        game_over2.goto(0, 0)
        game_over2.write("YOU HAVE WON!!", align="center", font=("Courier", 24,  "normal")) 

    s.update()


game_over = turtle.Turtle()
game_over.speed(0)
game_over.shape("square")
game_over.color("white")
game_over.penup()
game_over.hideturtle()
game_over.goto(0, 0)
game_over.write("GAME OVER", align="center", font=("Courier", 24,  "normal"))
if alien == []:
    s.resetscreen()
    game_over2 = turtle.Turtle()
    game_over2.speed(0)
    game_over2.shape("square")
    game_over2.color("white")
    game_over2.penup()
    game_over2.hideturtle()
    game_over2.goto(0, 0)
    game_over2.write("YOU HAVE WON!!", align="center", font=("Courier", 24,  "normal")) 

turtle.done()


