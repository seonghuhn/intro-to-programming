from turtle import Turtle, Screen
import turtle

cursor_size = 20
square_size = 60
FONT_SIZE = 40
FONT = ('Arial', FONT_SIZE, 'bold')
t = turtle.Turtle()

class TicTacToe:

    def __init__(self):
        self.board = [['?'] * 3 for i in range(3)]
        self.turn = 'X'
        self.p = 1
        self.count = 1
        self.b = [[0, 0, 0],
                  [0, 0, 0],
                  [0, 0, 0]]
    def drawBoard(self):
        background = Turtle('square')
        background.shapesize(square_size * 3 / cursor_size)
        background.color('black')
        background.stamp()
        background.hideturtle()

        for j in range(3):
            for i in range(3):
                box = Turtle('square', visible=False)
                box.shapesize(square_size / cursor_size)
                box.color('white')
                box.penup()
                box.goto(i * (square_size + 2) - (square_size + 2), j * (square_size + 2) - (square_size + 2))
                box.showturtle()
                box.stamp()
                self.board[j][i] = box
                box.onclick(lambda x, y, box=box, i=i, j=j: self.mouse(box, i, j))

    def mouse(self, box, i, j):
        box.onclick(None)
        box.hideturtle()
        box.color('black')
        box.sety(box.ycor() - FONT_SIZE / 2)
        box.write(self.turn, align='center', font=FONT)
        self.board[j][i] = self.turn
        self.turn = ['X', 'O'][self.turn == 'X']

        if self.count == 1 or self.count == 3 or self.count == 5 or self.count == 7 or self.count == 9:
            self.b[j][i] = -1
        else:
            self.b[j][i] = 1
        '''X win'''
        #bottom row
        if self.b[0][0] + self.b[0][1] + self.b[0][2] == -3:
            screen.textinput("Game over!", "X won!")
        #middle row
        if self.b[1][0] + self.b[1][1] + self.b[1][2] == -3:
            screen.textinput("Game over!", "X won!")
        #top row
        if self.b[2][0] + self.b[2][1] + self.b[2][2] == -3:
            screen.textinput("Game over!", "X won!")
        #right column
        if self.b[0][2] + self.b[1][2] + self.b[2][2] == -3:
            screen.textinput("Game over!", "X won!")
        #middle column
        if self.b[0][1] + self.b[1][1] + self.b[2][1] == -3:
            screen.textinput("Game over!", "X won!")
        #left column
        if self.b[0][0] + self.b[1][0] + self.b[2][0] == -3:
            screen.textinput("Game over!", "X won!")
        #diagonals
        if self.b[0][2] + self.b[1][1] + self.b[2][0] == -3:
            screen.textinput("Game over!", "X won!")
        if self.b[0][0] + self.b[1][1] + self.b[2][2] == -3:
            screen.textinput("Game over!", "X won!")
        '''O win'''
        # bottom row
        if self.b[0][0] + self.b[0][1] + self.b[0][2] == 3:
            screen.textinput("Game over!", "O won!")
        # middle row
        if self.b[1][0] + self.b[1][1] + self.b[1][2] == 3:
            screen.textinput("Game over!", "O won!")
        # top row
        if self.b[2][0] + self.b[2][1] + self.b[2][2] == 3:
            screen.textinput("Game over!", "O won!")
        # right column
        if self.b[0][2] + self.b[1][2] + self.b[2][2] == 3:
            screen.textinput("Game over!", "O won!")
        # middle column
        if self.b[0][1] + self.b[1][1] + self.b[2][1] == 3:
            screen.textinput("Game over!", "O won!")
        # left column
        if self.b[0][0] + self.b[1][0] + self.b[2][0] == 3:
             screen.textinput("Game over!", "O won!")
        # diagonals
        if self.b[0][2] + self.b[1][1] + self.b[2][0] == 3:
            screen.textinput("Game over!", "O won!")
        if self.b[0][0] + self.b[1][1] + self.b[2][2] == 3:
            screen.textinput("Game over!", "O won!")

        if self.count == 9:
            screen.textinput("Game over!", "Tie!")
        else:
            pass
        self.count += 1

screen = Screen()
game = TicTacToe()
game.drawBoard()
screen.mainloop()