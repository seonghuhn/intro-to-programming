from random import randint
from random import choice
from turtle import Turtle, Screen, Shape, done


'''
end = Turtle()
end.hideturtle()
end.penup()
end.goto(100, 0)
end.pendown()
end.write('End', align="center", font=("Arial", 26, "bold"))

def summon_enemy():
    enemy = Turtle()
    enemy.penup()
    point_ends = 20
    point_bottoms = 7
    enemy_shape = Shape('compound')
    enemy_points = ((point_ends, 0), (point_bottoms, point_bottoms), (0, point_ends),
                    (-point_bottoms, point_bottoms),(-point_ends, 0), (-point_bottoms, -point_bottoms), (0, -point_ends),
                    (point_bottoms, -point_bottoms))
    enemy_shape.addcomponent(enemy_points, 'red')
    screen.register_shape('star', enemy_shape)
    enemy.shape('star')
    x_pos = -100
    enemy.setx(x_pos)
    enemy.pendown()
    while x_pos < 100:
        x_pos += 10
        enemy.setx(x_pos)
        sleep(0.1)
        if x_pos == 99:
            frame_number += 1
            score -= 7
            frame(frame_number)
'''
def random():
    ran = randint(-20, 20)
    if ran == 0:
        ran += 1
    return ran
def answers(correct, index):
    numbers = []
    while len(numbers) < 4:
        numbers.append(correct + random())

    numbers[index] = correct
    return numbers


def math_problem(operation):
    outcome = []
    xrange = 0
    correctindex = randint(0, 3)
    if operation == 'x':
        choose = list(range(-10, 0)) + list(range(1, 11))
        num1 = choice(choose)
        num2 = choice(choose)
        problem = str(num1) + ' x ' + str(num2)
        ans = answers(num1 * num2, correctindex)
        return (problem, ans, correctindex)

    #elif operation == '/':
    #elif operation == '+':
    #elif operation == '-':


def give_problem():
    global problem, ans1, ans2, ans3, ans4, correct
    prob = math_problem('x')
    problem = prob[0]
    ans1 = (prob[1])[0]
    ans2 = (prob[1])[1]
    ans3 = (prob[1])[2]
    ans4 = (prob[1])[3]
    correct = (prob[1])[prob[2]]
    write_problem()


def one():
    global ans1, score, frame_number, correct
    if ans1 == correct:
        score += 10
        write_score()
    else:
        score -= 5
        write_score()
    frame_number += 1
    frame(frame_number)

def two():
    global ans2, score, frame_number, correct
    if ans2 == correct:
        score += 10
        write_score()
    else:
        score -= 5
        write_score()
    frame_number += 1
    frame(frame_number)

def three():
    global ans3, score, frame_number, correct
    if ans3 == correct:
        score += 10
        write_score()
    else:
        score -= 5
        write_score()
    frame_number += 1
    frame(frame_number)

def four():
    global ans4, score, frame_number, correct
    if ans4 == correct:
        score += 10
        write_score()
    else:
        score -= 5
        write_score()
    frame_number += 1
    frame(frame_number)

end_turtle = Turtle()
end_turtle.hideturtle()

def end_screen():
    screen.clear()
    end_turtle.speed(0)
    end_turtle.goto(0,0)
    end_turtle.pencolor('green')
    end_turtle.write('Your Final Score is: %s' %score, align="center", font=("Chalkboard", 26, "bold"))
    

def frame(number):
    if number < 10:
        give_problem()
    else:
       end_screen()

screen = Screen()
screen.setup(600, 400)  # width, height
screen.tracer(0)
screen.onkeypress(one, "1")
screen.onkeypress(two, "2")
screen.onkeypress(three, "3")
screen.onkeypress(four, "4")


screen.listen()
score_turtle = Turtle()
score_turtle.penup()
score_turtle.hideturtle()
score = 0
def write_score():
    score_turtle.clear()
    score_turtle.goto(-180, -160)
    score_turtle.write(f'Score: %s' %score, align="center", font=("Arial", 26, "bold"))

frame_number = 0
problem = ''
ans1 = ''
ans2 = ''
ans3 = ''
ans4 = ''
correct = ''
problem_turtle = Turtle()
def write_problem():
    problem_turtle.clear()
    problem_turtle.goto(-120, 120)
    problem_turtle.write(f'{problem} = (1) {ans1}    (2) {ans2} \n    '
                         f'         (3) {ans3}    (4) {ans4}', align="center", font=("Arial", 20, "bold"))


write_score()
write_problem()
frame(frame_number)
done()



