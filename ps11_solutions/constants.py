# User event frequencies
RELOAD_SPEED = 400
MOVE_SIDEWAYS = 1000
MOVE_DOWN = 1000
BONUS_FREQ = 10000
INV_SHOOT_FREQ = 500

# colors
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)
YELLOW = (255, 255, 0)
DIMGRAY = (105, 105, 105)

# invader animation variables
animation_time = 60
animate_invaders = False
invader_width = 30
invader_height = 15


class Constants:
    # User event frequencies
    RELOAD_SPEED = 400
    MOVE_SIDEWAYS = 1000
    MOVE_DOWN = 1000
    BONUS_FREQ = 10000
    INV_SHOOT_FREQ = 500

    # colors
    BLACK = (0, 0, 0)
    WHITE = (255, 255, 255)
    RED = (255, 0, 0)
    GREEN = (0, 255, 0)
    BLUE = (0, 0, 255)
    YELLOW = (255, 255, 0)
    DIMGRAY = (105, 105, 105)

    # invader animation variables
    animation_time = 60
    animate_invaders = False
    invader_width = 30
    invader_height = 15
