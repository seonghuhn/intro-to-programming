import turtle
from turtle import done, Screen, Shape
from random import randint
screen = turtle.Screen()
screen.setup(width=700,height=400)
screen.tracer(0)
FRAME_RATE_MS = 40
ball_move_h = 10  # Horizontal movement per frame
ball_move_v = 7 # Vertical movement per frame
player_move = 7
def reset_ball():
    global ball_move_h, ball_move_v
    ball.setpos(0, 0)
    ball_move_h = randint(4, 10)
    ball_move_v = randint(3, 7)
    if randint(0, 100) > 50:  # 50% chance of going left instead of right
        ball_move_h *= -1
    if randint(0, 100) > 50:  # 50% chance of going down instead of up
        ball_move_v *= -1
def check_if_someone_scores():
    global score_L, score_R
    if (ball.xcor() + ball_radius) >= play_right:  # right of ball at right of field
        score_L += 1
        write_scores()
        reset_ball()
    elif play_left >= (ball.xcor() - ball_radius):  # left of ball at left of field
        score_R += 1
        write_scores()
        reset_ball()


player1_direction_y = 0
player1_direction_x = 0
player2_direction_y = 0
player2_direction_x = 0
def player1Up():
  global player1_direction_y
  player1_direction_y = 1
def player1Down():
    global player1_direction_y
    player1_direction_y = -1
def player1Right():
    global player1_direction_x
    player1_direction_x = 1
def player1Left():
    global player1_direction_x
    player1_direction_x = -1
def player2Up():
  global player2_direction_y
  player2_direction_y = 1
def player2Down():
    global player2_direction_y
    player2_direction_y = -1
def player2Right():
    global player2_direction_x
    player2_direction_x = 1
def player2Left():
    global player2_direction_x
    player2_direction_x = -1
def player1OffX():
    global player1_direction_x
    player1_direction_x = 0
def player1OffY():
    global player1_direction_y
    player1_direction_y = 0
def player2OffX():
    global player2_direction_x
    player2_direction_x = 0
def player2OffY():
    global player2_direction_y
    player2_direction_y = 0
def update_paddle_positions():
    player1_new_y_pos = Player1.ycor() + (player1_direction_y * player_move)
    player2_new_y_pos = Player2.ycor() + (player2_direction_y * player_move)
    player1_new_x_pos = Player1.xcor() + (player1_direction_x * player_move)
    player2_new_x_pos = Player2.xcor() + (player2_direction_x * player_move)
    if is_paddle_allowed_to_move_here_y_pos(player1_new_y_pos):
        Player1.sety(player1_new_y_pos)
    if is_paddle_allowed_to_move_here_x_pos(player1_new_x_pos) and is_player1_paddle_allowed_past_middle(player1_new_x_pos):
        Player1.setx(player1_new_x_pos)
    if is_paddle_allowed_to_move_here_y_pos(player2_new_y_pos):
        Player2.sety(player2_new_y_pos)
    if is_paddle_allowed_to_move_here_x_pos(player2_new_x_pos) and is_player2_paddle_allowed_past_middle(player2_new_x_pos):
        Player2.setx(player2_new_x_pos)

def is_paddle_allowed_to_move_here_y_pos(new_y_pos):
    if (play_bottom > new_y_pos - paddle_h_half):  # bottom of paddle below bottom of field
        return False
    if (new_y_pos + paddle_h_half > play_top):  # top of paddle above top of field
        return False
    return True

def is_paddle_allowed_to_move_here_x_pos(new_x_pos):
    if (play_left > new_x_pos - paddle_w_half):
        return False
    if (new_x_pos + paddle_w_half > play_right):
        return False
    return True

def is_player1_paddle_allowed_past_middle(new_x_pos):
    if play_middle < new_x_pos + paddle_w_half:
        return False
    return True
def is_player2_paddle_allowed_past_middle(new_x_pos):
    if play_middle > new_x_pos + paddle_w_half:
        return False
    return True

def ball_collides_with_paddle(paddle):
    xx = abs(paddle.xcor() - ball.xcor())
    yy = abs(paddle.ycor() - ball.ycor())
    overlap_horizontally = ball_radius + paddle_w_half >= xx
    overlap_vertically = ball_radius + paddle_h_half >= yy
    return overlap_horizontally and overlap_vertically

def update_ball_position():
    global ball_move_h, ball_move_v
    if ball.ycor() + ball_radius >= play_top:  # top of ball at or above top of field
        ball_move_v *= -1
    elif play_bottom >= ball.ycor() - ball_radius:  # bottom of ball at or below bottom of field
        ball_move_v *= -1
    if ball_collides_with_paddle(Player1) or ball_collides_with_paddle(Player2):
        ball_move_h *= -1
    ball.setx(ball.xcor() + ball_move_h)
    ball.sety(ball.ycor() + ball_move_v)



def frame():
    update_paddle_positions()
    check_if_someone_scores()
    update_ball_position()
    screen.update()  # show the new frame
    screen.ontimer(frame, FRAME_RATE_MS)

screen.onkeypress(player1Up, "w")
screen.onkeypress(player1Down, "s")
screen.onkeypress(player1Right, "d")
screen.onkeypress(player1Left, "a")
screen.onkeyrelease(player1OffX, "d")
screen.onkeyrelease(player1OffX, "a")
screen.onkeyrelease(player1OffY, "w")
screen.onkeyrelease(player1OffY, "s")

screen.onkeypress(player2Up, "Up")
screen.onkeypress(player2Down, "Down")
screen.onkeypress(player2Right, "Right")
screen.onkeypress(player2Left, "Left")
screen.onkeyrelease(player2OffX, "Right")
screen.onkeyrelease(player2OffX, "Left")
screen.onkeyrelease(player2OffY, "Up")
screen.onkeyrelease(player2OffY, "Down")

play_top = screen.window_height() / 2 - 100  # top of screen minus 100 units
play_bottom = -screen.window_height() / 2 + 100  # 100 from bottom
play_left = -screen.window_width() / 2 + 50  # 50 from left
play_right = screen.window_width() / 2 - 50  # 50 from right
play_middle = -screen.window_width() / 2 + 350  # 350 from left
# Draw playing field
# https://wecode24.com/stories/Abraham/pong-with-turtle-graphics-part-1/pong_layout.png
area = turtle.Turtle()
area.hideturtle()
area.speed(8)  # make it move faster
area.penup()
area.goto(play_right, play_top)
area.pendown()
area.goto(play_left, play_top)
area.goto(play_left, play_bottom)
area.goto(play_right, play_bottom)
area.goto(play_right, play_top)
area.goto(play_middle,play_top)
area.goto(play_middle, play_bottom)
# Ball
ball = turtle.Turtle()
ball.penup()
ball.shape("circle")  # Use the built-in shape "circle"
ball.shapesize(0.5, 0.5)  # Stretch it to half default size
ball_radius = 10 * 0.5  # Save the new radius for later

Player1 = turtle.Turtle()
Player2 = turtle.Turtle()
Player1.penup()
Player2.penup()
# Set up paddles shape
paddle_w_half = 10 / 2  # 10 units wide
paddle_h_half = 20 / 2  # 40 units high
paddle_shape = Shape("compound")
paddle_points = ((-paddle_h_half, -paddle_w_half),
                 (-paddle_h_half, paddle_w_half),
                 (paddle_h_half, paddle_w_half),
                 (paddle_h_half, -paddle_w_half))
paddle_shape.addcomponent(paddle_points, "black")
screen.register_shape("paddle", paddle_shape)
# Update shape of paddles
Player1.shape("paddle")
Player2.shape("paddle")
# Move paddles to starting positions
Player1.setx(play_left + 10)
Player2.setx(play_right - 10)
# Score



screen.listen()
score_turtle = turtle.Turtle()
score_turtle.penup()
score_turtle.hideturtle()
score_L = 0
score_R = 0
def write_scores():
    score_turtle.clear()
    score_turtle.goto(-screen.window_width() / 4, screen.window_height() / 2 - 80)
    score_turtle.write(score_L, align="center", font=("Arial", 32, "bold"))
    score_turtle.goto(screen.window_width() / 4, screen.window_height() / 2 - 80)
    score_turtle.write(score_R, align="center", font=("Arial", 32, "bold"))
write_scores()
frame()
done()