import turtle
from random import *
from turtle import *
from freegames import vector
screen = Screen()
screen.bgcolor("black") #I made the screen black so it would be spacelike
#I added a spaceship for fun bc I couldnt use it as a vector but I still wanted a spaceship, referenced https://wecode24.com/stories/Abraham/animation-with-turtle-graphics/
screen.addshape("spaceship.gif")
spaceship_t = turtle.Turtle()
spaceship_t.speed(0)
spaceship_t.shape("spaceship.gif")
spaceship_t.penup()

spaceship = vector(0, 0)

balls = []
# I decided to modify the flappy bird game from freegames

"Move spaceship up in response to screen tap."
#I changed it from screen tapping to the arrow keys and spacebar so you can go up, backwards, and forward
speed = 1
def up():
    up = vector(0, 30)
    spaceship.move(up)
def forward():
    forward = vector(30, 0)
    spaceship.move(forward)
def backward():
    backward = vector(-30, 0)
    spaceship.move(backward)


turtle.listen()
turtle.onkey(up, "space") #tap spacebar to go up
turtle.onkey(forward, "Right")
turtle.onkey(backward, "Left")


def inside(point):
    "Return True if point on screen."
    return -200 < point.x < 200 and -200 < point.y < 200

def draw(alive):
    "Draw screen objects."
    clear()

    goto(spaceship.x, spaceship.y)
    if alive:
        dot(10, 'green')
    else:
        dot(10, 'red')

    for ball in balls:
        goto(ball.x, ball.y)
        dot(randint(15, 25), 'white') #changed the balls to be white to be "asteroids" and varied the sizes


def move():
    "Update object positions."
    spaceship.y -= 5

    for ball in balls:
        ball.x -= 3

    if randrange(10) == 0:
        y = randrange(-199, 199)
        ball = vector(199, y)
        balls.append(ball)

    while len(balls) > 0 and not inside(balls[0]):
        balls.pop(0)

    if not inside(spaceship):
        draw(False)
        return

    for ball in balls:
        if abs(ball - spaceship) < 15:
            draw(False)
            return
    t = 40 #changing t varies the speed, (original speed = 50)
    ontimer(move, t)
    draw(True)

'''score_turtle = Turtle()
score_turtle.penup()
score_turtle.hideturtle()
score = 0
def write_scores():
    score_turtle.clear()
    score_turtle.goto(-screen.window_width() / 4, screen.window_height() / 2 - 80)
    score_turtle.write(score, pencolor = "white", align="center", font=("Arial", 32, "bold"))'''
#I couldn't get the score to show up on the screen or count by seconds so idk at this point but I tried a lot of different ways and couldnt get it to work

screen.update()
setup(420, 420, 370, 0)
hideturtle()
up()
tracer(False)
move()
done()
