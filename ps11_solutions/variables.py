from itertools import cycle


class Variables:
    # flashing text vars
    the_text = cycle(["Press Enter To Play, Earthling...", ""])
    insert = next(the_text)
    flash_timer = 0

    # flashing bonus item vars
    y1, y2, y3, y4, y5, y6 = (255, 255, 0), (225, 225, 0), (195, 195, 0), (165, 165, 0), (135, 135, 0), (105, 105, 0)
    bonus_colors = cycle([y1, y2, y3, y4, y5, y6])
    bonus_color = next(bonus_colors)
    bonus_x = cycle([4, 11, 18, 25, 32, 39])  # change draw x coord
    bonus_timer = 0  # used to control frequency of changes

    # vars for moving invaders down
    move_right, move_down, reloaded = True, True, True
    vert_steps = 0
    side_steps = 0
    moved_down = False
    invaders_paused = False

    invaders = 0  # prevents error until list is created
    initial_invaders = 0  # use to manage freq of inv shots as invaders removed
    shoot_level = 1  # manage freq of shots

    # various gameplay variables
    game_over = True
    score = 0
    lives = 2
    level = 0
    playing = False


# flashing text vars
the_text = cycle(["Press Enter To Play, Earthling...", ""])
insert = next(the_text)
flash_timer = 0

# flashing bonus item vars
y1, y2, y3, y4, y5, y6 = (255, 255, 0), (225, 225, 0), (195, 195, 0), (165, 165, 0), (135, 135, 0), (105, 105, 0)
bonus_colors = cycle([y1, y2, y3, y4, y5, y6])
bonus_color = next(bonus_colors)
bonus_x = cycle([4, 11, 18, 25, 32, 39])  # change draw x coord
bonus_timer = 0  # used to control frequency of changes

# vars for moving invaders down
move_right, move_down, reloaded = True, True, True
vert_steps = 0
side_steps = 0
moved_down = False
invaders_paused = False

invaders = 0  # prevents error until list is created
initial_invaders = 0  # use to manage freq of inv shots as invaders removed
shoot_level = 1  # manage freq of shots

# various gameplay variables
game_over = True
score = 0
lives = 2
level = 0
playing = False
