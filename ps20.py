class MySet(object):
    def __init__(self):
        # TODO
        pass

    def insert(self, e):
        # TODO
        pass

    def member(self, e):
        # TODO
        pass

    def remove(self, e):
        # TODO
        pass

    def intersect(self, other):
        # TODO
        pass

    def __len__(self):
        # TODO
        pass

    def __str__(self):
        # TODO
        pass


if __name__ == "__main__":
    # create set 1
    set_1 = MySet()
    assert len(set_1) == 0
    assert str(set_1) == "{}"

    # create set 2
    set_2 = MySet()
    assert len(set_2) == 0
    assert str(set_2) == "{}"

    # test intersection of two empty sets
    set_3 = set_1.intersect(set_2)
    assert len(set_3) == 0

    # test insert, remove, member
    set_1.insert(-17)
    assert len(set_1) == 1
    assert set_1.member(-17) is True
    set_1.insert(-12)
    set_1.insert(-6)
    set_1.insert(20)
    set_1.insert(3.14)
    assert len(set_1) == 5
    set_1.insert(3.14)
    assert len(set_1) == 5
    assert str(set_1) == "{-17,-12,-6,3.14,20}"
    print("\033[0;32mSet Insert works\t\t\t\t\033[0;33m2 pts")
    assert set_1.member(-12) is True
    assert set_1.member(-6) is True
    assert set_1.member(3.14) is True
    assert set_1.member(20) is True
    set_1.remove(-17)
    assert set_1.member(-17) is False
    assert len(set_1) == 4
    assert str(set_1) == "{-12,-6,3.14,20}"
    print("\033[0;36mSet Member works\t\t\t\t\033[0;33m2 pts")
    try:
        set_1.remove(-17)
        assert False
    except ValueError:
        print("\033[0;35mSet Remove works\t\t\t\t\033[0;33m2 pts")

    # test intersection
    set_3 = set_1.intersect(set_2)
    assert isinstance(set_3, MySet)
    assert len(set_3) == 0
    set_2.insert(-20)
    set_2.insert(-16)
    set_2.insert(-11)
    assert len(set_2) == 3
    assert str(set_2) == "{-20,-16,-11}"
    set_3 = set_1.intersect(set_2)
    assert len(set_3) == 0
    set_2.insert(3.14)
    set_3 = set_1.intersect(set_2)
    assert len(set_3) == 1
    assert str(set_3) == "{3.14}"

    print("\033[0;34mSet Intersect works\t\t\t\t\033[0;33m2 pts")

    # test _len__, __str__
    set_4 = MySet()
    set_4.insert(22)
    set_4.insert(2)
    set_4.insert(3)
    assert len(set_4) == 3
    assert str(set_4) == "{2,3,22}"
    set_4.remove(2)
    assert len(set_4) == 2
    assert str(set_4) == "{3,22}"

    print("\033[0;37mLen and Str works\t\t\t\t\033[0;33m2 pts")
