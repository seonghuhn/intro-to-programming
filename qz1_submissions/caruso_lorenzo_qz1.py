def count_vowels(s):
    s = s.lower()
    count = 0
    for letter in s:
        if letter == 'a':
            count += 1
        if letter == 'e':
            count += 1
        if letter == 'i':
            count += 1
        if letter == 'o':
            count += 1
        if letter == 'u':
            count += 1
    return count

