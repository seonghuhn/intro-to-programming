def count_vowels(s):
    # s = 'uysfaiufowyawidaiudiuiuoyggkwadg'
    vowels = 0
    for char in s:
        if char == 'a' or char == 'e' or char == 'i' \
                or char == 'o' or char == 'u':
            vowels += 1
    return vowels


def is_prime(n):
    # n = 10
    count = 2
    while count <= n:
        if (n / count) == int(n):
            # print('true')
            # break
            return True
        else:
            count += 1
        if count >= n:
            # print('false')
            return False
