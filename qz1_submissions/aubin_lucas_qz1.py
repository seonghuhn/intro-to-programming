def count_vowels(s):
    num_of_vowels = 0
    for a in s.lower():
        if a in ('a', 'e', 'i', 'o', 'u'):
            num_of_vowels += 1
    return num_of_vowels


def is_prime(n):
    h = int(n / 2)
    for x in range(2, h + 1):
        if n % x == 0:
            return False
    return True
