def is_vowel(s):
    return s.lower() in ['a', 'e', 'i', 'o', 'u']


def count_vowels(str):
    count = 0
    for i in range(len(str)):

        if is_vowel(str[i]):
            count += 1
    return count


def is_prime(n):
    for factor in range(2, n // 2):
        if n % factor == 0:
            return False
    return True
