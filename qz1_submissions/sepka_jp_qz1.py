def count_vowels(s):
    vowels = "aeiou"
    vowel_count = 0
    for a in range(len(s)):
        if s[a] in vowels:
            vowel_count += 1
    return vowel_count


def is_prime(n):
    for a in range(n):
        if a != 1 and a != 0:
            if n % a == 0:
                return False
    return True
