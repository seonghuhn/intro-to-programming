def count_vowels(s):
    # s = 'ajdefhkja'
    count = 0
    for vowel in range(len(s)):
        if 'a' in s.lower():
            count += 1
        if 'e' in s.lower():
            count += 1
        if 'i' in s.lower():
            count += 1
        if 'o' in s.lower():
            count += 1
        if 'u' in s.lower():
            count += 1
    # print(count)
    return count


def is_prime(n):
    # n = 12
    for i in range(2, n):
        if n % i == 0:
            return False
        else:
            return True
