def less_than_4(t):
    """ t, tuple
        Each element of t is a string
        t could be empty
        Returns the list of strings in t that have fewer than 4 characters"""
    ret = []
    for el in t:
        if len(el) < 4:
            ret.append(el)
    return ret


def max_val(t):
    """ t, tuple
        Each element of t is either an int, a tuple, or a list
        No tuple or list is empty
        All ints are positive
        Returns the maximum int in t or (recursively) in an element of t"""
    maximum = 0
    for x in t:
        if type(x) == int:
            local_max = x
        else:
            local_max = max_val(x)
        if not maximum or local_max > maximum:
            maximum = local_max
    return maximum


def poly(nums, x):
    """ nums, a tuple of numbers (n0, n1, n2, ... nk)
        nums is never empty
        Returns the value of the polynomial applied to the value x, i.e. n0 * x^k + n1 * x^(k-1) + ... nk * x^0 """
    k = len(nums) - 1
    ret = 0
    for n in nums:
        ret += n * x ** k
        k -= 1
    return ret


if __name__ == "__main__":
    assert less_than_4(()) == []
    assert less_than_4(("apple", "beta")) == []
    assert less_than_4(("apple", "cat", "dog", "banana")) == ["cat", "dog"]
    assert less_than_4(("VyFn", "oTzOJ", "YBnWr", "", "QMdAC", "", "", "BvHoi", "WdGL", "R")) == ["", "", "", "R"]
    print("\033[0;34m less_than_4 works\t\t\033[0;33m3 pts")

    assert max_val((1,)) == 1
    assert max_val((1, 2)) == 2
    assert max_val((3, 1, 3)) == 3
    assert max_val((5, (1, 2), [1, 6])) == 6
    assert max_val((5, (1, 2), [[1], [2]])) == 5
    assert max_val((5, (1, 2), [[1], [2, 3, 4]])) == 5
    assert max_val((5, (1, 2), [[1], [9, 4]])) == 9
    print("\033[0;35m max_val works\t\t\t\033[0;33m7 pts")

    assert poly((1, 2, 3, 4), 10) == 1234
    assert poly((4, 3, 2, 1, 0), 10) == 43210
    assert poly((1, 0, 1, 1, 1), 2) == 23
    print("\033[0;36m poly works\t\t\t\t\033[0;33m3 pts")
