class Coordinate(object):
    def __init__(self, x, y):
        pass


class Circle(object):
    def __init__(self, r):
        pass

    def inside(self, coordinate):
        return False


class Line(object):
    def __init__(self, coordinate_1, coordinate_2):
        pass

    def x_intercept(self):
        return 0

    def y_intercept(self):
        return 0


if __name__ == "__main__":
    assert Circle(5).inside(Coordinate(0, 0)) is True
    assert Circle(5).inside(Coordinate(-2, -2)) is True
    assert Circle(5).inside(Coordinate(-2.9, 4)) is True
    assert Circle(5).inside(Coordinate(3, -3.95)) is True
    assert Circle(5).inside(Coordinate(3, 4)) is False
    assert Circle(5).inside(Coordinate(4, 3)) is False
    assert Circle(5).inside(Coordinate(5, 0)) is False
    assert Circle(5).inside(Coordinate(0, -5)) is False
    assert Circle(5).inside(Coordinate(3.1, 4.1)) is False
    print("\033[0;34mInside Circle works\t\t\t\t\t\t\033[0;33m2 pts")

    line = Line(Coordinate(5, 0), Coordinate(0, 10))
    assert line.x_intercept() == 5
    assert line.y_intercept() == 10
    line = Line(Coordinate(0, 0), Coordinate(-4324, 10432))
    assert line.x_intercept() == 0
    assert line.y_intercept() == 0
    line = Line(Coordinate(1.5, 3.75), Coordinate(6.5, 23.75))
    assert line.x_intercept() == 0.5625
    assert line.y_intercept() == -2.25
    print("\033[0;34mLine Intercepts works\t\t\t\t\t\033[0;33m3 pts")
