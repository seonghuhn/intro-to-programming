def is_palindrome_exact(s):
    if len(s) <= 1:
        return True
    return s[0] == s[-1] and is_palindrome_exact(s[1:-1])


def is_palindrome_ignore_case(s):
    if len(s) <= 1:
        return True
    return s[0].lower() == s[-1].lower() and is_palindrome_ignore_case(s[1:-1])


def is_palindrome(s):
    if len(s) <= 1:
        return True
    if not s[0].isalnum() and not s[-1].isalnum():
        return is_palindrome(s[1:-1])
    if not s[0].isalnum():
        return is_palindrome(s[1:])
    if not s[-1].isalnum():
        return is_palindrome(s[:-1])
    return s[0].lower() == s[-1].lower() and is_palindrome(s[1:-1])


if __name__ == "__main__":
    assert is_palindrome_exact("banana") is False
    assert is_palindrome_exact("")
    assert is_palindrome_exact("saippuakivikauppias")
    assert is_palindrome_exact("Bob") is False
    assert is_palindrome_exact("Anna") is False
    assert is_palindrome_exact("An.na") is False
    assert is_palindrome_exact("101")
    assert is_palindrome_exact("F1f") is False
    assert is_palindrome_exact("F1 f") is False
    assert is_palindrome_exact("A man, a plan, a canal: Panama") is False

    assert is_palindrome_ignore_case("banana") is False
    assert is_palindrome_ignore_case("")
    assert is_palindrome_ignore_case("saippuakivikauppias")
    assert is_palindrome_ignore_case("Bob")
    assert is_palindrome_ignore_case("Anna")
    assert is_palindrome_ignore_case("An.na")
    assert is_palindrome_ignore_case("101")
    assert is_palindrome_ignore_case("F1f")
    assert is_palindrome_ignore_case("F1 f") is False
    assert is_palindrome_ignore_case("A man, a plan, a canal: Panama") is False

    assert is_palindrome("banana") is False
    assert is_palindrome("")
    assert is_palindrome("saippuakivikauppias")
    assert is_palindrome("Bob")
    assert is_palindrome("Anna")
    assert is_palindrome("An.na")
    assert is_palindrome("101")
    assert is_palindrome("F1f")
    assert is_palindrome("F1 f")
    assert is_palindrome("A man, a plan, a canal: Panama")
