import simple_function_problems
from ps7_solutions import aubin_lucas_ps7, caruso_lorenzo_ps7, deboard_sean_ps7, kehl_dominic_ps7, kehl_julia_ps7, \
    sepka_jp_ps7, shen_joseph_ps7


def test_maximum():
    assert simple_function_problems.maximum(3, 2, 1) == 3
    assert simple_function_problems.maximum(3, 3, 1) == 3
    assert simple_function_problems.maximum(3, 3, 3) == 3
    assert simple_function_problems.maximum(2, 3, 1) == 3
    assert simple_function_problems.maximum(1, 3, 2) == 3
    assert simple_function_problems.maximum(1, 3, 3) == 3
    assert simple_function_problems.maximum(2, 1, 3) == 3
    assert simple_function_problems.maximum(1, 2, 3) == 3

    assert simple_function_problems.maximum(3, 2) == 3
    assert simple_function_problems.maximum(3, 3) == 3
    assert simple_function_problems.maximum(2, 3) == 3

    assert simple_function_problems.maximum(3) == 3

    assert aubin_lucas_ps7.maximum(3, 2, 1) == 3
    assert aubin_lucas_ps7.maximum(3, 3, 1) == 3
    assert aubin_lucas_ps7.maximum(3, 3, 3) == 3
    assert aubin_lucas_ps7.maximum(2, 3, 1) == 3
    assert aubin_lucas_ps7.maximum(1, 3, 2) == 3
    assert aubin_lucas_ps7.maximum(1, 3, 3) == 3
    assert aubin_lucas_ps7.maximum(2, 1, 3) == 3
    assert aubin_lucas_ps7.maximum(1, 2, 3) == 3

    assert aubin_lucas_ps7.maximum(3, 2) == 3
    assert aubin_lucas_ps7.maximum(3, 3) == 3
    assert aubin_lucas_ps7.maximum(2, 3) == 3

    assert aubin_lucas_ps7.maximum(3) == 3

    assert kehl_julia_ps7.maximum(3, 2, 1) == 3
    assert kehl_julia_ps7.maximum(3, 3, 1) == 3
    assert kehl_julia_ps7.maximum(3, 3, 3) == 3
    assert kehl_julia_ps7.maximum(2, 3, 1) == 3
    assert kehl_julia_ps7.maximum(1, 3, 2) == 3
    assert kehl_julia_ps7.maximum(1, 3, 3) == 3
    assert kehl_julia_ps7.maximum(2, 1, 3) == 3
    assert kehl_julia_ps7.maximum(1, 2, 3) == 3

    assert kehl_julia_ps7.maximum(3, 2) == 3
    assert kehl_julia_ps7.maximum(3, 3) == 3
    assert kehl_julia_ps7.maximum(2, 3) == 3

    assert kehl_julia_ps7.maximum(3) == 3

    assert sepka_jp_ps7.maximum(3, 2, 1) == 3
    # assert sepka_jp_ps7.maximum(3, 3, 1) == 3
    assert sepka_jp_ps7.maximum(3, 3, 3) == 3
    assert sepka_jp_ps7.maximum(2, 3, 1) == 3
    assert sepka_jp_ps7.maximum(1, 3, 2) == 3
    assert sepka_jp_ps7.maximum(1, 3, 3) == 3
    assert sepka_jp_ps7.maximum(2, 1, 3) == 3
    assert sepka_jp_ps7.maximum(1, 2, 3) == 3

    assert sepka_jp_ps7.maximum(3, 2) == 3
    # assert sepka_jp_ps7.maximum(3, 3) == 3
    assert sepka_jp_ps7.maximum(2, 3) == 3

    assert sepka_jp_ps7.maximum(3) == 3

    assert shen_joseph_ps7.maximum(3, 2, 1) == 3
    assert shen_joseph_ps7.maximum(3, 3, 1) == 3
    assert shen_joseph_ps7.maximum(3, 3, 3) == 3
    assert shen_joseph_ps7.maximum(2, 3, 1) == 3
    assert shen_joseph_ps7.maximum(1, 3, 2) == 3
    assert shen_joseph_ps7.maximum(1, 3, 3) == 3
    assert shen_joseph_ps7.maximum(2, 1, 3) == 3
    assert shen_joseph_ps7.maximum(1, 2, 3) == 3

    assert shen_joseph_ps7.maximum(3, 2) == 3
    assert shen_joseph_ps7.maximum(3, 3) == 3
    assert shen_joseph_ps7.maximum(2, 3) == 3

    assert shen_joseph_ps7.maximum(3) == 3


def test_factorial():
    assert simple_function_problems.factorial(0) == 1
    assert simple_function_problems.factorial(1) == 1
    assert simple_function_problems.factorial(2) == 2
    assert simple_function_problems.factorial(3) == 6
    assert simple_function_problems.factorial(4) == 24
    assert simple_function_problems.factorial(5) == 120
    assert simple_function_problems.factorial(6) == 720
    assert simple_function_problems.factorial(7) == 5040
    assert simple_function_problems.factorial(10) == 3628800
    assert simple_function_problems.factorial(13) == 6227020800

    assert aubin_lucas_ps7.factorial(0) == 1
    assert aubin_lucas_ps7.factorial(1) == 1
    assert aubin_lucas_ps7.factorial(2) == 2
    assert aubin_lucas_ps7.factorial(3) == 6
    assert aubin_lucas_ps7.factorial(4) == 24
    assert aubin_lucas_ps7.factorial(5) == 120
    assert aubin_lucas_ps7.factorial(6) == 720
    assert aubin_lucas_ps7.factorial(7) == 5040
    assert aubin_lucas_ps7.factorial(10) == 3628800
    assert aubin_lucas_ps7.factorial(13) == 6227020800

    assert caruso_lorenzo_ps7.factorial(0) == 1
    assert caruso_lorenzo_ps7.factorial(1) == 1
    assert caruso_lorenzo_ps7.factorial(2) == 2
    assert caruso_lorenzo_ps7.factorial(3) == 6
    assert caruso_lorenzo_ps7.factorial(4) == 24
    assert caruso_lorenzo_ps7.factorial(5) == 120
    assert caruso_lorenzo_ps7.factorial(6) == 720
    assert caruso_lorenzo_ps7.factorial(7) == 5040
    assert caruso_lorenzo_ps7.factorial(10) == 3628800
    assert caruso_lorenzo_ps7.factorial(13) == 6227020800

    assert deboard_sean_ps7.factorial(0) == 1
    assert deboard_sean_ps7.factorial(1) == 1
    assert deboard_sean_ps7.factorial(2) == 2
    assert deboard_sean_ps7.factorial(3) == 6
    assert deboard_sean_ps7.factorial(4) == 24
    assert deboard_sean_ps7.factorial(5) == 120
    assert deboard_sean_ps7.factorial(6) == 720
    assert deboard_sean_ps7.factorial(7) == 5040
    assert deboard_sean_ps7.factorial(10) == 3628800
    assert deboard_sean_ps7.factorial(13) == 6227020800

    # assert kehl_dominic_ps7.factorial(0) == 1
    assert kehl_dominic_ps7.factorial(1) == 1
    assert kehl_dominic_ps7.factorial(2) == 2
    assert kehl_dominic_ps7.factorial(3) == 6
    assert kehl_dominic_ps7.factorial(4) == 24
    assert kehl_dominic_ps7.factorial(5) == 120
    assert kehl_dominic_ps7.factorial(6) == 720
    assert kehl_dominic_ps7.factorial(7) == 5040
    assert kehl_dominic_ps7.factorial(10) == 3628800
    assert kehl_dominic_ps7.factorial(13) == 6227020800

    assert kehl_julia_ps7.factorial(0) == 1
    assert kehl_julia_ps7.factorial(1) == 1
    assert kehl_julia_ps7.factorial(2) == 2
    assert kehl_julia_ps7.factorial(3) == 6
    assert kehl_julia_ps7.factorial(4) == 24
    assert kehl_julia_ps7.factorial(5) == 120
    assert kehl_julia_ps7.factorial(6) == 720
    assert kehl_julia_ps7.factorial(7) == 5040
    assert kehl_julia_ps7.factorial(10) == 3628800
    assert kehl_julia_ps7.factorial(13) == 6227020800

    assert sepka_jp_ps7.factorial(0) == 1
    assert sepka_jp_ps7.factorial(1) == 1
    assert sepka_jp_ps7.factorial(2) == 2
    assert sepka_jp_ps7.factorial(3) == 6
    assert sepka_jp_ps7.factorial(4) == 24
    assert sepka_jp_ps7.factorial(5) == 120
    assert sepka_jp_ps7.factorial(6) == 720
    assert sepka_jp_ps7.factorial(7) == 5040
    assert sepka_jp_ps7.factorial(10) == 3628800
    assert sepka_jp_ps7.factorial(13) == 6227020800

    assert simple_function_problems.factorial(0) == 1
    assert simple_function_problems.factorial(1) == 1
    assert simple_function_problems.factorial(2) == 2
    assert simple_function_problems.factorial(3) == 6
    assert simple_function_problems.factorial(4) == 24
    assert simple_function_problems.factorial(5) == 120
    assert simple_function_problems.factorial(6) == 720
    assert simple_function_problems.factorial(7) == 5040
    assert simple_function_problems.factorial(10) == 3628800
    assert simple_function_problems.factorial(13) == 6227020800


def test_fibonacci():
    assert simple_function_problems.fibonacci(1) == 0
    assert simple_function_problems.fibonacci(2) == 1
    assert simple_function_problems.fibonacci(3) == 1
    assert simple_function_problems.fibonacci(4) == 2
    assert simple_function_problems.fibonacci(5) == 3
    assert simple_function_problems.fibonacci(6) == 5
    assert simple_function_problems.fibonacci(7) == 8
    assert simple_function_problems.fibonacci(10) == 34
    assert simple_function_problems.fibonacci(13) == 144

    assert aubin_lucas_ps7.fibonacci(1) == 0
    assert aubin_lucas_ps7.fibonacci(2) == 1
    assert aubin_lucas_ps7.fibonacci(3) == 1
    assert aubin_lucas_ps7.fibonacci(4) == 2
    assert aubin_lucas_ps7.fibonacci(5) == 3
    assert aubin_lucas_ps7.fibonacci(6) == 5
    assert aubin_lucas_ps7.fibonacci(7) == 8
    assert aubin_lucas_ps7.fibonacci(10) == 34
    assert aubin_lucas_ps7.fibonacci(13) == 144

    assert kehl_julia_ps7.fibonacci(1) == 0
    assert kehl_julia_ps7.fibonacci(2) == 1
    assert kehl_julia_ps7.fibonacci(3) == 1
    assert kehl_julia_ps7.fibonacci(4) == 2
    assert kehl_julia_ps7.fibonacci(5) == 3
    assert kehl_julia_ps7.fibonacci(6) == 5
    assert kehl_julia_ps7.fibonacci(7) == 8
    assert kehl_julia_ps7.fibonacci(10) == 34
    assert kehl_julia_ps7.fibonacci(13) == 144

    assert sepka_jp_ps7.fibonacci(1) == 0
    assert sepka_jp_ps7.fibonacci(2) == 1
    assert sepka_jp_ps7.fibonacci(3) == 1
    assert sepka_jp_ps7.fibonacci(4) == 2
    assert sepka_jp_ps7.fibonacci(5) == 3
    assert sepka_jp_ps7.fibonacci(6) == 5
    assert sepka_jp_ps7.fibonacci(7) == 8
    assert sepka_jp_ps7.fibonacci(10) == 34
    assert sepka_jp_ps7.fibonacci(13) == 144

    assert shen_joseph_ps7.fibonacci(1) == 0
    assert shen_joseph_ps7.fibonacci(2) == 1
    assert shen_joseph_ps7.fibonacci(3) == 1
    assert shen_joseph_ps7.fibonacci(4) == 2
    assert shen_joseph_ps7.fibonacci(5) == 3
    assert shen_joseph_ps7.fibonacci(6) == 5
    assert shen_joseph_ps7.fibonacci(7) == 8
    assert shen_joseph_ps7.fibonacci(10) == 34
    assert shen_joseph_ps7.fibonacci(13) == 144
