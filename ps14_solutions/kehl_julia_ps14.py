# The 6.00 Word Game

import random
import string

VOWELS = 'aeiou'
CONSONANTS = 'bcdfghjklmnpqrstvwxyz'
HAND_SIZE = 7

SCRABBLE_LETTER_VALUES = {
    'a': 1, 'b': 3, 'c': 3, 'd': 2, 'e': 1, 'f': 4, 'g': 2, 'h': 4, 'i': 1, 'j': 8, 'k': 5, 'l': 1, 'm': 3, 'n': 1,
    'o': 1, 'p': 3, 'q': 10, 'r': 1, 's': 1, 't': 1, 'u': 1, 'v': 4, 'w': 4, 'x': 8, 'y': 4, 'z': 10
}

# -----------------------------------
# Helper code
# (you don't need to understand this helper code)

WORDLIST_FILENAME = "words.txt"


def load_words():
    """
    Returns a list of valid words. Words are strings of lowercase letters.

    Depending on the size of the word list, this function may
    take a while to finish.
    """
    print("Loading word list from file...")
    # in_file: file
    in_file = open(WORDLIST_FILENAME, 'r')
    # word_list: list of strings
    word_list = []
    for line in in_file:
        word_list.append(line.strip().lower())
    print("  ", len(word_list), "words loaded.")
    return word_list


def get_frequency_dict(sequence):
    """
    Returns a dictionary where the keys are elements of the sequence
    and the values are integer counts, for the number of times that
    an element is repeated in the sequence.

    sequence: string or list
    return: dictionary
    """
    # freqs: dictionary (element_type -> int)
    freq = {}
    for x in sequence:
        freq[x] = freq.get(x, 0) + 1
    return freq


# (end of helper code)
# -----------------------------------

#
# Problem #1: Scoring a word
#
def get_word_score(word, n):
    score = 0
    for letter in word:
        score += SCRABBLE_LETTER_VALUES[letter] * len(word)
    if len(word) == n:
        score += 50
    return score
#
# Problem #2: Make sure you understand how this function works and what it does!
#
def display_hand(hand):
    """
    Displays the letters currently in the hand.

    For example:
    >>> display_hand({'a':1, 'x':2, 'l':3, 'e':1})
    Should print out something like:
       a x x l l l e
    The order of the letters is unimportant.

    hand: dictionary (string -> int)
    """
    for letter in hand.keys():
        for j in range(hand[letter]):
            print(letter, end=" ")  # print all on the same line
    print()  # print an empty line


#
# Problem #2: Make sure you understand how this function works and what it does!
#
def deal_hand(n):
    """
    Returns a random hand containing n lowercase letters.
    At least n/3 the letters in the hand should be VOWELS.

    Hands are represented as dictionaries. The keys are
    letters and the values are the number of times the
    particular letter is repeated in that hand.

    n: int >= 0
    returns: dictionary (string -> int)
    """
    hand = {}
    num_vowels = n // 3

    for i in range(num_vowels):
        x = VOWELS[random.randrange(0, len(VOWELS))]
        hand[x] = hand.get(x, 0) + 1

    for i in range(num_vowels, n):
        x = CONSONANTS[random.randrange(0, len(CONSONANTS))]
        hand[x] = hand.get(x, 0) + 1

    return hand


#
# Problem #2: Update a hand by removing letters
#
def update_hand(hand, word):

    new_hand = hand.copy()
    for letter in word:
        if letter in hand:
            new_hand[letter] -= 1 #new_hand.pop(letter)
    return new_hand

#
# Problem #3: Test word validity
#
def is_valid_word(word, hand, word_list):
    if word not in word_list:
        return False
    hand_new = dict(hand)
    for letter in word:
        if letter in hand_new and hand_new[letter] > 0:
            hand_new[letter] -= 1
        else:
            return False
    return True


#
# Problem #4: Playing a hand
#

def calculate_hand_len(hand):
    count = 0
    for letter in hand:
            count += hand[letter]
    return count

def play_hand(hand, word_list, n):
    total_score = 0  # Keep track of the total score
    display_hand(hand)    # Display the hand
    word = input('Enter word or "." to indicate you are finished: ')
    while len(hand) != 0 and word != '.':  # As long as there are still letters left in the hand:,  # If the input is a single period:
        if is_valid_word(word, hand, word_list) != True:  # Otherwise (the input is not a single period):,  # If the word is not valid:
            word = input('That word is invalid, please try again: ')  # Reject invalid word (print a message followed by a blank line)
            print()
        elif is_valid_word(word, hand, word_list) == True:  # Otherwise (the word is valid):
            score = get_word_score(word, n)
            total_score += score
            print(word + " earned " + str(score) + " points, Your total score is: " + str(total_score))  # Tell the user how many points the word earned, and the updated total score, in one line followed by a blank line
            print()
            hand = update_hand(hand, word) # Update the hand
            display_hand(hand)
            if len(hand) != 0:
                word = input('Enter word or "." to indicate you are finished: ')
    print('Game over!')  # End the game (break out of the loop)
    print("Your total score is: " + str(total_score)) # Game is over (user entered a '.' or ran out of letters), so tell user the total score


def play_game(word_list):
    n = HAND_SIZE
    hand = deal_hand(n)
    while True:
        response = input('Enter n to deal a new hand, r to replay the last hand, or e to end game: ')
        if response == "n":
            hand = deal_hand(n)
            play_hand(hand, word_list, n)
        elif response == "r":
            play_hand(hand, word_list, n)
        elif response == "e":
            break
        else:
            print('Invalid Input')



#
# Build data structures used for entire session and play game
#
if __name__ == '__main__':
    play_game(load_words())
from kehl_julia_ps14 import *




# Test code
# You don't need to understand how this test code works (but feel free to look it over!)

# To run these tests, simply run this file (open up in your IDE, then run the file as normal)

def test_get_word_score():
    """
    Unit test for get_word_score
    """
    failure = False
    # dictionary of words and scores
    words = {("", 7): 0, ("it", 7): 4, ("was", 7): 18, ("scored", 7): 54, ("waybill", 7): 155, ("outgnaw", 7): 127,
             ("fork", 7): 44, ("fork", 4): 94}
    for (word, n) in words.keys():
        score = get_word_score(word, n)
        if score != words[(word, n)]:
            print("FAILURE: test_get_word_score('" + word + "', " + str(n) + ")")
            print("\tExpected", words[(word, n)],
                  "points but got", score)
            failure = True
            break
    if not failure:
        print("SUCCESS: test_get_word_score()")


def test_update_hand():
    """
    Unit test for update_hand
    """
    # test 1
    hand_orig = {'a': 1, 'q': 1, 'l': 2, 'm': 1, 'u': 1, 'i': 1}
    hand_copy = hand_orig.copy()
    word = "quail"

    hand2 = update_hand(hand_copy, word)
    expected_hand1 = {'l': 1, 'm': 1}
    expected_hand2 = {'a': 0, 'q': 0, 'l': 1, 'm': 1, 'u': 0, 'i': 0}
    if hand2 != expected_hand1 and hand2 != expected_hand2:
        print("FAILURE: test_update_hand('" + word + "', " + str(hand_orig) + ")")
        print("\tReturned", hand2, "\n\t-- but expected", expected_hand1, "or", expected_hand2)

        return  # exit function
    if hand_copy != hand_orig:
        print("FAILURE: test_update_hand('" + word + "', " + str(hand_orig) + ")")
        print("\tOriginal hand was", hand_orig)
        print("\tbut implementation of update_hand mutated the original hand!")
        print("\tNow the hand looks like this", hand_copy)

        return  # exit function

    # test 2
    hand_orig = {'e': 1, 'v': 2, 'n': 1, 'i': 1, 'l': 2}
    hand_copy = hand_orig.copy()
    word = "evil"

    hand2 = update_hand(hand_copy, word)
    expected_hand1 = {'v': 1, 'n': 1, 'l': 1}
    expected_hand2 = {'e': 0, 'v': 1, 'n': 1, 'i': 0, 'l': 1}
    if hand2 != expected_hand1 and hand2 != expected_hand2:
        print("FAILURE: test_update_hand('" + word + "', " + str(hand_orig) + ")")
        print("\tReturned", hand2, "\n\t-- but expected", expected_hand1, "or", expected_hand2)

        return  # exit function

    if hand_copy != hand_orig:
        print("FAILURE: test_update_hand('" + word + "', " + str(hand_orig) + ")")
        print("\tOriginal hand was", hand_orig)
        print("\tbut implementation of update_hand mutated the original hand!")
        print("\tNow the hand looks like this", hand_copy)

        return  # exit function

    # test 3
    hand_orig = {'h': 1, 'e': 1, 'l': 2, 'o': 1}
    hand_copy = hand_orig.copy()
    word = "hello"

    hand2 = update_hand(hand_copy, word)
    expected_hand1 = {}
    expected_hand2 = {'h': 0, 'e': 0, 'l': 0, 'o': 0}
    if hand2 != expected_hand1 and hand2 != expected_hand2:
        print("FAILURE: test_update_hand('" + word + "', " + str(hand_orig) + ")")
        print("\tReturned", hand2, "\n\t-- but expected", expected_hand1, "or", expected_hand2)

        return  # exit function

    if hand_copy != hand_orig:
        print("FAILURE: test_update_hand('" + word + "', " + str(hand_orig) + ")")
        print("\tOriginal hand was", hand_orig)
        print("\tbut implementation of update_hand mutated the original hand!")
        print("\tNow the hand looks like this", hand_copy)

        return  # exit function

    print("SUCCESS: test_update_hand()")


def test_is_valid_word(word_list):
    """
    Unit test for is_valid_word
    """
    # test 1
    word = "hello"
    hand_orig = get_frequency_dict(word)
    hand_copy = hand_orig.copy()

    if not is_valid_word(word, hand_copy, word_list):
        print("FAILURE: test_is_valid_word()")
        print("\tExpected True, but got False for word '" + word + "' and hand", hand_orig)

        return

    # Test a second time to see if word_list or hand has been modified
    if not is_valid_word(word, hand_copy, word_list):
        print("FAILURE: test_is_valid_word()")

        if hand_copy != hand_orig:
            print("\tTesting word", word, "for a second time - be sure you're not modifying hand.")
            print("\tAt this point, hand ought to be", hand_orig, "but it is", hand_copy)

        else:
            print("\tTesting word", word, "for a second time - have you modified word_list?")
            word_in_wl = word in word_list
            print("The word", word, "should be in word_list - is it?", word_in_wl)

        print("\tExpected True, but got False for word '" + word + "' and hand", hand_copy)

        return

    # test 2
    hand = {'r': 1, 'a': 3, 'p': 2, 'e': 1, 't': 1, 'u': 1}
    word = "rapture"

    if is_valid_word(word, hand, word_list):
        print("FAILURE: test_is_valid_word()")
        print("\tExpected False, but got True for word '" + word + "' and hand", hand)

        return

    # test 3
    hand = {'n': 1, 'h': 1, 'o': 1, 'y': 1, 'd': 1, 'w': 1, 'e': 2}
    word = "honey"

    if not is_valid_word(word, hand, word_list):
        print("FAILURE: test_is_valid_word()")
        print("\tExpected True, but got False for word '" + word + "' and hand", hand)

        return

    # test 4
    hand = {'r': 1, 'a': 3, 'p': 2, 't': 1, 'u': 2}
    word = "honey"

    if is_valid_word(word, hand, word_list):
        print("FAILURE: test_is_valid_word()")
        print("\tExpected False, but got True for word '" + word + "' and hand", hand)

        return

    # test 5
    hand = {'e': 1, 'v': 2, 'n': 1, 'i': 1, 'l': 2}
    word = "evil"

    if not is_valid_word(word, hand, word_list):
        print("FAILURE: test_is_valid_word()")
        print("\tExpected True, but got False for word '" + word + "' and hand", hand)

        return

    # test 6
    word = "even"

    if is_valid_word(word, hand, word_list):
        print("FAILURE: test_is_valid_word()")
        print("\tExpected False, but got True for word '" + word + "' and hand", hand)
        print("\t(If this is the only failure, make sure is_valid_word() isn't mutating its inputs)")

        return

    print("SUCCESS: test_is_valid_word()")


def test_calculate_hand_len():
    """
    Unit test for calculate_hand_len
    """
    failure = False
    # list of test words
    words = ["", "scored", "aaaaaa", "abababab", "mississippi"]
    for word in words:
        hand = get_frequency_dict(word)
        expected_len = len(word)
        actual_len = calculate_hand_len(hand)
        if actual_len != expected_len:
            print("FAILURE: test_calculate_hand_len()")
            print("\tExpected", expected_len,
                  "but got", actual_len, "for hand", hand)
            failure = True
            break
    if not failure:
        print("SUCCESS: test_calculate_hand_len()")


print("----------------------------------------------------------------------")
print("Testing get_word_score...")
test_get_word_score()
print("----------------------------------------------------------------------")
print("Testing update_hand...")
test_update_hand()
print("----------------------------------------------------------------------")
print("Testing is_valid_word...")
test_is_valid_word(load_words())
print("----------------------------------------------------------------------")
print("Testing calculate_hand_len...")
test_calculate_hand_len()
print("----------------------------------------------------------------------")
print("All done!")
