# The 6.00 Word Game

import random
import string

VOWELS = 'aeiou'
CONSONANTS = 'bcdfghjklmnpqrstvwxyz'
HAND_SIZE = 7

SCRABBLE_LETTER_VALUES = {
    'a': 1, 'b': 3, 'c': 3, 'd': 2, 'e': 1, 'f': 4, 'g': 2, 'h': 4, 'i': 1, 'j': 8, 'k': 5, 'l': 1, 'm': 3, 'n': 1,
    'o': 1, 'p': 3, 'q': 10, 'r': 1, 's': 1, 't': 1, 'u': 1, 'v': 4, 'w': 4, 'x': 8, 'y': 4, 'z': 10
}

# -----------------------------------
# Helper code
# (you don't need to understand this helper code)

WORDLIST_FILENAME = "words.txt"


def load_words():
    """
    Returns a list of valid words. Words are strings of lowercase letters.

    Depending on the size of the word list, this function may
    take a while to finish.
    """
    print("Loading word list from file...")
    # in_file: file
    in_file = open(WORDLIST_FILENAME, 'r')
    # word_list: list of strings
    word_list = []
    for line in in_file:
        word_list.append(line.strip().lower())
    print("  ", len(word_list), "words loaded.")
    return word_list


def get_frequency_dict(sequence):
    """
    Returns a dictionary where the keys are elements of the sequence
    and the values are integer counts, for the number of times that
    an element is repeated in the sequence.

    sequence: string or list
    return: dictionary
    """
    # freqs: dictionary (element_type -> int)
    freq = {}
    for x in sequence:
        freq[x] = freq.get(x, 0) + 1
    return freq


# (end of helper code)
# -----------------------------------

#
# Problem #1: Scoring a word
#
def get_word_score(word, n):


    l = 0
    for letter in word:
        l += SCRABBLE_LETTER_VALUES[letter] * len(word)
    if n - len(word) == 0:
        l += 50

    return l
# Problem #2: Make sure you understand how this function works and what it does!
#
def display_hand(hand):
    """
    Displays the letters currently in the hand.

    For example:
    >>> display_hand({'a':1, 'x':2, 'l':3, 'e':1})
    Should print out something like:
       a x x l l l e
    The order of the letters is unimportant.

    hand: dictionary (string -> int)
    """
    for letter in hand.keys():
        for j in range(hand[letter]):
            print(letter, end=" ")  # print all on the same line
    print()  # print an empty line


#
# Problem #2: Make sure you understand how this function works and what it does!
#
def deal_hand(n):
    """
    Returns a random hand containing n lowercase letters.
    At least n/3 the letters in the hand should be VOWELS.

    Hands are represented as dictionaries. The keys are
    letters and the values are the number of times the
    particular letter is repeated in that hand.

    n: int >= 0
    returns: dictionary (string -> int)
    """
    hand = {}
    num_vowels = n // 3

    for i in range(num_vowels):
        x = VOWELS[random.randrange(0, len(VOWELS))]
        hand[x] = hand.get(x, 0) + 1

    for i in range(num_vowels, n):
        x = CONSONANTS[random.randrange(0, len(CONSONANTS))]
        hand[x] = hand.get(x, 0) + 1

    return hand


#
# Problem #2: Update a hand by removing letters
#
def update_hand(hand, word):

    hand1 = hand.copy()
    for i in word:
        hand1[i] -= 1
    return hand1


#
# Problem #3: Test word validity
#
def is_valid_word(word, hand, word_list):


    hand1 = hand.copy()
    for letter in word:
        if hand1.get(letter, 0) > 0:
            hand1[letter] -= 1
        else:
            return False
    return True


#
# Problem #4: Playing a hand
#

def calculate_hand_len(hand):

    return sum([hand[i] for i in hand if hand[i] >= 1])

def play_hand(hand, word_list, n):

    score = 0
    while calculate_hand_len(hand) > 0:
        print("Your hand is: ")
        print(display_hand(hand))
        word = input('Enter word, or a "." to finish: ')
        if word == '.':
            break
        if len(hand) == 0:
            print(f"You are out of letters, total points:{score}")
        if word_list == "Pizza":
            print(f"Goodbye, total points:{score}")
        else:
            if word in word_list:
                score_word = get_word_score(word, n)
                score += score_word
                print('"%s" earned %s points. Total: %s points.\n' % (word, score_word, score))
                hand = update_hand(hand, word)
            else:
                print('Invalid word.\n')
def play_game(word_list):
    n = HAND_SIZE
    hand = {}
    while True:
        user_input = input('Enter n to deal a new hand "r" to play last hand or "." to end game: ')
        if user_input == 'r':
            if hand:
                print('You have not played a hand yet.')
            else:
                play_hand(hand, word_list, n)
        elif user_input == 'n':
            play_hand(deal_hand(n), word_list, n)
        elif user_input == '.':
            print
            break
        else:
            print('Invalid.')
if __name__ == '__main__':
    play_game(load_words())
