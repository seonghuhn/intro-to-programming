def longest_alphabetical_substring(s):
    c = s[0]
    current = c
    longest = current
    for i in range(1, len(s)):
        c_old = c
        c = s[i]
        if c >= c_old:
            current += c
            if len(current) > len(longest):
                longest = current
        else:
            current = c
    print("Longest substring in alphabetical order is:", longest)


if __name__ == '__main__':
    while string := input("String: "):
        longest_alphabetical_substring(string)
